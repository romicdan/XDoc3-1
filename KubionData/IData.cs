using System;
using System.Data;

namespace KubionDataNamespace
{
    public interface IData
    {
        int CommandTimeout{get;set;}
        void Dispose();
        bool ConnOpen();
        IDataReader GetDataReader(string sqlString);
        DataTable GetDataTable(string sqlString);
        DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml);
        DataTable GetDataTable(string sqlString, IDataParameter[] arrParams);
        DataTable GetSchema(string sqlString);
        string  ExecuteNonQuery(string sqlString);
        string  ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml);
        string  ExecuteNonQuery(string sqlString, IDataParameter[] arrParams);
        void BeginTransaction();
        void BeginTransaction(IsolationLevel isolationLevel);
        void CommitTransaction();
        void RollbackTransaction();
        string GetResponse(string sqlString);
    }
}