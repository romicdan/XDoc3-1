using System;
using System.Data;
using System.Data.Odbc ;
using System.Configuration;
using System.Diagnostics;
using KubionLogNamespace;

namespace KubionDataNamespace
{
    public class OdbcData : IData, IDisposable
    {
        OdbcConnection m_conn = null;
        OdbcTransaction m_trans = null;
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }

        public OdbcData(string connectionString)
        {

            try
            {
                m_conn = new OdbcConnection(connectionString);
                OpenConnection();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
        }

        public void Dispose()
        {
            if (m_conn != null && m_conn.State != ConnectionState.Closed)
            {
                try
                {
                    m_conn.Close();
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    throw;
                }
            }
        }

        private void OpenConnection()
        {
            if (m_conn.State != ConnectionState.Open)
            {
                try
                {
                    m_conn.Open();
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    throw;
                }
            }
        }

        public bool ConnOpen()
        {
            OpenConnection();
            return (m_conn.State == ConnectionState.Open);
        }

        public DataTable GetSchema(string sqlString)
        {
            if (!ConnOpen()) return null;
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
            try
            {
                da.FillSchema(ds, SchemaType.Source);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }

        public IDataReader GetDataReader(string sqlString)
        {
            OpenConnection();
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans == null) BeginTransaction(IsolationLevel.ReadUncommitted);
            command.Transaction = m_trans;
            return command.ExecuteReader(CommandBehavior.SequentialAccess);
        }

        public string GetResponse(string sqlString)
        {
            OpenConnection();
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {
            }
            string ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            return ret;
        }

        public DataTable GetDataTable(string sqlString)
        {
            if (!ConnOpen()) return null;
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return GetDataTable(sqlString);
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            DataSet ds = new DataSet();
            OdbcCommand comm = new OdbcCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            DataTable dt = null;
            try
            {
                foreach (OdbcParameter  paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                OdbcDataAdapter da = new OdbcDataAdapter(comm);
                //DateTime dt1 = DateTime.Now;
                da.Fill(ds);
                //DateTime dt2 = DateTime.Now;
                //TimeSpan ts = dt2.Subtract(dt1);
                //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
                //System.Diagnostics.Debug.WriteLine(dt1.ToString("HH:mm:ss.fff") + " executed query:  " + duration + " query:" + sqlString); 
                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            return dt;
        }

        public string ExecuteNonQuery(string sqlString)
        {
            OpenConnection();
            OdbcCommand comm = new OdbcCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null) comm.Transaction = m_trans;

            int rowsAffected = -1;
            try
            {
                rowsAffected = comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
            return rowsAffected.ToString ();
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return ExecuteNonQuery(sqlString);
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            OdbcCommand comm = new OdbcCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            int rowsAffected = -1;

            try
            {
                foreach (OdbcParameter paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                rowsAffected = comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            return rowsAffected.ToString ();
        }

        public void BeginTransaction()
        {
            OpenConnection();
            try
            {
                m_trans = m_conn.BeginTransaction();
            }
            catch (Exception)
            {
            }
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            OpenConnection();
            try
            {
                if (m_trans != null) RollbackTransaction();
                m_trans = m_conn.BeginTransaction(isolationLevel);
            }
            catch (Exception)
            {
            }
        }
        public void CommitTransaction()
        {
            try
            {

                m_trans.Commit();
                m_trans = null;
            }
            catch (Exception)
            {
            }

        }
        public void RollbackTransaction()
        {
            try
            {
                m_trans.Rollback();
                m_trans = null;
            }
            catch (Exception)
            {
            }

        }

    }
}