using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Net;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace KubionDataNamespace
{
    public class WebserviceConsumer
    {
        CookieContainer cookieContainer = null;
        public WebserviceConsumer() { }
        public WebserviceConsumer(bool bCookieContainer)
        {
            if (bCookieContainer) cookieContainer = new CookieContainer();
        }

		private string GetResponse(string strUrl, string strXml, string strContentType, string strSOAPAction)
		{
			return this.GetResponse(strUrl, "", "", "","","","","", strXml, strContentType, strSOAPAction);
		}

        private string GetResponse(string strUrl, string strUserName, string strPassword, string strDomain, string strProxyUrl, string strProxyUserName, string strProxyPassword, string strProxyDomain, string strXml, string strContentType, string strSOAPAction)
        {
            /* Send the XML content */
            string sEncoding = null;
            Encoding objEncoding = null;

            if (strContentType.IndexOf("charset=") != -1)
            {
                int intStart = strContentType.IndexOf("charset=") + 8;
                int intEnd = (strContentType + ";").IndexOf(";", intStart);
                if (intEnd != -1) sEncoding = strContentType.Substring(intStart, intEnd - intStart);
                try { objEncoding = Encoding.GetEncoding(sEncoding); }
                catch (Exception e) { sEncoding = null; }
            }

            if (objEncoding == null)
            {
                sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
                if (sEncoding == null || sEncoding == "") sEncoding = "utf-8";
                try { objEncoding = Encoding.GetEncoding(sEncoding); }
                catch (Exception e) { sEncoding = null; }
            }
            byte[] postData = (objEncoding == null ? Encoding.Default.GetBytes(strXml) : objEncoding.GetBytes(strXml));

            ///* Post the XML */
            //byte[] postData = Encoding.Default.GetBytes(strXml);

            //if (strContentType.IndexOf("charset=") != -1)
            //{
            //    int intStart = strContentType.IndexOf("charset=") + 8;
            //    int intEnd = (strContentType + ";").IndexOf(";", intStart);
            //    if (intEnd != -1)
            //    {
            //        string strCharSet = strContentType.Substring(intStart, intEnd - intStart);
            //        try
            //        {
            //            postData = Encoding.Convert(Encoding.Default, Encoding.GetEncoding(strCharSet), postData);
            //        }
            //        catch (Exception e) { }
            //    }
            //}

            //Encoding.Convert(Encoding.Default, Encoding.GetEncoding(System.Text.Encoding.UTF32.w
            System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strUrl);
            if (cookieContainer != null) request.CookieContainer = cookieContainer; 
            request.Method = "POST";
            request.ContentType = strContentType;

            bool bstrSOAPAction = true;
            strSOAPAction = System.Web.HttpUtility.UrlDecode(strSOAPAction);
            foreach (string strRequestHeader in strSOAPAction.Split('|'))
            {
                if (bstrSOAPAction)
                {
                    strSOAPAction = strRequestHeader;
                    bstrSOAPAction = false;
                }
                if (strRequestHeader.Split('~').Length == 2)
                {
                    if ((strRequestHeader.Split('~')[0].ToLower()) == "accept")
                    {
                        request.Accept = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "connection")
                    {
                        request.Connection = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-length")
                    {
                        request.ContentLength = long.Parse(strRequestHeader.Split('~')[1]);
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-type")
                    {
                        request.ContentType = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "date") { }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "expect")
                    {
                        request.Expect = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "host") { }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "if-modified-since")
                    {
                        request.IfModifiedSince = DateTime.Parse(strRequestHeader.Split('~')[1]);
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "range") { }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "referer")
                    {
                        request.Referer = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "transfer-encoding")
                    {
                        request.TransferEncoding = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "user-agent")
                    {
                        request.UserAgent = strRequestHeader.Split('~')[1];
                    }
                    else if ((strRequestHeader.Split('~')[0].ToLower()) == "proxy-connection") { }
                    else
                    {
                        request.Headers.Add(strRequestHeader.Split('~')[0], strRequestHeader.Split('~')[1]);
                    }
                }
            }

            if (strSOAPAction != "") request.Headers.Add("SOAPAction", strSOAPAction);
            request.ContentLength = postData.Length;

            if ((strUserName == "" || strUserName == null) && (strPassword == "" || strPassword == null) && (strDomain == "" || strDomain == null))
            {
                /* Set the credentials to be like impersonate */
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }
            else
            {
                request.Credentials = new System.Net.NetworkCredential(strUserName, strPassword, strDomain);
            }

            if (strProxyUrl != "" && strProxyUrl != null)
            {

                WebProxy proxy = new WebProxy();
                proxy.Address = new Uri(strProxyUrl);
                if (strProxyUserName != "" && strProxyUserName != null)
                {
                    proxy.Credentials = new NetworkCredential(strProxyUserName, (strProxyPassword == null ? "" : strProxyPassword), (strProxyDomain == null ? "" : strProxyDomain));
                }
                request.Proxy = proxy;
            }

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(postData, 0, postData.Length);
            requestStream.Close();

            /* Read the response XML */
            WebResponse response = null;
            try { response = request.GetResponse(); }
            catch (WebException webex)
            {
                /* Need some special attention to get the error message from the response */
                String data = String.Empty;
                if (webex.Response != null)
                {
                    StreamReader r = new StreamReader(webex.Response.GetResponseStream());
                    data = r.ReadToEnd();
                    r.Close();
                }
                throw new Exception(webex.Message + "\r\n" + data + "\r\nURL (decoded): " + strUrl + "\r\nPosted data (decoded): " + System.Web.HttpUtility.UrlDecode(strXml, Encoding.Default));
            }
            Stream responseStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(responseStream);
            return sr.ReadToEnd();
        }

        private string GetParameterValue(string strParameters, string strKey)
        {
            strParameters = strParameters.Replace('?', '&');
            int intStart = strParameters.IndexOf("&" + strKey);
            if (intStart == -1) return null;
            intStart = intStart + strKey.Length + 1 + 1; /* plus 1 for the ampersand and plus 1 for index fix */

            int intEnd = strParameters.IndexOf('&', intStart);
            if (intEnd == -1) intEnd = strParameters.Length;

            return strParameters.Substring(intStart, intEnd - intStart);
        }

        /* Get the first table from the DataTableCollection */
        //public DataTable GetDataTable(string strCommandUrl, ref string refResponse, ref string refOuterXml, ref string refInnerXml)
        //{
        //    DataTableCollection objDataTableCollection = this.GetDataTableCollection(strCommandUrl, ref refResponse, ref refOuterXml, ref refInnerXml);
        //    if (objDataTableCollection == null) return null;
        //    else return objDataTableCollection[0];
        //}

        private string InvokeWS(string strCommandUrl, ref string refResponse, ref string refOuterXml, ref string refInnerXml)
        {
            return "";
        }
        /**
         * CommandURL example: ?xml=...&url=ontwikkel3&xpath=data
         **/
        //public DataTableCollection GetDataTableCollection(string strCommandUrl, ref string refResponse, ref string refOuterXml, ref string refInnerXml)
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public DataTable GetDataTable(string strCommandUrl, ref string refResponse, ref string refOuterXml, ref string refInnerXml)
        {
            try
            {
                string strUrl = this.GetParameterValue(strCommandUrl, "url");
                string strUserName = this.GetParameterValue(strCommandUrl, "username");
                string strPassword = this.GetParameterValue(strCommandUrl, "password");
                string strDomain = this.GetParameterValue(strCommandUrl, "domain");
                string strProxyUrl = this.GetParameterValue(strCommandUrl, "proxyurl");
                string strProxyUserName = this.GetParameterValue(strCommandUrl, "proxyusername");
                string strProxyPassword = this.GetParameterValue(strCommandUrl, "proxypassword");
                string strProxyDomain = this.GetParameterValue(strCommandUrl, "proxydomain");

                string strCmd = this.GetParameterValue(strCommandUrl, "cmd");
                string strXml = this.GetParameterValue(strCommandUrl, "xml");
                string strXPath = this.GetParameterValue(strCommandUrl, "xpath");
                string strNs = this.GetParameterValue(strCommandUrl, "ns");
                string strContentType = this.GetParameterValue(strCommandUrl, "contenttype");
                string strSOAPAction = this.GetParameterValue(strCommandUrl, "soapaction");
                string strRootNode = this.GetParameterValue(strCommandUrl, "rootnode");
                if (strUrl == null) throw new Exception("Missing parameter: url (" + strCommandUrl + ")");
                if (strXml == null) throw new Exception("Missing parameter: xml (" + strCommandUrl + ")");
                if (strXPath == null) throw new Exception("Missing parameter: xpath (" + strCommandUrl + ")");

                /* URL decode given parameters (except XML that need to be send encoded to the webservice) */
                strUrl = System.Web.HttpUtility.UrlDecode(strUrl, Encoding.Default);
                strUserName = System.Web.HttpUtility.UrlDecode(strUserName, Encoding.Default);
                strPassword = System.Web.HttpUtility.UrlDecode(strPassword, Encoding.Default);
                strDomain = System.Web.HttpUtility.UrlDecode(strDomain, Encoding.Default);
                strProxyUrl = System.Web.HttpUtility.UrlDecode(strProxyUrl, Encoding.Default);
                strProxyUserName = System.Web.HttpUtility.UrlDecode(strProxyUserName, Encoding.Default);
                strProxyPassword = System.Web.HttpUtility.UrlDecode(strProxyPassword, Encoding.Default);
                strProxyDomain = System.Web.HttpUtility.UrlDecode(strProxyDomain, Encoding.Default);

                strCmd = System.Web.HttpUtility.UrlDecode(strCmd, Encoding.Default);
                strXml = System.Web.HttpUtility.UrlDecode(strXml, Encoding.Default);
                strXPath = System.Web.HttpUtility.UrlDecode(strXPath, Encoding.Default);
                strNs = (strNs == null || strNs == "") ? "ns" : System.Web.HttpUtility.UrlDecode(strNs, Encoding.Default);
                strContentType = (strContentType == null || strContentType == "") ? "application/x-www-form-urlencoded" : System.Web.HttpUtility.UrlDecode(strContentType, Encoding.Default);
                strRootNode = (strRootNode == null) ? "" : System.Web.HttpUtility.UrlDecode(strRootNode, Encoding.Default);

                /* Build the XML to post and get the response */
                refResponse = this.GetResponse(strUrl + strCmd, strUserName, strPassword, strDomain, strProxyUrl, strProxyUserName, strProxyPassword, strProxyDomain,  strXml, strContentType, strSOAPAction);
                if (refResponse == "") return null;

                //if (strRootNode != "")
                //{
                //    string strStart = "<" + strRootNode + ">";
                //    string strEnd = "</" + strRootNode + ">";
                //    int intRootNodeStart = refResponse.IndexOf(strStart);
                //    if (intRootNodeStart == -1)
                //    {
                //        strStart = "<" + strRootNode + " ";
                //        intRootNodeStart = refResponse.IndexOf(strStart);
                //    }
                //    int intRootNodeEnd = refResponse.IndexOf(strEnd, intRootNodeStart + strStart.Length);
                //    if ((intRootNodeStart != -1) && (intRootNodeEnd != -1))
                //        refResponse = refResponse.Substring(intRootNodeStart, intRootNodeEnd - intRootNodeStart + strEnd.Length);
                //    else throw new Exception("Failed to find node: " + strRootNode + " in response: \r\n" + refResponse);
                //}

                /* no xml returned by the ws call */
                if (strRootNode == "noxml")
                    refResponse = "<noxml>" + refResponse + "</noxml>";
                if (strRootNode == "noxmlencode")
                    refResponse = "<noxmlencode>" + System.Web.HttpUtility.HtmlEncode(refResponse) + "</noxmlencode>";

                if (strRootNode != "")
                {
                    /* Handle root nodes in format:
                     * <node></node>
                     * <node a="1"></node>
                     * <node/>, <node />, <node a="1" />
                     */
                    int intRootNodeEnd = -1;
                    string strEnd = "";

                    string strStart = "<" + strRootNode + ">";
                    int intRootNodeStart = refResponse.IndexOf(strStart);
                    if (intRootNodeStart == -1)
                    {
                        strStart = "<" + strRootNode + " ";
                        intRootNodeStart = refResponse.IndexOf(strStart);
                    }

                    /* 17-01-2011 MdG added: detect directly closed nodes */
                    if (intRootNodeStart == -1)
                    {
                        strStart = "<" + strRootNode + "/>";
                        intRootNodeStart = refResponse.IndexOf(strStart);
                        intRootNodeEnd = intRootNodeStart + strStart.Length;
                    }

                    if (intRootNodeEnd == -1)
                    {
                        strEnd = "</" + strRootNode + ">";
                        intRootNodeEnd = refResponse.IndexOf(strEnd, intRootNodeStart + strStart.Length);
                    }

                    /* 17-01-2011 MdG added: if the character before the next close mark equals /, this is the end */
                    if (intRootNodeEnd == -1)
                    {
                        strEnd = ">";
                        intRootNodeEnd = refResponse.IndexOf(strEnd, intRootNodeStart + strStart.Length);
                        if (intRootNodeEnd != -1) intRootNodeEnd = (refResponse[intRootNodeEnd - 1].Equals('/') ? intRootNodeEnd : -1);
                    }

                    if ((intRootNodeStart != -1) && (intRootNodeEnd != -1))
                        refResponse = refResponse.Substring(intRootNodeStart, intRootNodeEnd - intRootNodeStart + strEnd.Length);
                    else throw new Exception("Failed to find node: " + strRootNode + " in response: \r\n" + refResponse);
                }

                if (refResponse == "") refResponse = "<root></root>";
                refResponse = refResponse.Replace(" xmlns=", " xml_ns=");

                /* Fix for illegal xml characters */
                //string strReplace = (char)0x0 + "|" + (char)0x1 + "|" + (char)0x2 + "|" + (char)0x3 + "|" + (char)0x4 + "|" + (char)0x5 + "|" + (char)0x6 + "|" + (char)0x7 + "|" + (char)0x8 + "|" + (char)0xB + "|" + (char)0xC + "|" + (char)0xE + "|" + (char)0xF + "|" + (char)0x10 + "|" + (char)0x11 + "|" + (char)0x12 + "|" + (char)0x13 + "|" + (char)0x14 + "|" + (char)0x15 + "|" + (char)0x1A + "|" + (char)0x1B + "|" + (char)0x1C + "|" + (char)0x1D + "|" + (char)0x1E + "|" + (char)0x1F + (char)0x16 + "|" + (char)0x17 + "|" + (char)0x18 + "|" + (char)0x19 + "|" + "|" + (char)0x7F;
                //Regex pattern = new Regex("[" + strReplace + "]");
                string strReplace = (char)0x0 + "|" + (char)0x1 + "|" + (char)0x2 + "|" + (char)0x3 + "|" + (char)0x4 + "|" + (char)0x5 + "|" + (char)0x6 + "|" + (char)0x7 + "|" + (char)0x8 + "|" + (char)0xB + "|" + (char)0xC + "|" + (char)0xE + "|" + (char)0xF + "|" + (char)0x10 + "|" + (char)0x11 + "|" + (char)0x12 + "|" + (char)0x13 + "|" + (char)0x14 + "|" + (char)0x15 + "|" + (char)0x1A + "|" + (char)0x1B + "|" + (char)0x1C + "|" + (char)0x1D + "|" + (char)0x1E + "|" + (char)0x1F + (char)0x16 + "|" + (char)0x17 + "|" + (char)0x18 + "|" + (char)0x19 + "|" + (char)0x7F;
                Regex pattern = new Regex(strReplace);
                refResponse = pattern.Replace(refResponse, " ");

                /* Put the response in an XML document */
                XmlDocument objXmlDocument = new XmlDocument();
                try { objXmlDocument.LoadXml(refResponse); }
                catch (Exception e) { throw new Exception("Failed to create an XML document from the response of the web service. Error: " + e.Message + "\r\nResponse: " + refResponse); }

                /*
                 * in plaats van altijd de namespace prefix NS voor de default namespace te gebruiken:
                 * als parameter ns leeg is geen namespacemanager toepassen
                 * als parameter ns gevuld is moet deze naar een geldige namespace verwijzen en halen we de URL hiervan op
                 */
                /* Need the namespace manager for XML documents that define more namespaces than only a default */
                XmlNamespaceManager objXmlNamespaceManager = new XmlNamespaceManager(objXmlDocument.NameTable);
                objXmlNamespaceManager.AddNamespace(strNs, objXmlDocument.DocumentElement.NamespaceURI);


                /////* Select the nodes given by the XPath and NS */
                ////XmlNodeList objXmlNodeList = objXmlDocument.SelectNodes(strXPath, objXmlNamespaceManager);
                ////if (objXmlNodeList.Count == 0) throw new Exception("XPath " + strXPath + " could not be found in the response.\r\nResponse: " + refResponse);
                ////if (objXmlNodeList.Count > 1) throw new Exception("XPath " + strXPath + " found to many time in the response.\r\nResponse: " + refResponse);
                ////refInnerXml = objXmlNodeList[0].InnerXml;
                ////refOuterXml = objXmlNodeList[0].OuterXml;

                /////* Build a DataTable from the XML and return it */
                ////DataSet objDataSet = new DataSet();
                ////using (XmlReader reader = new XmlNodeReader(objXmlNodeList[0]))
                ////{
                ////    objDataSet.ReadXml(reader);
                ////    reader.Close();
                ////}
                ////if (objDataSet.Tables.Count == 0) return null;
                ////else return objDataSet.Tables;

                /* Select the nodes given by the XPath and NS */
                DataTable ret = new DataTable();
                XmlNodeList objXmlNodeList = objXmlDocument.SelectNodes(strXPath, objXmlNamespaceManager);
                if (objXmlNodeList.Count > 0)
                {
                    XmlNode objXmlParentNode = objXmlNodeList[0].ParentNode;

                    /* objXmlParentNode can be null if objXmlNodeList[0] is the rootnode */
                    if (objXmlParentNode != null && objXmlParentNode.ChildNodes.Count != objXmlNodeList.Count)
                    {
                        objXmlParentNode.RemoveAll();
                        foreach (XmlNode node in objXmlNodeList)
                            objXmlParentNode.AppendChild(node);
                    }

                    if (objXmlParentNode == null) objXmlParentNode = objXmlNodeList[0];
                    if (objXmlParentNode != null)
                    {
                        refInnerXml = objXmlParentNode.InnerXml;
                        refOuterXml = objXmlParentNode.OuterXml;
                    }

                    DataSet objDataSet = new DataSet();
                    using (XmlReader reader = new XmlNodeReader(objXmlParentNode))
                    {
                        objDataSet.ReadXml(reader);
                        reader.Close();
                    }

                    if (objDataSet.Tables.Count > 0)
                    {
                        if (objDataSet.Tables.IndexOf(objXmlNodeList[0].Name) > -1)
                            ret = objDataSet.Tables[objXmlNodeList[0].Name];
                        else
                            ret = objDataSet.Tables[0];
                        if (ret == null) ret = objDataSet.Tables[0];
                    }

                    foreach (DataColumn dc in ret.Columns)
                    {
                        if (dc.ColumnMapping == MappingType.SimpleContent)
                        {
                            dc.ColumnMapping = MappingType.Element;
                            dc.ColumnName = dc.ColumnName.Replace("_Text", "");
                        }
                        if (dc.ColumnName.ToLower() == "xml") dc.ColumnName = "_" + dc.ColumnName;
                    }

                    ret.Columns.Add("XML", System.Type.GetType("System.String"));

                    int i = 0;
                    foreach (XmlNode objXmlNode in objXmlNodeList)
                    {
                        if (ret.Rows.Count < i + 1) ret.LoadDataRow(new string[0], true);
                        ret.Rows[i]["XML"] = objXmlNode.OuterXml;
                        i++;
                    }
                }
                return ret;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public string GetResponse(string strCommandUrl)
        {
            try
            {
                string strUrl = this.GetParameterValue(strCommandUrl, "url");
                string strUserName = this.GetParameterValue(strCommandUrl, "username");
                string strPassword = this.GetParameterValue(strCommandUrl, "password");
                string strDomain = this.GetParameterValue(strCommandUrl, "domain");
                string strProxyUrl = this.GetParameterValue(strCommandUrl, "proxyurl");
                string strProxyUserName = this.GetParameterValue(strCommandUrl, "proxyusername");
                string strProxyPassword = this.GetParameterValue(strCommandUrl, "proxypassword");
                string strProxyDomain = this.GetParameterValue(strCommandUrl, "proxydomain");

                string strCmd = this.GetParameterValue(strCommandUrl, "cmd");
                string strXml = this.GetParameterValue(strCommandUrl, "xml");
                string strXPath = this.GetParameterValue(strCommandUrl, "xpath");
                string strNs = this.GetParameterValue(strCommandUrl, "ns");
                string strContentType = this.GetParameterValue(strCommandUrl, "contenttype");
                string strSOAPAction = this.GetParameterValue(strCommandUrl, "soapaction");
                string strRootNode = this.GetParameterValue(strCommandUrl, "rootnode");
                if (strUrl == null) throw new Exception("Missing parameter: url (" + strCommandUrl + ")");
                if (strXml == null) throw new Exception("Missing parameter: xml (" + strCommandUrl + ")");
                if (strXPath == null) throw new Exception("Missing parameter: xpath (" + strCommandUrl + ")");

                /* URL decode given parameters (except XML that need to be send encoded to the webservice) */
                strUrl = System.Web.HttpUtility.UrlDecode(strUrl, Encoding.Default);
                strUserName = System.Web.HttpUtility.UrlDecode(strUserName, Encoding.Default);
                strPassword = System.Web.HttpUtility.UrlDecode(strPassword, Encoding.Default);
                strDomain = System.Web.HttpUtility.UrlDecode(strDomain, Encoding.Default);
                strProxyUrl = System.Web.HttpUtility.UrlDecode(strProxyUrl, Encoding.Default);
                strProxyUserName = System.Web.HttpUtility.UrlDecode(strProxyUserName, Encoding.Default);
                strProxyPassword = System.Web.HttpUtility.UrlDecode(strProxyPassword, Encoding.Default);
                strProxyDomain = System.Web.HttpUtility.UrlDecode(strProxyDomain, Encoding.Default);

                strCmd = System.Web.HttpUtility.UrlDecode(strCmd, Encoding.Default);
                strXml = System.Web.HttpUtility.UrlDecode(strXml, Encoding.Default);
                strXPath = System.Web.HttpUtility.UrlDecode(strXPath, Encoding.Default);
                strNs = (strNs == null || strNs == "") ? "ns" : System.Web.HttpUtility.UrlDecode(strNs, Encoding.Default);
                strContentType = (strContentType == null || strContentType == "") ? "application/x-www-form-urlencoded" : System.Web.HttpUtility.UrlDecode(strContentType, Encoding.Default);
                strRootNode = (strRootNode == null) ? "" : System.Web.HttpUtility.UrlDecode(strRootNode, Encoding.Default);

                /* Build the XML to post and get the response */
                string ret = this.GetResponse(strUrl + strCmd, strUserName, strPassword, strDomain, strProxyUrl, strProxyUserName, strProxyPassword, strProxyDomain, strXml, strContentType, strSOAPAction);
                //if (ret == "") return "{}";
                //string refResponse = ret;

                //if (strRootNode == "noxml")
                //    refResponse = "<noxml>" + refResponse + "</noxml>";
                //if (strRootNode == "noxmlencode")
                //    refResponse = "<noxmlencode>" + System.Web.HttpUtility.HtmlEncode(refResponse) + "</noxmlencode>";

                //if (strRootNode != "")
                //{
                //    /* Handle root nodes in format:
                //     * <node></node>
                //     * <node a="1"></node>
                //     * <node/>, <node />, <node a="1" />
                //     */
                //    int intRootNodeEnd = -1;
                //    string strEnd = "";

                //    string strStart = "<" + strRootNode + ">";
                //    int intRootNodeStart = refResponse.IndexOf(strStart);
                //    if (intRootNodeStart == -1)
                //    {
                //        strStart = "<" + strRootNode + " ";
                //        intRootNodeStart = refResponse.IndexOf(strStart);
                //    }

                //    /* 17-01-2011 MdG added: detect directly closed nodes */
                //    if (intRootNodeStart == -1)
                //    {
                //        strStart = "<" + strRootNode + "/>";
                //        intRootNodeStart = refResponse.IndexOf(strStart);
                //        intRootNodeEnd = intRootNodeStart + strStart.Length;
                //    }

                //    if (intRootNodeEnd == -1)
                //    {
                //        strEnd = "</" + strRootNode + ">";
                //        intRootNodeEnd = refResponse.IndexOf(strEnd, intRootNodeStart + strStart.Length);
                //    }

                //    /* 17-01-2011 MdG added: if the character before the next close mark equals /, this is the end */
                //    if (intRootNodeEnd == -1)
                //    {
                //        strEnd = ">";
                //        intRootNodeEnd = refResponse.IndexOf(strEnd, intRootNodeStart + strStart.Length);
                //        if (intRootNodeEnd != -1) intRootNodeEnd = (refResponse[intRootNodeEnd - 1].Equals('/') ? intRootNodeEnd : -1);
                //    }

                //    if ((intRootNodeStart != -1) && (intRootNodeEnd != -1))
                //        refResponse = refResponse.Substring(intRootNodeStart, intRootNodeEnd - intRootNodeStart + strEnd.Length);
                //    else throw new Exception("Failed to find node: " + strRootNode + " in response: \r\n" + refResponse);
                //}

                //if (refResponse == "") refResponse = "<root></root>";
                //refResponse = refResponse.Replace(" xmlns=", " xml_ns=");

                ///* Fix for illegal xml characters */
                ////string strReplace = (char)0x0 + "|" + (char)0x1 + "|" + (char)0x2 + "|" + (char)0x3 + "|" + (char)0x4 + "|" + (char)0x5 + "|" + (char)0x6 + "|" + (char)0x7 + "|" + (char)0x8 + "|" + (char)0xB + "|" + (char)0xC + "|" + (char)0xE + "|" + (char)0xF + "|" + (char)0x10 + "|" + (char)0x11 + "|" + (char)0x12 + "|" + (char)0x13 + "|" + (char)0x14 + "|" + (char)0x15 + "|" + (char)0x1A + "|" + (char)0x1B + "|" + (char)0x1C + "|" + (char)0x1D + "|" + (char)0x1E + "|" + (char)0x1F + (char)0x16 + "|" + (char)0x17 + "|" + (char)0x18 + "|" + (char)0x19 + "|" + "|" + (char)0x7F;
                ////Regex pattern = new Regex("[" + strReplace + "]");
                //string strReplace = (char)0x0 + "|" + (char)0x1 + "|" + (char)0x2 + "|" + (char)0x3 + "|" + (char)0x4 + "|" + (char)0x5 + "|" + (char)0x6 + "|" + (char)0x7 + "|" + (char)0x8 + "|" + (char)0xB + "|" + (char)0xC + "|" + (char)0xE + "|" + (char)0xF + "|" + (char)0x10 + "|" + (char)0x11 + "|" + (char)0x12 + "|" + (char)0x13 + "|" + (char)0x14 + "|" + (char)0x15 + "|" + (char)0x1A + "|" + (char)0x1B + "|" + (char)0x1C + "|" + (char)0x1D + "|" + (char)0x1E + "|" + (char)0x1F + (char)0x16 + "|" + (char)0x17 + "|" + (char)0x18 + "|" + (char)0x19 + "|" + (char)0x7F;
                //Regex pattern = new Regex(strReplace);
                //refResponse = pattern.Replace(refResponse, " ");

                ///* Put the response in an XML document */
                //XmlDocument objXmlDocument = new XmlDocument();
                //try { objXmlDocument.LoadXml(refResponse); }
                //catch (Exception e) { throw new Exception("Failed to create an XML document from the response of the web service. Error: " + e.Message + "\r\nResponse: " + refResponse); }

                
                
                ///*
                // * in plaats van altijd de namespace prefix NS voor de default namespace te gebruiken:
                // * als parameter ns leeg is geen namespacemanager toepassen
                // * als parameter ns gevuld is moet deze naar een geldige namespace verwijzen en halen we de URL hiervan op
                // */
                ///* Need the namespace manager for XML documents that define more namespaces than only a default */
                //XmlNamespaceManager objXmlNamespaceManager = new XmlNamespaceManager(objXmlDocument.NameTable);
                //objXmlNamespaceManager.AddNamespace(strNs, objXmlDocument.DocumentElement.NamespaceURI);

                //DataTable dt = new DataTable();
                //XmlNodeList objXmlNodeList = objXmlDocument.SelectNodes(strXPath, objXmlNamespaceManager);
                //if (objXmlNodeList.Count > 0)
                //{
                //    XmlNode objXmlParentNode = objXmlNodeList[0].ParentNode;

                //    /* objXmlParentNode can be null if objXmlNodeList[0] is the rootnode */
                //    if (objXmlParentNode != null && objXmlParentNode.ChildNodes.Count != objXmlNodeList.Count)
                //    {
                //        objXmlParentNode.RemoveAll();
                //        foreach (XmlNode node in objXmlNodeList)
                //            objXmlParentNode.AppendChild(node);
                //    }

                //    if (objXmlParentNode == null) objXmlParentNode = objXmlNodeList[0];
                //    //if (objXmlParentNode != null)
                //    //{
                //    //    refInnerXml = objXmlParentNode.InnerXml;
                //    //    refOuterXml = objXmlParentNode.OuterXml;
                //    //}

                //    DataSet objDataSet = new DataSet();
                //    using (XmlReader reader = new XmlNodeReader(objXmlParentNode))
                //    {
                //        objDataSet.ReadXml(reader);
                //        reader.Close();
                //    }

                //    if (objDataSet.Tables.Count > 0)
                //    {
                //        if (objDataSet.Tables.IndexOf(objXmlNodeList[0].Name) > -1)
                //            dt = objDataSet.Tables[objXmlNodeList[0].Name];
                //        else
                //            dt = objDataSet.Tables[0];
                //        if (dt == null) dt = objDataSet.Tables[0];
                //    }

                //    foreach (DataColumn dc in dt.Columns)
                //    {
                //        if (dc.ColumnMapping == MappingType.SimpleContent)
                //        {
                //            dc.ColumnMapping = MappingType.Element;
                //            dc.ColumnName = dc.ColumnName.Replace("_Text", "");
                //        }
                //        if (dc.ColumnName.ToLower() == "xml") dc.ColumnName = "_" + dc.ColumnName;
                //    }

                //    dt.Columns.Add("XML", System.Type.GetType("System.String"));

                //    int i = 0;
                //    foreach (XmlNode objXmlNode in objXmlNodeList)
                //    {
                //        if (dt.Rows.Count < i + 1) dt.LoadDataRow(new string[0], true);
                //        dt.Rows[i]["XML"] = objXmlNode.OuterXml;
                //        i++;
                //    }
                //}
                //ret = JsonConvert.SerializeObject(dt);

                return ret;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
