using System;
using System.Web;
using System.Xml;
using System.IO;
using XDocuments;
using XDataSourceModule;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized ;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using System.Net;
using KubionLogNamespace;

namespace XDocServices
{
	public class XDocService : IHttpHandler
    {
        HttpContext m_context=null;

        private string sCache = "";
        public string Cache
        {
            get
            {
                if ((sCache == "") && (m_context != null) && (m_context.Request.Params["cache"] != null)) sCache = m_context.Request.Params["cache"].ToString();
                if ((sCache == "") && (System.Configuration.ConfigurationManager.AppSettings["Cache"] != null)) sCache = System.Configuration.ConfigurationManager.AppSettings["Cache"];
                if (sCache == "") sCache = "1";
                return sCache;
            }
            set
            {
                sCache = value;
            }
        }
        private string sCacheSV = "";
        public string CacheSV
        {
            get
            {
                if ((sCacheSV == "") && (m_context != null) && (m_context.Request.Params["cachesv"] != null)) sCacheSV = m_context.Request.Params["cachesv"].ToString();
                if ((sCacheSV == "") && (System.Configuration.ConfigurationManager.AppSettings["CacheSV"] != null)) sCacheSV = System.Configuration.ConfigurationManager.AppSettings["CacheSV"];
                if (sCacheSV == "") sCacheSV = "1";
                return sCacheSV;
            }
            set
            {
                sCacheSV = value;
            }
        }
        public bool IsReusable
        {
            get { return false; }
        }

        bool bAcceptTypes = false;
        bool bHeaders = false;
        bool bCookies = false;
        bool bFiles = false;
        bool bUserLanguages = false;
        bool bContent = false;
        bool bUser = false;
        bool bParams = false;
        bool bUri = false;
        string sRootTemplate = "A";


        #region XDoc Template
        private string GetTemplateContentUC(string strTemplateName, Hashtable htParameters, ref string strDocType)
        {
            XDocCache m_xdocCache = null;
            XDocCacheTemplates m_xdocCacheTemplates = null;
            if ((CacheSV != "0") && (HttpContext.Current != null) && (HttpContext.Current.Session != null)) m_xdocCache = (XDocCache)HttpContext.Current.Session["XDocCache"];
            if ((Cache != "0") && (HttpContext.Current != null) && (HttpContext.Current.Application != null)) m_xdocCacheTemplates = (XDocCacheTemplates)HttpContext.Current.Application["XDocCacheTemplates"];
            string sReturn = GetTemplateContentUC(strTemplateName, htParameters, ref  strDocType, ref  m_xdocCacheTemplates, ref  m_xdocCache);
            //if ((Cache != "0") && (HttpContext.Current != null) && (HttpContext.Current.Application != null)) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            //if ((CacheSV != "0") && (HttpContext.Current != null) && (HttpContext.Current.Session != null)) HttpContext.Current.Session["XDocCache"] = m_xdocCache;
            if ((HttpContext.Current != null) && (HttpContext.Current.Application != null)) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            if ((HttpContext.Current != null) && (HttpContext.Current.Session != null)) HttpContext.Current.Session["XDocCache"] = m_xdocCache;
            return sReturn;
        }
        private string GetTemplateContentUC(string strTemplateName, Hashtable htParameters, ref string strDocType, ref XDocCacheTemplates m_xdocCacheTemplates, ref XDocCache m_xdocCache)
        {
            XDocumentsWebControls.XDoc xdocwebcontrol = new XDocumentsWebControls.XDoc();
            XDocManager xm = new XDocManager();
            if (m_xdocCache != null) xm.DocCache = m_xdocCache;
            if (m_xdocCacheTemplates != null) xm.DocCacheTemplates = m_xdocCacheTemplates;
            strDocType = xm.LoadTemplate(strTemplateName).DocType;
            string result = xdocwebcontrol.GetHTML(strTemplateName, htParameters, HttpContext.Current);
            m_xdocCacheTemplates = xm.DocCacheTemplates;
            m_xdocCache = xm.DocCache;
            xm.Dispose();
            return result;
        }
        private string GetTemplateContent(string strTemplateName, Hashtable htParameters, ref string strDocType)
        {
            XDocCache m_xdocCache = null;
            XDocCacheTemplates m_xdocCacheTemplates = null;
            if ((CacheSV != "0") && (HttpContext.Current != null) && (HttpContext.Current.Session != null)) m_xdocCache = (XDocCache)HttpContext.Current.Session["XDocCache"];
            if ((Cache != "0") && (HttpContext.Current != null) && (HttpContext.Current.Application != null)) m_xdocCacheTemplates = (XDocCacheTemplates)HttpContext.Current.Application["XDocCacheTemplates"];
            string sReturn = GetTemplateContent(strTemplateName, htParameters, ref  strDocType, ref  m_xdocCacheTemplates, ref  m_xdocCache);
            //if ((Cache != "0") && (HttpContext.Current != null) && (HttpContext.Current.Application != null)) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            //if ((CacheSV != "0") && (HttpContext.Current != null) && (HttpContext.Current.Session != null)) HttpContext.Current.Session["XDocCache"] = m_xdocCache;
            if ( (HttpContext.Current != null) && (HttpContext.Current.Application != null)) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            if ( (HttpContext.Current != null) && (HttpContext.Current.Session != null)) HttpContext.Current.Session["XDocCache"] = m_xdocCache;
            return sReturn;
        }
        private string GetTemplateContent(string strTemplateName, Hashtable htParameters, ref string strDocType, ref XDocCacheTemplates m_xdocCacheTemplates, ref XDocCache m_xdocCache)
        {
            //System.Diagnostics.Debug.WriteLine("LT1:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            XDocManager xm = new XDocManager();
            if (m_xdocCache != null) xm.DocCache = m_xdocCache;
            if (m_xdocCacheTemplates != null) xm.DocCacheTemplates = m_xdocCacheTemplates;
            //System.Diagnostics.Debug.WriteLine("LT2:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            IXTemplate xt = xm.LoadTemplate(strTemplateName);
            //System.Diagnostics.Debug.WriteLine("LT3:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            IXDocument xd = xt.ProcessTemplate(htParameters);
            //System.Diagnostics.Debug.WriteLine("LT4:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            strDocType = xt.DocType;
            m_xdocCacheTemplates = xm.DocCacheTemplates;
            m_xdocCache = xm.DocCache;           
            xm.Dispose();
            return xd.Content;
        }
        private string GetContextTemplateContent(HttpContext m_context, string strTemplateName, Hashtable htParameters, ref string strDocType, ref XDocCache m_xdocCache)
        {
            XDocCacheTemplates m_xdocCacheTemplates = null;
            if ((CacheSV != "0") && (m_context != null) && (m_context.Session != null)) m_xdocCache = (XDocCache)m_context.Session["XDocCache"];
            if ((Cache != "0") && (m_context != null) && (m_context.Application != null)) m_xdocCacheTemplates = (XDocCacheTemplates)m_context.Application["XDocCacheTemplates"];
            string sReturn = GetTemplateContent(strTemplateName, htParameters, ref  strDocType, ref  m_xdocCacheTemplates, ref  m_xdocCache);
            //if ((Cache != "0") && (m_context != null) && (m_context.Application != null)) m_context.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            //if ((CacheSV != "0") && (m_context != null) && (m_context.Session != null)) m_context.Session["XDocCache"] = m_xdocCache;
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            if ((m_context != null) && (m_context.Session != null)) m_context.Session["XDocCache"] = m_xdocCache;
            return sReturn;
        }
        #endregion

        #region old
        private Hashtable GetParameters(HttpRequest objHttpRequest)
        {
            /*
             * Path				/RESTService/Handler
             * QueryString		{ ApplicationName=tinyget }
             * ApplicationPath	/RESTService
             */
            Hashtable objHashtable = new Hashtable(StringComparer.InvariantCultureIgnoreCase);

            string strKey = "";

            string strTemplateName = "";

            foreach (string strPathPart in objHttpRequest.Path.Substring(objHttpRequest.ApplicationPath.Length).Split('/'))
            {
                if (strKey == "")
                {
                    strKey = strPathPart;
                    strTemplateName += (strTemplateName != "" ? "_" : "") + strPathPart;
                }
                else
                {
                    objHashtable.Remove(strKey);
                    objHashtable.Add(strKey, strPathPart);
                    strTemplateName += "_ID";
                    strKey = "";
                }
            }
            if (strKey != "")
            {
                objHashtable.Remove(strKey);
                objHashtable.Add(strKey, "");
            }

            objHashtable.Remove("TemplateName");
            objHashtable.Add("TemplateName", strTemplateName);

            foreach (string strQueryKey in objHttpRequest.QueryString.Keys)
            {
                if (strQueryKey != null && strQueryKey != "")
                {
                    objHashtable.Remove(strQueryKey);
                    objHashtable.Add(strQueryKey, objHttpRequest.QueryString[strQueryKey]);
                }
            }
            //objHttpRequest.QueryString.CopyTo(objHashtable, 0);


            return objHashtable;
        }
        private string GetParameterValue(string strParameters, string strKey)
        {
            //function used only by WriteLog to read LoginName and ApplicationName !
            strParameters = strParameters.Replace('?', '&');
            int intStart = strParameters.IndexOf("&" + strKey, StringComparison.CurrentCultureIgnoreCase);
            if (intStart == -1) return null;
            intStart = intStart + strKey.Length + 1 + 1; /* plus 1 for the ampersand and plus 1 for index fix */

            int intEnd = strParameters.IndexOf('&', intStart);
            if (intEnd == -1) intEnd = strParameters.Length;

            return strParameters.Substring(intStart, intEnd - intStart);
        }
        private string GetURLParameters(HttpRequest objHttpRequest)
		{
            //function not used 
			string strRequestUrl = objHttpRequest.RawUrl.Substring(objHttpRequest.ApplicationPath.Length).Replace('?', '&');
			string strParameters = "";
			string strPart = "";
			int intStart, intEnd, i;
			i = 1;

			while (strRequestUrl.IndexOf('/') != -1)
			{
				intStart = strRequestUrl.IndexOf('/');
				intEnd = strRequestUrl.IndexOf('/', intStart + 1);
				if (intEnd == -1) intEnd = strRequestUrl.Length;
				strPart = strRequestUrl.Substring(intStart + 1, intEnd - intStart - 1);
				if (strPart.IndexOf('&') != -1)
				{
					intEnd = strRequestUrl.IndexOf('&');
					strPart = strRequestUrl.Substring(intStart + 1, intEnd - intStart - 1);
				}
				if (i % 2 == 1) strParameters += (i == 1 ? "" : "&") + strPart;
				else strParameters += "=" + strPart;
				i++;
				strRequestUrl = strRequestUrl.Substring(intEnd - intStart);
			}
			if (strRequestUrl.IndexOf('&') != -1) strParameters += strRequestUrl.Substring(strRequestUrl.IndexOf('&'));
			return strParameters;
		}
		private String GetVal(Hashtable objHashtable, String strKey)
		{
			if (objHashtable[strKey] == null)
				return "";
			else
				return objHashtable[strKey].ToString();
		}
        //private String GetQueryStringFromHashtable(Hashtable objHashtable, System.Text.Encoding enc)
        //{
        //    //System.Web.HttpUtility.UrlEncode(, context.Request.ContentEncoding)

        //    //string strReturn = "";
        //    //foreach (string strKey in objHashtable.Keys)
        //    //{
        //    //    if (strKey != "TemplateName")
        //    //        strReturn += "&" + System.Web.HttpUtility.UrlEncode(strKey,enc) + "=" + System.Web.HttpUtility.UrlEncode(GetVal(objHashtable,strKey),enc);
        //    //}
        //    //return strReturn;

        //    StringBuilder strReturn = new StringBuilder();
        //    foreach (string strKey in objHashtable.Keys)
        //    {
        //        if (strKey != "TemplateName")
        //            strReturn.Append("&" + System.Web.HttpUtility.UrlEncode(strKey, enc) + "=" + System.Web.HttpUtility.UrlEncode(GetVal(objHashtable, strKey), enc));
        //    }
        //    return strReturn.ToString();
        //}
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        private DataTable DTPostedData(String postedData)
		{
			if (postedData != "")
			{

				/* Put the response in an XML document */
				XmlDocument objXmlDocumentPosted = new XmlDocument();
				objXmlDocumentPosted.LoadXml(postedData);

				/* Build a DataTable from the XML and return it */
				DataSet objDataSet = new DataSet();
				using (XmlReader objXmlReader = new XmlNodeReader(objXmlDocumentPosted.DocumentElement))
				{
					objDataSet.ReadXml(objXmlReader);
					objXmlReader.Close();
				}

				if (objDataSet.Tables.Count > 0)
					return objDataSet.Tables[0];
			}
			return null;
		}
		private string GetContentType(string strDocType, string strView)
		{
			/* Determine the content type */
			/* ?view= should affect the content-type returned, set by the web.config */
			/* Can only set the contentType after calling this.GetTemplateContentUC, because adding XDoc uc to the current context, it will set the contentType */

			/* DocType is comming from the template attributes */
			if (strView == null && strDocType != null && strDocType != "")
			{
				if (strDocType.StartsWith(".")) strDocType = strDocType.Substring(1);
				strView = strDocType.ToLower();
			}

			if (strView == null) strView = "default";

			/* Read the content type from the web.config */
			if (ConfigurationManager.AppSettings[strView] != null && ConfigurationManager.AppSettings[strView] != "")
			{
				return ConfigurationManager.AppSettings[strView].ToString();
			}
			else
			{
				return "text/xml; charset=utf-8";
			}
		}
		private string GetEncodingError(string strResponseContentType, System.Text.Encoding objResponseEncoding)
		{
			if (strResponseContentType.IndexOf("charset=") != -1)
			{
				int intStart = strResponseContentType.IndexOf("charset=") + 8;
				int intEnd = (strResponseContentType + ";").IndexOf(";", intStart);
				if (intEnd != -1)
				{
					string strCharSet = strResponseContentType.Substring(intStart, intEnd - intStart);
					System.Text.Encoding charsetEncoding = null;
					try
					{
						charsetEncoding = System.Text.Encoding.GetEncoding(strCharSet);
					}
					catch (Exception e)
					{
					}

					if (!objResponseEncoding.Equals(charsetEncoding))
					{
						return "Response charset " + strCharSet + " does not match content encoding " + objResponseEncoding.WebName;
					}
				}
			}
			return null;
		}
        #endregion

        #region Newton Request
        //class RequestResolver : DefaultContractResolver
        //{
        //    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        //    {
        //        JsonProperty property = base.CreateProperty(member, memberSerialization);

        //        property.ShouldSerialize = instance =>
        //        {
        //            try
        //            {
        //                if (member.Name == "RequestContext") return false;
        //                if (member.Name == "Browser") return false;
        //                if (member.Name == "Unvalidated") return false;
        //                if (member.Name == "ServerVariables") return false;
        //                if (member.Name == "Params") return false;


        //                PropertyInfo prop = (PropertyInfo)member;
        //                if (prop.CanRead)
        //                {
        //                    prop.GetValue(instance, null);
        //                    return true;
        //                }
        //            }
        //            catch
        //            {
        //            }
        //            return false;
        //        };

        //        return property;
        //    }
        //}
        //private string GetRequestJSONString(HttpContext context)
        //{
        //    JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
        //    serializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
        //    serializerSettings.ContractResolver = new RequestResolver();
        //    serializerSettings.MaxDepth = 5;
        //    serializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        //    serializerSettings.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
        //    serializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
        //    serializerSettings.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Reuse;
        //    serializerSettings.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore;

        //    return JsonConvert.SerializeObject(context.Request, serializerSettings);
        //}
        #endregion

        #region util
        //public static string _JsonEscape(string aText)
        //{
        //    string result = "";
        //    if (aText != null)
        //        foreach (char c in aText)
        //        {
        //            switch (c)
        //            {
        //                case '\\': result += "\\\\"; break;
        //                case '\"': result += "\\\""; break;
        //                case '\n': result += "\\n"; break;
        //                case '\r': result += "\\r"; break;
        //                case '\t': result += "\\t"; break;
        //                case '\b': result += "\\b"; break;
        //                case '\f': result += "\\f"; break;
        //                default: result += c; break;
        //            }
        //        }
        //    return result;
        //}
        //private int myPathParamsRender(StringBuilder sJSON, string sPath)
        //{
        //    int i = 0;
        //    sJSON.Append("{");
        //    string[] aPath = sPath.Split('/');
        //    string sIdsPath = "";
        //    string sIdsPath1 = "";
        //    sIdsPath = aPath[0];
        //    for (i = 0; i < aPath.Length; i++)
        //    {
        //        sJSON.Append("\"");
        //        sJSON.Append(Utils._JsonEscape("Path" + (i + 1).ToString()));
        //        sJSON.Append("\":\"");
        //        sJSON.Append(Utils._JsonEscape(aPath[i]));
        //        sJSON.Append("\",");
        //        if (i > 0)
        //        {
        //            sJSON.Append("\"");
        //            sJSON.Append(Utils._JsonEscape(aPath[i - 1]));
        //            sJSON.Append("\":\"");
        //            sJSON.Append(Utils._JsonEscape(aPath[i]));
        //            sJSON.Append("\",");
        //        }
        //        if (i > 0)
        //        {
        //            if (i % 2 == 0)
        //            {
        //                sIdsPath += "_" + aPath[i];
        //                sIdsPath1 += "_ID";
        //            }
        //            else
        //            {
        //                sIdsPath += "_ID";
        //                sIdsPath1 += "_" + aPath[i];
        //            }

        //        }
        //    }
        //    if (i > 0)
        //    {
        //        sJSON.Append("\"PathLast\":\"");
        //        sJSON.Append(Utils._JsonEscape(aPath[i - 1]));
        //        sJSON.Append("\",");
        //    }
        //    if (i > 1)
        //    {
        //        sJSON.Append("\"PathLast1\":\"");
        //        sJSON.Append(Utils._JsonEscape(aPath[i - 2]));
        //        sJSON.Append("\",");
        //    }
        //    sJSON.Append("\"IDsPath\":\"");
        //    sJSON.Append(Utils._JsonEscape(sIdsPath));
        //    sJSON.Append("\",");
        //    if (sIdsPath1.StartsWith("_")) sIdsPath1 = sIdsPath1.Substring(1);
        //    sJSON.Append("\"IDsPath1\":\"");
        //    sJSON.Append(Utils._JsonEscape(sIdsPath1));
        //    sJSON.Append("\",");
        //    sJSON.Append("\"Count\":\"");
        //    sJSON.Append(Utils._JsonEscape(i.ToString()));
        //    sJSON.Append("\"");
        //    sJSON.Append("}");
        //    return i;
        //}
        private string myReadContent(HttpRequest req)
        {
            string postedData = "";
            int iContentLength = req.ContentLength;
            if (iContentLength < 5000)
            {
                StreamReader reader = new StreamReader(req.InputStream, req.ContentEncoding);
                postedData = reader.ReadToEnd().Trim();
                return postedData;
            }
            else
                return "[[datasize exceeds limit]]";
        }
        private int myNameValueCollectionRender(StringBuilder sJSON, NameValueCollection col, NameValueCollection exclude)
        {
            int i = 0;
            sJSON.Append("{\n");
            foreach (string key in col.Keys)
            {
                if ((exclude==null ) || (exclude[key] == null))
                {
                    sJSON.Append("\"");
                    sJSON.Append(Utils._JsonEscape(key));
                    sJSON.Append("\":\"");
                    sJSON.Append(Utils._JsonEscape(col[key].ToString()));
                    sJSON.Append("\",\n");
                    i++;
                }
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(Utils._JsonEscape(i.ToString()));
            sJSON.Append("\"\n");
            sJSON.Append("}");
            return col.Count;
        }
        private int myStringArrayRender(StringBuilder sJSON, string[] col)
        {
            bool isFirst = true;
            sJSON.Append("[");
            foreach (string key in col)
            {
                if (isFirst)
                    isFirst = false;
                else
                    sJSON.Append(",\n");
                sJSON.Append("\""); sJSON.Append(Utils._JsonEscape((key))); sJSON.Append("\"");
            }
            sJSON.Append("]");
            return col.GetLength (0);
        }
        private int myCookieCollectionRender(StringBuilder sJSON, HttpCookieCollection col)
        {
            sJSON.Append("{\n");
            foreach (string skey in col)
            {
                HttpCookie key = col[skey];
                sJSON.Append("\"");
                sJSON.Append(Utils._JsonEscape(key.Name));
                sJSON.Append("\":\"");
                sJSON.Append(Utils._JsonEscape(key.Value));
                sJSON.Append("\",\n");
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(Utils._JsonEscape(col.Count.ToString()));
            sJSON.Append("\"\n");
            sJSON.Append("}");
            return col.Count;
        }
        private int myCookieCollectionArrayRender(StringBuilder sJSON, HttpCookieCollection col)
        {
            bool isFirst = true;
            sJSON.Append("[");
            foreach (string skey in col)
            {
                HttpCookie key = col[skey];
                if (isFirst)
                    isFirst = false;
                else
                    sJSON.Append(",\n");
                sJSON.Append("{\n");
                sJSON.Append("\"Domain\":\""); sJSON.Append(Utils._JsonEscape(key.Domain)); sJSON.Append("\",\n");
                sJSON.Append("\"Name\":\""); sJSON.Append(Utils._JsonEscape(key.Name)); sJSON.Append("\",\n");
                sJSON.Append("\"Path\":\""); sJSON.Append(Utils._JsonEscape(key.Path )); sJSON.Append("\",\n");
                sJSON.Append("\"Value\":\""); sJSON.Append(Utils._JsonEscape(key.Value)); sJSON.Append("\",\n");
                sJSON.Append("\"Expires\":\""); sJSON.Append(Utils._JsonEscape(key.Expires.ToString())); sJSON.Append("\"\n");
                sJSON.Append("}");
            }
            sJSON.Append("]");
            return col.Count;
        }
        //private int myFilesCollectionRender(StringBuilder sJSON, HttpFileCollection col)
        //{
        //    sJSON.Append("{\n");
        //    foreach (string key in col)
        //    {
        //        HttpPostedFile _file = col[key];
        //        sJSON.Append("\"");
        //        sJSON.Append(Utils._JsonEscape(_file.FileName));
        //        sJSON.Append("\":\"");
        //        sJSON.Append(Utils._JsonEscape(_file.ContentType));
        //        sJSON.Append("\",\n");
        //    }
        //    sJSON.Append("\"Count\":\"");
        //    sJSON.Append(Utils._JsonEscape(col.Count.ToString()));
        //    sJSON.Append("\"\n");
        //    sJSON.Append("}");
        //    return col.Count;
        //}
        private string mySaveUploadFile(HttpPostedFile objHttpPostedFile,IXManagerImportExport xm)
        {
            string m_FileID ="";
            try
            {

                string strFullFileName = objHttpPostedFile.FileName;
                int intLength = objHttpPostedFile.ContentLength;
                string m_FileName = System.IO.Path.GetFileName(strFullFileName); /* only the attched file name not its path */
                string m_FileExtension = System.IO.Path.GetExtension(strFullFileName).ToLower();
                byte[] m_FileContent = new byte[intLength];
                objHttpPostedFile.InputStream.Read(m_FileContent, 0, intLength);
                m_FileID = xm.WriteSFile ("", m_FileName, m_FileExtension, m_FileContent, objHttpPostedFile.ContentType);
            }
            catch(Exception ){};
            return m_FileID ;
        }
        private int myFilesCollectionArrayRender(StringBuilder sJSON, HttpFileCollection col)
        {
            sJSON.Append("[");
            if (col.Count > 0)
            {
                XDocManager xm = new XDocManager();
                bool isFirst = true;
                foreach (string  key in col)
                {
                    HttpPostedFile _file = col[key];
                    if (isFirst)
                        isFirst = false;
                    else
                        sJSON.Append(",\n");
                    sJSON.Append("{\n");
                    sJSON.Append("\"FileId\":\""); sJSON.Append(Utils._JsonEscape(mySaveUploadFile(_file,xm))); sJSON.Append("\",\n");
                    sJSON.Append("\"FileName\":\""); sJSON.Append(Utils._JsonEscape(_file.FileName)); sJSON.Append("\",\n");
                    sJSON.Append("\"ContentType\":\""); sJSON.Append(Utils._JsonEscape(_file.ContentType)); sJSON.Append("\",\n");
                    sJSON.Append("\"ContentLength\":\""); sJSON.Append(Utils._JsonEscape(_file.ContentLength.ToString())); sJSON.Append("\",\n");
                    sJSON.Append("}");
                }
                xm.Dispose();
            }
            sJSON.Append("]");
            return col.Count;
        }

        private void LoadConfig()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] != null) bUri = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Uri"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_AcceptTypes"] != null) bAcceptTypes = (string)System.Configuration.ConfigurationManager.AppSettings["Request_AcceptTypes"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Headers"] != null) bHeaders = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Headers"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Cookies"] != null) bCookies = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Cookies"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Files"] != null) bFiles = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Files"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_UserLanguages"] != null) bUserLanguages = (string)System.Configuration.ConfigurationManager.AppSettings["Request_UserLanguages"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Content"] != null) bContent = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Content"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_User"] != null) bUser = (string)System.Configuration.ConfigurationManager.AppSettings["Request_User"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Params"] != null) bParams = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Params"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] != null) bUri = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Uri"]=="1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Template"] != null) sRootTemplate = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Template"];
        }

        private string myRequestRender(HttpRequest req)
        {//URI = scheme "://" authority "/" path [ "?" query ] [ "#" fragment ]
            string sApplicationPath = req.ApplicationPath;
            string sPath = req.Path;
            string sRelativePath = "";
            if (sPath.Length > sApplicationPath.Length) sRelativePath = sPath.Substring(sApplicationPath.Length + 1, sPath.Length - sApplicationPath.Length - 1);
            if (sApplicationPath.StartsWith("/")) sApplicationPath = sApplicationPath.Substring(1, sApplicationPath.Length - 1);

            if (sRelativePath == "dummy")
                return "dummy";
            string postedData = myReadContent(req);

            StringBuilder sJSON  = new StringBuilder ();
            sJSON.Append("{\"request\":{\n");

            if (bParams)
            {
                sJSON.Append("\"Params\":"); myNameValueCollectionRender(sJSON, req.Params, req.ServerVariables); sJSON.Append(",\n");
                sJSON.Append("\"PathParams\":"); Utils.myPathParamsRender(sJSON, sRelativePath); sJSON.Append(",\n");
            }
            if (bAcceptTypes) { sJSON.Append("\"AcceptTypes\":"); myStringArrayRender(sJSON, req.AcceptTypes); sJSON.Append(",\n"); }
            if (bHeaders) { sJSON.Append("\"Headers\":"); myNameValueCollectionRender(sJSON, req.Headers,null); sJSON.Append(",\n"); }
            if (bCookies)
            {
                sJSON.Append("\"CookiesArray\":"); myCookieCollectionArrayRender(sJSON, req.Cookies); sJSON.Append(",\n");
                sJSON.Append("\"Cookies\":"); myCookieCollectionRender(sJSON, req.Cookies); sJSON.Append(",\n");
            }
            if (bFiles) { sJSON.Append("\"Files\":"); myFilesCollectionArrayRender(sJSON, req.Files); sJSON.Append(",\n"); }
            if(bUserLanguages) { sJSON.Append("\"UserLanguages\":"); myStringArrayRender(sJSON, req.UserLanguages); sJSON.Append(",\n"); }

            if (bUri)
            {
                sJSON.Append("\"UriScheme\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Scheme)); sJSON.Append("\",\n");
                sJSON.Append("\"UriHost\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Host)); sJSON.Append("\",\n");
                sJSON.Append("\"UriPort\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Port.ToString())); sJSON.Append("\",\n");
                sJSON.Append("\"UriServer\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Scheme + ":\\" + req.Url.Host + ":" + req.Url.Port.ToString())); sJSON.Append("\",\n");
                sJSON.Append("\"UriWebsite\":\""); sJSON.Append(Utils._JsonEscape(sApplicationPath)); sJSON.Append("\",\n");
                sJSON.Append("\"UriPath\":\""); sJSON.Append(Utils._JsonEscape(sRelativePath)); sJSON.Append("\",\n");
                sJSON.Append("\"UriQuery\":\""); sJSON.Append(Utils._JsonEscape(req.QueryString.ToString())); sJSON.Append("\",\n");
            }
            if (bContent)
            {
                sJSON.Append("\"ContentType\":\""); sJSON.Append(Utils._JsonEscape(req.ContentType)); sJSON.Append("\",\n");
                int iContentLength = req.ContentLength;
                sJSON.Append("\"ContentLength\":\""); sJSON.Append(Utils._JsonEscape(iContentLength.ToString())); sJSON.Append("\",\n");
                sJSON.Append("\"Content\":\""); sJSON.Append(Utils._JsonEscape(postedData)); sJSON.Append("\",\n");
            }
            if (bUser)
            {
                sJSON.Append("\"UserHostAddress\":\""); sJSON.Append(Utils._JsonEscape(req.UserHostAddress)); sJSON.Append("\",\n");
                sJSON.Append("\"UserHostName\":\""); sJSON.Append(Utils._JsonEscape(req.UserHostName)); sJSON.Append("\",\n");
                sJSON.Append("\"UserAgent\":\""); sJSON.Append(Utils._JsonEscape(req.UserAgent)); sJSON.Append("\",\n");
                sJSON.Append("\"UserName\":\""); sJSON.Append(Utils._JsonEscape(req.LogonUserIdentity.Name)); sJSON.Append("\",\n");
            }
            sJSON.Append("\"HttpMethod\":\""); sJSON.Append(Utils._JsonEscape(req.HttpMethod)); sJSON.Append("\",\n");
            sJSON.Append("\"HttpUrl\":\""); sJSON.Append(Utils._JsonEscape(req.Url.ToString())); sJSON.Append("\"\n");
            sJSON.Append("}}");

            return sJSON.ToString(); 
        }
        #endregion


        public void ProcessRequest(HttpContext context)
        {
            System.Diagnostics.Debug.WriteLine("start" + "" + ": " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            LoadConfig();
            m_context = context;
            context.Request.ValidateInput();
            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "input validated");
            string sRequest = myRequestRender(context.Request);
            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "request builded");

            if (sRequest == "dummy")
            {
                //CookieContainer cc = new CookieContainer();
                //HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("http://www.google.com");
                //req.CookieContainer = cc;
                //HttpWebResponse res = (HttpWebResponse)req.GetResponse();

                //context.Response.Write("response.cookies.count" + res.Cookies.Count.ToString());
                //context.Response.Write(" CookieContainer.count" + cc.Count.ToString());

                context.Response.Write("dummy");
                HttpCookie cookie = new HttpCookie("myname");
                cookie.Name = "myname";
                cookie.Value = "myvalue" + DateTime.Now.ToString ();
                cookie.Expires = DateTime.Now.AddMinutes (5);

                context.Response.Cookies.Add(cookie);
                System.Diagnostics.Debug.WriteLine("end:" + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
                return;
            }

            XDocCache m_xdocCache = null;
            string strTemplateName = sRootTemplate;
            string strContent = "";
            string strDocType = "";
            Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
            newHash.Add("Request", sRequest);
            strContent = this.GetContextTemplateContent(context, strTemplateName, newHash, ref strDocType, ref m_xdocCache);
            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "template processed");

            context.Response.Write(strContent);
            context.Response.ContentType = "text/html; charset=utf-8";// "text/xml; charset=utf-8";
            //context.Response.AddHeader("X-Powered-By", "Kubion");
            //context.Response.AddHeader("Content-Type", "text/json; charset=utf-8");// "text/xml; charset=utf-8";
            string sHeaders = m_xdocCache.GetSV("*", "Response_Headers", "");
            string[] aHeaders = MySplit(sHeaders, ';');
            foreach (string sHeader in aHeaders)
            {
                string[] aHeader = MySplit(sHeader + "=", '='); 
                context.Response.AddHeader(aHeader [0], aHeader [1]);
            }
            string sStatus = m_xdocCache.GetSV("Status", "Response_", "200 OK");
            context.Response.Status = sStatus;  
            System.Diagnostics.Debug.WriteLine("end:" + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
        }

        bool inBuildPage = false;
        DateTime dtStart = DateTime.Now;
        public void BuildPage(HttpContext context)
        {
            if (inBuildPage) return;
            dtStart = DateTime.Now;
            string strContent = "";
            string strDocType="";

            inBuildPage = true;
            try
            {
                string Parameters = context.Request.RawUrl; 
                Hashtable providedParameters = XDocManager.HashtableFromQueryString(Parameters);

                string c_portal = null;

                if (c_portal == null && providedParameters != null && providedParameters["PORTAL"] != null) c_portal = providedParameters["PORTAL"].ToString();
                if (c_portal == null && context != null && context.Session != null) c_portal = (string)context.Session["Portal"];
                if (c_portal == null) c_portal = ConfigurationManager.AppSettings["portal"];
                if (c_portal == null) c_portal = "Portal";
                if (context != null && context.Session != null) context.Session["Portal"] = c_portal;

                string c_context = string.Empty;
                if (providedParameters != null && providedParameters["CONTEXT"] != null)
                    c_context = providedParameters["CONTEXT"].ToString();
                else
                {
                    c_context = c_portal + "_Main";
                }

                string c_httpmethod = string.Empty;
                if ((context != null) && (context.Request != null) && (context.Request.HttpMethod != null)) c_httpmethod = context.Request.HttpMethod;

                string c_method = string.Empty;
                if (providedParameters != null && providedParameters["METHOD"] != null)
                    c_method = providedParameters["METHOD"].ToString();

                //if (ValidateRequest(c_portal, c_context, c_httpmethod, c_method, providedParameters))
                //{
                //    string strTemplateName = "";
                //    if (c_httpmethod.ToUpper ()=="POST") strTemplateName = c_context + "_Post";
                //    else strTemplateName = c_context + "_BI";


                //    strContent = this.GetTemplateContent(strTemplateName, providedParameters, ref strDocType);
                //}
			}
			catch (Exception e)
			{
				strContent = e.Message;
                //strBody = this.GetErrorXml(e.Message, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);
			}
            //System.Diagnostics.Debug.WriteLine("GTC:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            context.Response.ContentType = this.GetContentType(strDocType, "");

            //this.WriteLog(context, strTemplateName, htParams, strMethod, postedData, strContent, dtStart, strLogError);

            context.Response.Write(strContent);
            inBuildPage = false;
        }

        //private XDocManager getXDocManager()
        //{
        //    if (xdm == null)
        //        if (ConnectionString == "")
        //            xdm = new XDocManager(ConnectionString);
        //        else
        //            xdm = new XDocManager();
        //    xdm.myHttpContext = HttpContext.Current;
        //    return xdm;
        //}


        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        //public void ProcessRequestSingle(HttpContext context, Hashtable htParams)
        //{
        //    string strTemplateName = GetVal(htParams,"TemplateName") + "_" + context.Request.HttpMethod;

        //    //System.Diagnostics.Debug.WriteLine("BEG:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
        //    /* Needed for logging: */
        //    DateTime dtStart = DateTime.Now;
        //    string strLogError = "";

        //    string strBody = "";
        //    string strDocType = "";

        //    /**
        //     * 
        //     * Template structure:
        //     * GET      /{object}/                  call get{object}List()
        //     * GET      /{object}/{id}              call get{object}({id})
        //     * ?? GET      /{object1}/{id}/{object2}/  call get{object2}ListFor{object1}({id}) - information should be given in previous request already
        //     * POST     /{object}/                  call insert{object}(postData)
        //     * POST     /{object}/{id}              call update{object}({id}, postData)
        //     * DELETE   /{object}/{id}              call delete{object}({id})
        //     * 
        //     **/

        //    string strMethod = context.Request.HttpMethod;

        //    /* Get all parameters */
        //    //string strParameters = this.GetURLParameters(context.Request);

        //    /* Read posted data into parameters */
        //    StreamReader reader = new StreamReader(context.Request.InputStream, context.Request.ContentEncoding);
        //    string postedData = reader.ReadToEnd().Trim();
        //    //string postedData = "";
        //    DataTable objDataTable = null;
        //    try
        //    {
        //        objDataTable = this.DTPostedData(postedData);
        //        //objDataTable = this.DTPostedData(reader );
        //    }
        //    catch (Exception e)
        //    {
        //        strBody = this.GetErrorXml("Failed to create an XML document. Error: " + e.Message, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);
        //        strLogError = "Failed to create an XML document. Error: " + e.Message;
        //        this.WriteLog(context, strTemplateName, htParams, strMethod, postedData, strBody, dtStart, strLogError);
        //        context.Response.Write(strBody);
        //        return;
        //    }
        //    if (objDataTable!=null)
        //    {
        //        foreach (DataRow objDataRow in objDataTable.Rows)
        //        {
        //            for (int colNr = 0; colNr < objDataRow.ItemArray.Length; colNr++)
        //            {
        //                //strParameters += "&" + System.Web.HttpUtility.UrlEncode(objDataTable.Columns[colNr].ColumnName, context.Request.ContentEncoding) + "=" + System.Web.HttpUtility.UrlEncode(objDataRow.ItemArray[colNr].ToString(), context.Request.ContentEncoding);
        //                htParams.Remove(objDataTable.Columns[colNr].ColumnName);
        //                htParams.Add(objDataTable.Columns[colNr].ColumnName, objDataRow.ItemArray[colNr].ToString());
        //            }
        //        }
        //    }

        //    if (ConfigurationManager.AppSettings["PassRequest"] != null && ConfigurationManager.AppSettings["PassRequest"].ToString() == "1")
        //    {
        //        //strParameters += "&REQUESTMETHOD=" + System.Web.HttpUtility.UrlEncode(strMethod, context.Request.ContentEncoding);
        //        //strParameters += "&REQUESTURL=" + System.Web.HttpUtility.UrlEncode(context.Request.Url.AbsoluteUri, context.Request.ContentEncoding);

        //        htParams.Remove("REQUESTMETHOD");
        //        htParams.Add("REQUESTMETHOD", strMethod);
        //        htParams.Remove("REQUESTURL");
        //        htParams.Add("REQUESTURL", context.Request.Url.AbsoluteUri);

        //        string strXml = "";
        //        if (objDataTable != null)
        //        {
        //            XmlDocument doc = new XmlDocument();
        //            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
        //            {
        //                objDataTable.WriteXml(writer, XmlWriteMode.IgnoreSchema);
        //                writer.Close();
        //            }
        //            XmlNode objXmlNodeMethod = doc.DocumentElement.SelectSingleNode("method");
        //            if (objXmlNodeMethod != null)
        //                objXmlNodeMethod.ParentNode.RemoveChild(objXmlNodeMethod);
        //            strXml = doc.DocumentElement.InnerXml;
        //        }
        //        //strParameters += "&REQUESTXML=" + System.Web.HttpUtility.UrlEncode(strXml, context.Request.ContentEncoding);
        //        htParams.Remove("REQUESTXML");
        //        htParams.Add("REQUESTXML", strXml);
        //    }

        //    //strParameters += "&ENCODING=" + context.Response.ContentEncoding.WebName;
        //    htParams.Remove("ENCODING");
        //    htParams.Add("ENCODING", context.Response.ContentEncoding.WebName);

        //    //System.Diagnostics.Debug.WriteLine("BTC:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));

        //    try
        //    {
        //        if (GetVal(htParams, "uc").ToLower() == "true" || GetVal(htParams,"uc") == "1")
        //        {
        //            strBody = this.GetTemplateContentUC(strTemplateName, htParams, ref strDocType);
        //        }
        //        else
        //        {
        //            strBody = this.GetTemplateContent(strTemplateName, htParams, ref strDocType);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        strLogError = e.Message;
        //        strBody = this.GetErrorXml(e.Message, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);
        //    }
        //    //System.Diagnostics.Debug.WriteLine("GTC:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
        //    context.Response.ContentType = this.GetContentType(strDocType, GetVal(htParams, "view"));
        //    string strEncodingError = this.GetEncodingError(context.Response.ContentType, context.Response.ContentEncoding);
        //    if(strEncodingError!=null)
        //        strBody = this.GetErrorXml(strEncodingError, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);

        //    this.WriteLog(context, strTemplateName, htParams, strMethod, postedData, strBody, dtStart, strLogError);
        //    context.Response.Write(strBody);
        //    //System.Diagnostics.Debug.WriteLine("END:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
        //}

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        //public void ProcessRequestBatch(HttpContext context, Hashtable htParams)
        //{
        //    /* Needed for logging: */
        //    DateTime dtStart = DateTime.Now;
        //    string strLogError = "";

        //    string strBody = "";
        //    string strDocType = "";

        //    /**
        //     * 
        //     * Template structure:
        //     * GET      /{object}/                  call get{object}List()
        //     * GET      /{object}/{id}              call get{object}({id})
        //     * ?? GET      /{object1}/{id}/{object2}/  call get{object2}ListFor{object1}({id}) - information should be given in previous request already
        //     * POST     /{object}/                  call insert{object}(postData)
        //     * POST     /{object}/{id}              call update{object}({id}, postData)
        //     * DELETE   /{object}/{id}              call delete{object}({id})
        //     * 
        //     **/
        //    string strMethod = context.Request.HttpMethod;
        //    //string strParameters = this.GetURLParameters(context.Request);

        //    StreamReader reader = new StreamReader(context.Request.InputStream, context.Request.ContentEncoding);
        //    string postedData = reader.ReadToEnd().Trim();

        //    DataTable objDataTable = null;
        //    try
        //    {
        //        objDataTable = this.DTPostedData(postedData);
        //    }
        //    catch (Exception e)
        //    {
        //        strBody = this.GetErrorXml("Failed to create an XML document. Error: " + e.Message, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);
        //        strLogError = "Failed to create an XML document. Error: " + e.Message;

        //        this.WriteLog(context, GetVal(htParams,"TemplateName"), htParams, strMethod, postedData, strBody, dtStart, strLogError);
        //        context.Response.Write(strBody);
        //        return;
        //    }

        //    if (objDataTable != null)
        //    {
        //        foreach (DataRow objDataRow in objDataTable.Rows)
        //        {
        //            //string strCommandParameters = ""; //setting to strParameters gives problem for adding resourcenames
        //            //strCommandParameters = strParameters;
        //            Hashtable htCommandParameters = (Hashtable)htParams.Clone();


        //            for (int colNr = 0; colNr < objDataRow.ItemArray.Length; colNr++)
        //            {
        //                //if (("&" + strParameters + "=").IndexOf("&" + objDataTable.Columns[colNr].ColumnName + "=", StringComparison.InvariantCultureIgnoreCase) != -1)
        //                //    strParameters.Replace("&" + objDataTable.Columns[colNr].ColumnName + "=", "&_" + objDataTable.Columns[colNr].ColumnName + "=");

        //                //strCommandParameters += "&" + System.Web.HttpUtility.UrlEncode(objDataTable.Columns[colNr].ColumnName, context.Request.ContentEncoding) + "=" + System.Web.HttpUtility.UrlEncode(objDataRow.ItemArray[colNr].ToString(), context.Request.ContentEncoding);
        //                if (!objDataRow.IsNull(colNr))
        //                {
        //                    htCommandParameters.Remove(objDataTable.Columns[colNr].ColumnName);
        //                    htCommandParameters.Add(objDataTable.Columns[colNr].ColumnName, objDataRow.ItemArray[colNr].ToString());
        //                }

        //            }

        //            //string[] arrParameters = 

        //            //string strRequestMethod = this.GetParameterValue(strCommandParameters, "method");
        //            string strRequestMethod = GetVal(htCommandParameters,"method");
        //            string strTemplateName = GetVal(htParams,"TemplateName") + "_" + strRequestMethod;

        //            if (ConfigurationManager.AppSettings["PassRequest"] != null && ConfigurationManager.AppSettings["PassRequest"].ToString() == "1")
        //            {
        //                //strCommandParameters += "&REQUESTMETHOD=" + System.Web.HttpUtility.UrlEncode(strRequestMethod, context.Request.ContentEncoding);
        //                //strCommandParameters += "&REQUESTURL=" + System.Web.HttpUtility.UrlEncode(context.Request.Url.AbsoluteUri, context.Request.ContentEncoding);
        //                htCommandParameters.Remove("REQUESTMETHOD");
        //                htCommandParameters.Add("REQUESTMETHOD", strRequestMethod);
        //                htCommandParameters.Remove("REQUESTURL");
        //                htCommandParameters.Add("REQUESTURL", context.Request.Url.AbsoluteUri);

        //                string strXml = "";
        //                if (objDataTable != null)
        //                {
        //                    DataTable objCommandDataTable = objDataTable.Clone();
        //                    objCommandDataTable.ImportRow(objDataRow);
        //                    XmlDocument doc = new XmlDocument();
        //                    using (XmlWriter writer = doc.CreateNavigator().AppendChild())
        //                    {
        //                        objCommandDataTable.WriteXml(writer, XmlWriteMode.IgnoreSchema);
        //                        writer.Close();
        //                    }
        //                    XmlNode objXmlNodeMethod = doc.DocumentElement.FirstChild.SelectSingleNode("method");
        //                    if (objXmlNodeMethod != null)
        //                        objXmlNodeMethod.ParentNode.RemoveChild(objXmlNodeMethod);
        //                    strXml = doc.DocumentElement.FirstChild.InnerXml;
        //                }
        //                //strCommandParameters += "&REQUESTXML=" + System.Web.HttpUtility.UrlEncode(strXml, context.Request.ContentEncoding);
        //                htCommandParameters.Remove("REQUESTXML");
        //                htCommandParameters.Add("REQUESTXML", strXml);
        //            }

        //            //strCommandParameters += "&ENCODING=" + context.Response.ContentEncoding.WebName;
        //            htCommandParameters.Remove("ENCODING");
        //            htCommandParameters.Add("ENCODING", context.Response.ContentEncoding.WebName);

        //            try
        //            {
        //                if (GetVal(htCommandParameters, "uc").ToLower() == "true" || GetVal(htCommandParameters,"uc") == "1")
        //                {
        //                    strBody += this.GetTemplateContentUC(strTemplateName, htCommandParameters, ref strDocType);
        //                }
        //                else
        //                {
        //                    strBody += this.GetTemplateContent(strTemplateName, htCommandParameters, ref strDocType);
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                strLogError = e.Message;
        //                strBody += this.GetErrorXml(e.Message, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, false);
        //            }
        //        }
        //    }

        //    context.Response.ContentType = this.GetContentType(strDocType, GetVal(htParams,"view"));
        //    string strEncodingError = this.GetEncodingError(context.Response.ContentType, context.Response.ContentEncoding);
        //    if (strEncodingError != null)
        //        strBody = this.GetErrorXml(strEncodingError, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);


        //    this.WriteLog(context, GetVal(htParams,"TemplateName"), htParams, strMethod, postedData, strBody, dtStart, strLogError);
        //    context.Response.Write("<serviceresponseList>" + strBody + "</serviceresponseList>");
        //}

		private void WriteLog(HttpContext context, string strTemplateName, Hashtable htParameters, string strMethod, string postedData, string strBody, DateTime dtStart, string strLogError)
		{
            if (!((context.Request.QueryString["LogRequest"] != null) && (context.Request.QueryString["LogRequest"] == "1")))
            {

                // why CASE in sql and not an if in C#??
                if (ConfigurationManager.AppSettings["Log"] == null || ConfigurationManager.AppSettings["Log"] != "1" || ConfigurationManager.AppSettings["XDocConnectionString"] == null || ConfigurationManager.AppSettings["XDocConnectionString"] == "")
                    return;
            }
            if ((context.Request.QueryString["LogRequest"] != null) && (context.Request.QueryString["LogRequest"] == "0")) 
                return;
            if ((context.Request.QueryString["LogRequestXML"] != null) && (context.Request.QueryString["LogRequestXML"] == "0")) postedData = "[no log]";
            if ((context.Request.QueryString["LogResponseXML"] != null) && (context.Request.QueryString["LogResponseXML"] == "0")) strBody = "[no log]";

			try
			{
				/* LoginName, first try by parameter. If not set, try with integrated identity */
                string strLoginName = System.Web.HttpUtility.UrlDecode(this.GetVal(htParameters, "LoginName"), context.Request.ContentEncoding);
				if (strLoginName == null) strLoginName = "";

				string strUserName = context.Request.ServerVariables["REMOTE_USER"];
				if (strUserName == null) strUserName = "";

                //string LogParameters = "";
                //LogParameters += "ApplicationName=" + this.GetParameterValue(strParameters, "ApplicationName");
                //LogParameters += "&LoginName=" + System.Web.HttpUtility.UrlEncode(strLoginName, System.Text.Encoding.Default);
                //LogParameters += "&UserName=" + System.Web.HttpUtility.UrlEncode(strUserName, System.Text.Encoding.Default);
                //LogParameters += "&RequestURL=" + System.Web.HttpUtility.UrlEncode(context.Request.Url.AbsoluteUri, System.Text.Encoding.Default);
                //LogParameters += "&RequestMethod=" + System.Web.HttpUtility.UrlEncode(strMethod, System.Text.Encoding.Default);
                //LogParameters += "&ResourceTemplate=" + System.Web.HttpUtility.UrlEncode(strTemplateName, System.Text.Encoding.Default);

				KubionDataNamespace.ClientData objClientData = new KubionDataNamespace.ClientData(ConfigurationManager.AppSettings["XDocConnectionString"]);
				string strQueryText = @"INSERT INTO tblLog(RequestMethod, RequestURL, RequestXML, ResponseXML, ApplicationName, StartTime, EndTime, Duration, LoginName, UserName, UserAgent, UserHostAddress, UserHostName, ResourceTemplate, Error)
	VALUES
	(
	  CASE WHEN '" + L_String(strMethod) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strMethod) + @"',1,10) END
	  , CASE WHEN '" + L_String(context.Request.Url.AbsoluteUri) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.Url.AbsoluteUri) + @"',1,500) END
	  , CASE WHEN '" + L_String(postedData) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(postedData) + @"',1,8000) END
	  , CASE WHEN '" + L_String(strBody) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strBody) + @"',1,8000) END
	  , CASE WHEN '" + L_String(System.Web.HttpUtility.UrlDecode(this.GetVal(htParameters, "ApplicationName"), context.Request.ContentEncoding)) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(System.Web.HttpUtility.UrlDecode(this.GetVal(htParameters, "ApplicationName"), context.Request.ContentEncoding)) + @"',1,50) END
	  , CASE WHEN '" + dtStart.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' = '' THEN NULL ELSE '" + dtStart.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' END
	  , CASE WHEN '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' = '' THEN NULL ELSE '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' END
	  , " + ((int)DateTime.Now.Subtract(dtStart).TotalMilliseconds).ToString() + @"
	  , CASE WHEN '" + L_String(strLoginName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strLoginName) + @"',1,50) END
	  , CASE WHEN '" + L_String(strUserName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strUserName) + @"',1,50) END
	  , CASE WHEN '" + L_String(context.Request.UserAgent) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.UserAgent) + @"',1,500) END
	  , CASE WHEN '" + L_String(context.Request.UserHostAddress) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.UserHostAddress) + @"',1,50) END
	  , CASE WHEN '" + L_String(context.Request.UserHostName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.UserHostName) + @"',1,50) END
	  , CASE WHEN '" + L_String(strTemplateName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strTemplateName) + @"',1,100) END
	  , CASE WHEN '" + L_String(strLogError) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strLogError) + @"',1,8000) END
	)";

				/* Run the query */
				string logResult = objClientData.ExecuteNonQuery(strQueryText);
				objClientData.Dispose();
			}
			catch (Exception e) { }
		}

		private string L_String(string strString)
		{
			if (strString == null) return "";
			else return strString.Replace("'", "''");
		}

        //private string GetErrorXml(string strError, string strMethod, string strUrl, string strRequestXml, string strEncoding, bool blnXmlDeclaration)
        //{
        //    //must be an much easyer way to build that string
        //    try
        //    {
        //        StringWriter sw = new StringWriter();

        //        XmlDocument objXmlDocument = new XmlDocument();
        //        if(blnXmlDeclaration)
        //            objXmlDocument.AppendChild(objXmlDocument.CreateXmlDeclaration("1.0", strEncoding, null));

        //        XmlNode node_response = objXmlDocument.CreateElement("serviceresponse");
        //        objXmlDocument.AppendChild(node_response);

        //        XmlNode node_message = objXmlDocument.CreateElement("message");
        //        node_message.AppendChild(objXmlDocument.CreateTextNode("Method not implemented"));
        //        node_response.AppendChild(node_message);

        //        XmlNode node_error = objXmlDocument.CreateElement("error");
        //        node_error.AppendChild(objXmlDocument.CreateTextNode(strError));
        //        node_response.AppendChild(node_error);

        //        if (ConfigurationManager.AppSettings["PassRequest"] != null && ConfigurationManager.AppSettings["PassRequest"].ToString() == "1")
        //        {
        //            XmlNode node_method = objXmlDocument.CreateElement("method");
        //            node_method.AppendChild(objXmlDocument.CreateTextNode(strMethod));
        //            node_response.AppendChild(node_method);

        //            XmlNode node_url = objXmlDocument.CreateElement("url");
        //            node_url.AppendChild(objXmlDocument.CreateTextNode(strUrl));
        //            node_response.AppendChild(node_url);

        //            XmlNode node_xml = objXmlDocument.CreateElement("requestxml");
        //            //node_xml.AppendChild(objXmlDocument.CreateCDataSection(strRequestXml));
        //            node_xml.AppendChild(objXmlDocument.CreateTextNode(HttpUtility.HtmlEncode(strRequestXml)));
        //            node_response.AppendChild(node_xml);
        //        }

        //        XmlTextWriter xw = new XmlTextWriter(sw);
        //        xw.Formatting = Formatting.Indented;
        //        objXmlDocument.WriteTo(xw);
        //        return sw.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        StringWriter sw2 = new StringWriter();

        //        XmlDocument objXmlDocument = new XmlDocument();
        //        objXmlDocument.AppendChild(objXmlDocument.CreateXmlDeclaration("1.0", "utf-8", null));

        //        XmlNode node_response = objXmlDocument.CreateElement("serviceresponse");
        //        objXmlDocument.AppendChild(node_response);

        //        XmlNode node_error = objXmlDocument.CreateElement("error");
        //        node_error.AppendChild(objXmlDocument.CreateTextNode("Error in creating Error XML: " + HttpUtility.HtmlEncode(e.Message)));
        //        node_response.AppendChild(node_error);

        //        XmlTextWriter xw = new XmlTextWriter(sw2);
        //        xw.Formatting = Formatting.Indented;
        //        objXmlDocument.WriteTo(xw);
        //        return sw2.ToString();
        //    }
        //}
        private string[] MySplit(string sVal, char cSeparator)
        {
            //sVal = sVal.Replace("\\\\", "<_backslash_>");
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());
                //aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString()).Replace("<_backslash_>", "\\\\");
                //if (aData[i].EndsWith("\\\\")) 
                //    aData[i] = aData[i].Substring(0, aData[i].Length - 1);
            }
            return aData;
        }
        // ^[ ]*(id|name)[ ]*$|^[ ]*\([ ]*(id|name)[ ]*\)[ ]*([ ]*(OR|AND)[ ]*\([ ]*(id|name)[ ]*\))*[ ]*$|^[ ]*\([ ]*\([ ]*(id|name)[ ]*\)[ ]*([ ]*(OR|AND)[ ]*\([ ]*(id|name)[ ]*\))*[ ]*\)[ ]*$
        //  (name) 
        //(id) 
        // name  
        //( name  ) OR (id)
        // ( (name) AND ( id ) OR (name)  OR  (name)   ) 
        //(id)AND (name) 
    }
}