using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using KubionDataNamespace;
using System.Timers;
using System.Net;
using System.IO;

namespace KubionScheduler
{
    public partial class KubionScheduler : ServiceBase
    {
        private string m_sConnectionString;
        private double m_dInterval = 0;
        private int m_dRequestTimeout = 0;
        private Timer m_timer = new Timer();
        private bool m_bInProcess = false;

        public KubionScheduler()
        {
            InitializeComponent();
        }

        #region Confiruration 
        private string ConnectionString
        {
            get 
            {
                if(m_sConnectionString == null)
                    m_sConnectionString = ConfigurationManager.AppSettings.Get("ConnectionString");
                return m_sConnectionString;
            }
        }

        private double Interval
        {
            get
            {
                if (m_dInterval == 0)
                {
                    m_dInterval = 60000 * Double.Parse(ConfigurationManager.AppSettings.Get("Interval"));
                }
                return m_dInterval;
            }
        }

        private int RequestTimeout
        {
            get
            {
                if (m_dRequestTimeout == 0)
                {
                    m_dRequestTimeout = 1000 * Int32.Parse(ConfigurationManager.AppSettings.Get("Timeout"));
                }
                return m_dRequestTimeout;
            }
        }
        #endregion

        #region ServiceBase methods
        
        protected override void OnStart(string[] args)
        {
            try
            {
                m_timer.Start();
                m_timer.Elapsed += new ElapsedEventHandler(ProcessPendingTasks);
                m_timer.Interval = Interval;
                m_timer.Enabled = true;
            }
            catch (Exception ex)
            {
                LogEventLog("Problem starting KubionScheduler: " + ex.Message);
                this.Stop();
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (m_timer != null && m_timer.Enabled)
                {
                    m_timer.Enabled = false;
                    m_timer.Stop();
                }
            }
            catch (Exception ex)
            {
                LogEventLog("Problem stopping KubionScheduler: " + ex.Message);
            }
        }
        #endregion

        #region implementation

        private void ProcessPendingTasks(object sender, ElapsedEventArgs e)
        {
            if (!m_bInProcess)
            {               
                m_bInProcess = true;
                try
                {
                    DataTable dt = GetPendingTasks();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ProcessSingleTask(dr);
                        }
                    }
                    else
                    {
                        LogTaskExecution("-1", "NoTasks", "No tasks found");
                    }
                }
                catch (Exception ex)
                {
                    LogEventLog("Problem Processing pending tasks: " + ex.Message);
                }
                finally
                {
                    m_bInProcess = false;
                }
            }
        }

        private void ProcessSingleTask(DataRow dr)
        {
            string sTaskID = string.Empty;
            try
            {
                sTaskID = GetStringFromDataRow(dr, "SchedulerTaskID");
                if (sTaskID.Length == 0)
                    throw new Exception("TaskID should not be empty");

                string sEnvironment = GetStringFromDataRow(dr, "ExecutionEnvironment");
                if (sEnvironment.Length == 0)
                    throw new Exception("ExecutionEnvironment should not be empty");

                string sEnvironmentConn = ConfigurationManager.AppSettings.Get(sEnvironment + "URL");
                if(sEnvironmentConn.Length == 0)
                    throw new Exception("Environment URL for " + sEnvironment + " doesn't exist");

                string sExecutionURL = GetStringFromDataRow(dr, "ExecutionURL");
                if (sExecutionURL.Length == 0)
                    throw new Exception("ExecutionURL should not be empty");

                string sResponse = string.Empty;
                System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sEnvironmentConn + sExecutionURL);
                request.Credentials = CredentialCache.DefaultCredentials;
                request.Timeout = RequestTimeout;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    sResponse += reader.ReadToEnd();
                }

                if (sResponse.Contains(@"class=""error"""))
                    LogTaskExecution(sTaskID, "Error", sResponse);
                else
                    LogTaskExecution(sTaskID, "Success", sResponse);
                UpdateNextExecutionTime(sTaskID, dr);
            }
            catch (Exception ex)
            {
                LogTaskExecution(sTaskID, "Exception", ex.Message);
                UpdateNextExecutionTime(sTaskID, dr);
            }
        }
        private void UpdateNextExecutionTime(string sTaskID, DataRow dr)
        {
            string sNextExecution = string.Empty;
            string sIterative = GetStringFromDataRow(dr, "Iterative");
            if (sIterative == "1")
            {
                DateTime dtCurrTime = DateTime.Now;
                //string sExecutionTime = GetStringFromDataRow(dr, "NextExecutionTime");
                //if (sExecutionTime != null && sExecutionTime.Length > 0)
                //{
                //    try
                //    {
                //        dtCurrTime = DateTime.Parse(sExecutionTime);
                //    }
                //    catch (Exception ex)
                //    {
                //        LogEventLog(ex.Message);
                //    }
                //}
                dtCurrTime = GetDateTimeFromDataRow(dr, "NextExecutionTime");
                //if (dtCurrTime == null || dtCurrTime == DateTime.MinValue)
                //{
                //    dtCurrTime = DateTime.Now;
                //}
                double dInterval = double.Parse(GetStringFromDataRow(dr, "ExecutionInterval"));
                if (dInterval > 0)
                {
                    while (dtCurrTime < DateTime.Now)
                    {
                        dtCurrTime = dtCurrTime.AddMinutes(dInterval);
                    }
                }
                sNextExecution = dtCurrTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
            ClientData objClientData = new ClientData(ConnectionString);
            DataTable objDataTable = objClientData.DTExecuteQuery(@"DEFSchedulerTask/" + sTaskID + @"

/serviceresponse

PUT
<DEFSchedulerTask>    
    <NextExecutionTime>" + sNextExecution + @"</NextExecutionTime>    
</DEFSchedulerTask>");

        }

        private void LogTaskExecution(string sTaskID, string sResult, string sContent)
        {
            ClientData objClientData = new ClientData(ConnectionString);
            DataTable objDataTable = objClientData.DTExecuteQuery(@"DEFSchedulerLog

/serviceresponse

POST
<DEFSchedulerLog>
    <SchedulerTaskID>" + sTaskID + @"</SchedulerTaskID>
    <ExecutionTime>" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"</ExecutionTime>
    <Result>" + sResult + @"</Result>
    <Content><![CDATA[" + sContent + @"]]></Content>
</DEFSchedulerLog>");
        }

//        private void UpdateNextExecutionTime(string sTaskID, DataRow dr)
//        {
//            string sNextExecution = string.Empty;
//            string sIterative = GetStringFromDataRow(dr, "Iterative");
//            if(sIterative == "1")
//            {                
//               DateTime dtCurrTime = DateTime.Now;
//               double dInterval = double.Parse(GetStringFromDataRow(dr, "ExecutionInterval"));
//               if (dInterval > 0)
//                   dtCurrTime = dtCurrTime.AddMinutes(dInterval);
//                sNextExecution = dtCurrTime.ToString("yyyy-MM-dd HH:mm:ss");
//            }
//            ClientData objClientData = new ClientData(ConnectionString);
//            DataTable objDataTable = objClientData.DTExecuteQuery(@"DEFSchedulerTask/"+ sTaskID + @"
//
///serviceresponse
//
//PUT
//<DEFSchedulerTask>    
//    <NextExecutionTime>" + sNextExecution + @"</NextExecutionTime>    
//</DEFSchedulerTask>");

//        }

//        private void LogTaskExecution(string sTaskID, string sResult, string sContent)
//        {
//            ClientData objClientData = new ClientData(ConnectionString);
//            sContent = sContent.Replace("<![CDATA[", string.Empty);
//            sContent = sContent.Replace("]]>", string.Empty);
//            DataTable objDataTable = objClientData.DTExecuteQuery(@"DEFSchedulerLog
//
///serviceresponse
//
//POST
//<DEFSchedulerLog>
//    <SchedulerTaskID>" + sTaskID + @"</SchedulerTaskID>
//    <ExecutionTime>" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"</ExecutionTime>
//    <Result>" + sResult + @"</Result>
//    <Content><![CDATA[" + sContent + @"]]></Content>
//</DEFSchedulerLog>");
//        }

        private DataTable GetPendingTasks()
        {
            DataTable objDataTable = null;
            try
            {
                ClientData objClientData = new ClientData(ConnectionString);
                objDataTable = objClientData.DTExecuteQuery(@"DEFSchedulerTask?Active=1

/DEFSchedulerTaskList/DEFSchedulerTask

GET");
            }
            catch (Exception ex)
            {
                LogEventLog("Problem retrieving pending tasks: " + ex.Message);
            }
            return objDataTable;
        }

        private string GetStringFromDataRow(DataRow dr, string sColumnName)
        {
            string sRet = "";
            if (dr != null && dr[sColumnName] != null)
                return dr[sColumnName].ToString();
            return sRet;
        }

        private DateTime GetDateTimeFromDataRow(DataRow dr, string sColumnName)
        {
            string sRet = "";
            //LogTaskExecution("1", "Debug1", dr[sColumnName].ToString());
            sRet = dr[sColumnName].ToString();
            //LogTaskExecution("1", "Debug2", sRet);
            if (dr != null && dr[sColumnName] != null)
                return DateTime.Parse (sRet );
            return DateTime.Now;
        }

        private void LogEventLog(string sLog)
        {
            try
            {
                string sSource = "KubionScheduler";
                string sType = "Application";

                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sType);
                EventLog.WriteEntry(sSource, sLog, EventLogEntryType.Warning);
            }
            catch { }               
        }
        #endregion
    }
}
