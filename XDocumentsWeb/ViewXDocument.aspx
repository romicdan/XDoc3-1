<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewXDocument.aspx.cs" Inherits="ViewXDocument" ValidateRequest="false" %>
<%@ Register Assembly="XDocumentsWebControls" Namespace="XDocumentsWebControls" TagPrefix="cc1" %>

<%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml" >
--%>
<asp:literal runat="server" id="dt"></asp:literal>
<html xmlns="http://www.w3.org/1999/xhtml" style="width: 100%; height: 100%; overflow :hidden; margin: 0px; padding: 0px;">
<head runat="server">
    <title ><asp:literal runat="server" id="pagetitle"></asp:literal></title> 
</head>
<body style="margin: 0px; padding: 0px; height: 100%; overflow :hidden;" scroll="no" >
    <form id="FormXDoc" runat="server" style="width: 100%; height: 100%; overflow :auto" onsubmit="_killEvent(event); return false">
    <div style="width: 100%; height: 100%; overflow :auto">
        <cc1:ViewXDoc ID="ctrlViewXDoc" runat="server" />
    </div>
    </form>
</body>
</html>
