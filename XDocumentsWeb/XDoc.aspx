<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeFile="XDoc.aspx.cs" Inherits="XDoc" %>
<%@ Register Assembly="XDocumentsWebControls" Namespace="XDocumentsWebControls" TagPrefix="cc1" %>
 
<asp:literal runat="server" id="dt"></asp:literal>
<%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >--%>
<html xmlns="http://www.w3.org/1999/xhtml" style="width: 100%; height: 100%; overflow :hidden; margin: 0px; padding: 0px;">
<head runat="server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <asp:literal runat="server" id="pagehead"></asp:literal> 
    <title ><asp:literal runat="server" id="pagetitle"></asp:literal></title> 
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
</head>
<body style="margin: 0px; padding: 0px; height: 100%; overflow :hidden;" scroll="no" >
    <form id="FormXDoc" runat="server" style="width: 100%; height: 100%; overflow :auto" >
    <div style="width: 100%; height: 100%; overflow :auto">
        <asp:Label runat="server" ID="lbError" Visible="false" ForeColor="red"></asp:Label>
        <cc1:XDoc1 ID="ctrlXDoc" runat="server" style="width: 100%; height: 100%; display:block; " />
    </div>
    </form>
</body>
</html>
