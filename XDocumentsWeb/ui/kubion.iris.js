function mainInit()
{	
	try
	{
		$('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>');
		_hashRegister("R_Portal_2_Main");
		$('form').submit(false);
	}
	catch(err) { _error("Fout in mainInit: " + err.message); }
}
function _registerHtml(strPanelName, strContent)
{
	document.getElementById(strPanelName).innerHTML = strContent;
	var $div= angular.element(document.getElementById(strPanelName));
	var scope = angular.element('#main').scope();
	var $compile = angular.element('#main').injector().get('$compile');
	/*var alertScope = scope.$new(true);*/
	$div.replaceWith($compile($div)(scope));	
}

function _parseBlock(strResponse, fromDiv)
{
	function fillPanelFn(strPanelName, strContent)
	{
		try
		{
			if(strContent.indexOf("[ bl"+"ock", 0)!=-1) strContent = strContent.replace("[ bl"+"ock","[bl"+"ock");
			if(strPanelName=="")
			{
				if (fromDiv==1) strContent=_htmlDecode(strContent);
				_registerScript(strContent);
				_runInitialize();
			}
			else if(strPanelName=="script")
			{
				if (fromDiv==1) strContent=_htmlDecode(strContent);
				_registerText(strContent);
				_runInitialize();
			}
			else if(strPanelName=="style")
			{
				_registerStyleText(strContent);
			}
			else if(document.getElementById(strPanelName)!=null)
			{
				_registerHtml(strPanelName, strContent);
			}
		}
		catch(err) { _debug("Fout in _parseBlock -> fillPanelFn: " + err.message); }
	}

	try
	{
		strResponse = _trim(strResponse);
		if(strResponse.length === 0){}
		else if(strResponse.substring(0,7)=="[bl"+"ock_")
		{
			var stringIndex = 0;
			while (strResponse.indexOf("[bl"+"ock_", stringIndex)!=-1)
			{
				stringIndex = strResponse.indexOf("[bl"+"ock_", stringIndex) + 7;
				var strBlockName = strResponse.substring(stringIndex, strResponse.indexOf("]", stringIndex));
				stringIndex = strResponse.indexOf("]", stringIndex)
				var stingEndIndex = strResponse.indexOf("[bl"+"ock]", stringIndex);
				if(stingEndIndex==-1)
				{
					try { fillPanelFn(strBlockName, strResponse.substring(stringIndex+1)) } catch(fillErr) {}
					break;
				}
				else
				{
					try { fillPanelFn(strBlockName, strResponse.substring(stringIndex+1, stingEndIndex)) } catch(fillErr) {}
				}
			}
		}
		else
		{
			try { fillPanelFn("", strResponse) } catch(fillErr) {}
		}
	}
	catch(err) { _debug("Fout in _parseBlock: " + err.message); }
}
function _showPanelContent() {
    $('#PageContent').slideUp();
    $('#PanelContent').slideDown();
    $('#LoginContent').slideUp();    
}
function _showPageContent() {
    $('#PageContent').slideDown();
    $('#PanelContent').slideUp();
    $('#LoginContent').slideUp();    
}
function _showLoginContent() {
    $('#PageContent').slideUp();
    $('#PanelContent').slideUp();
    $('#LoginContent').slideDown();    
}

    $(window).bind("resize", function () {
        screenOrientation = ($(window).width() > $(window).height()) ? 90 : 0;

        if (screenOrientation = 90 && $(window).width() > 641)
            $('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');
    });

    function _sendJPostback(url,strEvent, data, cbfn) {
        function _sendEventJPostbackHandler(strResponse) {
            try {
                if ((_checkErrors(strResponse) & 5) == 0) _parseBlock(strResponse);
                if (cbfn !== undefined && cbfn != null) try { cbfn(strId); } catch (err) { }
            }
            catch (err) { _debug("Fout in _sendEventJPostbackHandler: " + err.message); }
        }
        //var strEvent = "LoginPost";
        var strContent = "";        

        for (var propt in data) {            
            strContent += "<input type=\"text\" param=\"" + propt + "\" value=\"" + data[propt] + "\">";
        }
        var strParams = "";
        
        strParams += "&INITIALSESSIONID=" + _strInitialSessionId;
        strParams += "&UUID=" + _strUUID;
        strParams += "&BLOCK=1";
        strParams += "&METHOD=" + _urlEncode(strEvent);
        _makePostback(url + strParams, _sendEventJPostbackHandler, strContent, "", "", "");
    }

    function toggleIdentifInformation() {
        if ($('#IdentifInformation_ddb_ul').hasClass('open')) {
            $('#IdentifInformation_ddb_ul').removeClass('open');
            $('#IdentifInformation_ddb').removeClass('open');
        }
        else {
            $('#IdentifInformation_ddb_ul').addClass('open');
            $('#IdentifInformation_ddb').addClass('open');
        }
    }
    function refreshSessionData() {
        var scope = angular.element('#main').scope();
        scope.loadData();
    }