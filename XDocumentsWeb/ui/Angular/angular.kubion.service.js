app
  .service("TableSorter", function () {
      return {
          sortObjectsArray: function (objectsArray, field) {

              return objectsArray.sort(function (a, b) {

                  function isNumeric(obj) {
                      return !jQuery.isArray(obj) && obj - parseFloat(obj) >= 0;
                  }

                  if (isNumeric(a[field]) && isNumeric(b[field])) {
                      x = parseInt(a[field]);
                      y = parseInt(b[field]);
                  } else {
                      x = a[field];
                      y = b[field];
                  }

                  return x < y ? -1 : x > y ? 1 : 0;
              });
          }
      }
  })
  .service('DataLoader', function ($q, DataCache) {
      return {
          loadData: function (componentName, componentID, cacheKey) {
              var deferred = $q.defer();
              var data = null;
              if (cacheKey && cacheKey.length > 0)
                  data = DataCache.get(cacheKey);
              if (data)
                  deferred.resolve(data);
              else {
                  var url = "?Context=" + componentName + "&ID=" + componentID + "&Method=LoadData&DATA1=&DATA2=&DATA3=&noblocks=1";
                  _makeCallback(url, function (strResponse) {
                      try {
                          var data = JSON.parse(strResponse);
                          if (cacheKey && cacheKey.length)
                              DataCache.put(cacheKey, data);
                          deferred.resolve(data);
                      }
                      catch (ex) {
                          deferred.reject(ex);
                      }
                  });
              }
              return deferred.promise;
          },
          submitData: function (componentName, componentID, submitMethod, data) {
              var deferred = $q.defer();
              var url = "?Context=" + componentName + "&ID=" + componentID + "&noblocks=1";
              _sendJPostback(url, submitMethod, data, function (strResponse) {
                  /*  
                  var data = JSON.parse(strResponse);
                  if (data.error && data.error.length > 0)
                      deferred.reject({message : data.error, description : data.message });
                  else
                  */                  
                  deferred.resolve("success");
              });
              return deferred.promise;
          }
      }
  })
  .service('Formatter', function () {
      var Time = {};
      var DateTime = {};

      Time.AM_to_PM = function (time) {
          var hours = Number(time.match(/^(\d+)/)[1]);
          var minutes = Number(time.match(/:(\d+)/)[1]);
          var seconds = Number(time.match(/(!=:)\d+\s+/));
          var AMPM = time.match(/\s(.*)$/)[1];
          if (AMPM == "pm" && hours < 12) hours = hours + 12;
          if (AMPM == "am" && hours == 12) hours = hours - 12;
          var sHours = hours.toString();
          var sMinutes = minutes.toString();
          var sSeconds = seconds.toString();
          if (hours < 10) sHours = "0" + sHours;
          if (minutes < 10) sMinutes = "0" + sMinutes;
          if (seconds < 10) sSeconds = "0" + sSeconds;
          return (sHours + ':' + sMinutes + ':' + sSeconds);
      }

      DateTime.AM_to_PM = function (dateTimeString) {
          var dateTimeString = dateTimeString;
          var timeString = dateTimeString.split(" ")[1] + " " + dateTimeString.split(" ")[2];
          var PMString = Time.AM_to_PM(timeString.toLowerCase());
          return dateTimeString.split(" ")[0] + " " + PMString.toLowerCase();
      }

      var image = function (value) {
          return "<img src='" + value + "' />";
      }

      var date = function (data, type, full, meta) {
          var formattedDateTimeString = "";
          try {
              var dateTimeString = DateTime.AM_to_PM(data);
              var formattedDateTimeString = moment(dateTimeString).format("DD-MM-YYYY");
          } catch (ex) { }
          return (data === undefined || data === null || data.length <= 0) ? "" : formattedDateTimeString;
      }

      var datetime = function (data, type, full, meta) {
          var formattedDateTimeString = "";
          try {
              var dateTimeString = DateTime.AM_to_PM(data);
              var formattedDateTimeString = moment(dateTimeString).format("DD-MM-YYYY HH:mm");
          } catch (ex) { }
          return (data === undefined || data === null || data.length <= 0) ? "" : formattedDateTimeString;
      }

      var checkmark = function (value) {
          return value == "1" ? '\u2713' : '\u2718';
      }

      var datetext = function (value) {
          var dateTimeString = DateTime.AM_to_PM(value);
          var date = moment(dateTimeString);
          var today = moment();
          if (today.format("YYYYMMDD") == date.format("YYYYMMDD"))
              return date.format("HH:mm");
          else if (today.add('days', +1).format("YYYYMMDD") == date.format("YYYYMMDD"))
              return "Morgen"
          else if ((today.add('days', +7).format("YYYYMMDD") >= date.format("YYYYMMDD")) && (today.add('days', +1).format("YYYYMMDD") < date.format("YYYYMMDD")))
              return date.format("dddd");
          else if (today.add('days', -1).format("YYYYMMDD") == date.format("YYYYMMDD"))
              return "Gisteren"
          else if (today.add('days', -7).format("YYYYMMDD") <= date.format("YYYYMMDD"))
              return date.format("dddd");
          else
              return date.format("DD-MM-YY");
      }

      return {
          date: date,
          image: image,
          datetime: datetime,
          checkmark: checkmark,
          datetext: datetext
      }

  });