app.filter('formatter', function (Formatter) {

    return function (input, type) {
        return (type) ? Formatter[type](input) : input;
    };
});