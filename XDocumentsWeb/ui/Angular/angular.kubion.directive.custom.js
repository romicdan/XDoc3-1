app

  .directive('acNGForm', function ($compile) {
      return {
          restrict: 'AE',
          replace: true,
          transclude: true,
          template: "<div ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.id = attrs.id;
              };
          },
          link: function (scope, elem, attrs) {
              scope.compile();
              scope.init(attrs);
          }
      }
  })
  .directive('acCalcdistance', function () {
    return {
        restrict: "A",
        controller: function ($scope, $element) {
            $scope.init = function (attrs) {
                var distance = _appIris.calculateDistance(attrs.lat, attrs.long);
                $scope.distance = distance;
            };
        },
        link: function (scope, element, attrs) {
            scope.init(attrs);
        }
    }
});