app
  .factory("Pagination", function ($q) {
      var Pagination = function (lObject, itemsPerPage, pageNr) {
          this.Object = lObject;
          this.itemsPerPage = itemsPerPage;
          this.currentPageNumber = pageNr;
          this.currentPageIndex = pageNr - 1;
          this.totalPages = 0;
          this.pages = [];
          this.currentPageFirstItem = 0;
          this.currentPageLastItem = 0;
      }
      Pagination.prototype.hasNext = function () {
          return (this.currentPageNumber < this.totalPages && this.currentPageNumber >= 0);
      }
      Pagination.prototype.hasPrevious = function () {
          return (this.currentPageNumber <= 1) ? false : true;
      }
      Pagination.prototype.toPageNumber = function (pageNumber) {
          if (pageNumber <= 0) {
              throw Error("pageNumber should be greater than 0");
          } else if (pageNumber > this.totalPages) {
              throw Error("pageNumber can't be greater than totalPages");
          }

          this.currentPageNumber = pageNumber;
          this.currentPageIndex = pageNumber - 1;
          this.currentPageFirstItem = (this.currentPageIndex * this.itemsPerPage) + 1;
          this.currentPageLastItem = (this.currentPageNumber * this.itemsPerPage);
          if (this.currentPageLastItem > this.Object.totalsize) this.currentPageLastItem = this.Object.totalsize;

          var deferred = $q.defer();
          var self = this;
          if (self.Object.loadalldata) {
              self.Object.list = self.getCurrentPageItems();
              deferred.resolve();
          }
          else {
              _sendEvent(this.Object.componentName, this.Object.componentID, "PageNr", pageNumber, "", "", function (strId) {
                  deferred.resolve(self.Object.refreshList(true));
              });
          }
          return deferred.promise;
      }
      Pagination.prototype.doRefresh = function () {
          this.totalPages = Math.ceil(this.Object.totalsize / this.itemsPerPage);
          this.currentPageFirstItem = (this.currentPageIndex * this.itemsPerPage) + 1;
          this.currentPageLastItem = (this.currentPageNumber * this.itemsPerPage);
          if (this.currentPageLastItem > this.Object.totalsize) this.currentPageLastItem = this.Object.totalsize;
          this.pages = [];
          var numberOfPages = this.totalPages;
          for (var i = 0; i < numberOfPages; i++) {
              var rangeItem = {};
              rangeItem.value = i + 1;
              this.pages.push(rangeItem);
          }
      }
      Pagination.prototype.getCurrentPageItems = function () {
          if (this.Object.loadalldata)
              return this.Object.data.List.slice(this.currentPageIndex * this.itemsPerPage, this.itemsPerPage + this.currentPageIndex * this.itemsPerPage);
          else
              return this.Object.data.List;

      }
      return Pagination;
  })
  .factory("ListObject", function (DataLoader, Pagination, TableSorter, $q) {
      var ListObject = function (componentName, componentID, loadalldata, orderdir, ordercol, pagesize, pagenr) {
          this.itemIndex = 0;
          this.loadalldata = loadalldata;
          this.orderdir = orderdir;
          this.ordercol = ordercol;
          this.componentName = componentName;
          this.componentID = componentID;
          this.list = [];
          this.data = {};
          this.totalsize = 0;
          this.Pagination = new Pagination(this, pagesize, pagenr);
      }
      ListObject.prototype.refreshList = function (useCache) {
          var deferred = $q.defer();
          var self = this;
          var cacheKey = null;
          if (useCache)
              cacheKey = this.orderdir + '_' + this.ordercol + '_' + this.Pagination.currentPageNumber;

          DataLoader.loadData(this.componentName, this.componentID, cacheKey)
          .then(
          function (data) {
              self.totalsize = data.RealCount;
              self.data = data;
              if (self.loadalldata) {
                  self.sortList();
              }
              self.Pagination.doRefresh();
              self.list = self.Pagination.getCurrentPageItems();
              deferred.resolve('success');
          }, function (ex) {
              self.list = [];
              deferred.reject(ex);
          });
          return deferred.promise;
      }
      ListObject.prototype.sortList = function () {
          if (this.orderdir == "DESC")
              this.list = TableSorter.sortObjectsArray(this.list, this.ordercol).reverse();
          else
              this.list = TableSorter.sortObjectsArray(this.list, this.ordercol);
      }
      ListObject.prototype.changeOrder = function (ordercol) {
          var orderdir = "ASC";
          if (ordercol == this.ordercol && this.orderdir == "ASC") orderdir = "DESC";

          this.ordercol = ordercol;
          this.orderdir = orderdir;
          var self = this;
          var deferred = $q.defer();

          if (this.loadalldata) {
              this.sortList();
              deferred.resolve(self.Pagination.getCurrentPageItems());
          }
          else {
              _sendEvent(this.componentName, this.componentID, "SortData", ordercol, orderdir, "", function () {
                  deferred.resolve(self.refreshList());
              });
          }
          return deferred.promise;
      }
      return ListObject;
  })
  .factory("ItemObject", function (DataLoader, $q) {
      var ItemObject = function (componentName, componentID, containerName, containerID, submitMethod) {
          this.componentName = componentName;
          this.componentID = componentID;
          this.containerName = containerName;
          this.containerID = containerID;
          this.submitMethod = submitMethod;
          this.item = {};
      }
      ItemObject.prototype.refreshItem = function (useCache) {
          var deferred = $q.defer();
          var self = this;
          var cacheKey = null;
          if (useCache)
              cacheKey = this.componentName + '_' + componentID;

          DataLoader.loadData(this.componentName, this.componentID, cacheKey)
          .then(
          function (data) {
              self.item = data;
              deferred.resolve(data);
          }, function (ex) {
              console.log(ex.message);
              deferred.reject(ex);
          });
          return deferred.promise;
      }
      ItemObject.prototype.submitItem = function () {
          return DataLoader.submitData(this.containerName, this.containerID, this.submitMethod, this.item);
      }
      return ItemObject;
  })
  .factory('DataCache', function ($cacheFactory) {
      return $cacheFactory('DataCache', { capacity: 15 });
  })
  .factory("GoogleMap", function ($q) {
      var GoogleMap = function (componentName, componentID, options) {
          this.componentName = componentName;
          this.componentID = componentID;
          this.mapOptions = options;

          this.geocoder = new google.maps.Geocoder();
          var mapOptions = {
              /*center:[25, 90],*/
              zoom: 15
          };
          this.map = new google.maps.Map(document.getElementById("map_canvas_" + this.componentID), mapOptions);
      }

      GoogleMap.prototype.showAddress = function (address) {
          var deferred = $q.defer();
          if (address && address.length > 0) {
              try {
                  var me = this;
                  me.geocoder.geocode({ 'address': address }, function (results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                          var centerLocation = results[0].geometry.location;

                          me.map.setCenter(centerLocation);
                          var marker = new google.maps.Marker({
                              position: centerLocation,
                              map: me.map,
                              title: address
                          });
                          deferred.resolve('success');
                      }
                      else
                          console.log("Adres: [" + address + "] not found");
                  });
              }
              catch (ex) {
                  console.log(ex.message);
                  deferred.reject(ex.message);
              }
          }
          else
              deferred.reject('error');
          return deferred.promise;
      }
      return GoogleMap;
  });

  