using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

public partial class MakeXDoc : System.Web.UI.Page
{
    #region Events

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
    protected override void Render(HtmlTextWriter writer)
    {
        string toWrite = "";
        StringBuilder sb = new StringBuilder();
        StringWriter tw = new StringWriter(sb);
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        base.Render(hw);
        hw.Close();
        tw.Close();
        toWrite = sb.ToString();
        if (!toWrite.Contains("ctrlXDoc$hiddenUnique"))
        {
            int i = toWrite.IndexOf("<!DOCTYPE");
            if (i < 1) toWrite = "";
            else toWrite = toWrite.Substring(0, i - 1);
        }
        Page.Controls.Clear();
        Page.Controls.Add(new LiteralControl(toWrite));
        base.Render(writer);
    }
   
    //protected void oldPage_Load(object sender, EventArgs e)
    //{
    //    //this.dt.Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
    //    //this.dt.Text = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
    //    string strDocType = ConfigurationManager.AppSettings["DOCTYPE"];
    //    if (strDocType == null)
    //    {
    //        string strDocType1 = ConfigurationManager.AppSettings["DOCTYPE1"];
    //        string strDocType2 = ConfigurationManager.AppSettings["DOCTYPE2"];
    //        if (strDocType1 == null) strDocType1 = "-//W3C//DTD HTML 4.01 Transitional//EN";
    //        if (strDocType2 == null) strDocType2 = "";
    //        else strDocType2 = " \"" + strDocType2 + "\"";
    //        //            strDocType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
    //        strDocType = "<!DOCTYPE HTML PUBLIC \"" + strDocType1 + "\"" + strDocType2 + ">";
    //    }
    //    this.dt.Text = strDocType;
    //    string strPageTitle = ConfigurationManager.AppSettings["PageTitle"];
    //    if (strPageTitle == null) strPageTitle = "IRIS";
    //    this.pagetitle.Text = strPageTitle;

    //    ctrlXDoc.AnswerEvent += new XDocumentsWebControls.XDoc.XDocCtrlEventHandler(ctrlXDoc_AnswerEvent);
    //    string parameters = GetQueryString();
    //    ctrlXDoc.Parameters = parameters;

    //    //bool processControls = false;

    //    //if (Request.QueryString["uc"] != null)
    //    //{
    //    //    string uc = Request.QueryString["uc"];
    //    //    if (uc.ToLower() == "true" || uc == "1")
    //    //        processControls = true;
    //    //}

    //    ////MIMI
    //    //processControls = false;

    //    //if (!IsPostBack || processControls)
    //    //    if (Request.QueryString["postbackGuid"] == null)
    //    //        ctrlXDoc.BuildPage();

    //    if (!IsPostBack)
    //        if (Request.QueryString["postbackGuid"] == null)
    //        {
    //            DateTime dt1 = DateTime.Now;
    //            ctrlXDoc.BuildPage();
    //            DateTime dt2 = DateTime.Now;
    //            //System.Diagnostics.Debug.WriteLine(dt2.Subtract(dt1));  
    //        }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        //this.dt.Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
        //this.dt.Text = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
        string strDocType = ConfigurationManager.AppSettings["DOCTYPE"];
        if (strDocType == null)
        {
            string strDocType1 = ConfigurationManager.AppSettings["DOCTYPE1"];
            string strDocType2 = ConfigurationManager.AppSettings["DOCTYPE2"];
            if (strDocType1 == null) strDocType1 = "-//W3C//DTD HTML 4.01 Transitional//EN";
            if (strDocType2 == null) strDocType2 = "";
            else strDocType2 = " \"" + strDocType2 + "\"";
            //            strDocType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
            strDocType = "<!DOCTYPE HTML PUBLIC \"" + strDocType1 + "\"" + strDocType2 + ">";
        }
        this.dt.Text = strDocType;
        string strPageTitle = ConfigurationManager.AppSettings["PageTitle"];
        if (strPageTitle == null) strPageTitle = "IRIS";
        this.pagetitle.Text = strPageTitle;

        ctrlXDoc.PageLoad("");

    }
    #endregion Events

}
