/*************************
* FILE VERSION: 3.9.9.4  *
*************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ViewXDocument : System.Web.UI.Page
{
    #region Protected methods

    string GetQueryString()
    {
        string ret = "";
        foreach (string key in Request.QueryString.AllKeys)
        {
            ret += key + "=" + HttpUtility.UrlEncode(Request.QueryString[key]) + "&";
        }
        if (ret.EndsWith("&"))
            ret = ret.Remove(ret.Length - 1);
        return ret;
    }

    #endregion Protected methods

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        //this.dt.Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
        //this.dt.Text = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
        string strDocType = ConfigurationManager.AppSettings["DOCTYPE"];
        if (strDocType == null)
        {
            string strDocType1 = ConfigurationManager.AppSettings["DOCTYPE1"];
            string strDocType2 = ConfigurationManager.AppSettings["DOCTYPE2"];
            if (strDocType1 == null) strDocType1 = "-//W3C//DTD HTML 4.01 Transitional//EN";
            if (strDocType2 == null) strDocType2 = "";
            else strDocType2 = " \"" + strDocType2 + "\"";
            //            strDocType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
            strDocType = "<!DOCTYPE HTML PUBLIC \"" + strDocType1 + "\"" + strDocType2 + ">";
        }
        this.dt.Text = strDocType;
        string strPageTitle = ConfigurationManager.AppSettings["PageTitle"];
        if (strPageTitle == null) strPageTitle = "IRIS";
        this.pagetitle.Text = strPageTitle;
        
        //if (!IsPostBack)
        {
            string parameters = GetQueryString();
            ctrlViewXDoc.Parameters = parameters;
            ctrlViewXDoc.BuildPage();
        }
    }

    #endregion Event handlers
}
