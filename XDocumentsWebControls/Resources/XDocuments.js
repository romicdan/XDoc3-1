﻿/* FILE VERSION: 3.9.9.4 */
/*
[assembly: WebResource("AJAXWrite.js.AJAXWrite.js", "application/javascript", PerformSubstitution = true)] 
function launchApp(strCmdLine)
function getBrowser() 
function appendClassName(obj, classname)
function removeClassName(obj, classname)
function _trim(s)
function _executeCallback(strFn, strId, strParam1, strParam2, strParam3 )
function _killEvent(evt)
function _killEnter(evt)
function _fwd_key(strId, event)
function _addListener(obj, evType, fn)
function _removeListener(id)
function _addEvent(obj, evType, fn)
function _detachEvent(obj, evType, fn)
function _getAtt(strId, strAttribute) 
function _setAtt(strId, strAttribute, strValue) 

function _doBlockEvent(block)
function _sendingEvent(block) 
function _sendEvent(strContext, strId, strEvent, strData1, strData2, strData3, cbfn)
function _sendEventUC(strContext, strId, strEvent, strData1, strData2, strData3, cbfn)
function _sendEventExt(strContext, strId, strEvent, strData1, strData2, strData3, cbfn)
function _sendPost(strContext, strId, strContent, cbfn)

function _sendJPostback(url, strEvent, data, cbfn)
function _sendJSONPostback(url, strEvent, data, cbfn)
function _sendJSONEventExt(strContext, strId, strEvent, strData1, strData2, strData3, cbfn)

function _htmlEncode (str)
function _htmlDecode(str)
function _urlEncode (str)
function _urlDecode (str)
function _uriEncode(str)
function _uriDecode(str)
function _encode(str)
function _decode(str)
function _JsonEscape(str)

function _save(uniqueID, withValidation, btnSaveDocument_ClientID)
function _saveNoUpdates(uniqueID, withValidation, btnSaveDocumentNoUpdates_ClientID) 
function _generateGuid()
function _makePostback(url, callbackFn, content, parameters, answer, targetUniqueID)
function _makeJPostback(url, callbackFn, content) 
function _makeCallback(url, callbackFn)
function _makeCallbackExt(url,sLongParameters,callbackFn)

function _addInitialize(fn)
function _runInitialize()

function _hashRegister(bi)
function _hashGet()
function _hashSet(strHash)
// if strHash is missing the functions will get and set the hash of the browser and we will get an bi event
function _hashMerge(strKeyValues, strHash)
function _hashDelKey(strKey, strHash)
function _hashSetKey(strKey, strValue, strHash)
function _hashGetKey(strKey, strHash)

function _addError(intCode, strRegex, fnShowError)
function _removeError(intCode)
function _getError(str, blnStripHtml)

function _createFocusGroup(strId, listIds, fnFocus, fnBlur)
function _linkFocusGroup(strIdParent, strIdChild)
--sample--
_createFocusGroup('c_paging_#PAR.ID#',new Array('c_paging_#PAR.ID#_component','c_paging_#par.ID#_input'),f1,b1);

_createFocusGroup('c_list_#PAR.ID#',new Array('c_list_#PAR.ID#_component','c_list_#PAR.ID#_input1'),f,b);
_linkFocusGroup('c_list_#PAR.ID#','c_paging_c_list_#PAR.ID#_pg');

function f(id){	document.title+="F"+id+" " ;}
function b(id){ document.title+="B"+id+" " ;}
-----------
function _shiftPressed(evt)
function _ctrlPressed(evt)
function _altPressed(evt)
function _getChildElement(obj, intChildNr)
function _getNextElement(obj)
function _getPrevElement(obj)
function _getFirstElement(obj)
function _getLastElement(obj)
function _getY(oElement, blnMentionScroll)
function _getX(oElement, blnMentionScroll)
function _getBGPositionX(obj)
function _getBGPositionY(obj)

function _getCaretPos(objTxt)
function _setCaretPosFocus(objTxt, intPos)
function _getCaretWord(objTxt, strSep, blnFullWord)
function _setCaretWord(objTxt, strSep, strWord)

function _scrollPanel(panel, intPercentage)
function _scrollPanelToObject(panel, object)
function _moveDiv(objDivToMove, objDivToStick, strPosition, blnAttachToBody)
function _moveDivEx(objDivToMove, objDivToStick, strPosition)
function _getNextHighestZindex(obj)

function _isDate(strValue)
function _isDateTime(strValue)
function _isYear(strValue)
function _isNumeric(strValue)
function _isEmail(strValue)
function _isZipcode(strValue)
function _isDecimal(strValue, intDecimals)
function _isLengthBetween(strValue, minLength, maxLength)
function _isAccountNumber(strValue) 
function _isPhoneInt(strValue)
function _translateDate(strValue, strYearPrefix)
function _translateDecimal(strValue, intDecimals)
function _translatePhoneInt(strValue)
function hex_md5(s)

*/

/* application */
function launchApp(strCmdLine)
{
	try
	{
		var obj = new ActiveXObject("LaunchinIE.Launch");
		obj.LaunchApplication(strCmdLine);
	}
	catch(err)
	{
		try
		{
			var obj = new ActiveXObject("KubionLauncher.Launch");
			obj.run(strCmdLine);
		}
		catch(err1){ _debug(err.message + " | " + err1.message); }
	}
}
function getBrowser() 
{
	var agt=navigator.userAgent.toLowerCase();
	if(agt.indexOf("opera") != -1) return 'Opera';
	if(agt.indexOf("firefox") != -1) return 'Firefox';
	if(agt.indexOf("safari") != -1) return 'Safari';
	if(agt.indexOf('\/')!=-1) 
	{
		if (agt.substr(0,agt.indexOf('\/')) != 'mozilla') 
		{
			return navigator.userAgent.substr(0,agt.indexOf('\/'));
		}
		else
		{
			return 'IE';
		}
	}
	else if(agt.indexOf(' ') != -1)
	{
		return navigator.userAgent.substr(0,agt.indexOf(' '));
	}
	else
	{
		return navigator.userAgent;
	}
}
function appendClassName(obj, classname)
{
	if (obj != null) obj.className += ' ' + classname + ' ';
}
function removeClassName(obj, classname)
{
	if (obj != null) obj.className = obj.className.replace(' ' + classname + ' ',' ');
}
function _trim(s)
{
	/* 0 sec: */
	if(s.substring(0,1)!=" " && s.substring(0,1)!="\t" && s.substring(0,1)!="\r" && s.substring(0,1)!="\n" && s.substring(s.length-1,s.length)!=" " && s.substring(s.length-1,s.length)!="\t" && s.substring(s.length-1,s.length)!="\r" && s.substring(s.length-1,s.length)!="\n") return s;

	for(var intFirst=0; intFirst<s.length; intFirst++)
	{
		if(s.substring(intFirst, intFirst+1)!=" " && s.substring(intFirst, intFirst+1)!="\t" && s.substring(intFirst, intFirst+1)!="\r" && s.substring(intFirst, intFirst+1)!="\n")
		{
			for(var intLast=s.length-1; intLast>=intFirst; intLast--)
			{
				if(s.substring(intLast, intLast+1)!=" " && s.substring(intLast, intLast+1)!="\t" && s.substring(intLast, intLast+1)!="\r" && s.substring(intLast, intLast+1)!="\n")
				{
					return s.substring(intFirst, intLast+1);
				}
			}
		}
	}

	return s.substring(s.length, s.length);
}
function _executeCallback(strFn, strId, strParam1, strParam2, strParam3 )
{
	iRet=-1;
	try
	{
		if(strFn != "" && strFn != null)
		{
			if(strParam3 != undefined)
				iRet = window[strFn](strId, strParam1, strParam2, strParam3);
			else if(strParam2 != undefined)
				iRet = window[strFn](strId, strParam1, strParam2);
			else if(strParam1 != undefined)
				iRet = window[strFn](strId, strParam1);
			else
				iRet = window[strFn](strId);
		}
	}
	catch(errFn) { _debug("Error in _executeCallback " + strFn + ":" + errFn.message); }
	return iRet;
}
/* application */

/* KILL */
function _killEvent(evt)
{
	var e = (window.event) ? window.event : evt;
	e.cancelBubble = true; 
	if (e.stopPropagation) e.stopPropagation();
	if (e.preventDefault) e.preventDefault();
	e.returnValue = false;
}

function _killEnter(evt)
{
	var e = (window.event) ? window.event : evt;
	if(e.keyCode == 13) 
	{
		_killEvent(e);
		return false;
	} 
	else
		return true;
}
/* KILL */

/* EVENTS */
var Handler = (function(){
	var listeners = {};

	return {
		addListener: function(element, event, handler, capture) {
		var id = element.id+'_'+event;	
			if(id in listeners) Handler.removeListener(id);
		if (element.addEventListener) element.addEventListener(event, handler, capture);
			else if (element.attachEvent) element.attachEvent("on"+event, handler);
			listeners[id] = {element: element, event: event, handler: handler, capture: capture};
		return id;
		},
		removeListener: function(id) {
			if(id in listeners) {
				var h = listeners[id];
		if (h.element.removeEventListener) h.element.removeEventListener(h.event, h.handler, h.capture);
				else if (h.element.detachEvent) h.element.detachEvent("on"+h.event, h.handler);
		delete listeners[id];
			}
		}
	};
}());

function _fwd_key(strId, event)
{
	var event = (window.event) ? window.event : event;
	var key = (event.which) ? event.which : event.keyCode;	
	var fireOnThis = document.getElementById(strId);

	if( window.KeyEvent ) 
	{
		var evObj = document.createEvent('KeyEvents');
		evObj.initKeyEvent( 'keydown', true, true, window, false, false, false, false, key, 0 );
		fireOnThis.dispatchEvent(evObj);
	} 
	else if(event.keyCode)
	{
		var evObj = document.createEventObject();
		evObj.keyCode=key;
		fireOnThis.fireEvent('onkeydown',evObj);
	}
	else
	{
		var evObj = document.createEvent('UIEvents');
		evObj.initUIEvent( 'keydown', true, true, window, 1 );
		evObj.keyCode = key;
		fireOnThis.dispatchEvent(evObj);
	}
}

function _addListener(obj, evType, fn)
{
	return Handler.addListener(obj,evType,fn,false);
}
function _removeListener(id)
{
	Handler.removeListener(id);
}

function _addEvent(obj, evType, fn)
{
    if (!obj) return false;
	if (obj.addEventListener)
	{
		obj.addEventListener(evType, fn, false);
		return true;
	}
	else if (obj.attachEvent)
	{
		var r = obj.attachEvent("on"+evType, fn);
		return r;
	}
	return false;
}

function _detachEvent(obj, evType, fn)
{
    if (!obj) return false;
	if (obj.removeEventListener)
	{
		obj.removeEventListener(evType, fn, false);
		return true;
	}
	else if (obj.detachEvent)
	{
		var r = obj.detachEvent("on"+evType, fn);
		return r;
	}
	return false;
}

function _addEvent2(obj, evType, fn)
{
    if (!obj) return false;
	if (obj.addEventListener)
	{
		obj.addEventListener(evType, fn, true);
		return true;
	}
	else if (obj.attachEvent)
	{
		var r = obj.attachEvent("on"+evType, fn);
		return r;
	}
	return false;
}
function _detachEvent2(obj, evType, fn)
{
    if (!obj) return false;
	if (obj.removeEventListener)
	{
		obj.removeEventListener(evType, fn, true);
		return true;
	}
	else if (obj.detachEvent)
	{
		var r = obj.detachEvent("on"+evType, fn);
		return r;
	}
	return false;
}
/* EVENTS */

/* ATTRIBUTE */
/* return: string. null when not found */
function _getAtt(strId, strAttribute) 
{
	try { return document.getElementById(strId).getAttribute(strAttribute); }
	catch(err1)
	{
		try { return document.getElementById(strId).getAttribute(strAttribute.toLowerCase()); }
		catch(err) { return null; }
	}
}

/* return: bool */
function _setAtt(strId, strAttribute, strValue) 
{ 
	try { document.getElementById(strId).setAttribute(strAttribute, strValue); return true; }
	catch(err1) 
	{ 
		try { document.getElementById(strId).setAttribute(strAttribute.toLowerCase(), strValue); return true; }
		catch(err) { return false; }
	}
}
/* ATTRIBUTE */


/* SEND EVENT */
var _blockEvent = false;
var _seed = 'SEED';
function _doBlockEvent(block)
{
	_blockEvent = block;
	_sendingEvent(block);
}
function _sendingEvent(block) {}
function _sendEvent(strContext, strId, strEvent, strData1, strData2, strData3, cbfn)
{
	if(_blockEvent===true) return;
	_doBlockEvent(true);

	function _sendEventHandler(strResponse)
	{
		try
		{
			if( (_checkErrors(strResponse)&5)==0) _parseBlock(strResponse);
			if(cbfn !== undefined && cbfn != null) try { cbfn(strId); } catch(err) {}
		}
		catch(err) { _debug("Fout in _sendEventHandler: " + err.message); }
		_doBlockEvent(false);
	}

	var strParams = "";
	strParams += "&ID=" + _urlEncode(strId);
	strParams += "&METHOD=" + _urlEncode(strEvent);
	strParams += "&DATA1=" + _urlEncode(strData1);
	strParams += "&DATA2=" + _urlEncode(strData2);
	strParams += "&DATA3=" + _urlEncode(strData3);
	strParams += "&INITIALSESSIONID=" + _strInitialSessionId;
	strParams += "&UUID=" + _strUUID;
	strParams += "&BLOCK=1";
	_makeCallback(location.pathname + "?Context=" + strContext + strParams, _sendEventHandler);
}

function _sendEventUC(strContext, strId, strEvent, strData1, strData2, strData3, cbfn)
{
	if(_blockEvent===true) return;
	_doBlockEvent(true);

	function _sendEventUCHandler(strResponse)
	{
		try
		{
			if( (_checkErrors(strResponse)&5)==0) _parseBlock(strResponse);
			if(cbfn !== undefined && cbfn != null) try { cbfn(strId); } catch(err) {}
		}
		catch(err) { _debug("Fout in _sendEventUCHandler: " + err.message); }
		_doBlockEvent(false);
	}

	var strParams = "";
	strParams += "&ID=" + _urlEncode(strId);
	strParams += "&METHOD=" + _urlEncode(strEvent);
	strParams += "&DATA1=" + _urlEncode(strData1);
	strParams += "&DATA2=" + _urlEncode(strData2);
	strParams += "&DATA3=" + _urlEncode(strData3);
	strParams += "&UC=true";
	strParams += "&INITIALSESSIONID=" + _strInitialSessionId;
	strParams += "&UUID=" + _strUUID;
	strParams += "&BLOCK=1";
	_makeCallback(location.pathname + "?Context=" + strContext + strParams, _sendEventUCHandler);
}

function _sendEventExt(strContext, strId, strEvent, strData1, strData2, strData3, cbfn)
{
	if(_blockEvent===true) return;
	_doBlockEvent(true);
	/* to be called by C_Form_Script on OK/Save */
	function _sendEventExtHandler(strResponse)
	{
		try
		{
			if( (_checkErrors(strResponse)&5)==0) _parseBlock(strResponse);
			if(cbfn !== undefined && cbfn != null) try { cbfn(strId); } catch(err) {}
		}
		catch(err) { _debug("Fout in _sendEventExtHandler: " + err.message); }
		_doBlockEvent(false);
	}

	var strContent = "";
	strContent += "<input type=\"text\" param=\"EVENT\" value=\"" + _urlEncode(strEvent) + "\">";
	strContent += "<input type=\"text\" param=\"DATA1\" value=\"" + _urlEncode(strData1) + "\">";
	strContent += "<input type=\"text\" param=\"DATA2\" value=\"" + _urlEncode(strData2) + "\">";
	strContent += "<input type=\"text\" param=\"DATA3\" value=\"" + _urlEncode(strData3) + "\">";

	var strParams = "";
	strParams += "&ID=" + _urlEncode(strId);
	strParams += "&INITIALSESSIONID=" + _strInitialSessionId;
	strParams += "&UUID=" + _strUUID;
	strParams += "&BLOCK=1";
	strParams += "&METHOD=" + _urlEncode(strEvent);
	_makePostback(location.pathname + "?CONTEXT=" + strContext + strParams, _sendEventExtHandler, strContent, "", "", "");
}

function _sendPost(strContext, strId, strContent, cbfn)
{
	if(_blockEvent===true) return;
	_doBlockEvent(true);

	function _sendPostHandler(strResponse)
	{
		try
		{
			if( (_checkErrors(strResponse)&5)==0) _parseBlock(strResponse);
			if(cbfn !== undefined && cbfn != null) try { cbfn(strId); } catch(err) {}
		}
		catch(err) { _debug("Fout in _sendPostHandler: " + err.message); }
		_doBlockEvent(false);
	}

	strContent=strContent.replace(/\+/g,"%252b");

	var strParams = "";
	strParams += "&ID=" + _urlEncode(strId);
	strParams += "&INITIALSESSIONID=" + _strInitialSessionId;
	strParams += "&UUID=" + _strUUID;
	strParams += "&BLOCK=1";
	_makePostback(location.pathname + "?Context=" + strContext + strParams, _sendPostHandler, strContent, "", "", "");
}

function _sendJPostback(url, strEvent, data, cbfn)
{
    if (_blockEvent === true) return;
    _doBlockEvent(true);

    function _sendEventJPostbackHandler(strResponse) {
        try {
            if ((_checkErrors(strResponse) & 5) == 0) _parseBlock(strResponse);
            if (cbfn !== undefined && cbfn != null) try { cbfn(strId); } catch (err) { }
        }
        catch (err) { _debug("Fout in _sendEventJPostbackHandler: " + err.message); }
        _doBlockEvent(false);
    }

    var strContent = "";
    for (var propt in data) {
        strContent += "<input type=\"text\" param=\"" + propt + "\" value=\"" + data[propt] + "\">";
    }

    var strParams = "";
    strParams += "&INITIALSESSIONID=" + _strInitialSessionId;
    strParams += "&UUID=" + _strUUID;
    strParams += "&BLOCK=1";
    strParams += "&METHOD=" + _urlEncode(strEvent);
    _makePostback(url + strParams, _sendEventJPostbackHandler, strContent, "", "", "");
}

function _sendJSONPostback(url, strEvent, data, cbfn)
{
    if (_blockEvent === true) return;
    _doBlockEvent(true);

    function _sendEventJPostbackHandler(strResponse) {
        try {
            if ((_checkErrors(strResponse) & 5) == 0) _parseBlock(strResponse);
            if (cbfn !== undefined && cbfn != null) try { cbfn(strResponse); } catch (err) { }
        }
        catch (err) { _debug("Fout in _sendEventJPostbackHandler: " + err.message); }
        _doBlockEvent(false);
    }

    if (typeof data !== 'string') { data = JSON.stringify(data); }

    var strParams = "";
    strParams += "&INITIALSESSIONID=" + _strInitialSessionId;
    strParams += "&UUID=" + _strUUID;
    strParams += "&BLOCK=1";
    strParams += "&METHOD=" + _urlEncode(strEvent);
    _makeJPostback(url + strParams, _sendEventJPostbackHandler, data);
}

function _sendJSONEventExt(strContext, strId, strEvent, strData1, strData2, strData3, cbfn)
{
    if (_blockEvent === true) return;
    _doBlockEvent(true);

    function _sendJSONEventExtHandler(strResponse) {
        try {
            if ((_checkErrors(strResponse) & 5) == 0) _parseBlock(strResponse);
            if (cbfn !== undefined && cbfn != null) try { cbfn(strId); } catch (err) { }
        }
        catch (err) { _debug("Fout in _sendJSONEventExtHandler: " + err.message); }
        _doBlockEvent(false);
    }

    var strContent = "{";
    strContent += "\"EVENT\": \"" + _JsonEscape(strEvent) + "\", ";
    strContent += "\"DATA1\": \"" + _JsonEscape(strData1) + "\", ";
    strContent += "\"DATA2\": \"" + _JsonEscape(strData2) + "\", ";
    strContent += "\"DATA3\": \"" + _JsonEscape(strData3) + "\" }";

    var strParams = "";
    strParams += "&ID=" + _urlEncode(strId);
    strParams += "&INITIALSESSIONID=" + _strInitialSessionId;
    strParams += "&UUID=" + _strUUID;
    strParams += "&BLOCK=1";
    strParams += "&METHOD=" + _urlEncode(strEvent);
    _makeJPostback(location.pathname + "?CONTEXT=" + strContext + strParams, _sendJSONEventExtHandler, strContent);
}

/* SEND EVENT */

/* ENCODE */
function _htmlEncode (str)
{
	if(typeof(str) == "number") str=String(str);
	var div = document.createElement('div_encode');
	div.appendChild(document.createTextNode(str.replace(/\r\n|\r|\n/g, "[[br]]")));
	return div.innerHTML.replace(/"/g, "&quot;").replace(/\[\[br\]\]/g, "&#13;&#10;");
}

function _htmlDecode(str)
{
	try
	{
	var div = document.createElement("div");
	if (typeof (div.innerText) != "undefined")
	{
		/* Internet Explorer, Chrome, Safari, Opera: replace quot, spaces, newlines, encoded breaks and single spaces */
		div.innerHTML = str.replace(/&quot;/g, "\"").replace(/ /g, "&nbsp;").replace(/\r\n|\r|\n/g,"<br>").replace(/&#13;&#10;|&#13;|&#10;/g,"<br>");
		return div.innerText.replace(new RegExp(String.fromCharCode(160),"g"),String.fromCharCode(32));
	}
	else
	{
		/* Firefox: replace quot, spaces, breaks, encoded breaks and single spaces */
		div.innerHTML = str.replace(/&quot;/g, "\"").replace(/ /g, "&nbsp;").replace(/\<br[\s|\/]*>/g,"\r\n").replace(/&#13;&#10;|&#13;|&#10;/g,"\r\n");
		return div.textContent.replace(new RegExp(String.fromCharCode(160),"g"),String.fromCharCode(32));
	}
	}
	catch(err) {}
} 
/*
function old_htmlDecode(str)
{
	var div = document.createElement("div");
	if (typeof (div.innerText) != "undefined")
	{
		div.innerHTML = str.replace(/&quot;/g, "\"").replace(/ /g, "&nbsp;").replace(/\r\n|\r|\n/g,"<br>").replace(/&#13;&#10;|&#13;|&#10;/g,"<br>");
		return div.innerText.replace(new RegExp(String.fromCharCode(160),"g"),String.fromCharCode(32)); 
	}
	else
	{
		div.innerHTML = str.replace(/&quot;/g, "\"").replace(/ /g, "&nbsp;").replace(/\<br[\s|\/]*>/g,"\r\n").replace(/&#13;&#10;|&#13;|&#10;/g,"\r\n");
		return div.textContent.replace(new RegExp(String.fromCharCode(160),"g"),String.fromCharCode(32));
	}
}
*/
function _urlEncode(str)
{
	return encodeURIComponent(str).replace(/\(/g, "%28").replace(/\)/g, "%29").replace(/\~/g, "%7E").replace(/\!/g, "%21").replace(/\'/g, "%27");
}


function _urlDecode(str)
{
	return decodeURIComponent(str);
}


function _urlEncode1 (str)
{
	str = escape(str);
	str = str.replace(/\+/g, "%2B");
	str = str.replace(/\@/g, "%40");
	str = str.replace(/\//g, "%2F");
	return str;
}

function _urlDecode1 (str)
{
	return unescape(str);
}

function _uriEncode(str)
{
return encodeURIComponent(str);
}

function _uriDecode(str)
{
return decodeURIComponent(str.replace(/\+/g,  " "));
}

function _encode(str)
{
	return _htmlEncode(_urlEncode(str));
}

function _decode(str)
{
	return _urlDecode(_htmlDecode(str));
}

function _JsonEscape(str)
{
    return str.replace(/\\n/g, "\\n")
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f");
}
/* ENCODE */

/* SAVE */
function _save(uniqueID, withValidation, btnSaveDocument_ClientID)
{
	var retVal;
	try
	{
		if (withValidation != 0) retVal = eval (uniqueID + "validate()");
	}
	catch (e){}
	if (retVal != 0 && retVal != false)
		document.getElementById(btnSaveDocument_ClientID).click(); 
}

function _saveNoUpdates(uniqueID, withValidation, btnSaveDocumentNoUpdates_ClientID) 
{
	var retVal;
	try
	{
		if (withValidation != 0) retVal = eval (uniqueID + "validate()");
	}
	catch (e){}
	if (retVal != 0 && retVal != false)
		document.getElementById(btnSaveDocumentNoUpdates_ClientID).click(); 
}
/* SAVE */

/* AJAX */
var _arrReqObj = new Array();
function _getReqObj()
{
	for (var i=0; i<_arrReqObj.length; i++) if(_arrReqObj[i].readyState==4) return _arrReqObj[i];
	if( typeof (XMLHttpRequest) != "undefined" ) { _arrReqObj.push( new XMLHttpRequest() ); return _arrReqObj[_arrReqObj.length - 1]; }
	try { _arrReqObj.push(new ActiveXObject("Msxml2.XMLHTTP")); return _arrReqObj[_arrReqObj.length - 1]; }
	catch(e)
	{
		try { _arrReqObj.push(new ActiveXObject("Microsoft.XMLHTTP")); return _arrReqObj[_arrReqObj.length - 1];}
		catch(oc) {}
	}
	return null;
}
function _processResponse(req, callbackFn)
{
	try
	{
		if (req != null && req.readyState == 4 && callbackFn)
		{
			/* only if "OK" */
			if (req.status == 200)
			{
				try { callbackFn(req.responseText); }
				catch(e){}
			}
			else
			{
				try { callbackFn("error: " + req.status.toString()); }
				catch(e){}
			}
		}
	}
	catch(e) {}
}
/* AJAX */

/* CALLBACK */
function _mydelegate( thatMethod )
{
	if(arguments.length > 1)
	{
	  var _params = [];
	  for(var n = 1; n < arguments.length; ++n) _params.push(arguments[n]);
	  return function() { return thatMethod.apply(null,_params); }
	}
	else
	  return function() { return thatMethod.call(null); }
}
function _generateGuid()
{
	var result, i, j;
	result = '';
	for(j=0; j<32; j++)
	{
		if( j == 8 || j == 12|| j == 16|| j == 20)
		result = result + '-';
		i = Math.floor(Math.random()*16).toString(16).toUpperCase();
		result = result + i;
	}
	return result;
}
function _makePostback(url, callbackFn, content, parameters, answer, targetUniqueID)
{
	var reqObj = _getReqObj();
	if(reqObj!=null)
	{
		reqObj.open("POST", url + ( (url.indexOf("?") == -1) ? "?" : "&" ) + "postbackGuid=" + _generateGuid() + "&pSeed=" + _seed, true);
		reqObj.onreadystatechange = _mydelegate( _processResponse, reqObj, callbackFn );
		reqObj.send( _getPostXML(content, parameters, answer, targetUniqueID) );
	}
}
function _makeJPostback(url, callbackFn, content)
{
    var reqObj = _getReqObj();
    if (reqObj != null) {
        reqObj.open("POST", url + ((url.indexOf("?") == -1) ? "?" : "&") + "postbackGuid=" + _generateGuid() + "&pSeed=" + _seed, true);
        reqObj.onreadystatechange = _mydelegate(_processResponse, reqObj, callbackFn);
        reqObj.setRequestHeader("Content-type", "application/json; charset=utf-8");
        reqObj.send(content);
    }
}
function _makeCallback(url, callbackFn)
{
	var reqObj = _getReqObj();
	if(reqObj!=null)
	{
		reqObj.open("GET", url + ( (url.indexOf("?") == -1) ? "?" : "&" ) + "callbackGuid=" + _generateGuid()+ "&pSeed=" + _seed, true);
		reqObj.onreadystatechange = _mydelegate( _processResponse, reqObj, callbackFn );
		reqObj.send(null);
	}
}
function _makeCallbackExt(url,sLongParameters,callbackFn)
{
  var sDiv = "";
  var aLongParameters = sLongParameters.split("&");
  for(i=0; i < aLongParameters.length; i++)
	if(aLongParameters[i]!="")
	{
	  var aLP = aLongParameters[i].split("=");
	  if (aLP.length>1)
		sDiv += "<input type='hidden' param=" + aLP[0] + " value=\"" +  aLP[1] + "\" />";   
	}
  _makePostback(url, callbackFn, sDiv, "", "", "%UNIQUE%");
}
function _makeCallbackExt_NewNotUsed(url,sLongParameters,callbackFn, br)
{
//problem: value will be decoded by xdoc again, causing all '+' characters to become spaces
//br is not needed anymore because urlencode correctly encodes this too
 if(br===undefined) br=false;
 var sDiv = "";
 var aLongParameters = sLongParameters.split("&");
 for(i=0; i < aLongParameters.length; i++)
 {
  if(aLongParameters[i]!="")
  {
   var aLP = aLongParameters[i].split("=");
   if (aLP.length>1)
	if(br===true) sDiv += "<input type='hidden' param=" + aLP[0] + " value=\"" +  _htmlEncode(_urlDecode(aLP[1]).replace(/\r/g,"").replace(/\n/g,"[br]")) + "\" />";   
	else sDiv += "<input type='hidden' param=" + aLP[0] + " value=\"" +  _htmlEncode(_urlDecode(aLP[1])) + "\" />";   
  }
 }
 _makePostback(url, callbackFn, sDiv, "", "", "%UNIQUE%");
}
function _getPostXML(content, parameters, answer, uniqueID)
{
	var xmlDoc = _createXML("xDocPostData", "");
   
	var paramsNode = xmlDoc.createElement("parameters");
	paramsNode.appendChild(xmlDoc.createTextNode(parameters));
	xmlDoc.childNodes[0].appendChild(paramsNode);
   
	var answerNode = xmlDoc.createElement("answer");
	answerNode.appendChild(xmlDoc.createTextNode(answer));
	xmlDoc.childNodes[0].appendChild(answerNode);
   
	var contentNode = xmlDoc.createElement("content");
	contentNode.appendChild(xmlDoc.createTextNode(content));
	xmlDoc.childNodes[0].appendChild(contentNode);
   
	var uniqueIDNode = xmlDoc.createElement("uniqueID");
	uniqueIDNode.appendChild(xmlDoc.createTextNode(uniqueID));
	xmlDoc.childNodes[0].appendChild(uniqueIDNode);

	if (xmlDoc.xml)
		return xmlDoc.xml;
   
	var xmlString = (new XMLSerializer()).serializeToString(xmlDoc.documentElement);
	return xmlString;
}
function _createXML(rootTagName, namespaceURL) 
{
	if (!rootTagName) rootTagName = "";
	if (!namespaceURL) namespaceURL = "";
	if (document.implementation && document.implementation.createDocument) 
	{
		// This is the W3C standard way to do it
		return document.implementation.createDocument(namespaceURL, rootTagName, null);
	}
	else 
	{   
		// This is the IE way to do it
		// Create an empty document as an ActiveX object
		// If there is no root element, this is all we have to do
		var doc = new ActiveXObject("MSXML2.DOMDocument");
		// If there is a root tag, initialize the document
		if (rootTagName) 
		{
		  // Look for a namespace prefix
		  var prefix = "";
		  var tagname = rootTagName;
		  var p = rootTagName.indexOf(':');
		  if (p != -1) 
		  {
			prefix = rootTagName.substring(0, p);
			tagname = rootTagName.substring(p+1);
		  }
		  // If we have a namespace, we must have a namespace prefix
		  // If we don't have a namespace, we discard any prefix
		  if (namespaceURL) 
		  {
			if (!prefix) prefix = "a0"; // What Firefox uses
		  }
		  else prefix = "";
		  // Create the root element (with optional namespace) as a
		  // string of text
		  var text = "<" + (prefix?(prefix+":"):"") +  tagname +
			  (namespaceURL
			   ?(" xmlns:" + prefix + '="' + namespaceURL +'"')
			   :"") +
			  "/>";
		  // And parse that text into the empty document
		  doc.loadXML(text);
		  }
	}
	return doc;
}
/* CALLBACK */


/* PARSE BLOCK */
function _generateUUID(){
	var d = new Date().getTime();
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = (d + Math.random()*16)%16 | 0;
		d = Math.floor(d/16);
		return (c=='x' ? r : (r&0x7|0x8)).toString(16);
	});
	return uuid;
};
var _strInitialSessionId = "";
var _strUUID = "";


function _registerBlock(strPanelName, strContent) {
    _registerHTML(strPanelName, strContent);
}
function _registerHTML(strPanelName, strContent) {
    if (document.getElementById(strPanelName) != null)
        document.getElementById(strPanelName).innerHTML = strContent;
}

/* Angular parse

function _registerBlock(strPanelName, strContent) {
    if (strPanelName.substr(0, 5) == "data_") {
        _registerNgData(strPanelName, strContent);
    }
    else
        _registerNgCode(strPanelName, strContent);
}

function _registerNGCode(strPanelName, strContent) {
    if (document.getElementById(strPanelName) != null) {
        document.getElementById(strPanelName).innerHTML = strContent;
        var $div = angular.element(document.getElementById(strPanelName));
        var scope = angular.element('#main').scope();
        var $compile = angular.element('#main').injector().get('$compile');
        $div.replaceWith($compile($div)(scope));
    }
}

function _registerNGData(strPanelName, strContent) {
    if ((angular != null) && (angular.element('%' + strPanelName) != null) && (angular.element(angular.element('%' + strPanelName).children()[0]) != null)) {
        var scope = angular.element(angular.element('%' + strPanelName).children()[0]).scope();
        if (scope != null) scope.pushData(strContent);
    }
}
*/

function _parseBlock(strResponse, fromDiv)
{
	function fillPanelFn(strPanelName, strContent)
	{
		try
		{
			/*alert("Panel: " + strPanelName + "\r\n----------------------------------\r\n" + strContent);*/
			if(strContent.indexOf("[ bl"+"ock", 0)!=-1) strContent = strContent.replace("[ bl"+"ock","[bl"+"ock");
			if(strPanelName=="")
			{
				if (fromDiv==1) strContent=_htmlDecode(strContent);
				_registerScript(strContent);
				_runInitialize();
			}
			else if(strPanelName=="script")
			{
				if (fromDiv==1) strContent=_htmlDecode(strContent);
				_registerText(strContent);
				_runInitialize();
			}
			else if(strPanelName=="style")
			{
				_registerStyleText(strContent);
			}
			else
			{
			    _registerBlock(strPanelName, strContent);
			}
		}
		catch(err) { _debug("Fout in _parseBlock -> fillPanelFn: " + err.message); }
	}

	try
	{
		strResponse = _trim(strResponse);
		if(strResponse.length === 0)
		{
			/* Nothing to do */
		}
		else if(strResponse.substring(0,7)=="[bl"+"ock_")
		{
			/* start parsing */
			var stringIndex = 0;

			while (strResponse.indexOf("[bl"+"ock_", stringIndex)!=-1)
			{
				stringIndex = strResponse.indexOf("[bl"+"ock_", stringIndex) + 7;
				var strBlockName = strResponse.substring(stringIndex, strResponse.indexOf("]", stringIndex));
				stringIndex = strResponse.indexOf("]", stringIndex)
				var stingEndIndex = strResponse.indexOf("[bl"+"ock]", stringIndex);
				if(stingEndIndex==-1)
				{
					/* This is the last block */
					try { fillPanelFn(strBlockName, strResponse.substring(stringIndex+1)) } catch(fillErr) {}
					break;
				}
				else
				{
					/* Find the end until the next block */
					try { fillPanelFn(strBlockName, strResponse.substring(stringIndex+1, stingEndIndex)) } catch(fillErr) {}
				}
			}
		}
		else
		{
			/* put everything in the -empty- container */
			try { fillPanelFn("", strResponse) } catch(fillErr) {}
		}
	}
	catch(err) { _debug("Fout in _parseBlock: " + err.message); }
}

function _parseScript(text)
{
	var io1 = -1;
	var retscript = "";
	do
	{
		io1 = text.toLowerCase().indexOf("<script", io1 + 1);
		io1Length = text.indexOf(">", io1)-io1+1;
		if (io1 >= 0)
		{
			var io2 = text.toLowerCase().indexOf("<\/script>", io1 + 1);
			if (io2 >=0 )
			{
				retscript += " " + text.substring(io1 + io1Length, io2);
			}
		}
	} while (io1 != -1);
	return retscript;
}

function _registerStyleText(strStyle)
{
	var objStyle = document.getElementById("styleDiv");
	if(objStyle==null)
	{
		objStyle = document.createElement("style");
		objStyle.setAttribute("type","text/css");
		objStyle.setAttribute("id", "styleDiv");
		document.getElementsByTagName("head")[0].appendChild(objStyle);
	}

	if (objStyle.styleSheet)
	{
		var strTotalStyle = objStyle.styleSheet.cssText + "\r\n" + strStyle;
		var count = strTotalStyle.match(/:/g);  
		if(count!=null && count.length>=4095)
		{
			objStyle.setAttribute("id", "");
			objStyle = document.createElement("style");
			objStyle.setAttribute("type","text/css");
			objStyle.setAttribute("id", "styleDiv");
			document.getElementsByTagName("head")[0].appendChild(objStyle);
			objStyle.styleSheet.cssText = strStyle;
		}
		else
		{
			objStyle.styleSheet.cssText = strTotalStyle;
		}
	}
	else
	{

		objStyle.appendChild(document.createTextNode(strStyle));
	}
}
function _registerStyleText_orig(strStyle)
{
	document.getElementById("ctrlXDoc_hiddenStyle").innerHTML="&nbsp;<style type=\"text/css\" id=\"ctrlXDoc_hiddenStyle_style\">" + (document.getElementById("ctrlXDoc_hiddenStyle_style")==null ? "" : document.getElementById("ctrlXDoc_hiddenStyle_style").innerHTML) + strStyle + "</style>";
}

function _registerScript(text)
{
	var javaScript = _parseScript (text);
	if (javaScript != "")
	{
	 if (window.execScript)
	  window.execScript(javaScript);
	 else
	  window.setTimeout(javaScript,0);
 } 
}
function _registerText(javaScript)
{
	if (javaScript != "")
	{
		if (window.execScript)
			window.execScript(javaScript);
		else
			eval.call(window, javaScript);
	}	
}

function _registerScriptNoTags(text)
{
	text = "<" + "script>" + text + "<" + "/script>";
	_registerScript(text);
}
/* PARSE BLOCK */

/* INITIALIZE */
var _aInit = new Array();
function _addInitialize(fn)
{
  for(i=0;i<_aInit.length; i++)
  {
	if(_aInit[i].toString() == fn.toString()) return;
  }

  _aInit.push(fn);
}

function _runInitialize()
{
	if (window.execScript) _doRunInitialize();
	else window.setTimeout(_doRunInitialize, 0);

	function _doRunInitialize()
	{
		while(_aInit.length>0)
		{
			try { fn = _aInit.shift(); fn(); } catch(e) { }
		}
		try { _runResize(null); } catch(e) { }
	} 
}

function _runResize(evt, obj)
{
/*abstract function*/
}
function _callInitialize()
{
	try
	{
		_runInitialize();
	}
	catch (e){}
	try
	{
		_detachEvent(window, "load", _callInitialize);
	}
	catch (e){}
}
/* INITIALIZE */

/* ERRORS */
function _checkErrors(str, intCodeShowMask)
{
	var i = 0;
	var b = false;

	for (var intCode in _arrError)
	{
		b = false;
		var regex = new RegExp(_arrError[intCode]["regex"], "gim");
		var arrMatch = null;
		while( (arrMatch = regex.exec(str)) != null )
		{
			if (intCodeShowMask===undefined || (intCode&intCodeShowMask) != 0)
			{
				b = true;
				try { _arrError[intCode]["showerror"](arrMatch[1]); } catch(err) { }
			}
		}
		if (b) i += parseInt(intCode);
	}
	return i;
}

function _addError(intCode, strRegex, fnShowError)
{
	_arrError[intCode] = new Array();
	_arrError[intCode]["regex"] = strRegex;
	_arrError[intCode]["showerror"] = fnShowError;
}

function _removeError(intCode)
{
	_arrError.splice(intCode, 1);
}

function _getError(str, blnStripHtml)
{
	blnStripHtml = (blnStripHtml == undefined) ? false : blnStripHtml ;
	var strError = "<font color=\"red\" size=\"2\">";
	var strErrorEnd = "</font>";
	if (str.search(strError) == -1)
	{
		strError = "<span class=\"error\">";
		strErrorEnd = "</span>";
	}

	if (str.search(strError) == -1)
	{
		return null;
	}
	else
	{
		/* get string from xdoc message without html tags */
		var intStart = str.indexOf(strError);

		var intEnd = str.indexOf(strErrorEnd, intStart);
		intStart += (blnStripHtml) ? strError.length : 0;
		intEnd += (blnStripHtml) ? 0 : strErrorEnd.length;
		return str.substring(intStart,intEnd).replace(/<br\/>/gi,"\r\n");
	}
}


var _arrError = new Array();
function _registerStandardErrors()
{
_addError(1, "<font color=\"red\" size=\"2\">([\\s\\S]*?)</font>", _debug);
_addError(2, "<span class=\"error\">([\\s\\S]*?)</span>", _debug);
_addError(4, "^error:([^<]*)$", function(sMsg) { _debug("HTTP error: " + sMsg); } );
_addError(8, "<span class=\"sessionexpired\">([\\s\\S]*?)</span>", _sessionExpired);
}

if(typeof(_debug)!="function")
{
	function _debug(strMsg) 
	{ 
		//alert(strMsg);
	}
}

function _sessionExpired(strMessage) 
{ 
window.location.reload();
}

_registerStandardErrors();
/* ERRORS */

/* FOCUS GROUP */
var _aFG_flag = new Array();
var _aFG_focus  = new Array();
var _aFG_blur = new Array();
var _aFG_parentFG = new Array();

function _createFocusGroup(strId, aIds, fnFocus, fnBlur)
{
	for(var iId in aIds)
	{
		_addListener(document.getElementById(aIds[iId]),"focus",function(event){_fnFG_focus(strId);});
		_addListener(document.getElementById(aIds[iId]),"blur",function(event){_fnFG_blur(strId);});
	}
	_aFG_focus[strId] = fnFocus;
	_aFG_blur[strId] = fnBlur;
}

function _linkFocusGroup(strIdParent, strIdChild)
{
	_aFG_parentFG[strIdChild] = strIdParent;
}

function _fnFG_focus(strId)
{
	if((_aFG_flag[strId]!=2)&&(_aFG_flag[strId]!=1) ) 
	{
		if(_aFG_parentFG[strId]!=null) _fnFG_focus(_aFG_parentFG[strId]);
		if(_aFG_focus[strId]) setTimeout(function() { _aFG_focus[strId](strId); },700);
	}
	_aFG_flag[strId]=1;
}

function _fnFG_blur(strId)
{
	_aFG_flag[strId]=2;
	setTimeout('_fnFG_confirmblur(\'' + strId+ '\');',600);
}

function _fnFG_confirmblur(strId)
{
	if(_aFG_flag[strId]==2) 
	{
		_aFG_flag[strId] = 0;
		if(_aFG_parentFG[strId]!=null) _fnFG_blur(_aFG_parentFG[strId]);
		if(_aFG_blur[strId]) _aFG_blur[strId](strId);
	}
}
/* FOCUS GROUP */

/* HASH */
var _hashTimer = null;
var _hash = "";
var _hashFn = null;
var _hashSkip;
var _sPrevHash="empty";


function _setHash(fn)
{
	_hashFn = fn;
}

function _removeHash(fn)
{
	_hashFn = null;
}

function _hashStartProc__notimer()
{
	function onHashChange_event()
	{
		if(_hashSkip) return _hashSkip = false;
		if(_hashFn!=null) try { _hashFn(); } catch(err) {}
	}

	/* Save the current hash, so it can be checked for changes */
	_hash = window.location.hash;

	_addEvent(window, "hashchange", onHashChange_event);

	window.location.hash= (_hash=="" ? "#": _hash );
}

function _hashStartProc()
{
	function onHashChange_event()
	{
		if(_hashSkip) return _hashSkip = false;
		if(_hashFn!=null) try { _hashFn(); } catch(err) {}
	}

	function onHashChange_timer()
	{
		if(_hashSkip)
		{
			_hash = window.location.hash;
			_hashSkip = false;
		}
		else if(_hash!=window.location.hash)
		{
			_hash=window.location.hash;
			if(_hashFn!=null) try { _hashFn(); } catch(err) {}

			/* For IE6, IE7 (and lower?), fix hash now showing in address bar after manual change */
			if(navigator.appVersion.match(/MSIE (.\..)/)[1]<8.0)
			{
			   window.setTimeout(function() {  _callHashChange(); },0);
			}
		}
		_hashTimer = window.setTimeout(onHashChange_timer, 500);
	}

	var _hFirst = 0;

	function onHashChange_timer1()
	{
		if(_hFirst == 1) {return;}
		_hashTimer = setTimeout(function(){onHashChange_timer();}, 500);
	}

	/* When the onhashchange event is triggered, replace the time-out solution with this event */
	function onHashChange_tryEvent()
	{
		_hFirst = 1;
		_detachEvent(window, "hashchange",onHashChange_tryEvent);
		_addEvent(window, "hashchange", onHashChange_event);
		window.clearTimeout(_hashTimer);
		_hashTimer = null;
	}
	/* Save the current hash, so it can be checked for changes */
	_hash = window.location.hash;

	_hashSkip = true;
	_hashTimer1 = setTimeout(function(){onHashChange_timer1();}, 500);


	/* Add the on haschange event and try to trigger it */
	_addEvent(window, "hashchange", onHashChange_tryEvent);
	window.location.hash="#action=tryEvent";
	window.location.hash= (_hash=="" ? "#": _hash );
}

_hashStartProc();
var _hashBI = "";
function _hashChangeHandler(blnOnLoad)
{
   /* Old IE browsers do a reload on hash change (by timer). So while reloading do not have to send event. */
   var ieMatches = navigator.appVersion.match(/MSIE (.\..)/);
   if( (ieMatches==null || ieMatches[1]>=8.0) || blnOnLoad===true)
   {
	   /* _sendEvent(_hashBI,"","hashchange", (window.location.hash.length==0?"":window.location.hash.substr(1))); */
	  setTimeout(function() {  _callHashChange(); },100);
   }
}
function _callHashChange()
{
   var sHash =  (window.location.hash.length==0?"":window.location.hash.substr(1));
   if(_sPrevHash != sHash)
   {
	  _sPrevHash = sHash;
	  _sendEvent(_hashBI,"","hashChange", sHash);
   }
}

function _hashRegister(bi)
{
	try
	{
		_hashBI=bi;
		_setHash(_hashChangeHandler);
		var _hash = window.location.hash;
		if(_hash.length!=0 && _hash.charAt(0)=="#") _hash = _hash.substr(1);
		_hashChangeHandler( true);
	}
	catch(err) { _error("Fout in _hashRegister: " + err.message); }
}

																	 
																	 
											 
function _hashGet()
{
	if(window.location.hash.length!=0 && window.location.hash.charAt(0)=="#") return window.location.hash.substr(1);
	else return window.location.hash;
}

function _hashSet(strHash, blnEvent)
{
	if(blnEvent!==true) blnEvent = false;
	if( !blnEvent ) _sPrevHash = strHash;
	if(window.location.hash.substr(1) != strHash) _hashSkip = !blnEvent;
	window.location.hash = "#" + strHash;
}

function _hashGetKey(strKey, strHash)
{
	if(strHash === undefined || strHash == null) strHash = _hashGet();
	else if(strHash.length!=0 && strHash.charAt(0)=="#") strHash = strHash.substr(1);

	var intStart = ("&" + strHash + "&").search(new RegExp("&" + strKey + "=", "i"));
	if(intStart!=-1)
	{
		var intEnd = ("&" + strHash + "&").indexOf("&", (intStart + 1));
		return strHash.substring(intStart + strKey.length + 1, intEnd - 1);
	}
	else return null;
}

function _hashSetKey(strKey, strValue, strHash, blnEvent)
{
	var blnHashSet = false;
	if(strHash === undefined || strHash == null)
	{
		strHash = _hashGet();
		blnHashSet = true;
	}
	else
	{
		if(strHash.length!=0 && strHash.charAt(0)=="#") strHash = strHash.substr(1);
	}

	var intStart = ("&" + strHash + "&").search(new RegExp("&" + strKey + "=", "i"));
	if(intStart==-1)
	{
		if(strValue!=null) strHash = strHash + (strHash.length==0?"":"&") + strKey + "=" + strValue;
		else { /* do nothing */ }
	}
	else
	{
		var intEnd = ("&" + strHash + "&").indexOf("&", (intStart + 1));
		if(strValue==null)
			strHash = strHash.substring(0,intStart-1) + strHash.substr((intStart==0?intEnd:intEnd-1));
		else
			strHash = strHash.substring(0,intStart) + strKey + "=" + strValue + strHash.substr(intEnd-1);
	}

	if(blnHashSet) _hashSet(strHash, blnEvent)
	return strHash;
}

function _hashDelKey(strKey, strHash, blnEvent)
{
	var blnHashSet = false;
	if(strHash === undefined || strHash == null)
	{
		strHash = _hashGet();
		blnHashSet = true;
	}
	else
	{
		if(strHash.length!=0 && strHash.charAt(0)=="#") strHash = strHash.substr(1);
	}

	var intStart = ("&" + strHash + "&").search(new RegExp("&" + strKey + "=", "i"));
	if(intStart!=-1)
	{
		var intEnd = ("&" + strHash + "&").indexOf("&", (intStart + 1));
		strHash = strHash.substring(0,intStart-1) + strHash.substr((intStart==0?intEnd:intEnd-1));
	}

	if(blnHashSet) _hashSet(strHash, blnEvent)
	return strHash;
}

function _hashMerge(strKeyValues, strHash, blnEvent)
{
	var blnHashSet = false;
	if(strHash === undefined || strHash == null)
	{
		strHash = _hashGet();
		blnHashSet = true;
	}
	else
	{
		if(strHash.length!=0 && strHash.charAt(0)=="#") strHash = strHash.substr(1);
	}

	if(strKeyValues.length==0) return strHash;

	var intStart = 0;
	var intEnd = -1;
	var strKey = "";
	var strValue = "";

	while(strKeyValues.charAt(intStart)=="&") strKeyValues = strKeyValues.substr(1);
	while(strKeyValues.length>0)
	{
		intEnd = ( strKeyValues + "&" ).indexOf("&");
		strKey = strKeyValues.substring(0, intEnd);
		if(strKey.indexOf("=")==-1)
		{
			strHash = _hashSetKey(strKey, "", strHash);
		}
		else
		{
			strValue = strKey.substr(strKey.indexOf("=")+1);
			strKey = strKey.substring(0, strKey.indexOf("="));
			strHash = _hashSetKey(strKey, strValue, strHash);
		}
		strKeyValues = strKeyValues.substr(intEnd);
		while(strKeyValues.charAt(intStart)=="&") strKeyValues = strKeyValues.substr(1);
	}

	if(blnHashSet) _hashSet(strHash, blnEvent)
	return strHash;
}
/* hash */




if(typeof(Sys) !== "undefined" && typeof(Sys.Application) !== "undefined")
	Sys.Application.notifyScriptLoaded();
	


/* KEYBOARD */
function _shiftPressed(evt)
{
	var e = (window.event) ? window.event : evt;
	if (parseInt(navigator.appVersion)<=3) return;
	if (navigator.appName=="Netscape" && parseInt(navigator.appVersion)==4) return ((e.modifiers+32).toString(2).substring(3,6).charAt(0)=="1");
	else return evt.shiftKey;
}

function _ctrlPressed(evt)
{
	var e = (window.event) ? window.event : evt;
	if (parseInt(navigator.appVersion)<=3) return;
	if (navigator.appName=="Netscape" && parseInt(navigator.appVersion)==4) return ((e.modifiers+32).toString(2).substring(3,6).charAt(1)=="1");
	else return evt.ctrlKey;
}

function _altPressed(evt)
{
	var e = (window.event) ? window.event : evt;
	if (parseInt(navigator.appVersion)<=3) return;
	if (navigator.appName=="Netscape" && parseInt(navigator.appVersion)==4) return ((e.modifiers+32).toString(2).substring(3,6).charAt(2)=="1");
	else return evt.altKey;
}
/* KEYBOARD */

/* GET ELEMENT */
function _getChildElement(obj, intChildNr)
{
	if (obj==null) throw new Error("Cannot get child elements for an object that is null");
	var objChild = _getFirstElement(obj);
	for(var i=0; i<intChildNr; i++)
	{
		objChild = _getNextElement(objChild);
	}
	return objChild;
}

function _getNextElement(obj)
{
	if (obj==null) throw new Error("Cannot get the next element for an object that is null");
	var current = obj.nextSibling;
	while (current!=null)
		if (current.nodeType==1) return current;
		else current = current.nextSibling;
	return null;
}
	
function _getPrevElement(obj)
{
	if (obj==null) throw new Error("Cannot get the previous element for an object that is null");
	var current = obj.previousSibling;
	while (current!=null)
		if (current.nodeType==1) return current;
		else current = current.previousSibling;
	return null;
}

function _getFirstElement(obj)
{
	if (obj==null) throw new Error("Cannot get the first element for an object that is null");
	var current = obj.firstChild;
	while (current!=null)
		if (current.nodeType==1) return current;
		else current = current.nextSibling;
	return null;
}
	
function _getLastElement(obj)
{
	if (obj==null) throw new Error("Cannot get the last element for an object that is null");
	var current = obj.lastChild;
	while (current!=null)
		if (current.nodeType==1) return current;
		else current = current.previousSibling;
	return null;
}
/* GET ELEMENT */

/* COORDINATES */
function _getY(oElement, blnMentionScroll)
{
	var iReturnValue = 0;
	var obj = oElement;
	while( obj != null ) 
	{
		iReturnValue += obj.offsetTop;
		obj = obj.offsetParent;
	}

	/* offsetParent does not include all partent nodes, for scrolling we use parentNode */
	if(blnMentionScroll===true)
	{
		obj = oElement;
		while( obj != null ) 
		{
			if(obj.scrollTop!==undefined) iReturnValue -= obj.scrollTop;
			obj = obj.parentNode;
		}
	}
	return iReturnValue;
}

function _getX(oElement, blnMentionScroll)
{
	var iReturnValue = 0;
	var obj = oElement;
	while( obj != null ) 
	{
		iReturnValue += obj.offsetLeft;
		//if(blnMentionScroll===true) iReturnValue -= obj.scrollLeft;
		obj = obj.offsetParent;
	}

	/* offsetParent does not include all partent nodes, for scrolling we use parentNode */
	if(blnMentionScroll===true)
	{
		obj = oElement;
		while( obj != null ) 
		{
			if(obj.scrollTop!==undefined) iReturnValue -= obj.scrollLeft;
			obj = obj.parentNode;
		}
	}

	return iReturnValue;
}

function _getBGPositionX(obj)
{
	if(document.defaultView)
	{
		/* FireFox */
		return document.defaultView.getComputedStyle(obj, "").getPropertyValue("background-position").split(" ")[0];
	}
	else if(obj.currentStyle)
	{
		/* Internet explorer */
		return obj.currentStyle.backgroundPositionX;
	}
}

function _getBGPositionY(obj)
{
	if(document.defaultView)
	{
		/* FireFox */
		return document.defaultView.getComputedStyle(obj, "").getPropertyValue("background-position").split(" ")[1];
	}
	else if(obj.currentStyle)
	{
		/* Internet explorer */
		return obj.currentStyle.backgroundPositionY;
	}
}
/* COORDINATES */

/* CARET */
function _getCaretPos(objTxt)
{
	try
	{
		if (objTxt.selectionStart===undefined)
		{
			/* IE */
			var oSel = document.selection.createRange();
			oSel.moveStart('character', -objTxt.value.length);
			return oSel.text.length;
		}
		else if (objTxt.selectionStart)
		{
			/* FF, Opera, Safari, Chrome */
			return objTxt.selectionStart;
		}
		return 0;
	}
	catch(err)
	{
		_debug("Fout in _getCaretPos: " + err.message);
	}
}

function _setCaretPosFocus(objTxt, intPos)
{
	try
	{
		if (objTxt.setSelectionRange)
		{
			/* FF, Opera, Safari, Chrome */
			objTxt.focus();
			window.setTimeout(function() { try { objTxt.setSelectionRange(intPos,intPos); } catch(err) {} }, 0);
		}
		else if (objTxt.createTextRange)
		{
			/* IE */
			var range = objTxt.createTextRange();
			range.collapse(true);
			range.moveEnd('character', intPos);
			range.moveStart('character', intPos);
			range.select();
		}
	}
	catch(err)
	{
		_debug("Fout in _setCaretPos: " + err.message);
	}
}

function _getCaretWord(objTxt, strSep, blnFullWord)
{
	try
	{
		var pos = _getCaretPos(objTxt);
		if (blnFullWord!==true) blnFullWord = false;
		var start_word = 0;
		var end_word = pos;
		if (objTxt.value.charAt(pos) == strSep) pos--;
		for (var i = pos; i>=0; i--)
		{
			if (objTxt.value.charAt(i) == strSep)
			{
				start_word = i+1;
				break;
			}
		}
		if(blnFullWord)
		{
			for (var i = pos+1; i<=objTxt.value.length; i++)
			{
				if ( (objTxt.value+strSep).charAt(i) == strSep)
				{
					end_word = i;
					break;
				}
			}
		} 
		return objTxt.value.substring(start_word, end_word);
	}
	catch(err)
	{
		_debug("Fout in _getCaretWord: " + err.message);
	}
}

function _setCaretWord(objTxt, strSep, strWord)
{
	try
	{
		var pos = _getCaretPos(objTxt);
		var start_word = 0;
		var end_word = pos;
		if (objTxt.value.charAt(pos) == strSep) pos--;
		for (var i = pos; i>=0; i--)
		{
			if (objTxt.value.charAt(i) == strSep)
			{
				start_word = i+1;
				break;
			}
		}
		for (var i = pos+1; i<=objTxt.value.length; i++)
		{
			if ( (objTxt.value+strSep).charAt(i) == strSep)
			{
				end_word = i;
				break;
			}
		}

		objTxt.value = objTxt.value.substring(0, start_word) + strWord + objTxt.value.substring(end_word);
		_setCaretPosFocus(objTxt, start_word + strWord.length)
	}
	catch(err)
	{
		_debug("Fout in _setCaretWord: " + err.message);
	}
}
/* CARET */

/* SCROLL */
function _scrollPanel(panel, intPercentage)
{
	try
	{
		if(typeof(panel)=="string") panel = document.getElementById(panel);
		intPercentage = parseInt(intPercentage);
		panel.scrollTop = panel.scrollHeight / 100 * intPercentage;
	}
	catch(err) { _debug(err.message); }
}
function _scrollPanelToObject(panel, object)
{ 
	try 
	{ 
		if(typeof(panel)=="string") panel = document.getElementById(panel); 
		if(typeof(object)=="string") object = document.getElementById(object); 
		if(panel.scrollTop > object.offsetTop) panel.scrollTop = object.offsetTop; 
		if(panel.scrollTop + panel.offsetHeight < object.offsetTop+object.offsetHeight) panel.scrollTop = object.offsetTop; 
	} 
	catch(err) { _debug(err.message); }
} 

function _moveDiv(objDivToMove, objDivToStick, strPosition, blnAttachToBody)
{
  try
	{
		if( typeof(objDivToMove)!="string" && typeof(objDivToMove)!="object" ) return false;
		if( typeof(objDivToStick)!="string" && typeof(objDivToStick)!="object" ) return false;
		if( typeof(objDivToMove)=="string" ) objDivToMove = document.getElementById(objDivToMove);
		if( typeof(objDivToStick)=="string" ) objDivToStick = document.getElementById(objDivToStick);
		if(objDivToMove==null || objDivToStick==null) return false;
		if(blnAttachToBody!==false && objDivToMove!=document.body && objDivToMove.parentNode!=null)
		{
			var tempObjDiv;
			var objDivToMoveToAppend;//alert(1);
			objDivToMoveToAppend=objDivToMove.parentNode.removeChild(objDivToMove);//alert(2);
			tempObjDiv=document.getElementById(objDivToMove.id);
			while(tempObjDiv != null){
				objDivToMoveToAppend12=tempObjDiv.parentNode.removeChild(tempObjDiv);
				tempObjDiv=document.getElementById(objDivToMove.id);
			}
			document.body.appendChild(objDivToMoveToAppend);
		}
		objDivToMove=document.getElementById(objDivToMove.id);   
		if(strPosition=="top")
		{
			objDivToMove.style.left = (_getX(objDivToStick,true)) + "px";
		var y_ = _getY(objDivToStick,true) - objDivToMove.offsetHeight;
			objDivToMove.style.top = y_ + "px";
		}
		else if(strPosition=="right")
		{
		var x_ = _getX(objDivToStick,true) + objDivToStick.offsetWidth;
			objDivToMove.style.left = x_ + "px";
			objDivToMove.style.top = (_getY(objDivToStick,true) )+ "px";
		}
		else if(strPosition=="left")
		{
		var x_ = _getX(objDivToStick,true) - objDivToMove.offsetWidth ;
			objDivToMove.style.left = x_ + "px";
			objDivToMove.style.top = (_getY(objDivToStick,true)) + "px";
		}
		else if(strPosition=="inside_center")
		{
		var x_ = (objDivToStick.clientWidth / 2 - objDivToMove.clientWidth / 2);
			objDivToMove.style.left =x_ + "px";
		var y_ = (objDivToStick.clientHeight / 2 - objDivToMove.clientHeight / 2);
			objDivToMove.style.top = y_ + "px";
		}
		else if(strPosition=="inside_topleft")
		{
			objDivToMove.style.left = (_getX(objDivToStick)) + "px";
			objDivToMove.style.top =( _getY(objDivToStick)) + "px";
		}
		else
		{
			objDivToMove.style.left = (_getX(objDivToStick,true)) + "px";
		var y_ =(_getY(objDivToStick,true) + objDivToStick.offsetHeight);
			objDivToMove.style.top = y_ + "px";
		}
	objDivToMove.style.position = "absolute";
		if(_getY(objDivToMove) < 0) objDivToMove.style.top = "0px";
		else
		if(_getY(objDivToMove) + objDivToMove.offsetHeight > document.body.offsetHeight){
			y_ = (_getY(objDivToMove) - objDivToStick.offsetHeight - objDivToMove.offsetHeight);
			objDivToMove.style.top = y_ + "px";
		}
		if(_getX(objDivToMove) < 0) objDivToMove.style.left = "0px";
		else
		if(_getX(objDivToMove) + objDivToMove.offsetWidth > document.body.offsetWidth) {
			x_ = (_getX(objDivToMove) - objDivToStick.offsetWidth  - objDivToMove.offsetWidth);
			objDivToMove.style.left = x_ + "px";
		}   
   
		return true;
	}
	catch(err) { _debug("Fout in _moveDiv: " + err.message); return false; };
} 

function _moveDivEx(objDivToMove, objDivToStick, strPosition)
{
  try
    {
        if( typeof(objDivToMove)!="string" && typeof(objDivToMove)!="object" ) return false;
        if( typeof(objDivToStick)!="string" && typeof(objDivToStick)!="object" ) return false;
        if( typeof(objDivToMove)=="string" ) objDivToMove = document.getElementById(objDivToMove);
        if( typeof(objDivToStick)=="string" ) objDivToStick = document.getElementById(objDivToStick);
        if(objDivToMove==null || objDivToStick==null) return false;
        var _parent = objDivToStick.parentNode;
        if(_parent == null) return false;
        var __parent = _parent;
        var __parentDiv = _parent;
        while(_parent.parentNode != null ){
            var _tag = _parent.tagName;
    /*Find the farthest parent div for objDivToStick */
            if(_tag.toLowerCase()=='div') __parentDiv = _parent;
    /*Find nearest parent div for objDivToStick with position:relative, so the objDivToStick and objDivToMove will be scroll together*/
            if(_parent.style.position == 'relative') {
                __parent = _parent; 
                break;
            }
            _parent = _parent.parentNode;
        }
        
        if(objDivToMove!=document.body && objDivToMove.parentNode!=null)
        {
            var tempObjDiv;
            var objDivToMoveToAppend;
            objDivToMoveToAppend=objDivToMove.parentNode.removeChild(objDivToMove);//alert(2);
            tempObjDiv=document.getElementById(objDivToMove.id);
            while(tempObjDiv != null){
                objDivToMoveToAppend12=tempObjDiv.parentNode.removeChild(tempObjDiv);
                tempObjDiv=document.getElementById(objDivToMove.id);
            }
            document.body.appendChild(objDivToMoveToAppend);
        }
        objDivToMove=document.getElementById(objDivToMove.id);           
        if(strPosition=="top")
        {
            objDivToMove.style.left = (_getX(objDivToStick,true)) + "px";
        var y_ = _getY(objDivToStick,true) - objDivToMove.offsetHeight;
            objDivToMove.style.top = y_ + "px";
        }
        else if(strPosition=="right")
        {
        var x_ = _getX(objDivToStick,true) + objDivToStick.offsetWidth;
            objDivToMove.style.left = x_ + "px";
            objDivToMove.style.top = (_getY(objDivToStick,true) )+ "px";
        }
        else if(strPosition=="left")
        {
        var x_ = _getX(objDivToStick,true) - objDivToMove.offsetWidth ;
            objDivToMove.style.left = x_ + "px";
            objDivToMove.style.top = (_getY(objDivToStick,true)) + "px";
        }
        else if(strPosition=="inside_center")
        {
        var x_ = (objDivToStick.clientWidth / 2 - objDivToMove.clientWidth / 2);
            objDivToMove.style.left =x_ + "px";
        var y_ = (objDivToStick.clientHeight / 2 - objDivToMove.clientHeight / 2);
            objDivToMove.style.top = y_ + "px";
        }
        else if(strPosition=="inside_topleft")
        {
            objDivToMove.style.left = (_getX(objDivToStick)) + "px";
            objDivToMove.style.top =( _getY(objDivToStick)) + "px";
        }
        else
        {
            objDivToMove.style.left = (_getX(objDivToStick,true)) + "px";
        var y_ =(_getY(objDivToStick,true) + objDivToStick.offsetHeight);
            objDivToMove.style.top = y_ + "px";
        }
        objDivToMove.style.position = "absolute";
        if(_getY(objDivToMove) < 0) objDivToMove.style.top = "0px";
        else
        if(_getY(objDivToMove) + objDivToMove.offsetHeight > document.body.offsetHeight){
            y_ = (_getY(objDivToMove) - objDivToStick.offsetHeight - objDivToMove.offsetHeight);
            objDivToMove.style.top = y_ + "px";
        }
        if(_getX(objDivToMove) < 0) objDivToMove.style.left = "0px";
        else
        if(_getX(objDivToMove) + objDivToMove.offsetWidth > document.body.offsetWidth) {
            x_ = (_getX(objDivToMove) - objDivToStick.offsetWidth  - objDivToMove.offsetWidth);
            objDivToMove.style.left = x_ + "px";
        }   
/*need a position :relative div to attache a div with position:absolute*/
        if(__parent.style.position != 'relative'){
            __parent=__parentDiv;
            __parent.style.position = 'relative';
        }
        var _x=_getX(objDivToMove,true)-_getX(__parent,true);
        objDivToMove.style.left = _x+"px";
        var _y=_getY(objDivToMove)-_getY(__parent,true); 
        objDivToMove.style.top = _y+"px";
        objDivToMoveToAppend=document.body.removeChild(objDivToMove);
        __parent.appendChild(objDivToMoveToAppend);

        return true;
    }
    catch(err) { _debug("Fout in _movePopup: " + err.message); return false; };
}

function _getNextHighestZindex(obj){ 
	   var highestIndex = 0; 
	   var currentIndex = 0; 
	   var elArray = Array(); 
	   if(obj){ elArray = obj.getElementsByTagName('*'); }else{ elArray = document.getElementsByTagName('*'); } 
	   for(var i=0; i < elArray.length; i++){ 
		  if (elArray[i].currentStyle){ 
			 currentIndex = parseFloat(elArray[i].currentStyle['zIndex']); 
		  }else if(window.getComputedStyle){ 
			 currentIndex = parseFloat(document.defaultView.getComputedStyle(elArray[i],null).getPropertyValue('z-index')); 
		  } 
		  if(!isNaN(currentIndex) && currentIndex > highestIndex){ highestIndex = currentIndex; } 
	   } 
	   return(highestIndex+1); 
	}

/* old _moveDiv
function _moveDiv(objDivToMove, objDivToStick, strPosition, blnAttachToBody)
{
	try
	{
		if( typeof(objDivToMove)!="string" && typeof(objDivToMove)!="object" ) return false;
		if( typeof(objDivToStick)!="string" && typeof(objDivToStick)!="object" ) return false;

		if( typeof(objDivToMove)=="string" ) objDivToMove = document.getElementById(objDivToMove);
		if( typeof(objDivToStick)=="string" ) objDivToStick = document.getElementById(objDivToStick);
		if(objDivToMove==null || objDivToStick==null) return false;

		if(blnAttachToBody!==false && objDivToMove!=document.body)
		{
			objDivToMove.parentNode.removeChild(objDivToMove);

			var i=50;
			while(objDivToMove.id!="" && document.getElementById(objDivToMove.id)!=null && i>0)
			{
				document.getElementById(objDivToMove.id).parentNode.removeChild(document.getElementById(objDivToMove.id));
				i--;
			}
			document.body.appendChild(objDivToMove);
		}

		if(strPosition=="top")
		{
			objDivToMove.style.left = _getX(objDivToStick,true) + "px";
			objDivToMove.style.top = (_getY(objDivToStick,true) - objDivToMove.offsetHeight) + "px";
		}
		else if(strPosition=="right")
		{
			objDivToMove.style.left = (_getX(objDivToStick,true) + objDivToStick.offsetWidth ) + "px";
			objDivToMove.style.top = _getY(objDivToStick,true) + "px";
		}
		else if(strPosition=="left")
		{
			objDivToMove.style.left = (_getX(objDivToStick,true) - objDivToMove.offsetWidth ) + "px";
			objDivToMove.style.top = _getY(objDivToStick,true) + "px";
		}
		else if(strPosition=="inside_center")
		{
			objDivToMove.style.left = (objDivToStick.clientWidth / 2 - objDivToMove.clientWidth / 2) + "px";
			objDivToMove.style.top = (objDivToStick.clientHeight / 2 - objDivToMove.clientHeight / 2) + "px";
		}
		else if(strPosition=="inside_topleft")
		{
			objDivToMove.style.left = _getX(objDivToStick) + "px";
			objDivToMove.style.top = _getY(objDivToStick) + "px";
		}
		else 
		{
			objDivToMove.style.left = _getX(objDivToStick,true) + "px";
			objDivToMove.style.top = (_getY(objDivToStick,true) + objDivToStick.offsetHeight) + "px";
		}

		objDivToMove.style.position = "absolute";

		if(_getY(objDivToMove) < 0) objDivToMove.style.top = "0px";
		else if(_getY(objDivToMove) + objDivToMove.offsetHeight > document.body.offsetHeight) objDivToMove.style.top = (document.body.offsetHeight - objDivToMove.offsetHeight - 5) + "px";
		if(_getX(objDivToMove) < 0) objDivToMove.style.left = "0px";
		else if(_getX(objDivToMove) + objDivToMove.offsetWidth > document.body.offsetWidth) objDivToMove.style.left = (document.body.offsetWidth - objDivToMove.offsetWidth - 5) + "px";

		return true;
	}
	catch(err) { _debug("Fout in _moveDiv: " + err.message); return false; }
}
*/

/* SCROLL */

/* UTIL */
/*IS*/
/* Check a string to be in format: 31-01-2007 */ 
function _isDate(strValue)
{
	var strDays = "(0[0-9]|[0-9]|[1-2][0-9]|3[0-1])";
	var strMonths = "(0[1-9]|[0-9]|1[0-2])";
	var strYears = "(1[8-9]|[2-9][0-9])([0-9]{2})";
	var pattern = new RegExp("^"+ strDays + "-" + strMonths + "-" + strYears + "$");
	if (!pattern.test(strValue)) return false;
	var arrResult = pattern.exec(strValue);
	return (strValue.substring(0,2) == new Date(arrResult[2]+"/"+arrResult[1]+"/"+arrResult[3]+arrResult[4]).getDate() )
}

/* Check a string to be in format: 31-01-2007 12:30:59 */
function _isDateTime(strValue)
{
	var strDays = "(0[0-9]|[0-9]|[1-2][0-9]|3[0-1])";
	var strMonths = "(0[1-9]|[0-9]|1[0-2])";
	var strYears = "(1[8-9]|[2-9][0-9])([0-9]{2})";
	var strHours = "([0-1][0-9]|2[0-3])";
	var strMinutes = "[0-5][0-9]";
	var strSeconds = strMinutes;
	var pattern = new RegExp("^"+ strDays + "-" + strMonths + "-" + strYears + " " + strHours + ":" + strMinutes + ":" + strSeconds + "$");
	if (!pattern.test(strValue)) return false;
	var arrResult = pattern.exec(strValue);
	return (strValue.substring(0,2) == new Date(arrResult[2]+"/"+arrResult[1]+"/"+arrResult[3]+arrResult[4]).getDate() )
}

/* Check a string to be in format: 2007 */
function _isYear(strValue)
{
	var strYears = "(1[8-9]|[2-9][0-9])[0-9]{2}";
	var pattern = new RegExp("^"+strYears+"$");
	return pattern.test(strValue);
}

/* Check a string to be in format: 123...etc */
function _isNumeric(strValue)
{
	return !isNaN(strValue);
}

/* Check a string to be in format: hans.de_groot@sub-dom_ain.co.uk */
function _isEmail(strValue)
{
	var pattern = new RegExp(/^[a-z0-9._-]+@[a-z0-9][a-z0-9._-]*\.[a-z]{2,6}$/i);
	return pattern.test(strValue);
}

/* Check a string to be in format: 1234 AA */
function _isZipcode(strValue)
{
 var pattern = new RegExp(/^[1-9]{1}[0-9]{3}(\s)?([A-Z]{2}|[a-z]{2})$/i);
 return pattern.test(strValue);
}


/* Check a string to be a number with .. decimals */
function _isDecimal(strValue, intDecimals)
{
	intDecimals = ( intDecimals === undefined || parseInt(intDecimals) < 1 ) ? 2 : parseInt(intDecimals) ;
	var pattern = new RegExp("^[0-9]+,[0-9]{"+intDecimals+"}$");
	return pattern.test(strValue);
}

function _isLengthBetween(strValue, minLength, maxLength)
{
	return (strValue.length >= minLength && strValue.length <= maxLength);
}

function _isAccountNumber(strValue) 
{
	/* Keuze: leeg is ook geen goed rekeningnummer; alternatief: inputveld al dan niet verplicht en alleen inhoud controleren */
	if (strValue=="") return false;

	/* NL: bank heeft 9 posities of 10 met een voorloopnul */
	var n = strValue.replace(/[^\d\.]/g,"");
	if (n.length==10 && n.substring(0,1) == "0")
	{
		n = n.substring(1);
	}
	var c = n.replace(/\D/g,"").split("");
	var e = 0;
	if (c.length == 9) 
	{
		/* Ongeldig rekeningnummer ingevoerd; dit is het Equens testbankrekeningnummer */
		if (n == "123456789") return false;

		for (var i = 0; i < 9; i++)
		{
			e += (9 - i) * c[i];
		}
	}

	/* lengte 6 of 7: gironummer; < 6: alleen zeer bijzondere gironummers bv. "555" van een goed doel */
	/* U heeft een ongeldig bank- of gironummer ingevuld */
	if(c.length < 6 || c.length == 8 || e % 11 != 0 || c.length > 9) return false;

	return true;
}

function _isPhoneInt(strValue)
{
	return new RegExp("^\\d{10,15}$").test(strValue);
}


/* translates 01/01/09 to 01-01-2009 */
function _translateDate(strValue, strYearPrefix)
{
	if(strValue == "") return "";
	strYearPrefix = ( strYearPrefix === undefined ) ? "20" : String(strYearPrefix) ;

	strValue=strValue.replace(/\//g,"-");
	if (strValue.length == 8 && strValue.substring(strValue.length-3, strValue.length-2) == "-")
	{
		strValue = strValue.substring(0,strValue.length-2) + strYearPrefix + strValue.substring(strValue.length-2);
	}
	return strValue;
}

/* Input translation for amounts, with max 10 decimals */
function _translateDecimal(strValue, intDecimals)
{
	intDecimals = ( intDecimals === undefined || parseInt(intDecimals) < 1 ) ? 2 : parseInt(intDecimals) ;
	var strD = "0000000000";
	if(intDecimals>10)
	{
		for (var i=10; i<intDecimals; i++) strD += "0";
	}

	strValue = strValue.replace(".", ",", "g");
	strValue += ( (strValue.lastIndexOf(",") == -1) ? "," : "") + strD;
	return strValue.substring(0, strValue.lastIndexOf(",") + intDecimals + 1);
}

/* Translate international phone numbers, "+31(0)30 - 60/69/276" to "0031306069276" */
function _translatePhoneInt(strValue)
{
	return strValue.replace(/\+/g,"00").replace(/\(0\)/g,"").replace(/[^\d]/g, "");
}
/* UTIL */



/* MD5 */
function hex_md5(s)
{
	function rstr2hex(input)
	{
		try
		{
			hexcase
		}
		catch (e)
		{
			hexcase = 0;
		}
		var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
		var output = "";
		var x;
		for (var i = 0; i < input.length; i++)
		{
			x = input.charCodeAt(i);
			output += hex_tab.charAt((x >>> 4) & 0x0F) + hex_tab.charAt(x & 0x0F);
		}
		return output;
	}

	function rstr_md5(s)
	{

		function rstr2binl(input)
		{
			var output = Array(input.length >> 2);
			for (var i = 0; i < output.length; i++)
			output[i] = 0;
			for (var i = 0; i < input.length * 8; i += 8)
			output[i >> 5] |= (input.charCodeAt(i / 8) & 0xFF) << (i % 32);
			return output;
		}

		function binl_md5(x, len)
		{

			function safe_add(x, y)
			{
				var lsw = (x & 0xFFFF) + (y & 0xFFFF);
				var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
				return (msw << 16) | (lsw & 0xFFFF);
			}


			function md5_cmn(q, a, b, x, s, t)
			{
				function bit_rol(num, cnt)
				{
					return (num << cnt) | (num >>> (32 - cnt));
				}
				return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b);
			}

			function md5_ff(a, b, c, d, x, s, t)
			{
				return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
			}

			function md5_gg(a, b, c, d, x, s, t)
			{
				return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
			}

			function md5_hh(a, b, c, d, x, s, t)
			{
				return md5_cmn(b ^ c ^ d, a, b, x, s, t);
			}

			function md5_ii(a, b, c, d, x, s, t)
			{
				return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
			}

			/* append padding */
			x[len >> 5] |= 0x80 << ((len) % 32);
			x[(((len + 64) >>> 9) << 4) + 14] = len;

			var a = 1732584193;
			var b = -271733879;
			var c = -1732584194;
			var d = 271733878;

			for (var i = 0; i < x.length; i += 16)
			{
				var olda = a;
				var oldb = b;
				var oldc = c;
				var oldd = d;

				a = md5_ff(a, b, c, d, x[i + 0], 7, -680876936);
				d = md5_ff(d, a, b, c, x[i + 1], 12, -389564586);
				c = md5_ff(c, d, a, b, x[i + 2], 17, 606105819);
				b = md5_ff(b, c, d, a, x[i + 3], 22, -1044525330);
				a = md5_ff(a, b, c, d, x[i + 4], 7, -176418897);
				d = md5_ff(d, a, b, c, x[i + 5], 12, 1200080426);
				c = md5_ff(c, d, a, b, x[i + 6], 17, -1473231341);
				b = md5_ff(b, c, d, a, x[i + 7], 22, -45705983);
				a = md5_ff(a, b, c, d, x[i + 8], 7, 1770035416);
				d = md5_ff(d, a, b, c, x[i + 9], 12, -1958414417);
				c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
				b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
				a = md5_ff(a, b, c, d, x[i + 12], 7, 1804603682);
				d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
				c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
				b = md5_ff(b, c, d, a, x[i + 15], 22, 1236535329);

				a = md5_gg(a, b, c, d, x[i + 1], 5, -165796510);
				d = md5_gg(d, a, b, c, x[i + 6], 9, -1069501632);
				c = md5_gg(c, d, a, b, x[i + 11], 14, 643717713);
				b = md5_gg(b, c, d, a, x[i + 0], 20, -373897302);
				a = md5_gg(a, b, c, d, x[i + 5], 5, -701558691);
				d = md5_gg(d, a, b, c, x[i + 10], 9, 38016083);
				c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
				b = md5_gg(b, c, d, a, x[i + 4], 20, -405537848);
				a = md5_gg(a, b, c, d, x[i + 9], 5, 568446438);
				d = md5_gg(d, a, b, c, x[i + 14], 9, -1019803690);
				c = md5_gg(c, d, a, b, x[i + 3], 14, -187363961);
				b = md5_gg(b, c, d, a, x[i + 8], 20, 1163531501);
				a = md5_gg(a, b, c, d, x[i + 13], 5, -1444681467);
				d = md5_gg(d, a, b, c, x[i + 2], 9, -51403784);
				c = md5_gg(c, d, a, b, x[i + 7], 14, 1735328473);
				b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);

				a = md5_hh(a, b, c, d, x[i + 5], 4, -378558);
				d = md5_hh(d, a, b, c, x[i + 8], 11, -2022574463);
				c = md5_hh(c, d, a, b, x[i + 11], 16, 1839030562);
				b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
				a = md5_hh(a, b, c, d, x[i + 1], 4, -1530992060);
				d = md5_hh(d, a, b, c, x[i + 4], 11, 1272893353);
				c = md5_hh(c, d, a, b, x[i + 7], 16, -155497632);
				b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
				a = md5_hh(a, b, c, d, x[i + 13], 4, 681279174);
				d = md5_hh(d, a, b, c, x[i + 0], 11, -358537222);
				c = md5_hh(c, d, a, b, x[i + 3], 16, -722521979);
				b = md5_hh(b, c, d, a, x[i + 6], 23, 76029189);
				a = md5_hh(a, b, c, d, x[i + 9], 4, -640364487);
				d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
				c = md5_hh(c, d, a, b, x[i + 15], 16, 530742520);
				b = md5_hh(b, c, d, a, x[i + 2], 23, -995338651);

				a = md5_ii(a, b, c, d, x[i + 0], 6, -198630844);
				d = md5_ii(d, a, b, c, x[i + 7], 10, 1126891415);
				c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
				b = md5_ii(b, c, d, a, x[i + 5], 21, -57434055);
				a = md5_ii(a, b, c, d, x[i + 12], 6, 1700485571);
				d = md5_ii(d, a, b, c, x[i + 3], 10, -1894986606);
				c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
				b = md5_ii(b, c, d, a, x[i + 1], 21, -2054922799);
				a = md5_ii(a, b, c, d, x[i + 8], 6, 1873313359);
				d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
				c = md5_ii(c, d, a, b, x[i + 6], 15, -1560198380);
				b = md5_ii(b, c, d, a, x[i + 13], 21, 1309151649);
				a = md5_ii(a, b, c, d, x[i + 4], 6, -145523070);
				d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
				c = md5_ii(c, d, a, b, x[i + 2], 15, 718787259);
				b = md5_ii(b, c, d, a, x[i + 9], 21, -343485551);

				a = safe_add(a, olda);
				b = safe_add(b, oldb);
				c = safe_add(c, oldc);
				d = safe_add(d, oldd);
			}
			return Array(a, b, c, d);
		}

		function binl2rstr(input)
		{
			var output = "";
			for (var i = 0; i < input.length * 32; i += 8)
			output += String.fromCharCode((input[i >> 5] >>> (i % 32)) & 0xFF);
			return output;
		}


		return binl2rstr(binl_md5(rstr2binl(s), s.length * 8));
	}

	function str2rstr_utf8(input)
	{
		var output = "";
		var i = -1;
		var x, y;

		while (++i < input.length)
		{ /* Decode utf-16 surrogate pairs */
			x = input.charCodeAt(i);
			y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
			if (0xD800 <= x && x <= 0xDBFF && 0xDC00 <= y && y <= 0xDFFF)
			{
				x = 0x10000 + ((x & 0x03FF) << 10) + (y & 0x03FF);
				i++;
			}

			/* Encode output as utf-8 */
			if (x <= 0x7F) output += String.fromCharCode(x);
			else if (x <= 0x7FF) output += String.fromCharCode(0xC0 | ((x >>> 6) & 0x1F), 0x80 | (x & 0x3F));
			else if (x <= 0xFFFF) output += String.fromCharCode(0xE0 | ((x >>> 12) & 0x0F), 0x80 | ((x >>> 6) & 0x3F), 0x80 | (x & 0x3F));
			else if (x <= 0x1FFFFF) output += String.fromCharCode(0xF0 | ((x >>> 18) & 0x07), 0x80 | ((x >>> 12) & 0x3F), 0x80 | ((x >>> 6) & 0x3F), 0x80 | (x & 0x3F));
		}
		return output;
	}

	return rstr2hex(rstr_md5(str2rstr_utf8(s)));
}
/* MD5 */

