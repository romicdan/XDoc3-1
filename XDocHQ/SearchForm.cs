using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using KubionDataNamespace;
using KubionLogNamespace;
using XDocuments;
//using XDocuments.
using System.Threading;

namespace XDocHQ
{
    public partial class SearchForm : Form
    {

    #region Memebers

        private string strZoekterm = "Enter your searchterm here..."; //Alleen gebruiken in constructor en _Load. Geen verwarring met 2 zoektermen. Zoekterm altijd uit tbbZoekterm halen.
        private int _IDinNewW;     //Het ID waarop is geklikt met de rechter muisknop in het GRID        

    #endregion Memebers

    #region Contructors

        public SearchForm(string _strZoekterm)
        {   
            strZoekterm = _strZoekterm; //De globale Zoekterm wordt eenmalig gevuld. Bij zoeken de zoekterm altijd uit tbxZoekterm halen
            InitializeComponent();

            MenuItem[] menuItems = {ContextMenuItemNewWindow};
            dgvFoundTemplates.ContextMenu = new ContextMenu(menuItems);
        }

        //public SearchForm()
        //{
        //    InitializeComponent();
        //} 

    #endregion Constructors

    #region Events

        private void SearchForm_Load(object sender, System.EventArgs e) //Een event van het laden van het window
        {                    
            lblNiksGevonden.Visible = false; //De boodschap dat er niks gevonden is hoeft niet getoond te worden.         

            if (strZoekterm.Trim().Length > 0)
            {
                tbxZoekterm.Text = strZoekterm; //De strZoekterm heeft eventueel een waarde meegekregen in de constructor
            }
            strZoekterm = null;             //Ik wil voorkomen dat de strZoekterm misbruikt wordt. Hij is alleen bedoelt om eenmalig de waarde uit de constructor op te slaan. Anders hebben we twee zoektermen en wordt het verwarrend
            btnZoek_Click(null, null);      //We gaan meteen een zoekactie uitvoeren   
            
            tbxZoekterm.Focus(); //De focus moet in de tekstbox staan    
            PlaatsJuist();       //Zet het zoekscherm op de juiste plaats neer           
        } 

        public void btnZoek_Click(object sender, EventArgs e) //Er wordt op de knop geklikt en we moeten zoeken
        {
            if (tbxZoekterm.Text.Trim().Length > 0)
            {
                if (BindFoundTemplates()) //We gaan het grid vullen.
                {   //Als hij succesvol gevuld is, dan gaan we hem tonen:                
                    dgvFoundTemplates.Visible = true;
                    lblNiksGevonden.Visible = false;
                }
                else
                {   //Als hij niet gevuld is, dan tonen we een boodschap dat de zoekterm niet voorkomt.
                    dgvFoundTemplates.Visible = false;
                    lblNiksGevonden.Visible = true;
                }
            }
            else
            {
                tbxZoekterm.Text = "Enter your searchterm here...";
                if (sender != null) 
                {   //Als sender null is, dan is er niet rechtstreeks op de knop gedrukt en komen we b.v. van het andere formulier. De melding hoeft nu niet geplaats te worden.
                    MessageBox.Show("Enter a correct searchterm!", "Wrong searchterm", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }

        private void dgvFoundTemplates_MouseDoubleClick(object sender, MouseEventArgs e) //Er wordt een gedubbelklikt, dus we moeten een template gaan openen in het voorgaande window
        {
            int idClicked = -1;
            bool blnSearchAllWasOn = false;

            try
            {
                if (dgvFoundTemplates.SelectedCells.Count > 0) //Er was goed geklikt en er is iets geselecteerd...
                {
                    int.TryParse(dgvFoundTemplates.SelectedCells[0].OwningRow.Cells[0].Value.ToString(),out idClicked);
                    
                    MainForm hoofdScherm = (MainForm)this.Owner;        //Maak referentie naar MainForm
                    hoofdScherm.SelecteerRijOpID(idClicked);            //Laad template
                    hoofdScherm.rtbZoekterm.Text = tbxZoekterm.Text;    //Stel de zoekterm in op de hiergebruikte zoekterm
                    if (hoofdScherm.btnDoorzoekAlles.Checked)
                    {
                        blnSearchAllWasOn = true;
                        hoofdScherm.btnDoorzoekAlles.Checked = false;           //Zorg dat er alleen in het huidige window gezocht wordt, anders wordt er niet het juiste template geselecteerd!
                    }
                    hoofdScherm.btnZoekVerder_Click(null, null);        //Voer de zoekfunctie van het hoofdscherm uit
                    if (blnSearchAllWasOn)
                    {
                        hoofdScherm.btnDoorzoekAlles.Checked = true;        //Zet het vinkje op searchall weer aan als het oorsponkelijk aan stond
                    }
                    hoofdScherm.Select();                               //Zet focus op hoofdscherm
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured loading the clicked template into the MainForm.\n\n" + ex.Message, "Error loading template", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(ex);
            }
        }

        private void dgvFoundTemplates_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e) //Enkele muisklik wordt afgevangen om contextmenu te kunnen tonen bij rechtermuisklik.
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {   //We hebben een right mouseclick, dus toon: Open in new window:                                        
                DataGridView.HitTestInfo testInfo = dgvFoundTemplates.HitTest(e.X, e.Y);    //We proberen een ID op te halen op basis van de positie van de muis 

                if (testInfo.RowIndex > -1)
                {   //There was no row found under the mouseposition, so there is no use in showing the contextmenu!
                    int.TryParse(dgvFoundTemplates.Rows[testInfo.RowIndex].Cells[0].Value.ToString(),out _IDinNewW);  //op het moment van de rechtermuisklik.
                    dgvFoundTemplates.ContextMenu.Show(dgvFoundTemplates, e.Location);
                }
            }   
        }

        private void tbxZoekterm_DoubleClick(object sender, System.EventArgs e)
        {
            tbxZoekterm.SelectAll();
        }

        private void tbxZoekterm_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnZoek_Click(sender, null);
            }
        }

        private void imgInfo_Click(object sender, EventArgs e)      //Dit vangt de klik af op het infoicoontje, achter het label Info
        {
            MessageBox.Show("This form shows an overview of templates in which the searchterm occurs. There will be searched through:"+
                                "\nTemplateName\nTemplateText\nQueryName\nQueryText\n\n" + 
                                "In the column Hits there is a summation over all these fields.\n\nWatch out!\n"+
                                "The searchterm which is filled in here, will go directly into the SQL-query. " +
                                "Therefor it can give different results than the Main form, if SQL-wildcards are used ('_', '%').",
                                "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        private void ContextMenuItemNewWindow_Click(object sender, EventArgs e)
        {   //Dit is de enige knop in het contextmenu (rechtermuisknop), van het dgvFoundTemplates;           
            if (_IDinNewW>-1)
            {   //Open een nieuwe XDocHQ, met commandlineparameter om het gewenste template in een nieuw window te laden.
                System.Diagnostics.Process.Start("XDocHQ.exe", "templateid=" + _IDinNewW.ToString());
                _IDinNewW = -2;     //Make sure you don't use the old ID the next time
            }
            else
            {
                MessageBox.Show("Failed in finding a good template ID under your right mouse click.\nTry again.", 
                                    "Failed to get ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }        

    #endregion Events

    #region Methods

        private bool BindFoundTemplates() //We gaan het grid vullen met gezochte templates:
        {
            bool blnSucces = false;
            string strZoekterm = tbxZoekterm.Text;
            ClientData cd = null;
       
            try
            {                
                base.Cursor = Cursors.WaitCursor;  //Deze routine duurt lang, zodat we de cursor op zandloper willen zetten.                
                this.Owner.Cursor = Cursors.WaitCursor;
                
                SortOrder sortOrder = dgvFoundTemplates.SortOrder;
                DataGridViewColumn sortColumn = dgvFoundTemplates.SortedColumn;
                DataTable dtFoundTemplates = new DataTable();

                dtFoundTemplates.Columns.Add("ID", typeof(int));        //kolomheaders instellen
                dtFoundTemplates.Columns.Add("Name", typeof(string));
                dtFoundTemplates.Columns.Add("Hits", typeof(int));

                if (ConfigurationManager.AppSettings["XDocConnectionString"] != null)
                {   
                    cd = new ClientData(ConfigurationManager.AppSettings["XDocConnectionString"]); //Een KubionDataNamespaceobject dat gegevens kan ophalen                                 
                    string sSQL = "SELECT distinct s_templates.TemplateID AS ID, s_templates.TemplateName AS Name, -1 AS Hits " +
                                    " FROM s_templates LEFT OUTER JOIN s_queries ON s_templates.TemplateID = s_queries.TemplateID " +
                                    " WHERE (s_queries.QueryText LIKE '%" + strZoekterm + "%') " +
                                    " OR (s_queries.QueryName LIKE '%" + strZoekterm + "%') " +
                                    " OR (s_templates.TemplateContent LIKE '%" + strZoekterm + "%') " +
                                    " OR (s_templates.TemplateName LIKE '%" + strZoekterm + "%') ";

                    dtFoundTemplates = cd.DTExecuteQuery(sSQL);                    //DataTable komt terug en kan getoond worden in datagrid
                }
                else
                {
                    MessageBox.Show("The connectionstring for filling the datagrid in dgvFoundTemplates is not present in the configfile (XDocConnectionString)",
                                        "Connectionstring not present.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //We zetten nog wat instellingen van het datagrid:
                dgvFoundTemplates.SuspendLayout();
                dgvFoundTemplates.ClearSelection();
                dgvFoundTemplates.DataSource = dtFoundTemplates;

                if (cbxTelTreffers.Checked)
                {
                    dgvFoundTemplates.Columns["Hits"].Visible = true;   //Hij kan tijdens een vorige zoekactie op false zijn gezet.
                    AddTreffers(ref dgvFoundTemplates); //We vullen de laatste kolom met het aantal treffers, dat is het aantal keer dat de zoekterm voor komt                                         
                }
                else
                {
                    dgvFoundTemplates.Columns["Hits"].Visible = false; //De treffers zijn niet gevuld, dus de kolom kan verborgen worden.
                }

                dgvFoundTemplates.Columns["ID"].Width = 46;
                dgvFoundTemplates.Columns["Name"].Width = 300;
                dgvFoundTemplates.Columns["Hits"].Width = 46;
                
                dgvFoundTemplates.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;                
                dgvFoundTemplates.ClearSelection();
                dgvFoundTemplates.ResumeLayout();
                                
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                MessageBox.Show("There was an error filling the grid with templates.\nProbably a database error.\n\n"
                                    + ex.Message, "Error filling the grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (dgvFoundTemplates.RowCount > 0)
                {   //Het is gelukt, dus we hoeven in de caller geen bericht te tonen dat er niks gevonden is
                    blnSucces = true;
                }
                if (cd != null)
                {
                    cd.Dispose();
                }
                base.Cursor = Cursors.Arrow;
                this.Owner.Cursor = Cursors.Arrow;
            }

            return blnSucces;
        }

        private void PlaatsJuist() //We gaan het zoekscherm op de juist plaats naast het MainForm zetten.
        {
            int xScreenBound = Screen.GetWorkingArea(this).Right;
            int yScreenBound = Screen.GetWorkingArea(this).Bottom;            
            int x;
            int y;

            try
            {                
                x = this.Owner.Bounds.Right;
                y = this.Owner.Bounds.Top;
                this.Location = new System.Drawing.Point(x, y); //We plaatsen hem standaard rechts naast het hoofdscherm

                if (this.Bounds.Right > xScreenBound)
                { //De rechter zijde van het windows valt buiten het display
                    x = xScreenBound - this.Width;
                    y = this.Owner.Bottom - this.Height;
                    this.Location = new System.Drawing.Point(x, y); //We plaatsen hem nu net binnen het display, onderaan het hoofdscherm
                }
                if (this.Bounds.Bottom > yScreenBound)
                {
                    y = yScreenBound - this.Height;
                    this.Location = new System.Drawing.Point(x, y); //Wanneer de bodem onder de displayrand valt, dan zetten we hem net binnen het display.
                }

                
            }
            catch { }//Het is geen probleem als dit mislukt
        }

        private void AddTreffers(ref DataGridView dgvFoundTemplates) //Deze methode voegt in de laatste kolom van het datagrid het aantal treffers toe:
        {
            IXTemplate template = null;
            XDocManager m_manager = new XDocManager();
            string strZoekterm = tbxZoekterm.Text.ToLower();
            int iTotRijen = dgvFoundTemplates.RowCount; //totaal aantal itteraties
            int iTreffers; //aantal keer dat een string voor komt in iedere template            
            int iTemplateID = -1;
            string strContent = ""; //De tekst waarin gezocht wordt
            string strTemplateNaam = "";
            
            for (int iRij = 0; iRij < iTotRijen; iRij++)
            {
                int.TryParse(dgvFoundTemplates.Rows[iRij].Cells[0].Value.ToString(),out iTemplateID);                
                strTemplateNaam = dgvFoundTemplates.Rows[iRij].Cells[1].Value.ToString().ToLower();

                try
                {
                    strContent = string.Empty; //If not emptied and rule below crashes, the content of previous template is used in the counting.
                    template = m_manager.LoadTemplate(iTemplateID);
                    strContent = template.Content.ToLower(); //De inhoud van de template is geladen                     
                }
                catch { } //Het is niet zo erg als dit fout gaat.

                iTreffers = iStringTelRoutine(ref strContent, ref strZoekterm);          // Tel de treffers in de templatetekst
                iTreffers += iStringTelRoutine(ref strTemplateNaam, ref strZoekterm);          // Tel de treffers in de templatenaam
                iTreffers += iCountQueryTreffers(ref m_manager, iTemplateID, ref strZoekterm);    // Tel de treffers in de querynaam en -tekst hier bij op

                dgvFoundTemplates.Rows[iRij].Cells[2].Value = iTreffers;  //We noteren het aantal treffers in de laatste kolom
            }
        }

        private int iCountQueryTreffers(ref XDocManager m_manager, int iTemplateID, ref string strZoekterm) //Deze hele telroutine is erg langzaam, daarom wil ik toch maar even refs gebruiken. Vooral voor de XDocManager kan het voordelig zijn.
        { 
            int iTreffers = 0;
            string strNaam;
            string strTekst;

            IDName[] queries = m_manager.GetQueriesList(iTemplateID);       //Laad lijst met queries
            foreach (IDName qIDName in queries)     
            {
                IXQuery query = m_manager.LoadQuery(qIDName.ID);
                strNaam = query.Name.ToLower();
                strTekst = query.QueryText.ToLower();
                iTreffers += iStringTelRoutine(ref strNaam, ref strZoekterm);       //Tel de treffers in de querynaam
                iTreffers += iStringTelRoutine(ref strTekst, ref strZoekterm);      //Tel de treffers in de querytekst
            }                 

            return iTreffers;
        }
         
        private int iStringTelRoutine(ref string strContent, ref string strZoekTerm) //Deze hele (bovenliggende)telroutine is erg langzaam, daarom wil ik toch maar even refs gebruiken. Vooral voor de grote lap templatetekst is dit misschien voordelig.
        {
            int iPos1 = 0;    //Positie waarop de laatste hit is gevonden 
            int iPos2 = 0;
            int iTreffers = -1; //We beginnen op -1, omdat we er altijd eerst 1 bij tellen

            while (iPos1 >= 0) //Doorgaan zolang er iets gevonden is
            {
                iTreffers += 1; //Vorige itteratie heeft een hit opgeleverd, dus nu 1 erbij tellen
                if (iPos2 <= strContent.Length)
                {
                    iPos1 = strContent.IndexOf(strZoekTerm, iPos2);
                    iPos2 = iPos1 + 1; //De volgende zoekpositie
                }
                else
                {
                    iPos1 = -2;
                }
            }

            return iTreffers;
        }
        
    #endregion Methods          

    }
}