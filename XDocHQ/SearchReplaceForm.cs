using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KubionLogNamespace;

namespace XDocHQ
{  
    public partial class SearchReplaceForm : Form
    {
        #region Members

            private TextBox _tbxContent;
            private string _strZoekterm = "";
            private int _iPos = -1;                     //Huidige cursorpositie vanaf waar gezocht wordt
            private StringObject _strobjVervangterm;    //Deze wordt gevuld via een ref uit de MainForm. Als deze aangepast wordt, dan verandert dat dus de waarde in de MainForm
            private ToolStripTextBox _tbxZoektermMain;            //De zoekterm in het MainForm moet aangepast wordt met de laatstgebruikte zoekterm in dit form, dus we vullen hem vanuit een referentie die in de constructor mee komt, net als _tbxContent en _strobjVervangterm

        #endregion Members

        #region Constructor

            public SearchReplaceForm(ref StringObject strobjVervangterm, ref TextBoxPlus tbxContent, ref ToolStripTextBox tbxZoektermMain, bool blnSearchOnly)
            {
                InitializeComponent();      //Deze moet bovenaan staan, ander gaat tbxVervangTerm.Text e.d. fout, omdat die nog null zijn.

                SwitchSearchReplace(blnSearchOnly); //Show search, or Search-and-replaceform?
                
                _strobjVervangterm = strobjVervangterm;             //De referentie naar het stringobject in het MainForm wordt vastgelegd, zodat we waarden in het mainform op kunnen slaan, welke niet verloren gaan bij het sluiten van het Zoek-en-vervangscherm
                tbxVervangTerm.Text = strobjVervangterm.Waarde;     //De oude vervangterm wordt in ere hersteld

                _tbxContent = tbxContent;   //We hebben een globale variabele die een referentie naar het de textbox van de owner heeft. Wijzigingen in deze textbox verschijnen in het ownerwindow           
                _tbxZoektermMain = tbxZoektermMain;     //We leggen de referentie naar de zoekterm in het MainForm vast om aangepast te kunnen worden.

                _strZoekterm = tbxZoektermMain.Text.ToLower();  //Gebruik de bestaande zoekterm
                tbxZoekterm.Text = _strZoekterm;                
                                
                if (_strZoekterm.Length < 1)
                {
                    btnAllesVervangen.Enabled = false;
                    btnVolgende.Enabled = false;
                    btnVervangen.Enabled = false;
                }
            }

        #endregion Constructor

        #region Eventhandlers

            private void SearchReplaceForm_Load(object sender, EventArgs e)
            {
                PlaatsJuist();

                if (_strZoekterm.Length > 0)
                {                    
                    //SelecteerTekstVerder(true);
                    btnVervangen.Focus();
                }

            }

            private void tbxZoekterm_TextChanged(object sender, EventArgs e)
            {
                if (tbxZoekterm.Text.Length < 1)
                {
                    btnAllesVervangen.Enabled = false;
                    btnVolgende.Enabled = false;
                    btnVervangen.Enabled = false;
                }
                else
                {
                    btnAllesVervangen.Enabled = true;
                    btnVolgende.Enabled = true;
                    btnVervangen.Enabled = true;
                }
            }

            private void tbxZoekterm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter)
                { 
                    btnVolgende_Click(null, null);
                }
                else if (e.Alt & e.KeyCode == Keys.F)
                {   
                    btnVolgende_Click(null, null);
                }
                else if (e.Alt & e.KeyCode == Keys.R)
                {
                    btnVervangen_Click (null, null);
                }
            }

            private void tbxVervangTerm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnVolgende_Click(null, null);
                }
                else if (e.Alt & e.KeyCode == Keys.F)
                {
                    btnVolgende_Click(null, null);
                }
                else if (e.Alt & e.KeyCode == Keys.R)
                {
                    btnVervangen_Click(null, null);
                }
            }

            public void btnVolgende_Click(object sender, EventArgs e)
            {
                _strZoekterm = tbxZoekterm.Text.ToLower();
                _tbxZoektermMain.Text = tbxZoekterm.Text;   //Zodra de zoekterm gebruikt is, dan moet hij ook in het MainForm geupdate worden met deze waarde

                if (_tbxContent.SelectionLength > 0)
                {   //Als er al een selectie is, dan beginnen we met zoeken vanaf 1 plaats verder dan het begin van de selectie (+1 wordt later gedaan)
                    _iPos = _tbxContent.SelectionStart; //Zet de zoekstartpositie op de plaats van de huidige cursor
                }
                else
                {   //Als er nog geen selectie is, dan beginnen we met zoeken vanaf de huidige cursorplaats (+1 wordt later gedaan)
                    _iPos = _tbxContent.SelectionStart - 1; //Zet de zoekstartpositie op de plaats van de huidige cursor
                }

                SelecteerTekstVerder(true);     //Selecteer volgende hit en toon boodschappen = true                
            }

            private void btnVervangen_Click(object sender, EventArgs e)
            {
                _strZoekterm = tbxZoekterm.Text.ToLower();
                _tbxZoektermMain.Text = tbxZoekterm.Text;               //Zodra de zoekterm gebruikt is, dan moet hij ook in het MainForm geupdate worden met deze waarde
                _strobjVervangterm.Waarde = tbxVervangTerm.Text;    //Deze vult de _Vervangterm in de MainForm, om later hergebruikt te worden

                if (_tbxContent.SelectionLength > 0)
                {   //Als er al een selectie is, dan beginnen we met zoeken vanaf 1 plaats verder dan het begin van de selectie (+1 wordt later gedaan)
                    _iPos = _tbxContent.SelectionStart; //Zet de zoekstartpositie op de plaats van de huidige cursor
                }
                else
                {   //Als er nog geen selectie is, dan beginnen we met zoeken vanaf de huidige cursorplaats (+1 wordt later gedaan)
                    _iPos = _tbxContent.SelectionStart - 1; //Zet de zoekstartpositie op de plaats van de huidige cursor
                }

                Vervang(true);                                      //Vervang en toon boodschappen = true
            }

            private void btnAllesVervangen_Click(object sender, EventArgs e)
            {
                this.Cursor = Cursors.WaitCursor;           //Dit kan lang duren als de zoekterm veel voor komt
                                
                int iTel = 0;
                _strZoekterm = tbxZoekterm.Text.ToLower();
                _tbxZoektermMain.Text = tbxZoekterm.Text;   //Zodra de zoekterm gebruikt is, dan moet hij ook in het MainForm geupdate worden met deze waarde
                _strobjVervangterm.Waarde = tbxVervangTerm.Text;     //Deze vult de _Vervangterm in de MainForm, om later hergebruikt te worden                

                _iPos = -1;                         //We beginnen vanaf het begin
                _tbxContent.SelectionLength = 0;    //En we hebben niks geselecteerd

                while (Vervang(false))              //Vervang zolang er iets gevonden wordt met toonbericht = false
                {
                    iTel += 1;
                }

                _iPos = -1;                         //Ze de zoekbeginpositie weer vooraan     
                _tbxContent.SelectionStart = 0;     //Zet de cursor vooraan
                _tbxContent.SelectionLength = 0;    //En we hebben niks geselecteerd

                CreateMessage(iTel);                //Toon het bericht van het aantal vervangen stukken tekst

                this.Cursor = Cursors.Default;      //Herstel de cursor als we klaar zijn.
            }

            private void btnAnnuleren_Click(object sender, EventArgs e)
            {
                this.Close();
            }

            private void SearchReplaceForm_GotFocus(object sender, System.EventArgs e)
            {
                btnVolgende.Focus();
            }

            public void Focus(bool blnSearchOnly)
            {
                SwitchSearchReplace(blnSearchOnly);
                this.Focus();
            }

          
        #endregion Eventhandlers

        #region Methods

            public void SetZoekTerm(string strZoekterm)
            {
                if (strZoekterm.Trim().Length > 0)
                {   //Leave the current searchterm in search-and-replace, if the new searchterm from the mainform is empty
                    _strZoekterm = strZoekterm.ToLower();
                    tbxZoekterm.Text = strZoekterm;
                }
            }

            private bool SelecteerTekstVerder(bool blnShowMessage) //Hier wordt de zoekterm geselecteerd in de tekstbox:
            {
                bool blnFound = true;
                string strContent = _tbxContent.Text.ToLower();
                int iLen = tbxZoekterm.Text.Length;
                int iPosTemp = -1;

                if (_iPos < -1)
                {
                    _iPos = -1;
                }

                if (strContent.Length >= _iPos + 1) //Hebben we de hele tekst al doorzocht?
                {
                    try
                    {
                        iPosTemp = strContent.IndexOf(_strZoekterm, _iPos + 1);

                        if (iPosTemp >= 0)
                        {   //De zoekterm komt voor in de templatetekst
                            _iPos = iPosTemp;
                            _tbxContent.Select(_iPos, iLen); //Maak de juiste selectie 
                            _tbxContent.ScrollToCaret();     //Dit zorgt ervoor dat de geselecteerde tekst ook zichtbaar wordt: scrollen als nodig
                        }
                        else
                        {
                            if (blnShowMessage)
                            {
                                MessageBox.Show("The searchterm does not occur any more in the text.", "End of search", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            blnFound = false;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error while searching", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        KubionLog.WriteLine(ex);
                    }
                }

                return blnFound;
            }

            private bool Vervang(bool blnShowMessage)
            {
                bool blnFoundNext = true;                
                string strContent = _tbxContent.Text;
                string strVervangTerm = tbxVervangTerm.Text;
                int iContentLength = strContent.Length;
                int iZoektermLength = _strZoekterm.Length;
                int iVervangTermLength = strVervangTerm.Length;
                int iSelectionStart = _tbxContent.SelectionStart;

                if (_tbxContent.SelectedText.ToLower() != _strZoekterm)
                {   //Er is nog geen juiste selectie om te vervangen (zelfde gedrag als zoek-en-vervang van Notepad)
                    _iPos -= 1;
                    if (!SelecteerTekstVerder(blnShowMessage))  //Ga meteen verder met zoeken, zoals ook in Notepad gebeurt
                    {
                        blnFoundNext = false;
                    }
                }
                else
                {
                    StringBuilder strBuild = new StringBuilder(iContentLength - iZoektermLength + iVervangTermLength);  //Optimalisatie van stringbuilder. Beetje overbodig, maar waarom ook niet?               
                    strBuild.Append(strContent.Substring(0, iSelectionStart));    //Deel totaan de selectie toevoegen
                    strBuild.Append(strVervangTerm);                                        //VervangTerm toevoegen
                    strBuild.Append(strContent.Substring(iSelectionStart + iZoektermLength, iContentLength - (iSelectionStart + iZoektermLength)));  //Deel na de selectie toevoegen

                    _tbxContent.Text = strBuild.ToString();     //Vul de tekstbox met het resultaat

                    _iPos = iSelectionStart + iVervangTermLength - 1;   //Let op! Dit is belangrijk, wanneer we b.v. 'a' vervangen door 'aa'. Als we niet opschuiven, dan komen we in een oneindige lus! 
                    if (!SelecteerTekstVerder(blnShowMessage))  //Ga meteen verder met zoeken, zoals ook in Notepad gebeurt
                    {
                        blnFoundNext = false;
                    }
                }                                

                return blnFoundNext;
            }

            private void PlaatsJuist() //We gaan het zoekscherm op de juiste plaats naast het MainForm zetten.
            {
                int x;
                int y;

                try
                {                    
                    y = this.Owner.Bottom - this.Height - 100;

                    if (y > this.Owner.Top)
                    {   //Deze situatie geldt voor het hoofdscherm
                        x = this.Owner.Right - this.Width - 50;
                        this.Location = new System.Drawing.Point(x, y); //We plaatsen hem nu rechtsonder in het hoofdscherm
                    }
                    else
                    {   //Deze situatie geldt voor het queryscherm
                        x = this.Owner.Right;
                        y = this.Owner.Top;
                        this.Location = new System.Drawing.Point(x, y); //We plaatsen hem rechtsboven, naast het queryscherm
                    }

                    
                }
                catch { }//Het is geen probleem als dit mislukt
            }

            private void CreateMessage(int iTel)    //We maken een mooi tekstje in scherm dat weergeeft hoeveel stukken tekst er vervangen zijn
            {
                lblAantal.Text = iTel.ToString();
                lblAantal.ForeColor = Color.Red;
                lblAantal.Font = new Font(lblAantal.Font,FontStyle.Bold);
                
                if (iTel == 1)
                {   //Meervoud of enkelvoud van 'stuk' gebruiken?
                    lblMsgBegin.Text  = "There is";
                    lblMsgEind.Text = "replacement made.";
                }
                else
                {
                    lblMsgBegin.Text  = "There are";
                    lblMsgEind.Text = "replacements made.";
                }

                lblAantal.Location = new Point(lblMsgBegin.Right-3, lblAantal.Location.Y);    //Zet het tweede label rechts na het eerste
                lblMsgEind.Location = new Point(lblAantal.Right-3, lblMsgEind.Location.Y);    //Zet het derde label rechts na het tweede
                            
                //MessageBox.Show(strMessage , "Vervangen", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            private void SwitchSearchReplace(bool blnSearchOnly)    //Show search, or Search-and-replaceform?
            {   
                if (blnSearchOnly)
                {
                    btnAllesVervangen.Visible = false;
                    btnVervangen.Visible = false;
                    tbxVervangTerm.Visible = false;
                    lblVervangTerm.Visible = false;
                    this.Text = "Search";
                }
                else
                {
                    btnAllesVervangen.Visible = true;
                    btnVervangen.Visible = true;
                    tbxVervangTerm.Visible = true;
                    lblVervangTerm.Visible = true;
                    this.Text = "Search-and-Replace";
                }
            }

        #endregion Methods

        }
}