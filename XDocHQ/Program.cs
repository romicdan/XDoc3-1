using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace XDocHQ
{
    static class Program
    {
        /// <summary>
        /// v1.8 compatible with XDoc_v3.3.4 => support for both S_Templates structures
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string Argument = "";

            foreach (string s in args)
            {
                if (s.Contains("="))
                {
                    Argument = s;
                    break;
                }
            }            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(Argument));
        }
    }
}