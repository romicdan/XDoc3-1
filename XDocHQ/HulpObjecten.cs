using System;

namespace XDocHQ
{

    /// <summary>
    /// Een simpel object dat een stringwaarde bevat. Gemaakt om als referentie door te geven aan onderliggend form, zodat daar de originele string gewijzigd kan worden.
    /// </summary>
    /// <param name="Waarde">param name</param>
    /// <returns>returns</returns>
    public class StringObject
    {   //Dit is een heel eenvoudig object dat ik nodig heb om als referentie van het mainform (eventueel via het queryform) naar het zoek-en-vervangscherm te sturen. Hierdoor kan in het zoek-en-vervangscherm de stringwaarde van de vervangterm opgeslagen worden. Het meesturen van een string als referentie werkte hiervoor niet.
        
        /// <summary>
        /// private string _Waarde = "";. De enige inhoud van StringObject
        /// </summary>
        private string _Waarde = "";
        
        /// <summary>
        /// Gets en sets de stringwaarde van StringObject. De enige methode en de enige inhoud van StringObject
        /// </summary>
        /// <param name="Waarde">param name</param>
        /// <returns>returns</returns>
        public string Waarde
        {
            get
            {
                return _Waarde;
            }
            set
            {
                _Waarde = value.ToString();                
            }
        }
    }

    /// <summary>
    /// System.Windows.Forms.TextBox met toegevoegde scrollmogelijkheden en de mogelijkheid om de caret (tekstcursor) te wijzigen
    /// </summary>
    public class TextBoxPlus : System.Windows.Forms.TextBox
    {
        //System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
        private const int _H_ScrollBar = 0x0;           //Een code voor het opvragen van de scrollbarpositie
        private const int _V_ScrollBar = 0x1;           //Een code voor het opvragen van de scrollbarpositie
        private const int _ScrollMessage = 0x00B6;      //Een code voor het versturen van een scrollbericht naar de Handle van een Control    

        private int iScrollPosHori = 0;
        private int iScrollPosVert = 0;  
      
        private int _iCarWidth = 4, _iCarHeight = 20;       //afmetingen van de caret (tekstcursor)
        
        [System.Runtime.InteropServices.DllImport("user32.dll")]    //Externe methode om de scrollpositie op te halen
        private static extern int GetScrollPos(IntPtr hwnd, int nBar);
        [System.Runtime.InteropServices.DllImport("user32.dll")]    //Externe methode om de scrollpositie in te stellen
        private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);
        [System.Runtime.InteropServices.DllImport("user32.dll")]    //Externe methode om een bericht te sturen naar de ControlHandle om de scrollbarposities te effectueren.
        private static extern int SendMessage(IntPtr hWnd, int wMsg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]        //Externe methode om een nieuwe caret te maken op basis van een bitmap
        private extern static int CreateCaret(IntPtr hwnd, IntPtr hBitmap, int nWidth, int nHeight);
        [System.Runtime.InteropServices.DllImport("user32.dll")]        //Externe methode om de caret (opnieuw) te tonen
        private extern static int ShowCaret(IntPtr hwnd);
        [System.Runtime.InteropServices.DllImport("user32.dll")]        //Externe methode om de caret te verbergen
        private extern static bool HideCaret(IntPtr hWnd);

        /// <summary>
        /// Stel de hoogte en breedte van de caret in op basis van de fontsize in Int16
        /// </summary>
        public int SetCaretSize
        {
            set
            {
                _iCarWidth = ((int)value/2)-2;      //Empirische formule. Komt wel mooi uit. Niet te breed, zodat duidelijk is dat de caret niet bij de selectie hoort.
                _iCarHeight = value * 2;            //Empirische formule. Hij is iets hoger dan de selectie, zodat duidelijk is dat de caret niet bij de selectie hoort.
                
                if (_iCarWidth < 2)
                {   //1 px is te klein, altijd minstens 2
                    _iCarWidth = 2;
                }
            }            
        }

        /// <summary>
        /// Maakt een nieuwe caret (tekstcursor) aan op basis van de opgegeven SetCaretSize
        /// </summary>
        public void CreateNewCaret()
        {
            CreateCaret(this.Handle, IntPtr.Zero, _iCarWidth, _iCarHeight);
            ShowCaret(this.Handle);
        }

        /// <summary>
        /// Verbergt de caret (tekstcursor)
        /// </summary>
        public void HideCaret()
        {
            HideCaret(this.Handle);
        }

        /// <summary>
        /// Gets de positie van de horizontal scrollbar
        /// </summary>
        public int ScrollPos_H
        {
            get
            {
                iScrollPosHori = Convert.ToInt32((decimal)GetScrollPos(this.Handle, _H_ScrollBar) / (decimal)9);  //Haal de positie van de horizontale scrollbar op. Vraag me niet waarom ik door 9 moet delen: Dat is empirisch vastgesteld!
                return iScrollPosHori;
            }
        }

        /// <summary>
        /// Sets de positie van de horizontale scrollbar. Let op! Hij telt op bij de huidige positie!
        /// </summary>
        public int ScrollAdd_H
        {
        set
            {
                iScrollPosHori = value;
                SetScrollPos(this.Handle, _H_ScrollBar, iScrollPosHori, true);      //Stel de gegeven positie van de scrollbar in
                SendMessage(this.Handle, _ScrollMessage, iScrollPosHori, 0);        //Deze regel is nodig om de bovenstaande door te voeren. Let op! In werkelijkheid telt hij de scrollpositie op bij de huidige
            }
        }
        
        /// <summary>
        /// Gets de positie van de verticale scrollbar.
        /// </summary>
        public int ScrollPos_V
        {
            get
            {
                iScrollPosVert = GetScrollPos(this.Handle, _V_ScrollBar);  //Haal de positie van de horizontale scrollbar op. Vraag me niet waarom ik door 9 moet delen: Dat is empirisch vastgesteld!
                return iScrollPosVert;
            }
        }

        /// <summary>
        /// Sets de positie van de verticale scrollbar. Let op! Hij telt op bij de huidige positie!
        /// </summary>
        public int ScrollAdd_V
        {        
            set
            {
                iScrollPosVert = value;
                SetScrollPos(this.Handle, _V_ScrollBar, iScrollPosVert, true);      //Stel de gegeven positie van de scrollbar in
                SendMessage(this.Handle, _ScrollMessage, 0, iScrollPosVert);        //Deze regel is nodig om de bovenstaande door te voeren. Let op! In werkelijkheid telt hij de scrollpositie op bij de huidige
            }
        }
    }

    /// <summary>
    /// Tekstbox die alleen numerieke input accepteert. Is afgeleid van System.Windows.Forms.TextBox
    /// </summary>
    public class NumericTextBox : System.Windows.Forms.TextBox
    {   //Dit is een gekopieerde klasse die ik toevoeg om een numerieke tekstbox te maken voor het invullen van het ID van het template

        protected override void OnKeyPress(System.Windows.Forms.KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            //NumberFormatInfo numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;                        

            string keyInput = e.KeyChar.ToString();

            if (Char.IsDigit(e.KeyChar) && this.Text.Length < 9)
            {
                // Digits are OK, als er ruimte over is
            }
            else if (e.KeyChar == '\b')
            {
                // Backspace key is OK
            }
            else
            {
                // Consume this invalid key and beep
                e.Handled = true;
                //    MessageBeep();
            }
        }

        /// <summary>
        /// Get en sets de waarde van de tekstbox in integerformaat. Accepteert alleen integers, maar schrijft deze als string naar de tekstbox.
        /// </summary>
        public int IntValue
        {
            get
            {
                if (this.Text.Length > 0)
                {
                    return Int32.Parse(this.Text);
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                if (value > -1)
                {
                    this.Text = value.ToString();
                }
                else
                {   //We willen -1 niet afbeelden in de tekstbox.
                    this.Text = string.Empty;
                }
            }
        }
   
    }

    /// <summary>
    /// Een checkbox die afgeleid is van de ToolStripButton welke het gedrag van een checkbox heeft. Extra: Methode Checked; Members hImageChecked & hImageUnchecked;
    /// </summary>
    public class ToolStripCheckbox : System.Windows.Forms.ToolStripButton
    {
        private bool _Checked;
        private IntPtr _hImageChecked = IntPtr.Zero;
        private IntPtr _hImageUnchecked = IntPtr.Zero;

        /// <summary>
        /// Getoonde afbeeldinge bij UNcheck. Geef een handle naar een bitmap op. B.v.: ((System.Drawing.Bitmap)(resources.GetObject("Voorbeeld.Image"))).GetHbitmap();
        /// </summary>
        public IntPtr hImageChecked
        {
            set
            {
                _hImageChecked = value;
            }
        }

        /// <summary>
        /// Getoonde afbeeldinge bij CHECK. Geef een handle naar een bitmap op. B.v.: ((System.Drawing.Bitmap)(resources.GetObject("Voorbeeld.Image"))).GetHbitmap();
        /// </summary>
        public IntPtr hImageUnchecked
        {
            set
            {
                _hImageUnchecked = value;
            }
        }
        
        /// <summary>
        /// Get: Checked? ;  Set: (Un)Check! en stelt het opgegeven plaatje in; hImageChecked of hImageUnchecked
        /// </summary>
        new public bool Checked
        {
            get
            {
                return _Checked;                                
            }
            set
            {
                _Checked = value;
                if (value)
                {
                    if (_hImageChecked != IntPtr.Zero)
                    {
                        this.Image = System.Drawing.Image.FromHbitmap(_hImageChecked);
                    }
                }
                else
                {
                    if (_hImageUnchecked != IntPtr.Zero)
                    {
                        this.Image = System.Drawing.Image.FromHbitmap(_hImageUnchecked);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Een checkbox die afgeleid is van de ToolStripMenuItem welke het gedrag van een checkbox heeft. Extra: Methode Checked; Members hImageChecked & hImageUnchecked;
    /// </summary>
    public class ToolStripMenuCheckbox : System.Windows.Forms.ToolStripMenuItem
    {
        private bool _Checked;
        private IntPtr _hImageChecked;
        private IntPtr _hImageUnchecked;

        /// <summary>
        /// Getoonde afbeeldinge bij UNcheck. Geef een handle naar een bitmap op. B.v.: ((System.Drawing.Bitmap)(resources.GetObject("Voorbeeld.Image"))).GetHbitmap();
        /// </summary>
        public IntPtr hImageChecked
        {
            set
            {
                _hImageChecked = value;
            }
        }

        /// <summary>
        /// Getoonde afbeeldinge bij CHECK. Geef een handle naar een bitmap op. B.v.: ((System.Drawing.Bitmap)(resources.GetObject("Voorbeeld.Image"))).GetHbitmap();
        /// </summary>
        public IntPtr hImageUnchecked
        {
            set
            {
                _hImageUnchecked = value;
            }
        }

        /// <summary>
        /// Get: Checked? ;  Set: (Un)Check! en stelt het opgegeven plaatje in; hImageChecked of hImageUnchecked
        /// </summary>
        new public bool Checked
        {
            get
            {
                return _Checked;
            }
            set
            {
                _Checked = value;
                if (value)
                {
                    if (_hImageChecked != IntPtr.Zero)
                    {
                        this.Image = System.Drawing.Image.FromHbitmap(_hImageChecked);
                    }
                }
                else
                {
                    if (_hImageUnchecked != IntPtr.Zero)
                    {
                        this.Image = System.Drawing.Image.FromHbitmap(_hImageUnchecked);
                    }
                }
            }
        }
    }

    /// <summary>
    /// A datagridview that is not sortable. Nothing happens when a columnheader is clicked.
    /// </summary>
    public class UnSortableDGV : System.Windows.Forms.DataGridView
    {
        protected override void OnColumnHeaderMouseClick(System.Windows.Forms.DataGridViewCellMouseEventArgs e)
        {        
            //By not executing the usual event, nothing happens. This is the most simple way I could find to make sure the DGV is unsortable!!!
            //base.OnColumnHeaderMouseClick(e);
        }
    }
}
