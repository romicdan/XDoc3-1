namespace XDocHQ
{
    partial class HistoryPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpQueries = new System.Windows.Forms.GroupBox();
            this.dgvQueries = new System.Windows.Forms.DataGridView();
            this.grpDetails = new System.Windows.Forms.GroupBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.txtDocType = new System.Windows.Forms.TextBox();
            this.lblDocType = new System.Windows.Forms.Label();
            this.txtRights = new System.Windows.Forms.TextBox();
            this.lblRights = new System.Windows.Forms.Label();
            this.chkDataTemplate = new System.Windows.Forms.CheckBox();
            this.chkUnattended = new System.Windows.Forms.CheckBox();
            this.chkToSave = new System.Windows.Forms.CheckBox();
            this.txtRedirect = new System.Windows.Forms.TextBox();
            this.lblRedirect = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rtbContent = new System.Windows.Forms.TextBox();
            this.grpHistory = new System.Windows.Forms.GroupBox();
            this.txtEndDate = new System.Windows.Forms.TextBox();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.txtHistoryID = new System.Windows.Forms.TextBox();
            this.lblHistoryID = new System.Windows.Forms.Label();
            this.chkActual = new System.Windows.Forms.CheckBox();
            this.txtEndUser = new System.Windows.Forms.TextBox();
            this.lblEndUser = new System.Windows.Forms.Label();
            this.txtStartUser = new System.Windows.Forms.TextBox();
            this.lblStartUser = new System.Windows.Forms.Label();
            this.txtStartDate = new System.Windows.Forms.TextBox();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.grpQueries.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQueries)).BeginInit();
            this.grpDetails.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.grpHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpQueries
            // 
            this.grpQueries.AutoSize = true;
            this.grpQueries.Controls.Add(this.dgvQueries);
            this.grpQueries.Location = new System.Drawing.Point(3, 223);
            this.grpQueries.MinimumSize = new System.Drawing.Size(743, 0);
            this.grpQueries.Name = "grpQueries";
            this.grpQueries.Size = new System.Drawing.Size(743, 171);
            this.grpQueries.TabIndex = 1;
            this.grpQueries.TabStop = false;
            this.grpQueries.Text = "Template queries";
            // 
            // dgvQueries
            // 
            this.dgvQueries.AllowUserToAddRows = false;
            this.dgvQueries.AllowUserToDeleteRows = false;
            this.dgvQueries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQueries.Location = new System.Drawing.Point(6, 19);
            this.dgvQueries.Name = "dgvQueries";
            this.dgvQueries.ReadOnly = true;
            this.dgvQueries.RowHeadersWidth = 20;
            this.dgvQueries.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvQueries.RowTemplate.ReadOnly = true;
            this.dgvQueries.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvQueries.Size = new System.Drawing.Size(728, 133);
            this.dgvQueries.TabIndex = 24;
            this.dgvQueries.DoubleClick += new System.EventHandler(this.dgvQueries_DoubleClick);
            // 
            // grpDetails
            // 
            this.grpDetails.AutoSize = true;
            this.grpDetails.Controls.Add(this.txtID);
            this.grpDetails.Controls.Add(this.lblID);
            this.grpDetails.Controls.Add(this.txtDocType);
            this.grpDetails.Controls.Add(this.lblDocType);
            this.grpDetails.Controls.Add(this.txtRights);
            this.grpDetails.Controls.Add(this.lblRights);
            this.grpDetails.Controls.Add(this.chkDataTemplate);
            this.grpDetails.Controls.Add(this.chkUnattended);
            this.grpDetails.Controls.Add(this.chkToSave);
            this.grpDetails.Controls.Add(this.txtRedirect);
            this.grpDetails.Controls.Add(this.lblRedirect);
            this.grpDetails.Controls.Add(this.txtDescription);
            this.grpDetails.Controls.Add(this.lblDescription);
            this.grpDetails.Controls.Add(this.txtName);
            this.grpDetails.Controls.Add(this.lblName);
            this.grpDetails.Location = new System.Drawing.Point(3, 87);
            this.grpDetails.MaximumSize = new System.Drawing.Size(0, 130);
            this.grpDetails.MinimumSize = new System.Drawing.Size(743, 130);
            this.grpDetails.Name = "grpDetails";
            this.grpDetails.Size = new System.Drawing.Size(743, 130);
            this.grpDetails.TabIndex = 0;
            this.grpDetails.TabStop = false;
            this.grpDetails.Text = "Template properties";
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.Color.LightGray;
            this.txtID.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtID.Location = new System.Drawing.Point(464, 19);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(108, 21);
            this.txtID.TabIndex = 16;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.BackColor = System.Drawing.Color.LightGray;
            this.lblID.Location = new System.Drawing.Point(438, 22);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(18, 13);
            this.lblID.TabIndex = 13;
            this.lblID.Text = "ID";
            this.lblID.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtDocType
            // 
            this.txtDocType.BackColor = System.Drawing.Color.LightGray;
            this.txtDocType.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtDocType.Location = new System.Drawing.Point(464, 99);
            this.txtDocType.Name = "txtDocType";
            this.txtDocType.ReadOnly = true;
            this.txtDocType.Size = new System.Drawing.Size(108, 21);
            this.txtDocType.TabIndex = 20;
            // 
            // lblDocType
            // 
            this.lblDocType.AutoSize = true;
            this.lblDocType.BackColor = System.Drawing.Color.LightGray;
            this.lblDocType.Location = new System.Drawing.Point(402, 102);
            this.lblDocType.Name = "lblDocType";
            this.lblDocType.Size = new System.Drawing.Size(54, 13);
            this.lblDocType.TabIndex = 11;
            this.lblDocType.Text = "Doc Type";
            // 
            // txtRights
            // 
            this.txtRights.BackColor = System.Drawing.Color.LightGray;
            this.txtRights.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtRights.Location = new System.Drawing.Point(74, 98);
            this.txtRights.Name = "txtRights";
            this.txtRights.ReadOnly = true;
            this.txtRights.Size = new System.Drawing.Size(322, 21);
            this.txtRights.TabIndex = 19;
            // 
            // lblRights
            // 
            this.lblRights.AutoSize = true;
            this.lblRights.Location = new System.Drawing.Point(8, 101);
            this.lblRights.Name = "lblRights";
            this.lblRights.Size = new System.Drawing.Size(37, 13);
            this.lblRights.TabIndex = 9;
            this.lblRights.Text = "Rights";
            // 
            // chkDataTemplate
            // 
            this.chkDataTemplate.AutoSize = true;
            this.chkDataTemplate.BackColor = System.Drawing.Color.LightGray;
            this.chkDataTemplate.Enabled = false;
            this.chkDataTemplate.Location = new System.Drawing.Point(605, 101);
            this.chkDataTemplate.Name = "chkDataTemplate";
            this.chkDataTemplate.Size = new System.Drawing.Size(92, 17);
            this.chkDataTemplate.TabIndex = 23;
            this.chkDataTemplate.Text = "Data template";
            this.chkDataTemplate.UseVisualStyleBackColor = false;
            // 
            // chkUnattended
            // 
            this.chkUnattended.AutoSize = true;
            this.chkUnattended.BackColor = System.Drawing.Color.LightGray;
            this.chkUnattended.Enabled = false;
            this.chkUnattended.Location = new System.Drawing.Point(605, 61);
            this.chkUnattended.Name = "chkUnattended";
            this.chkUnattended.Size = new System.Drawing.Size(82, 17);
            this.chkUnattended.TabIndex = 22;
            this.chkUnattended.Text = "Unattended";
            this.chkUnattended.UseVisualStyleBackColor = false;
            // 
            // chkToSave
            // 
            this.chkToSave.AutoSize = true;
            this.chkToSave.BackColor = System.Drawing.Color.LightGray;
            this.chkToSave.Enabled = false;
            this.chkToSave.Location = new System.Drawing.Point(605, 22);
            this.chkToSave.Name = "chkToSave";
            this.chkToSave.Size = new System.Drawing.Size(65, 17);
            this.chkToSave.TabIndex = 21;
            this.chkToSave.Text = "To save";
            this.chkToSave.UseVisualStyleBackColor = false;
            // 
            // txtRedirect
            // 
            this.txtRedirect.BackColor = System.Drawing.Color.LightGray;
            this.txtRedirect.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtRedirect.Location = new System.Drawing.Point(74, 72);
            this.txtRedirect.Name = "txtRedirect";
            this.txtRedirect.ReadOnly = true;
            this.txtRedirect.Size = new System.Drawing.Size(498, 21);
            this.txtRedirect.TabIndex = 18;
            // 
            // lblRedirect
            // 
            this.lblRedirect.AutoSize = true;
            this.lblRedirect.Location = new System.Drawing.Point(8, 75);
            this.lblRedirect.Name = "lblRedirect";
            this.lblRedirect.Size = new System.Drawing.Size(47, 13);
            this.lblRedirect.TabIndex = 4;
            this.lblRedirect.Text = "Redirect";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.Color.LightGray;
            this.txtDescription.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtDescription.Location = new System.Drawing.Point(74, 46);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new System.Drawing.Size(498, 21);
            this.txtDescription.TabIndex = 17;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(8, 49);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 2;
            this.lblDescription.Text = "Description";
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.LightGray;
            this.txtName.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtName.Location = new System.Drawing.Point(74, 20);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(322, 21);
            this.txtName.TabIndex = 15;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(8, 23);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rtbContent);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.grpHistory);
            this.splitContainer1.Panel2.Controls.Add(this.grpDetails);
            this.splitContainer1.Panel2.Controls.Add(this.grpQueries);
            this.splitContainer1.Size = new System.Drawing.Size(749, 720);
            this.splitContainer1.SplitterDistance = 335;
            this.splitContainer1.TabIndex = 8;
            // 
            // rtbContent
            // 
            this.rtbContent.AcceptsReturn = true;
            this.rtbContent.AcceptsTab = true;
            this.rtbContent.BackColor = System.Drawing.Color.LightGray;
            this.rtbContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbContent.Font = new System.Drawing.Font("Courier New", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rtbContent.HideSelection = false;
            this.rtbContent.Location = new System.Drawing.Point(0, 0);
            this.rtbContent.Margin = new System.Windows.Forms.Padding(0);
            this.rtbContent.MaxLength = 99999999;                                        
            this.rtbContent.Multiline = true;
            this.rtbContent.Name = "rtbContent";
            this.rtbContent.ReadOnly = true;
            this.rtbContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.rtbContent.Size = new System.Drawing.Size(749, 335);
            this.rtbContent.TabIndex = 7;
            this.rtbContent.WordWrap = false;
            this.rtbContent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtbContent_KeyDown);
            // 
            // grpHistory
            // 
            this.grpHistory.AutoSize = true;
            this.grpHistory.Controls.Add(this.txtEndDate);
            this.grpHistory.Controls.Add(this.lblEndDate);
            this.grpHistory.Controls.Add(this.txtHistoryID);
            this.grpHistory.Controls.Add(this.lblHistoryID);
            this.grpHistory.Controls.Add(this.chkActual);
            this.grpHistory.Controls.Add(this.txtEndUser);
            this.grpHistory.Controls.Add(this.lblEndUser);
            this.grpHistory.Controls.Add(this.txtStartUser);
            this.grpHistory.Controls.Add(this.lblStartUser);
            this.grpHistory.Controls.Add(this.txtStartDate);
            this.grpHistory.Controls.Add(this.lblStartDate);
            this.grpHistory.Location = new System.Drawing.Point(3, 3);
            this.grpHistory.MaximumSize = new System.Drawing.Size(0, 78);
            this.grpHistory.MinimumSize = new System.Drawing.Size(743, 78);
            this.grpHistory.Name = "grpHistory";
            this.grpHistory.Size = new System.Drawing.Size(743, 78);
            this.grpHistory.TabIndex = 24;
            this.grpHistory.TabStop = false;
            this.grpHistory.Text = "History properties";
            // 
            // txtEndDate
            // 
            this.txtEndDate.BackColor = System.Drawing.Color.LightGray;
            this.txtEndDate.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtEndDate.Location = new System.Drawing.Point(339, 20);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.ReadOnly = true;
            this.txtEndDate.Size = new System.Drawing.Size(195, 21);
            this.txtEndDate.TabIndex = 16;
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.BackColor = System.Drawing.Color.LightGray;
            this.lblEndDate.Location = new System.Drawing.Point(283, 23);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(50, 13);
            this.lblEndDate.TabIndex = 13;
            this.lblEndDate.Text = "End date";
            this.lblEndDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtHistoryID
            // 
            this.txtHistoryID.BackColor = System.Drawing.Color.LightGray;
            this.txtHistoryID.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtHistoryID.Location = new System.Drawing.Point(605, 19);
            this.txtHistoryID.Name = "txtHistoryID";
            this.txtHistoryID.ReadOnly = true;
            this.txtHistoryID.Size = new System.Drawing.Size(113, 21);
            this.txtHistoryID.TabIndex = 20;
            // 
            // lblHistoryID
            // 
            this.lblHistoryID.AutoSize = true;
            this.lblHistoryID.BackColor = System.Drawing.Color.LightGray;
            this.lblHistoryID.Location = new System.Drawing.Point(545, 24);
            this.lblHistoryID.Name = "lblHistoryID";
            this.lblHistoryID.Size = new System.Drawing.Size(53, 13);
            this.lblHistoryID.TabIndex = 11;
            this.lblHistoryID.Text = "History ID";
            // 
            // chkActual
            // 
            this.chkActual.AutoSize = true;
            this.chkActual.BackColor = System.Drawing.Color.LightGray;
            this.chkActual.Enabled = false;
            this.chkActual.Location = new System.Drawing.Point(605, 49);
            this.chkActual.Name = "chkActual";
            this.chkActual.Size = new System.Drawing.Size(56, 17);
            this.chkActual.TabIndex = 23;
            this.chkActual.Text = "Actual";
            this.chkActual.UseVisualStyleBackColor = false;
            // 
            // txtEndUser
            // 
            this.txtEndUser.BackColor = System.Drawing.Color.LightGray;
            this.txtEndUser.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtEndUser.Location = new System.Drawing.Point(339, 45);
            this.txtEndUser.Name = "txtEndUser";
            this.txtEndUser.ReadOnly = true;
            this.txtEndUser.Size = new System.Drawing.Size(195, 21);
            this.txtEndUser.TabIndex = 18;
            // 
            // lblEndUser
            // 
            this.lblEndUser.AutoSize = true;
            this.lblEndUser.Location = new System.Drawing.Point(283, 50);
            this.lblEndUser.Name = "lblEndUser";
            this.lblEndUser.Size = new System.Drawing.Size(49, 13);
            this.lblEndUser.TabIndex = 4;
            this.lblEndUser.Text = "End user";
            // 
            // txtStartUser
            // 
            this.txtStartUser.BackColor = System.Drawing.Color.LightGray;
            this.txtStartUser.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtStartUser.Location = new System.Drawing.Point(74, 46);
            this.txtStartUser.Name = "txtStartUser";
            this.txtStartUser.ReadOnly = true;
            this.txtStartUser.Size = new System.Drawing.Size(195, 21);
            this.txtStartUser.TabIndex = 17;
            // 
            // lblStartUser
            // 
            this.lblStartUser.AutoSize = true;
            this.lblStartUser.Location = new System.Drawing.Point(8, 49);
            this.lblStartUser.Name = "lblStartUser";
            this.lblStartUser.Size = new System.Drawing.Size(52, 13);
            this.lblStartUser.TabIndex = 2;
            this.lblStartUser.Text = "Start user";
            // 
            // txtStartDate
            // 
            this.txtStartDate.BackColor = System.Drawing.Color.LightGray;
            this.txtStartDate.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtStartDate.Location = new System.Drawing.Point(74, 20);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.ReadOnly = true;
            this.txtStartDate.Size = new System.Drawing.Size(195, 21);
            this.txtStartDate.TabIndex = 15;
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(8, 23);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(53, 13);
            this.lblStartDate.TabIndex = 0;
            this.lblStartDate.Text = "Start date";
            // 
            // HistoryPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 720);
            this.Controls.Add(this.splitContainer1);
            this.KeyPreview = true;
            this.Name = "HistoryPreview";
            this.Text = "Preview template history";
            this.Load += new System.EventHandler(this.HistoryPreview_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.HistoryPreview_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HistoryPreview_KeyDown);
            this.ResizeEnd += new System.EventHandler(this.HistoryPreview_ResizeEnd);
            this.grpQueries.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQueries)).EndInit();
            this.grpDetails.ResumeLayout(false);
            this.grpDetails.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.grpHistory.ResumeLayout(false);
            this.grpHistory.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.GroupBox grpDetails;
        private System.Windows.Forms.GroupBox grpQueries;
        private System.Windows.Forms.DataGridView dgvQueries;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.CheckBox chkDataTemplate;
        private System.Windows.Forms.CheckBox chkUnattended;
        private System.Windows.Forms.CheckBox chkToSave;
        private System.Windows.Forms.TextBox txtRedirect;
        private System.Windows.Forms.Label lblRedirect;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtRights;
        private System.Windows.Forms.Label lblRights;
        private System.Windows.Forms.TextBox txtDocType;
        private System.Windows.Forms.Label lblDocType;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox grpHistory;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.TextBox txtHistoryID;
        private System.Windows.Forms.Label lblHistoryID;
        private System.Windows.Forms.CheckBox chkActual;
        private System.Windows.Forms.TextBox txtEndUser;
        private System.Windows.Forms.Label lblEndUser;
        private System.Windows.Forms.TextBox txtStartUser;
        private System.Windows.Forms.Label lblStartUser;
        private System.Windows.Forms.TextBox txtStartDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.TextBox rtbContent;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtEndDate;
    }
}