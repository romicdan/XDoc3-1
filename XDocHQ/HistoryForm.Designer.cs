namespace XDocHQ
{
    partial class HistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBeschrijvingBegin = new System.Windows.Forms.Label();
            this.dgvHistory = new System.Windows.Forms.DataGridView();
            this.lblBeschrijvingEind = new System.Windows.Forms.Label();
            this.btnRestore = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblAantalHist = new System.Windows.Forms.Label();
            this.lblNiksGevonden = new System.Windows.Forms.Label();
            this.btnToon = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBeschrijvingBegin
            // 
            this.lblBeschrijvingBegin.AutoSize = true;
            this.lblBeschrijvingBegin.Location = new System.Drawing.Point(12, 9);
            this.lblBeschrijvingBegin.Name = "lblBeschrijvingBegin";
            this.lblBeschrijvingBegin.Size = new System.Drawing.Size(186, 13);
            this.lblBeschrijvingBegin.TabIndex = 4;
            this.lblBeschrijvingBegin.Text = "Here the history is shown of template: ";
            // 
            // dgvHistory
            // 
            this.dgvHistory.AllowUserToAddRows = false;
            this.dgvHistory.AllowUserToDeleteRows = false;
            this.dgvHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvHistory.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistory.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvHistory.Location = new System.Drawing.Point(15, 30);
            this.dgvHistory.Margin = new System.Windows.Forms.Padding(40);
            this.dgvHistory.MultiSelect = false;
            this.dgvHistory.Name = "dgvHistory";
            this.dgvHistory.RowHeadersVisible = false;
            this.dgvHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHistory.ShowEditingIcon = false;
            this.dgvHistory.Size = new System.Drawing.Size(584, 175);
            this.dgvHistory.TabIndex = 2;
            this.dgvHistory.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgvHistory_MouseDoubleClick);
            this.dgvHistory.SelectionChanged += new System.EventHandler(this.dgvHistory_SelectionChanged);
            // 
            // lblBeschrijvingEind
            // 
            this.lblBeschrijvingEind.AutoSize = true;
            this.lblBeschrijvingEind.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBeschrijvingEind.Location = new System.Drawing.Point(246, 9);
            this.lblBeschrijvingEind.Margin = new System.Windows.Forms.Padding(0);
            this.lblBeschrijvingEind.Name = "lblBeschrijvingEind";
            this.lblBeschrijvingEind.Size = new System.Drawing.Size(11, 13);
            this.lblBeschrijvingEind.TabIndex = 5;
            this.lblBeschrijvingEind.Text = ".";
            // 
            // btnRestore
            // 
            this.btnRestore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRestore.Location = new System.Drawing.Point(116, 286);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(98, 25);
            this.btnRestore.TabIndex = 6;
            this.btnRestore.Text = "&Restore";
            this.btnRestore.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRestore.UseVisualStyleBackColor = true;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(220, 286);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(98, 25);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblAantalHist
            // 
            this.lblAantalHist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAantalHist.AutoSize = true;
            this.lblAantalHist.Location = new System.Drawing.Point(352, 292);
            this.lblAantalHist.Name = "lblAantalHist";
            this.lblAantalHist.Size = new System.Drawing.Size(183, 13);
            this.lblAantalHist.TabIndex = 8;
            this.lblAantalHist.Text = "X historical records have been found.";
            this.lblAantalHist.Visible = false;
            // 
            // lblNiksGevonden
            // 
            this.lblNiksGevonden.AutoSize = true;
            this.lblNiksGevonden.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNiksGevonden.Location = new System.Drawing.Point(30, 52);
            this.lblNiksGevonden.Name = "lblNiksGevonden";
            this.lblNiksGevonden.Size = new System.Drawing.Size(314, 15);
            this.lblNiksGevonden.TabIndex = 9;
            this.lblNiksGevonden.Text = "The current template does not have any history available.";
            this.lblNiksGevonden.Visible = false;
            // 
            // btnToon
            // 
            this.btnToon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnToon.Location = new System.Drawing.Point(12, 286);
            this.btnToon.Name = "btnToon";
            this.btnToon.Size = new System.Drawing.Size(98, 25);
            this.btnToon.TabIndex = 3;
            this.btnToon.Text = "&Show";
            this.btnToon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnToon.UseVisualStyleBackColor = true;
            this.btnToon.Click += new System.EventHandler(this.btnToon_Click);
            // 
            // HistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(738, 323);
            this.Controls.Add(this.lblBeschrijvingEind);
            this.Controls.Add(this.lblNiksGevonden);
            this.Controls.Add(this.lblBeschrijvingBegin);
            this.Controls.Add(this.lblAantalHist);
            this.Controls.Add(this.dgvHistory);
            this.Controls.Add(this.btnToon);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRestore);
            this.Name = "HistoryForm";
            this.Text = "History";
            this.Load += new System.EventHandler(this.HistoryForm_Load);
            this.Resize += new System.EventHandler(this.HistoryForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Label lblBeschrijvingBegin;
        private System.Windows.Forms.DataGridView dgvHistory;
        private System.Windows.Forms.Label lblBeschrijvingEind;
        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblAantalHist;
        private System.Windows.Forms.Label lblNiksGevonden;
        private System.Windows.Forms.Button btnToon;
    }
}