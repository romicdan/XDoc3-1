using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using XDocuments;
using System.Collections;



namespace XDocHQ
{
    public partial class ConnectionsForm : Form
    {
        IDName[] m_connections = null;
        ArrayList m_arrConnections = new ArrayList();
        XDocManager m_manager = null;
        DataTable m_dataSource = null;

        public ConnectionsForm(IDName[] connections, XDocManager manager)
        {
            InitializeComponent();
            m_connections = connections;
            m_manager = manager;            
        }

        public IDName[] Connections
        {
            get { return m_connections; }
            set { m_connections = value; }
        }

        private void Connections_Load(object sender, EventArgs e)
        {
            m_dataSource = new DataTable();

            m_dataSource.Columns.Add("ID", typeof(int));
            m_dataSource.Columns.Add("Name", typeof(string));
            m_dataSource.Columns.Add("String", typeof(string));

            foreach (IDName idNameConn in m_connections)
            {
                IXConnection conn = m_manager.LoadConnection(idNameConn.ID);
                m_dataSource.Rows.Add(conn.ID, conn.Name, conn.ConnectionString);
                m_arrConnections.Add(conn); // used to cache the objects, to avoid loading them a second time, when they are modified and saved
            }

            dgvConnections.DataSource = m_dataSource;

            dgvConnections.Columns["String"].HeaderText = "Connection string";
            dgvConnections.Columns["String"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvConnections.Columns["ID"].ReadOnly = true;
            dgvConnections.Columns["ID"].Width = 60;
            dgvConnections.Columns["ID"].CellTemplate.Value = -1;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {           
            //We gaan eerst de template in het hoofdformulier opslaan, voordat we de connecties gaan wijzigen. Wijzigingen in de queries kunnen anders fout lopen.
            SaveTemplateMain();
            
            DataTable dt = dgvConnections.DataSource as DataTable;

            //Add new connections
            DataRow[] dv = dt.Select("ID is null");
            foreach (DataRow drv in dv)
            {
                IXConnection conn = m_manager.NewConnection();
                conn.ConnectionString = drv["String"].ToString();
                conn.Name = drv["Name"].ToString();
                conn.Save();
            }

            //Delete connections
            foreach (IDName idNameConn in m_connections)
            {
                dv = dt.Select("ID = " + idNameConn.ID.ToString());
                if (dv.Length == 0) // the connection has been deleted
                    m_manager.DeleteConnection(idNameConn.ID);
            }

            //Modify connections
            foreach (DataGridViewRow row in dgvConnections.Rows)
                
                if (row.Tag != null && row.Tag is int && (int)row.Tag == 1) // row is modified
                {
                    int id = -1;
                    int.TryParse(row.Cells["ID"].Value.ToString(), out id);                    
                    IXConnection conn = FindConnection(id);
                    if (conn != null)
                    {
                        conn.ConnectionString = row.Cells["String"].Value.ToString();
                        conn.Name = row.Cells["Name"].Value.ToString();
                        conn.Save();
                    }
                }

            //Create new connections list (this is faster to write, but it would have been better to track each connection and update this list
            m_connections = m_manager.GetConnectionsList();
        }

        private void SaveTemplateMain()
        {
            MainForm hoofdScherm = (MainForm)this.Owner; //Maak referentie naar MainForm
            if (hoofdScherm.m_dirty)
            {   //Save de template voordat het gecleard wordt!
                DialogResult dlgRes = MessageBox.Show("Save current template?\n\n"+ 
                                                        "Watch out!\nChanges in the template must be saved,\n"+
                                                        "before connections are being saved.\nIf you answer 'No' changes in the template will be lost.\n\n" +
                                                        "The reason for this is that you might have a connection selected in the query,\n"+
                                                        "which you have deleted in the mean time, using the connections form.", 
                                                        "Confirm save", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgRes == DialogResult.Yes)
                {
                    hoofdScherm.SaveTemplate();
                }
                else
                {
                    hoofdScherm.m_dirty = false;
                }
            }
        }

        private void dgvConnections_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dgvConnections.Rows[e.RowIndex].Tag = 1; // mark row as modified
        }

        IXConnection FindConnection(int id)
        {
            foreach (IXConnection conn in m_arrConnections)
                if (conn.ID == id)
                    return conn;
            return null;
        }

        
    }
}