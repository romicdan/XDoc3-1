namespace XDocHQ
{
    partial class DependenciesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvUsedBy = new System.Windows.Forms.DataGridView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblIsUsedBy1 = new System.Windows.Forms.Label();
            this.lblUses1 = new System.Windows.Forms.Label();
            this.lblIsUsedBy2 = new System.Windows.Forms.Label();
            this.lblUses2 = new System.Windows.Forms.Label();
            this.lblUitleg = new System.Windows.Forms.Label();
            this.dgvUses = new System.Windows.Forms.DataGridView();
            this.ContextMenuItemNewWindow = new System.Windows.Forms.MenuItem();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUses)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvUsedBy
            // 
            this.dgvUsedBy.AllowUserToAddRows = false;
            this.dgvUsedBy.AllowUserToDeleteRows = false;
            this.dgvUsedBy.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvUsedBy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsedBy.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvUsedBy.Location = new System.Drawing.Point(12, 37);
            this.dgvUsedBy.MultiSelect = false;
            this.dgvUsedBy.Name = "dgvUsedBy";
            this.dgvUsedBy.RowHeadersVisible = false;
            this.dgvUsedBy.ShowEditingIcon = false;
            this.dgvUsedBy.Size = new System.Drawing.Size(245, 346);
            this.dgvUsedBy.TabIndex = 0;
            this.dgvUsedBy.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvUsedBy_MouseClick);
            this.dgvUsedBy.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgvUsedBy_MouseDoubleClick);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(468, 406);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblIsUsedBy1
            // 
            this.lblIsUsedBy1.AutoSize = true;
            this.lblIsUsedBy1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsUsedBy1.Location = new System.Drawing.Point(12, 9);
            this.lblIsUsedBy1.Name = "lblIsUsedBy1";
            this.lblIsUsedBy1.Size = new System.Drawing.Size(105, 13);
            this.lblIsUsedBy1.TabIndex = 3;
            this.lblIsUsedBy1.Text = "The current template";
            // 
            // lblUses1
            // 
            this.lblUses1.AutoSize = true;
            this.lblUses1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUses1.Location = new System.Drawing.Point(295, 9);
            this.lblUses1.Name = "lblUses1";
            this.lblUses1.Size = new System.Drawing.Size(117, 13);
            this.lblUses1.TabIndex = 10;
            this.lblUses1.Text = "The current template    ";
            // 
            // lblIsUsedBy2
            // 
            this.lblIsUsedBy2.AutoSize = true;
            this.lblIsUsedBy2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsUsedBy2.Location = new System.Drawing.Point(114, 9);
            this.lblIsUsedBy2.Name = "lblIsUsedBy2";
            this.lblIsUsedBy2.Size = new System.Drawing.Size(103, 13);
            this.lblIsUsedBy2.TabIndex = 6;
            this.lblIsUsedBy2.Text = "is being used by:";
            // 
            // lblUses2
            // 
            this.lblUses2.AutoSize = true;
            this.lblUses2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUses2.Location = new System.Drawing.Point(397, 9);
            this.lblUses2.Name = "lblUses2";
            this.lblUses2.Size = new System.Drawing.Size(37, 13);
            this.lblUses2.TabIndex = 7;
            this.lblUses2.Text = "uses:";
            // 
            // lblUitleg
            // 
            this.lblUitleg.AutoSize = true;
            this.lblUitleg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUitleg.Location = new System.Drawing.Point(12, 400);
            this.lblUitleg.Name = "lblUitleg";
            this.lblUitleg.Size = new System.Drawing.Size(323, 16);
            this.lblUitleg.TabIndex = 11;
            this.lblUitleg.Text = "Double-click a template to open it in XDocHQ.";
            // 
            // dgvUses
            // 
            this.dgvUses.AllowUserToAddRows = false;
            this.dgvUses.AllowUserToDeleteRows = false;
            this.dgvUses.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvUses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUses.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvUses.Location = new System.Drawing.Point(298, 37);
            this.dgvUses.MultiSelect = false;
            this.dgvUses.Name = "dgvUses";
            this.dgvUses.RowHeadersVisible = false;
            this.dgvUses.ShowEditingIcon = false;
            this.dgvUses.Size = new System.Drawing.Size(245, 346);
            this.dgvUses.TabIndex = 12;
            this.dgvUses.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvUses_MouseClick);
            this.dgvUses.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgvUses_MouseDoubleClick);
            // 
            // ContextMenuItemNewWindow
            // 
            this.ContextMenuItemNewWindow.Index = -1;
            this.ContextMenuItemNewWindow.Text = "Open template in new window.";
            this.ContextMenuItemNewWindow.Click += new System.EventHandler(this.ContextMenuItemNewWindow_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 416);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(316, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "Right mouse click to open it in a new window.";
            // 
            // DependenciesForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(555, 441);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvUses);
            this.Controls.Add(this.lblUitleg);
            this.Controls.Add(this.lblUses2);
            this.Controls.Add(this.lblIsUsedBy2);
            this.Controls.Add(this.lblUses1);
            this.Controls.Add(this.lblIsUsedBy1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.dgvUsedBy);
            this.KeyPreview = true;
            this.Name = "DependenciesForm";
            this.Text = "Dependencies";
            this.Load += new System.EventHandler(this.DependenciesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.DataGridView dgvUsedBy; //Linker Datagrid
        private System.Windows.Forms.DataGridView dgvUses; //Rechter Datagrid
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblIsUsedBy1;
        private System.Windows.Forms.Label lblUses1;
        private System.Windows.Forms.Label lblIsUsedBy2;
        private System.Windows.Forms.Label lblUses2;
        private System.Windows.Forms.Label lblUitleg;
        private System.Windows.Forms.MenuItem ContextMenuItemNewWindow;
        private System.Windows.Forms.Label label1;
        
    }
}