namespace XDocHQ
{
    partial class ReportsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportsForm));
            this.ddbReports = new System.Windows.Forms.ComboBox();
            this.lblTop = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.imgInfo = new System.Windows.Forms.PictureBox();
            this.dgvReport = new System.Windows.Forms.DataGridView();
            this.lblAantal = new System.Windows.Forms.Label();
            this.ContextMenuItemNewWindow = new System.Windows.Forms.MenuItem();
            this.lblNoData = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.SuspendLayout();
            // 
            // ddbReports
            // 
            this.ddbReports.FormattingEnabled = true;
            this.ddbReports.Location = new System.Drawing.Point(12, 25);
            this.ddbReports.MaxDropDownItems = 20;
            this.ddbReports.Name = "ddbReports";
            this.ddbReports.Size = new System.Drawing.Size(222, 21);
            this.ddbReports.TabIndex = 1;
            this.ddbReports.MouseHover += new System.EventHandler(this.ddbReports_MouseHover);
            this.ddbReports.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ddbReports_MouseClick);
            this.ddbReports.SelectedIndexChanged += new System.EventHandler(this.ddbReports_SelectedIndexChanged);
            this.ddbReports.GotFocus += new System.EventHandler(this.ddbReports_GotFocus);
            this.ddbReports.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ddbReports_KeyDown);
            // 
            // lblTop
            // 
            this.lblTop.AutoSize = true;
            this.lblTop.Location = new System.Drawing.Point(12, 9);
            this.lblTop.Name = "lblTop";
            this.lblTop.Size = new System.Drawing.Size(85, 13);
            this.lblTop.TabIndex = 2;
            this.lblTop.Text = "Choose a report:";
            // 
            // lblInfo
            // 
            this.lblInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInfo.AutoSize = true;
            this.lblInfo.Cursor = System.Windows.Forms.Cursors.Help;
            this.lblInfo.Location = new System.Drawing.Point(396, 28);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(28, 13);
            this.lblInfo.TabIndex = 11;
            this.lblInfo.Text = "Info:";
            this.lblInfo.Click += new System.EventHandler(this.imgInfo_Click);
            // 
            // imgInfo
            // 
            this.imgInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgInfo.Cursor = System.Windows.Forms.Cursors.Help;
            this.imgInfo.Image = ((System.Drawing.Image)(resources.GetObject("imgInfo.Image")));
            this.imgInfo.InitialImage = ((System.Drawing.Image)(resources.GetObject("imgInfo.InitialImage")));
            this.imgInfo.Location = new System.Drawing.Point(430, 23);
            this.imgInfo.Name = "imgInfo";
            this.imgInfo.Size = new System.Drawing.Size(20, 20);
            this.imgInfo.TabIndex = 10;
            this.imgInfo.TabStop = false;
            this.imgInfo.Tag = "";
            this.imgInfo.Click += new System.EventHandler(this.imgInfo_Click);
            // 
            // dgvReport
            // 
            this.dgvReport.AllowDrop = true;
            this.dgvReport.AllowUserToAddRows = false;
            this.dgvReport.AllowUserToDeleteRows = false;
            this.dgvReport.AllowUserToOrderColumns = true;
            this.dgvReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReport.Location = new System.Drawing.Point(0, 64);
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.ReadOnly = true;
            this.dgvReport.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvReport.Size = new System.Drawing.Size(462, 383);
            this.dgvReport.TabIndex = 12;
            this.dgvReport.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvReport_MouseClick);
            this.dgvReport.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReportsForm_KeyDown);
            // 
            // lblAantal
            // 
            this.lblAantal.AutoSize = true;
            this.lblAantal.Location = new System.Drawing.Point(240, 28);
            this.lblAantal.Name = "lblAantal";
            this.lblAantal.Size = new System.Drawing.Size(47, 13);
            this.lblAantal.TabIndex = 13;
            this.lblAantal.Text = "lblAantal";
            // 
            // ContextMenuItemNewWindow
            // 
            this.ContextMenuItemNewWindow.Index = -1;
            this.ContextMenuItemNewWindow.Text = "Open template in new window.";
            this.ContextMenuItemNewWindow.Click += new System.EventHandler(this.ContextMenuItemNewWindow_Click);
            // 
            // lblNoData
            // 
            this.lblNoData.AutoSize = true;
            this.lblNoData.Location = new System.Drawing.Point(46, 136);
            this.lblNoData.Name = "lblNoData";
            this.lblNoData.Size = new System.Drawing.Size(0, 13);
            this.lblNoData.TabIndex = 14;
            // 
            // ReportsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 447);
            this.Controls.Add(this.lblNoData);
            this.Controls.Add(this.lblAantal);
            this.Controls.Add(this.dgvReport);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.imgInfo);
            this.Controls.Add(this.lblTop);
            this.Controls.Add(this.ddbReports);
            this.Name = "ReportsForm";
            this.Text = "Reports";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReportsForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.imgInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Label lblTop;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.PictureBox imgInfo;
        private System.Windows.Forms.DataGridView dgvReport;
        private System.Windows.Forms.Label lblAantal;
        private System.Windows.Forms.ComboBox ddbReports;
        private System.Windows.Forms.MenuItem ContextMenuItemNewWindow;
        private System.Windows.Forms.Label lblNoData;
    }
}