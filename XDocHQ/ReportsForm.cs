using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections.Specialized;
using KubionDataNamespace;
using KubionLogNamespace;

namespace XDocHQ
{
    public partial class ReportsForm : Form
    {
        #region Members

            private NameValueCollection _DynReportsColl;    //Dit is de lijst met dynamische rapporten uit het XDocHQ.exe.config            
            private bool _suppressEvents = false;           //Bij het opbouwen mag SelectionChanged van ddBox niet af gaan.
            private int _IDinNewW = -2;                     //This is the templateID of the template that was click upon with the right mouse button. IF available!
            private bool _blnTemplateID = false;            //This tells wheter or not we have got a column named TemplateID. Only if so, we show the contextmenu to open template in new window
            private bool _HasHistTables = true;             //Do not show reports refering to history tables if these are absent.

        #endregion Memebers

        #region Methods

            public ReportsForm(ref NameValueCollection DynReportsColl, string initKey, bool HasHistTables)
            {
                _suppressEvents = true;
                _DynReportsColl = DynReportsColl;
                _HasHistTables = HasHistTables;
                                
                InitializeComponent();
                
                FillDropDown(initKey);      //Deze moet na InitializeComponent! Anders bestaat de ddbox nog niet!

                ShowReport(initKey);        //Het eerste gekozen rapport meteen tonen.                

                _suppressEvents = false;

                ddbReports.SelectionLength = 0;

                MenuItem[] menuitems = { ContextMenuItemNewWindow };    
                dgvReport.ContextMenu = new ContextMenu(menuitems);     //Set the contextmenu of the rightmouseclick on the grid
            }

            private void FillDropDown(string initKey)
            {   //Vul de dropdownbox met de dynamische rapporten

                foreach (string key in _DynReportsColl.Keys)
                {
                    ddbReports.Items.Add(key);
                }
                
                ddbReports.SelectedItem = initKey;  //Maak de eerste selectie                                            
            }

            private void ShowReport(string key)
            {   
                ClientData cd = null;
                dgvReport.SuspendLayout();
                this.SuspendLayout();
                string strSQL = _DynReportsColl.GetValues(key)[0];

                try
                {
                    base.Cursor = Cursors.WaitCursor;  //Deze routine duurt lang, zodat we de cursor op zandloper willen zetten.                                    
                    lblAantal.Text = "";

                    System.Windows.Forms.SortOrder sortOrder = dgvReport.SortOrder;
                    DataGridViewColumn sortColumn = dgvReport.SortedColumn;                    
                                    
                    if (System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"] != null)
                    {
                        cd = new ClientData(System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"]); //Een KubionDataNamespaceobject dat gegevens kan ophalen                            


                        if (!_HasHistTables && (strSQL.ToLower().Contains("s_queries_hist") || strSQL.ToLower().Contains("s_templates_hist")))
                        {
                            dgvReport.Hide();
                            lblNoData.Show();
                            lblNoData.Text = "This report cannot be shown on this database,\n" +
                                                "because it references a history table (S_Queries_Hist or S_Templates_Hist),\n" +
                                                "which is not present in the current database.";                            
                        }
                        else
                        {
                            lblNoData.Hide();
                            dgvReport.Show();
                            dgvReport.DataSource = cd.DTExecuteQuery(strSQL);      //DataTable komt terug en kan getoond worden in datagrid
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error finding the connection string XDocConnectionString.", "Erron in config file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    //We zetten nog wat instellingen van het datagrid:                    
                    LijnDgvUit();
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    MessageBox.Show("An error occured filling the data grid.\n\n" + ex.Message, "Error filling datagrid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {                    
                    base.Cursor = Cursors.Arrow;                    
                    if (cd != null)
                    {
                        cd.Dispose();
                    }
                    dgvReport.ResumeLayout();
                    this.ResumeLayout();
                    ddbReports.SelectionLength = 0;

                    if (dgvReport.Columns.Contains("templateid"))   //Tests show that this check is run CASE INsensitively!
                    {
                        _blnTemplateID = true;
                    }
                    else
                    {
                        _blnTemplateID = false;
                    }
                }
            }

            private void LijnDgvUit()    //We gaan het DGV uitlijnen en opmaken.
            {
                int i;
                int idgvprefwidth;
                int formwidth = this.Width;
                int screenright = Screen.GetWorkingArea(this).Right;
                int iRows = dgvReport.Rows.Count;
                int iColumns = dgvReport.Columns.Count;

                for (i = 0; i < iColumns; i++)
                {
                    dgvReport.Columns[i].Width = dgvReport.Columns[i].HeaderText.Length * 6 + 11;
                }

                idgvprefwidth = dgvReport.PreferredSize.Width;

                if (idgvprefwidth > 470)
                {
                    this.Width = idgvprefwidth;

                    if (screenright < this.Right)
                    {
                        this.Width = screenright - this.Left;
                    }
                }
                else
                {
                    this.Width = 470;
                }

                if (iRows != 1)
                {
                    lblAantal.Text = iRows + " rows have been retrieved.";
                }
                else
                {
                    lblAantal.Text = "1 row has been retrieved.";
                }                
            }

        #endregion Methods

        #region Event Handlers

            private void imgInfo_Click(object sender, EventArgs e)
            {
                MessageBox.Show("This screen shows the queries which have been defined in the XDocHQ.exe.config file.\n" +
                                    "Add a report yourself by opening XDocHQ.exe.config and adding an item to the section 'Rapporten'.\n" +
                                    "At Key add a name which will be shown in the menu.\n" +
                                    "At Value add a piece of SQL which returns a correct result on the current database.\n\n\n" +
                                    "The SQL which generates the current report is the following:\n\n" +
                                    _DynReportsColl.GetValues(ddbReports.SelectedItem.ToString())[0]                
                                    , "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            private void ddbReports_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.PageDown || e.KeyCode == Keys.PageUp)
                { }
                else if (e.KeyCode == Keys.Escape)
                {
                    this.Close();
                }
                else
                {   //Alleen bovenstaande toetsen mogen iets uitvoeren, de rest wordt afgevangen en gestopt. Zo kan er ook niks getiept worden in de ddb.
                    e.Handled = true;
                    e.SuppressKeyPress = true;
                    ddbReports.SelectionLength = 0;
                }
            }

            private void ddbReports_SelectedIndexChanged(object sender, EventArgs e)
            {                
                if (!_suppressEvents)
                {                    
                    ShowReport(ddbReports.SelectedItem.ToString());                    
                }
                ddbReports.SelectionLength = 0;
            }

            private void ddbReports_GotFocus(object sender, System.EventArgs e)
            {
                ddbReports.SelectionLength = 0;
            }

            private void ddbReports_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
            {
                ddbReports.DroppedDown = true;                
                base.Cursor = Cursors.Arrow;
                ddbReports.SelectionLength = 0;
            }

            private void ddbReports_MouseHover(object sender, System.EventArgs e)
            {
                base.Cursor = Cursors.Arrow;
                ddbReports.SelectionLength = 0;
            }

            private void ReportsForm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Escape)
                {
                    this.Close();
                }
            }

            private void dgvReport_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
            {
                if (_blnTemplateID && e.Button == System.Windows.Forms.MouseButtons.Right)
                {   //We hebben een right mouseclick, dus toon: Open in new window:                                        
                    DataGridView.HitTestInfo testInfo = dgvReport.HitTest(e.X, e.Y);    //We proberen een ID op te halen op basis van de positie van de muis 

                    if (testInfo.RowIndex > -1)
                    {   //There was no row found under the mouseposition, so there is no use in showing the contextmenu!
                        int.TryParse(dgvReport.Rows[testInfo.RowIndex].Cells["TemplateID"].Value.ToString(),out _IDinNewW);  //op het moment van de rechtermuisklik.
                        dgvReport.ContextMenu.Show(dgvReport, e.Location);
                    }
                }   
            }

            private void ContextMenuItemNewWindow_Click(object sender, System.EventArgs e)
            {
                if (_IDinNewW > -1)
                {   //Open een nieuwe XDocHQ, met commandlineparameter om het gewenste template in een nieuw window te laden.
                    System.Diagnostics.Process.Start("XDocHQ.exe", "templateid=" + _IDinNewW.ToString());
                    _IDinNewW = -2;     //Make sure you don't use the old ID the next time
                }
                else
                {
                    MessageBox.Show("Failed to retrieve a correct template ID under the last right mouse click.\nTry again.", "Failed to get ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
                  
        #endregion Event Handlers   
    }
}