namespace XDocHQ
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStripForm = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshTemplatelistToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStriptbxZoekterm = new System.Windows.Forms.ToolStripTextBox();
            this.zoekTerugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoekVerderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doorzoekAllesToolStripMenuItem = new XDocHQ.ToolStripMenuCheckbox();
            this.zoekEnVervangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoekoverzichtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.templateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dependenciesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openInVSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tryParseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openTemplateInNewWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deletedTemplatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnIncrFont = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDecrFont = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitTemplatesText = new System.Windows.Forms.SplitContainer();
            this.splitAlleRecente = new System.Windows.Forms.SplitContainer();
            this.splitLabelAlle = new System.Windows.Forms.SplitContainer();
            this.tlstrpAllTemplates = new System.Windows.Forms.ToolStrip();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.lblAlleTemplates = new System.Windows.Forms.ToolStripLabel();
            this.btnHideGrid = new System.Windows.Forms.ToolStripButton();
            this.btnExpandGrid = new System.Windows.Forms.ToolStripButton();
            this.dgvTemplates = new System.Windows.Forms.DataGridView();
            this.splitLabelRecente = new System.Windows.Forms.SplitContainer();
            this.lblRecTemplates = new System.Windows.Forms.Label();
            this.dgvRecTemplates = new XDocHQ.UnSortableDGV();
            this.splitTextQueries = new System.Windows.Forms.SplitContainer();
            this.cbxWordWrap = new System.Windows.Forms.CheckBox();
            this.lbColumn = new System.Windows.Forms.Label();
            this.lbLine = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.rtbContent = new XDocHQ.TextBoxPlus();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.btnNewTemplate = new System.Windows.Forms.ToolStripButton();
            this.btnVS = new System.Windows.Forms.ToolStripButton();
            this.btnTryParse = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnZoekVervang = new System.Windows.Forms.ToolStripButton();
            this.btnZoekForm = new System.Windows.Forms.ToolStripButton();
            this.rtbZoekterm = new System.Windows.Forms.ToolStripTextBox();
            this.btnZoekTerug = new System.Windows.Forms.ToolStripButton();
            this.btnZoekVerder = new System.Windows.Forms.ToolStripButton();
            this.btnDoorzoekAlles = new XDocHQ.ToolStripCheckbox();
            this.btnHideQueries = new System.Windows.Forms.Button();
            this.grpQueries = new System.Windows.Forms.GroupBox();
            this.btnDeleteQuery = new System.Windows.Forms.Button();
            this.btnEditQuery = new System.Windows.Forms.Button();
            this.btnAddQuery = new System.Windows.Forms.Button();
            this.dgvQueries = new System.Windows.Forms.DataGridView();
            this.grpDetails = new System.Windows.Forms.GroupBox();
            this.txtID = new XDocHQ.NumericTextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.txtDocType = new System.Windows.Forms.TextBox();
            this.lblDocType = new System.Windows.Forms.Label();
            this.txtRights = new System.Windows.Forms.TextBox();
            this.lblRights = new System.Windows.Forms.Label();
            this.chkDataTemplate = new System.Windows.Forms.CheckBox();
            this.chkUnattended = new System.Windows.Forms.CheckBox();
            this.chkToSave = new System.Windows.Forms.CheckBox();
            this.txtRedirect = new System.Windows.Forms.TextBox();
            this.lblRedirect = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescr = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ContextMenuItemNewWindow = new System.Windows.Forms.MenuItem();
            this.ContextMenuItemRefresh = new System.Windows.Forms.MenuItem();
            this.proceduresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripForm.SuspendLayout();
            this.splitTemplatesText.Panel1.SuspendLayout();
            this.splitTemplatesText.Panel2.SuspendLayout();
            this.splitTemplatesText.SuspendLayout();
            this.splitAlleRecente.Panel1.SuspendLayout();
            this.splitAlleRecente.Panel2.SuspendLayout();
            this.splitAlleRecente.SuspendLayout();
            this.splitLabelAlle.Panel1.SuspendLayout();
            this.splitLabelAlle.Panel2.SuspendLayout();
            this.splitLabelAlle.SuspendLayout();
            this.tlstrpAllTemplates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemplates)).BeginInit();
            this.splitLabelRecente.Panel1.SuspendLayout();
            this.splitLabelRecente.Panel2.SuspendLayout();
            this.splitLabelRecente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecTemplates)).BeginInit();
            this.splitTextQueries.Panel1.SuspendLayout();
            this.splitTextQueries.Panel2.SuspendLayout();
            this.splitTextQueries.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.grpQueries.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQueries)).BeginInit();
            this.grpDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripForm
            // 
            this.menuStripForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.templateToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.proceduresToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStripForm.Location = new System.Drawing.Point(0, 0);
            this.menuStripForm.Name = "menuStripForm";
            this.menuStripForm.Size = new System.Drawing.Size(1195, 24);
            this.menuStripForm.TabIndex = 1;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveTemplateToolStripMenuItem,
            this.refreshTemplatelistToolStripMenuItem,
            this.newTemplateToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // saveTemplateToolStripMenuItem
            // 
            this.saveTemplateToolStripMenuItem.Enabled = false;
            this.saveTemplateToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveTemplateToolStripMenuItem.Image")));
            this.saveTemplateToolStripMenuItem.Name = "saveTemplateToolStripMenuItem";
            this.saveTemplateToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.saveTemplateToolStripMenuItem.Text = "Save template";
            this.saveTemplateToolStripMenuItem.ToolTipText = "Save template";
            this.saveTemplateToolStripMenuItem.Click += new System.EventHandler(this.saveTemplateToolStripMenuItem_Click);
            // 
            // refreshTemplatelistToolStripMenuItem
            // 
            this.refreshTemplatelistToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("refreshTemplatelistToolStripMenuItem.Image")));
            this.refreshTemplatelistToolStripMenuItem.Name = "refreshTemplatelistToolStripMenuItem";
            this.refreshTemplatelistToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.refreshTemplatelistToolStripMenuItem.Text = "Refresh templatelist";
            this.refreshTemplatelistToolStripMenuItem.ToolTipText = "Refresh the template list";
            this.refreshTemplatelistToolStripMenuItem.Click += new System.EventHandler(this.refreshTemplatelistToolStripMenuItem_Click);
            // 
            // newTemplateToolStripMenuItem
            // 
            this.newTemplateToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newTemplateToolStripMenuItem.Image")));
            this.newTemplateToolStripMenuItem.Name = "newTemplateToolStripMenuItem";
            this.newTemplateToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.newTemplateToolStripMenuItem.Text = "New template";
            this.newTemplateToolStripMenuItem.ToolTipText = "Start a new template";
            this.newTemplateToolStripMenuItem.Click += new System.EventHandler(this.newTemplateToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.ToolTipText = "Exit XDocHQ";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStriptbxZoekterm,
            this.zoekTerugToolStripMenuItem,
            this.zoekVerderToolStripMenuItem,
            this.doorzoekAllesToolStripMenuItem,
            this.zoekEnVervangToolStripMenuItem,
            this.zoekoverzichtToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // toolStriptbxZoekterm
            // 
            this.toolStriptbxZoekterm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStriptbxZoekterm.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.toolStriptbxZoekterm.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.toolStriptbxZoekterm.Name = "toolStriptbxZoekterm";
            this.toolStriptbxZoekterm.Size = new System.Drawing.Size(100, 21);
            this.toolStriptbxZoekterm.Text = "Search term...";
            this.toolStriptbxZoekterm.TextChanged += new System.EventHandler(this.toolStriptbxZoekterm_TextChanged);
            this.toolStriptbxZoekterm.Click += new System.EventHandler(this.toolStriptbxZoekterm_Click);
            // 
            // zoekTerugToolStripMenuItem
            // 
            this.zoekTerugToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("zoekTerugToolStripMenuItem.Image")));
            this.zoekTerugToolStripMenuItem.Name = "zoekTerugToolStripMenuItem";
            this.zoekTerugToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.zoekTerugToolStripMenuItem.Text = "Search backwards";
            this.zoekTerugToolStripMenuItem.ToolTipText = "Search backwards, starting at the caret position";
            this.zoekTerugToolStripMenuItem.Click += new System.EventHandler(this.zoekTerugToolStripMenuItem_Click);
            // 
            // zoekVerderToolStripMenuItem
            // 
            this.zoekVerderToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("zoekVerderToolStripMenuItem.Image")));
            this.zoekVerderToolStripMenuItem.Name = "zoekVerderToolStripMenuItem";
            this.zoekVerderToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.zoekVerderToolStripMenuItem.Text = "Search forwards";
            this.zoekVerderToolStripMenuItem.ToolTipText = "Search forwards, starting at the caret position";
            this.zoekVerderToolStripMenuItem.Click += new System.EventHandler(this.zoekVerderToolStripMenuItem_Click);
            // 
            // doorzoekAllesToolStripMenuItem
            // 
            this.doorzoekAllesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("doorzoekAllesToolStripMenuItem.Image")));
            this.doorzoekAllesToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.doorzoekAllesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.doorzoekAllesToolStripMenuItem.Name = "doorzoekAllesToolStripMenuItem";
            this.doorzoekAllesToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.doorzoekAllesToolStripMenuItem.Text = "Search all templates";
            this.doorzoekAllesToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.doorzoekAllesToolStripMenuItem.ToolTipText = "Search all templates";
            this.doorzoekAllesToolStripMenuItem.Click += new System.EventHandler(this.btnDoorzoekAlles_Click);
            // 
            // zoekEnVervangToolStripMenuItem
            // 
            this.zoekEnVervangToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("zoekEnVervangToolStripMenuItem.Image")));
            this.zoekEnVervangToolStripMenuItem.Name = "zoekEnVervangToolStripMenuItem";
            this.zoekEnVervangToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.zoekEnVervangToolStripMenuItem.Text = "Search-and-Replace";
            this.zoekEnVervangToolStripMenuItem.ToolTipText = "Open the Search-and-Replace form";
            this.zoekEnVervangToolStripMenuItem.Click += new System.EventHandler(this.zoekEnVervangToolStripMenuItem_Click);
            // 
            // zoekoverzichtToolStripMenuItem
            // 
            this.zoekoverzichtToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("zoekoverzichtToolStripMenuItem.Image")));
            this.zoekoverzichtToolStripMenuItem.Name = "zoekoverzichtToolStripMenuItem";
            this.zoekoverzichtToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.zoekoverzichtToolStripMenuItem.Text = "Search overview";
            this.zoekoverzichtToolStripMenuItem.ToolTipText = "Get a list of all templates containing the search term";
            this.zoekoverzichtToolStripMenuItem.Click += new System.EventHandler(this.zoekoverzichtToolStripMenuItem_Click);
            // 
            // templateToolStripMenuItem
            // 
            this.templateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dependenciesToolStripMenuItem,
            this.openInVSToolStripMenuItem,
            this.tryParseToolStripMenuItem,
            this.historyToolStripMenuItem,
            this.openTemplateInNewWindowToolStripMenuItem});
            this.templateToolStripMenuItem.Name = "templateToolStripMenuItem";
            this.templateToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.templateToolStripMenuItem.Text = "&Template";
            // 
            // dependenciesToolStripMenuItem
            // 
            this.dependenciesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("dependenciesToolStripMenuItem.Image")));
            this.dependenciesToolStripMenuItem.Name = "dependenciesToolStripMenuItem";
            this.dependenciesToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.dependenciesToolStripMenuItem.Text = "Dependencies";
            this.dependenciesToolStripMenuItem.ToolTipText = "Get two lists of all templates the current template depends on or is being depend" +
                "ed on";
            this.dependenciesToolStripMenuItem.Click += new System.EventHandler(this.dependenciesToolStripMenuItem_Click);
            // 
            // openInVSToolStripMenuItem
            // 
            this.openInVSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openInVSToolStripMenuItem.Image")));
            this.openInVSToolStripMenuItem.Name = "openInVSToolStripMenuItem";
            this.openInVSToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.openInVSToolStripMenuItem.Text = "Open in Visual Studio";
            this.openInVSToolStripMenuItem.ToolTipText = "Open the current template text in Visual Studio";
            this.openInVSToolStripMenuItem.Click += new System.EventHandler(this.openInVSToolStripMenuItem_Click);
            // 
            // tryParseToolStripMenuItem
            // 
            this.tryParseToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("tryParseToolStripMenuItem.Image")));
            this.tryParseToolStripMenuItem.Name = "tryParseToolStripMenuItem";
            this.tryParseToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.tryParseToolStripMenuItem.Text = "TryParse ";
            this.tryParseToolStripMenuItem.ToolTipText = "Start Debugging (F5) - Template will be saved first";
            this.tryParseToolStripMenuItem.Click += new System.EventHandler(this.tryParseToolStripMenuItem_Click);
            // 
            // historyToolStripMenuItem
            // 
            this.historyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("historyToolStripMenuItem.Image")));
            this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
            this.historyToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.historyToolStripMenuItem.Text = "History";
            this.historyToolStripMenuItem.ToolTipText = "Show the history of the current template";
            this.historyToolStripMenuItem.Click += new System.EventHandler(this.historyToolStripMenuItem_Click);
            // 
            // openTemplateInNewWindowToolStripMenuItem
            // 
            this.openTemplateInNewWindowToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openTemplateInNewWindowToolStripMenuItem.Image")));
            this.openTemplateInNewWindowToolStripMenuItem.Name = "openTemplateInNewWindowToolStripMenuItem";
            this.openTemplateInNewWindowToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.openTemplateInNewWindowToolStripMenuItem.Text = "Open template in new window";
            this.openTemplateInNewWindowToolStripMenuItem.ToolTipText = "Open the current template in a new window";
            this.openTemplateInNewWindowToolStripMenuItem.Click += new System.EventHandler(this.openTemplateInNewWindowToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deletedTemplatesToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.reportsToolStripMenuItem.Text = "&Reports";
            // 
            // deletedTemplatesToolStripMenuItem
            // 
            this.deletedTemplatesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("deletedTemplatesToolStripMenuItem.Image")));
            this.deletedTemplatesToolStripMenuItem.Name = "deletedTemplatesToolStripMenuItem";
            this.deletedTemplatesToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.deletedTemplatesToolStripMenuItem.Text = "Deleted templates";
            this.deletedTemplatesToolStripMenuItem.ToolTipText = "Delete the selected template";
            this.deletedTemplatesToolStripMenuItem.Click += new System.EventHandler(this.deletedTemplatesToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionsToolStripMenuItem,
            this.btnIncrFont,
            this.btnDecrFont});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.configurationToolStripMenuItem.Text = "&Settings";
            // 
            // connectionsToolStripMenuItem
            // 
            this.connectionsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("connectionsToolStripMenuItem.Image")));
            this.connectionsToolStripMenuItem.Name = "connectionsToolStripMenuItem";
            this.connectionsToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.connectionsToolStripMenuItem.Text = "Co&nnections";
            this.connectionsToolStripMenuItem.ToolTipText = "Open the connections form";
            this.connectionsToolStripMenuItem.Click += new System.EventHandler(this.connectionsToolStripMenuItem_Click);
            // 
            // btnIncrFont
            // 
            this.btnIncrFont.Image = ((System.Drawing.Image)(resources.GetObject("btnIncrFont.Image")));
            this.btnIncrFont.Name = "btnIncrFont";
            this.btnIncrFont.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Oemplus)));
            this.btnIncrFont.ShowShortcutKeys = false;
            this.btnIncrFont.Size = new System.Drawing.Size(156, 22);
            this.btnIncrFont.Tag = "Shortcut: Ctrl++  OR  Ctrl+Mouse wheel scroll forwards";
            this.btnIncrFont.Text = "Increase font size";
            this.btnIncrFont.ToolTipText = "Shortcut: Ctrl++  OR  Ctrl+Mouse wheel scroll forwards";
            this.btnIncrFont.Click += new System.EventHandler(this.btnIncrFont_Click);
            // 
            // btnDecrFont
            // 
            this.btnDecrFont.Image = ((System.Drawing.Image)(resources.GetObject("btnDecrFont.Image")));
            this.btnDecrFont.Name = "btnDecrFont";
            this.btnDecrFont.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.OemMinus)));
            this.btnDecrFont.ShowShortcutKeys = false;
            this.btnDecrFont.Size = new System.Drawing.Size(156, 22);
            this.btnDecrFont.Tag = "Shortcut: Ctrl+-  OR  Ctrl+Mouse wheel scroll backwards";
            this.btnDecrFont.Text = "Decrease font size";
            this.btnDecrFont.ToolTipText = "Shortcut: Ctrl+-  OR  Ctrl+Mouse wheel scroll backwards";
            this.btnDecrFont.Click += new System.EventHandler(this.btnDecrFont_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.aboutToolStripMenuItem.Text = "Abou&t";
            this.aboutToolStripMenuItem.ToolTipText = "Show information about XDocHQ";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // splitTemplatesText
            // 
            this.splitTemplatesText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitTemplatesText.Location = new System.Drawing.Point(0, 24);
            this.splitTemplatesText.Name = "splitTemplatesText";
            // 
            // splitTemplatesText.Panel1
            // 
            this.splitTemplatesText.Panel1.Controls.Add(this.splitAlleRecente);
            // 
            // splitTemplatesText.Panel2
            // 
            this.splitTemplatesText.Panel2.Controls.Add(this.splitTextQueries);
            this.splitTemplatesText.Size = new System.Drawing.Size(1195, 620);
            this.splitTemplatesText.SplitterDistance = 275;
            this.splitTemplatesText.TabIndex = 1;
            // 
            // splitAlleRecente
            // 
            this.splitAlleRecente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitAlleRecente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitAlleRecente.IsSplitterFixed = true;
            this.splitAlleRecente.Location = new System.Drawing.Point(0, 0);
            this.splitAlleRecente.Name = "splitAlleRecente";
            this.splitAlleRecente.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitAlleRecente.Panel1
            // 
            this.splitAlleRecente.Panel1.Controls.Add(this.splitLabelAlle);
            // 
            // splitAlleRecente.Panel2
            // 
            this.splitAlleRecente.Panel2.Controls.Add(this.splitLabelRecente);
            this.splitAlleRecente.Size = new System.Drawing.Size(275, 620);
            this.splitAlleRecente.SplitterDistance = 568;
            this.splitAlleRecente.TabIndex = 32;
            // 
            // splitLabelAlle
            // 
            this.splitLabelAlle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitLabelAlle.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitLabelAlle.IsSplitterFixed = true;
            this.splitLabelAlle.Location = new System.Drawing.Point(0, 0);
            this.splitLabelAlle.Name = "splitLabelAlle";
            this.splitLabelAlle.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitLabelAlle.Panel1
            // 
            this.splitLabelAlle.Panel1.Controls.Add(this.tlstrpAllTemplates);
            // 
            // splitLabelAlle.Panel2
            // 
            this.splitLabelAlle.Panel2.Controls.Add(this.dgvTemplates);
            this.splitLabelAlle.Size = new System.Drawing.Size(273, 566);
            this.splitLabelAlle.SplitterDistance = 25;
            this.splitLabelAlle.SplitterWidth = 1;
            this.splitLabelAlle.TabIndex = 32;
            // 
            // tlstrpAllTemplates
            // 
            this.tlstrpAllTemplates.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tlstrpAllTemplates.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRefresh,
            this.lblAlleTemplates,
            this.btnHideGrid,
            this.btnExpandGrid});
            this.tlstrpAllTemplates.Location = new System.Drawing.Point(0, 0);
            this.tlstrpAllTemplates.Name = "tlstrpAllTemplates";
            this.tlstrpAllTemplates.Size = new System.Drawing.Size(273, 26);
            this.tlstrpAllTemplates.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(23, 23);
            this.btnRefresh.ToolTipText = "Refresh the template list";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lblAlleTemplates
            // 
            this.lblAlleTemplates.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlleTemplates.Margin = new System.Windows.Forms.Padding(34, 1, 0, 2);
            this.lblAlleTemplates.Name = "lblAlleTemplates";
            this.lblAlleTemplates.Size = new System.Drawing.Size(82, 23);
            this.lblAlleTemplates.Text = "All templates";
            this.lblAlleTemplates.ToolTipText = "This frame shows all the active templates";
            // 
            // btnHideGrid
            // 
            this.btnHideGrid.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnHideGrid.AutoSize = false;
            this.btnHideGrid.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHideGrid.Image = ((System.Drawing.Image)(resources.GetObject("btnHideGrid.Image")));
            this.btnHideGrid.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHideGrid.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnHideGrid.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHideGrid.Margin = new System.Windows.Forms.Padding(0);
            this.btnHideGrid.Name = "btnHideGrid";
            this.btnHideGrid.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.btnHideGrid.Size = new System.Drawing.Size(11, 26);
            this.btnHideGrid.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnHideGrid.ToolTipText = "Click to hide";
            this.btnHideGrid.Click += new System.EventHandler(this.btnHideGrid_Click);
            // 
            // btnExpandGrid
            // 
            this.btnExpandGrid.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnExpandGrid.AutoSize = false;
            this.btnExpandGrid.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExpandGrid.Image = ((System.Drawing.Image)(resources.GetObject("btnExpandGrid.Image")));
            this.btnExpandGrid.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnExpandGrid.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExpandGrid.Margin = new System.Windows.Forms.Padding(0);
            this.btnExpandGrid.Name = "btnExpandGrid";
            this.btnExpandGrid.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.btnExpandGrid.Size = new System.Drawing.Size(11, 26);
            this.btnExpandGrid.ToolTipText = "Click to expand";
            this.btnExpandGrid.Visible = false;
            this.btnExpandGrid.Click += new System.EventHandler(this.btnExpandGrid_Click);
            // 
            // dgvTemplates
            // 
            this.dgvTemplates.AllowUserToAddRows = false;
            this.dgvTemplates.AllowUserToDeleteRows = false;
            this.dgvTemplates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTemplates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTemplates.Location = new System.Drawing.Point(0, 0);
            this.dgvTemplates.Name = "dgvTemplates";
            this.dgvTemplates.RowHeadersWidth = 20;
            this.dgvTemplates.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvTemplates.RowTemplate.ReadOnly = true;
            this.dgvTemplates.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTemplates.Size = new System.Drawing.Size(273, 540);
            this.dgvTemplates.TabIndex = 30;
            this.dgvTemplates.Sorted += new System.EventHandler(this.dgvTemplates_Sorted);
            this.dgvTemplates.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvTemplates_MouseClick);
            this.dgvTemplates.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvTemplates_KeyPress);
            this.dgvTemplates.SelectionChanged += new System.EventHandler(this.dgvTemplates_SelectionChanged);
            this.dgvTemplates.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dgvTemplates_KeyUp);
            // 
            // splitLabelRecente
            // 
            this.splitLabelRecente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitLabelRecente.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitLabelRecente.IsSplitterFixed = true;
            this.splitLabelRecente.Location = new System.Drawing.Point(0, 0);
            this.splitLabelRecente.Name = "splitLabelRecente";
            this.splitLabelRecente.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitLabelRecente.Panel1
            // 
            this.splitLabelRecente.Panel1.Controls.Add(this.lblRecTemplates);
            // 
            // splitLabelRecente.Panel2
            // 
            this.splitLabelRecente.Panel2.Controls.Add(this.dgvRecTemplates);
            this.splitLabelRecente.Size = new System.Drawing.Size(273, 46);
            this.splitLabelRecente.SplitterDistance = 25;
            this.splitLabelRecente.SplitterWidth = 1;
            this.splitLabelRecente.TabIndex = 0;
            // 
            // lblRecTemplates
            // 
            this.lblRecTemplates.AutoSize = true;
            this.lblRecTemplates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecTemplates.Location = new System.Drawing.Point(60, 4);
            this.lblRecTemplates.Name = "lblRecTemplates";
            this.lblRecTemplates.Size = new System.Drawing.Size(106, 13);
            this.lblRecTemplates.TabIndex = 0;
            this.lblRecTemplates.Tag = "asdfasfaef";
            this.lblRecTemplates.Text = "Recent templates";
            this.lblRecTemplates.MouseLeave += new System.EventHandler(this.lblRecTemplates_MouseLeave);
            this.lblRecTemplates.MouseHover += new System.EventHandler(this.lblRecTemplates_MouseHover);
            // 
            // dgvRecTemplates
            // 
            this.dgvRecTemplates.AllowUserToAddRows = false;
            this.dgvRecTemplates.AllowUserToResizeColumns = false;
            this.dgvRecTemplates.AllowUserToResizeRows = false;
            this.dgvRecTemplates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecTemplates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRecTemplates.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvRecTemplates.Location = new System.Drawing.Point(0, 0);
            this.dgvRecTemplates.Name = "dgvRecTemplates";
            this.dgvRecTemplates.ReadOnly = true;
            this.dgvRecTemplates.RowHeadersWidth = 20;
            this.dgvRecTemplates.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvRecTemplates.RowTemplate.ReadOnly = true;
            this.dgvRecTemplates.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecTemplates.Size = new System.Drawing.Size(273, 28);
            this.dgvRecTemplates.TabIndex = 31;
            this.dgvRecTemplates.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvRecTemplates_UserDeletingRow);
            this.dgvRecTemplates.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvRecTemplates_MouseClick);
            this.dgvRecTemplates.SelectionChanged += new System.EventHandler(this.dgvRecTemplates_SelectionChanged);
            // 
            // splitTextQueries
            // 
            this.splitTextQueries.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitTextQueries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitTextQueries.Location = new System.Drawing.Point(0, 0);
            this.splitTextQueries.Name = "splitTextQueries";
            this.splitTextQueries.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitTextQueries.Panel1
            // 
            this.splitTextQueries.Panel1.Controls.Add(this.cbxWordWrap);
            this.splitTextQueries.Panel1.Controls.Add(this.lbColumn);
            this.splitTextQueries.Panel1.Controls.Add(this.lbLine);
            this.splitTextQueries.Panel1.Controls.Add(this.label7);
            this.splitTextQueries.Panel1.Controls.Add(this.label6);
            this.splitTextQueries.Panel1.Controls.Add(this.rtbContent);
            this.splitTextQueries.Panel1.Controls.Add(this.toolStrip1);
            // 
            // splitTextQueries.Panel2
            // 
            this.splitTextQueries.Panel2.Controls.Add(this.btnHideQueries);
            this.splitTextQueries.Panel2.Controls.Add(this.grpQueries);
            this.splitTextQueries.Panel2.Controls.Add(this.grpDetails);
            this.splitTextQueries.Size = new System.Drawing.Size(916, 620);
            this.splitTextQueries.SplitterDistance = 363;
            this.splitTextQueries.TabIndex = 0;
            // 
            // cbxWordWrap
            // 
            this.cbxWordWrap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxWordWrap.AutoSize = true;
            this.cbxWordWrap.Location = new System.Drawing.Point(789, 344);
            this.cbxWordWrap.Name = "cbxWordWrap";
            this.cbxWordWrap.Size = new System.Drawing.Size(114, 17);
            this.cbxWordWrap.TabIndex = 8;
            this.cbxWordWrap.Text = "Wrap text around?";
            this.cbxWordWrap.UseVisualStyleBackColor = true;
            this.cbxWordWrap.CheckedChanged += new System.EventHandler(this.cbxWordWrap_CheckedChanged);
            // 
            // lbColumn
            // 
            this.lbColumn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbColumn.AutoSize = true;
            this.lbColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbColumn.Location = new System.Drawing.Point(148, 345);
            this.lbColumn.Name = "lbColumn";
            this.lbColumn.Size = new System.Drawing.Size(14, 13);
            this.lbColumn.TabIndex = 6;
            this.lbColumn.Text = "0";
            // 
            // lbLine
            // 
            this.lbLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbLine.AutoSize = true;
            this.lbLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbLine.Location = new System.Drawing.Point(28, 345);
            this.lbLine.Name = "lbLine";
            this.lbLine.Size = new System.Drawing.Size(14, 13);
            this.lbLine.TabIndex = 5;
            this.lbLine.Text = "0";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(108, 345);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Column:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 345);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Line:";
            // 
            // rtbContent
            // 
            this.rtbContent.AcceptsReturn = true;
            this.rtbContent.AcceptsTab = true;
            this.rtbContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbContent.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.rtbContent.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbContent.HideSelection = false;
            this.rtbContent.Location = new System.Drawing.Point(0, 25);
            this.rtbContent.Margin = new System.Windows.Forms.Padding(0);
            this.rtbContent.MaxLength = 99999999;
            this.rtbContent.Multiline = true;
            this.rtbContent.Name = "rtbContent";
            this.rtbContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.rtbContent.Size = new System.Drawing.Size(912, 317);
            this.rtbContent.TabIndex = 7;
            this.rtbContent.WordWrap = false;
            this.rtbContent.TextChanged += new System.EventHandler(this.rtbContent_TextChanged);
            this.rtbContent.GotFocus += new System.EventHandler(this.rtbContent_GotFocus);
            this.rtbContent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtbContent_KeyDown);
            this.rtbContent.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rtbContent_KeyUp);
            this.rtbContent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.rtbContent_MouseClick);
            this.rtbContent.LostFocus += new System.EventHandler(this.rtbContent_LostFocus);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSave,
            this.btnNewTemplate,
            this.btnVS,
            this.btnTryParse,
            this.toolStripSeparator1,
            this.btnZoekVervang,
            this.btnZoekForm,
            this.rtbZoekterm,
            this.btnZoekTerug,
            this.btnZoekVerder,
            this.btnDoorzoekAlles});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(912, 27);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "S";
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 24);
            this.btnSave.Text = "Save template";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNewTemplate
            // 
            this.btnNewTemplate.CheckOnClick = true;
            this.btnNewTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnNewTemplate.Image")));
            this.btnNewTemplate.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnNewTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNewTemplate.Name = "btnNewTemplate";
            this.btnNewTemplate.Size = new System.Drawing.Size(92, 24);
            this.btnNewTemplate.Text = "New template";
            this.btnNewTemplate.ToolTipText = "Start a new template";
            this.btnNewTemplate.Click += new System.EventHandler(this.btnNewTemplate_Click);
            // 
            // btnVS
            // 
            this.btnVS.Image = ((System.Drawing.Image)(resources.GetObject("btnVS.Image")));
            this.btnVS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnVS.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnVS.Name = "btnVS";
            this.btnVS.Size = new System.Drawing.Size(83, 24);
            this.btnVS.Text = "Open in VS";
            this.btnVS.ToolTipText = "Open in Visual Studio";
            this.btnVS.Click += new System.EventHandler(this.btnVS_Click);
            // 
            // btnTryParse
            // 
            this.btnTryParse.Image = ((System.Drawing.Image)(resources.GetObject("btnTryParse.Image")));
            this.btnTryParse.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnTryParse.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTryParse.Name = "btnTryParse";
            this.btnTryParse.Size = new System.Drawing.Size(74, 24);
            this.btnTryParse.Text = "TryParse";
            this.btnTryParse.ToolTipText = "Start Debugging (F5) - Template will be saved first";
            this.btnTryParse.Click += new System.EventHandler(this.btnTryParse_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // btnZoekVervang
            // 
            this.btnZoekVervang.Image = ((System.Drawing.Image)(resources.GetObject("btnZoekVervang.Image")));
            this.btnZoekVervang.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnZoekVervang.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnZoekVervang.Name = "btnZoekVervang";
            this.btnZoekVervang.Size = new System.Drawing.Size(128, 24);
            this.btnZoekVervang.Text = "Search-and-Replace";
            this.btnZoekVervang.ToolTipText = "Open the Search-and-Replace form";
            this.btnZoekVervang.Click += new System.EventHandler(this.btnZoekVervang_Click);
            // 
            // btnZoekForm
            // 
            this.btnZoekForm.Image = ((System.Drawing.Image)(resources.GetObject("btnZoekForm.Image")));
            this.btnZoekForm.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnZoekForm.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnZoekForm.Name = "btnZoekForm";
            this.btnZoekForm.Size = new System.Drawing.Size(107, 24);
            this.btnZoekForm.Text = "Search overview";
            this.btnZoekForm.ToolTipText = "Get a list of all templates containing the search term";
            this.btnZoekForm.Click += new System.EventHandler(this.btnZoekForm_Click);
            // 
            // rtbZoekterm
            // 
            this.rtbZoekterm.Name = "rtbZoekterm";
            this.rtbZoekterm.Size = new System.Drawing.Size(100, 27);
            this.rtbZoekterm.ToolTipText = "Type the search term";
            this.rtbZoekterm.DoubleClick += new System.EventHandler(this.rtbZoekterm_DoubleClick);
            this.rtbZoekterm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtbZoekterm_KeyPress);
            this.rtbZoekterm.TextChanged += new System.EventHandler(this.rtbZoekterm_TextChanged);
            // 
            // btnZoekTerug
            // 
            this.btnZoekTerug.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnZoekTerug.Image = ((System.Drawing.Image)(resources.GetObject("btnZoekTerug.Image")));
            this.btnZoekTerug.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnZoekTerug.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnZoekTerug.Name = "btnZoekTerug";
            this.btnZoekTerug.Size = new System.Drawing.Size(44, 24);
            this.btnZoekTerug.ToolTipText = "Search backwards, starting from the caret position";
            this.btnZoekTerug.Click += new System.EventHandler(this.btnZoekTerug_Click);
            // 
            // btnZoekVerder
            // 
            this.btnZoekVerder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnZoekVerder.Image = ((System.Drawing.Image)(resources.GetObject("btnZoekVerder.Image")));
            this.btnZoekVerder.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnZoekVerder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnZoekVerder.Name = "btnZoekVerder";
            this.btnZoekVerder.Size = new System.Drawing.Size(44, 24);
            this.btnZoekVerder.Text = "toolStripButton1";
            this.btnZoekVerder.ToolTipText = "Search forwards, starting from the caret position";
            this.btnZoekVerder.Click += new System.EventHandler(this.btnZoekVerder_Click);
            // 
            // btnDoorzoekAlles
            // 
            this.btnDoorzoekAlles.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDoorzoekAlles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDoorzoekAlles.Name = "btnDoorzoekAlles";
            this.btnDoorzoekAlles.Size = new System.Drawing.Size(107, 24);
            this.btnDoorzoekAlles.Text = "Search all templates";
            this.btnDoorzoekAlles.ToolTipText = "Search all templates";
            this.btnDoorzoekAlles.Click += new System.EventHandler(this.btnDoorzoekAlles_Click);
            // 
            // btnHideQueries
            // 
            this.btnHideQueries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHideQueries.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnHideQueries.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonFace;
            this.btnHideQueries.FlatAppearance.BorderSize = 0;
            this.btnHideQueries.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHideQueries.Location = new System.Drawing.Point(890, 0);
            this.btnHideQueries.Margin = new System.Windows.Forms.Padding(0);
            this.btnHideQueries.Name = "btnHideQueries";
            this.btnHideQueries.Size = new System.Drawing.Size(20, 8);
            this.btnHideQueries.TabIndex = 24;
            this.btnHideQueries.UseVisualStyleBackColor = true;
            this.btnHideQueries.MouseLeave += new System.EventHandler(this.btnHideQueries_MouseLeave);
            this.btnHideQueries.Click += new System.EventHandler(this.btnHideQueries_Click);
            this.btnHideQueries.MouseEnter += new System.EventHandler(this.btnHideQueries_MouseEnter);
            // 
            // grpQueries
            // 
            this.grpQueries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpQueries.Controls.Add(this.btnDeleteQuery);
            this.grpQueries.Controls.Add(this.btnEditQuery);
            this.grpQueries.Controls.Add(this.btnAddQuery);
            this.grpQueries.Controls.Add(this.dgvQueries);
            this.grpQueries.Location = new System.Drawing.Point(3, 140);
            this.grpQueries.Name = "grpQueries";
            this.grpQueries.Size = new System.Drawing.Size(906, 106);
            this.grpQueries.TabIndex = 1;
            this.grpQueries.TabStop = false;
            this.grpQueries.Text = "Template queries";
            // 
            // btnDeleteQuery
            // 
            this.btnDeleteQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteQuery.Enabled = false;
            this.btnDeleteQuery.Location = new System.Drawing.Point(825, 76);
            this.btnDeleteQuery.Name = "btnDeleteQuery";
            this.btnDeleteQuery.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteQuery.TabIndex = 27;
            this.btnDeleteQuery.Text = "Delete";
            this.btnDeleteQuery.UseVisualStyleBackColor = true;
            this.btnDeleteQuery.Click += new System.EventHandler(this.btnDeleteQuery_Click);
            // 
            // btnEditQuery
            // 
            this.btnEditQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditQuery.Enabled = false;
            this.btnEditQuery.Location = new System.Drawing.Point(825, 48);
            this.btnEditQuery.Name = "btnEditQuery";
            this.btnEditQuery.Size = new System.Drawing.Size(75, 23);
            this.btnEditQuery.TabIndex = 26;
            this.btnEditQuery.Text = "Edit";
            this.btnEditQuery.UseVisualStyleBackColor = true;
            this.btnEditQuery.Click += new System.EventHandler(this.btnEditQuery_Click);
            // 
            // btnAddQuery
            // 
            this.btnAddQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddQuery.Location = new System.Drawing.Point(825, 19);
            this.btnAddQuery.Name = "btnAddQuery";
            this.btnAddQuery.Size = new System.Drawing.Size(75, 23);
            this.btnAddQuery.TabIndex = 25;
            this.btnAddQuery.Text = "Add";
            this.btnAddQuery.UseVisualStyleBackColor = true;
            this.btnAddQuery.Click += new System.EventHandler(this.btnAddQuery_Click);
            // 
            // dgvQueries
            // 
            this.dgvQueries.AllowUserToAddRows = false;
            this.dgvQueries.AllowUserToDeleteRows = false;
            this.dgvQueries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvQueries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQueries.Location = new System.Drawing.Point(6, 19);
            this.dgvQueries.Name = "dgvQueries";
            this.dgvQueries.ReadOnly = true;
            this.dgvQueries.RowHeadersWidth = 20;
            this.dgvQueries.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvQueries.RowTemplate.ReadOnly = true;
            this.dgvQueries.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvQueries.Size = new System.Drawing.Size(813, 81);
            this.dgvQueries.TabIndex = 24;
            this.dgvQueries.DoubleClick += new System.EventHandler(this.dgvQueries_DoubleClick);
            this.dgvQueries.SelectionChanged += new System.EventHandler(this.dgvQueries_SelectionChanged);
            this.dgvQueries.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dgvQueries_KeyUp);
            // 
            // grpDetails
            // 
            this.grpDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpDetails.Controls.Add(this.txtID);
            this.grpDetails.Controls.Add(this.lblID);
            this.grpDetails.Controls.Add(this.txtDocType);
            this.grpDetails.Controls.Add(this.lblDocType);
            this.grpDetails.Controls.Add(this.txtRights);
            this.grpDetails.Controls.Add(this.lblRights);
            this.grpDetails.Controls.Add(this.chkDataTemplate);
            this.grpDetails.Controls.Add(this.chkUnattended);
            this.grpDetails.Controls.Add(this.chkToSave);
            this.grpDetails.Controls.Add(this.txtRedirect);
            this.grpDetails.Controls.Add(this.lblRedirect);
            this.grpDetails.Controls.Add(this.txtDescription);
            this.grpDetails.Controls.Add(this.lblDescr);
            this.grpDetails.Controls.Add(this.txtName);
            this.grpDetails.Controls.Add(this.lblName);
            this.grpDetails.Location = new System.Drawing.Point(3, 3);
            this.grpDetails.MinimumSize = new System.Drawing.Size(0, 105);
            this.grpDetails.Name = "grpDetails";
            this.grpDetails.Size = new System.Drawing.Size(906, 131);
            this.grpDetails.TabIndex = 0;
            this.grpDetails.TabStop = false;
            this.grpDetails.Text = "Template properties";
            // 
            // txtID
            // 
            this.txtID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtID.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtID.IntValue = -1;
            this.txtID.Location = new System.Drawing.Point(632, 19);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(108, 21);
            this.txtID.TabIndex = 16;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            this.txtID.GotFocus += new System.EventHandler(this.txtID_GotFocus);
            this.txtID.LostFocus += new System.EventHandler(this.txtID_LostFocus);
            // 
            // lblID
            // 
            this.lblID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(606, 22);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(18, 13);
            this.lblID.TabIndex = 13;
            this.lblID.Text = "ID";
            this.lblID.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtDocType
            // 
            this.txtDocType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDocType.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtDocType.Location = new System.Drawing.Point(632, 99);
            this.txtDocType.Name = "txtDocType";
            this.txtDocType.Size = new System.Drawing.Size(108, 21);
            this.txtDocType.TabIndex = 20;
            this.txtDocType.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // lblDocType
            // 
            this.lblDocType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDocType.AutoSize = true;
            this.lblDocType.Location = new System.Drawing.Point(570, 102);
            this.lblDocType.Name = "lblDocType";
            this.lblDocType.Size = new System.Drawing.Size(54, 13);
            this.lblDocType.TabIndex = 11;
            this.lblDocType.Text = "Doc Type";
            // 
            // txtRights
            // 
            this.txtRights.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRights.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtRights.Location = new System.Drawing.Point(74, 98);
            this.txtRights.Name = "txtRights";
            this.txtRights.Size = new System.Drawing.Size(490, 21);
            this.txtRights.TabIndex = 19;
            this.txtRights.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // lblRights
            // 
            this.lblRights.AutoSize = true;
            this.lblRights.Location = new System.Drawing.Point(8, 101);
            this.lblRights.Name = "lblRights";
            this.lblRights.Size = new System.Drawing.Size(37, 13);
            this.lblRights.TabIndex = 9;
            this.lblRights.Text = "Rights";
            // 
            // chkDataTemplate
            // 
            this.chkDataTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDataTemplate.AutoSize = true;
            this.chkDataTemplate.Location = new System.Drawing.Point(773, 101);
            this.chkDataTemplate.Name = "chkDataTemplate";
            this.chkDataTemplate.Size = new System.Drawing.Size(92, 17);
            this.chkDataTemplate.TabIndex = 23;
            this.chkDataTemplate.Text = "Data template";
            this.chkDataTemplate.UseVisualStyleBackColor = true;
            this.chkDataTemplate.CheckedChanged += new System.EventHandler(this.chkToSave_CheckedChanged);
            // 
            // chkUnattended
            // 
            this.chkUnattended.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkUnattended.AutoSize = true;
            this.chkUnattended.Location = new System.Drawing.Point(773, 61);
            this.chkUnattended.Name = "chkUnattended";
            this.chkUnattended.Size = new System.Drawing.Size(82, 17);
            this.chkUnattended.TabIndex = 22;
            this.chkUnattended.Text = "Unattended";
            this.chkUnattended.UseVisualStyleBackColor = true;
            this.chkUnattended.CheckedChanged += new System.EventHandler(this.chkToSave_CheckedChanged);
            // 
            // chkToSave
            // 
            this.chkToSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkToSave.AutoSize = true;
            this.chkToSave.Location = new System.Drawing.Point(773, 22);
            this.chkToSave.Name = "chkToSave";
            this.chkToSave.Size = new System.Drawing.Size(65, 17);
            this.chkToSave.TabIndex = 21;
            this.chkToSave.Text = "To save";
            this.chkToSave.UseVisualStyleBackColor = true;
            this.chkToSave.CheckedChanged += new System.EventHandler(this.chkToSave_CheckedChanged);
            // 
            // txtRedirect
            // 
            this.txtRedirect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRedirect.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtRedirect.Location = new System.Drawing.Point(74, 72);
            this.txtRedirect.Name = "txtRedirect";
            this.txtRedirect.Size = new System.Drawing.Size(666, 21);
            this.txtRedirect.TabIndex = 18;
            this.txtRedirect.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // lblRedirect
            // 
            this.lblRedirect.AutoSize = true;
            this.lblRedirect.Location = new System.Drawing.Point(8, 75);
            this.lblRedirect.Name = "lblRedirect";
            this.lblRedirect.Size = new System.Drawing.Size(47, 13);
            this.lblRedirect.TabIndex = 4;
            this.lblRedirect.Text = "Redirect";
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtDescription.Location = new System.Drawing.Point(74, 46);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(666, 21);
            this.txtDescription.TabIndex = 17;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // lblDescr
            // 
            this.lblDescr.AutoSize = true;
            this.lblDescr.Location = new System.Drawing.Point(8, 49);
            this.lblDescr.Name = "lblDescr";
            this.lblDescr.Size = new System.Drawing.Size(60, 13);
            this.lblDescr.TabIndex = 2;
            this.lblDescr.Text = "Description";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtName.Location = new System.Drawing.Point(74, 20);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(490, 21);
            this.txtName.TabIndex = 15;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            this.txtName.GotFocus += new System.EventHandler(this.txtName_GotFocus);
            this.txtName.LostFocus += new System.EventHandler(this.txtName_LostFocus);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(8, 23);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // ContextMenuItemNewWindow
            // 
            this.ContextMenuItemNewWindow.Index = -1;
            this.ContextMenuItemNewWindow.Text = "Open template in new window";
            this.ContextMenuItemNewWindow.Click += new System.EventHandler(this.ContextMenuItemNewWindow_Click);
            // 
            // ContextMenuItemRefresh
            // 
            this.ContextMenuItemRefresh.Index = -1;
            this.ContextMenuItemRefresh.Text = "Refesh";
            this.ContextMenuItemRefresh.Click += new System.EventHandler(this.ContextMenuItemRefresh_Click);
            // 
            // proceduresToolStripMenuItem
            // 
            this.proceduresToolStripMenuItem.Name = "proceduresToolStripMenuItem";
            this.proceduresToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.proceduresToolStripMenuItem.Text = "&Procedures";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1195, 644);
            this.Controls.Add(this.splitTemplatesText);
            this.Controls.Add(this.menuStripForm);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "XDocHQ";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseWheel);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyUp);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.menuStripForm.ResumeLayout(false);
            this.menuStripForm.PerformLayout();
            this.splitTemplatesText.Panel1.ResumeLayout(false);
            this.splitTemplatesText.Panel2.ResumeLayout(false);
            this.splitTemplatesText.ResumeLayout(false);
            this.splitAlleRecente.Panel1.ResumeLayout(false);
            this.splitAlleRecente.Panel2.ResumeLayout(false);
            this.splitAlleRecente.ResumeLayout(false);
            this.splitLabelAlle.Panel1.ResumeLayout(false);
            this.splitLabelAlle.Panel1.PerformLayout();
            this.splitLabelAlle.Panel2.ResumeLayout(false);
            this.splitLabelAlle.ResumeLayout(false);
            this.tlstrpAllTemplates.ResumeLayout(false);
            this.tlstrpAllTemplates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemplates)).EndInit();
            this.splitLabelRecente.Panel1.ResumeLayout(false);
            this.splitLabelRecente.Panel1.PerformLayout();
            this.splitLabelRecente.Panel2.ResumeLayout(false);
            this.splitLabelRecente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecTemplates)).EndInit();
            this.splitTextQueries.Panel1.ResumeLayout(false);
            this.splitTextQueries.Panel1.PerformLayout();
            this.splitTextQueries.Panel2.ResumeLayout(false);
            this.splitTextQueries.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.grpQueries.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQueries)).EndInit();
            this.grpDetails.ResumeLayout(false);
            this.grpDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }        
        #endregion

        private System.Windows.Forms.MenuStrip menuStripForm;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitTemplatesText;
        private System.Windows.Forms.SplitContainer splitTextQueries;
        private System.Windows.Forms.DataGridView dgvTemplates;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.GroupBox grpDetails;
        private System.Windows.Forms.GroupBox grpQueries;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.DataGridView dgvQueries;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.CheckBox chkDataTemplate;
        private System.Windows.Forms.CheckBox chkUnattended;
        private System.Windows.Forms.CheckBox chkToSave;
        private System.Windows.Forms.TextBox txtRedirect;
        private System.Windows.Forms.Label lblRedirect;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescr;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnDeleteQuery;
        private System.Windows.Forms.Button btnEditQuery;
        private System.Windows.Forms.Button btnAddQuery;
        private System.Windows.Forms.TextBox txtRights;
        private System.Windows.Forms.Label lblRights;
        private XDocHQ.TextBoxPlus rtbContent;
        private System.Windows.Forms.TextBox txtDocType;
        private System.Windows.Forms.Label lblDocType;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label lbColumn;
        private System.Windows.Forms.Label lbLine;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripButton btnNewTemplate;
        public  System.Windows.Forms.ToolStripTextBox rtbZoekterm;
        private XDocHQ.NumericTextBox txtID;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem templateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTemplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTemplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshTemplatelistToolStripMenuItem;
        private XDocHQ.ToolStripMenuCheckbox doorzoekAllesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoekTerugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoekVerderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoekEnVervangToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStriptbxZoekterm;
        private System.Windows.Forms.ToolStripMenuItem zoekoverzichtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dependenciesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openInVSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tryParseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openTemplateInNewWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deletedTemplatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnVS;
        private System.Windows.Forms.ToolStripButton btnZoekVervang;
        private System.Windows.Forms.ToolStripButton btnTryParse;
        private System.Windows.Forms.ToolStripButton btnZoekForm;
        private System.Windows.Forms.ToolStripButton btnZoekTerug;
        private System.Windows.Forms.ToolStripButton btnZoekVerder;
        private System.Windows.Forms.ToolStripMenuItem btnDecrFont;
        private System.Windows.Forms.ToolStripMenuItem btnIncrFont;
        public XDocHQ.ToolStripCheckbox btnDoorzoekAlles;
        private System.Windows.Forms.SplitContainer splitAlleRecente;
        private System.Windows.Forms.SplitContainer splitLabelAlle;
        private System.Windows.Forms.SplitContainer splitLabelRecente;
        private System.Windows.Forms.Label lblRecTemplates;
        private XDocHQ.UnSortableDGV dgvRecTemplates;   //A special object is used, to make sure the dgv of recent templates is always in chronological order and is not sortable.
        private System.Windows.Forms.ToolStrip tlstrpAllTemplates;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripLabel lblAlleTemplates;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolStripButton btnHideGrid;
        private System.Windows.Forms.ToolStripButton btnExpandGrid;
        private System.Windows.Forms.MenuItem ContextMenuItemNewWindow;
        private System.Windows.Forms.MenuItem ContextMenuItemRefresh;
        private System.Windows.Forms.Button btnHideQueries;
        private System.Windows.Forms.CheckBox cbxWordWrap;
        private System.Windows.Forms.ToolStripMenuItem proceduresToolStripMenuItem;

    }


}

