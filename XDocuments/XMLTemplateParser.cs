using System;
using System.Data;
using System.Xml;
using System.Collections;
using System.Collections.ObjectModel;
using XDocuments;

/// <summary>
/// Summary description for XMLTemplateParser
/// </summary>
public class XMLTemplateParser
{

    #region Class Members

    private string m_XmlContent;
    private XmlDocument m_Document;
    private XDocManager m_DocManager;
    private XmlNode root;

    #endregion

    #region Public Constructors

    public XMLTemplateParser(string xml, XDocManager  manager)
    {
        this.m_XmlContent = xml;
        this.m_DocManager = manager;
        m_Document = new XmlDocument();
        if (xml.LastIndexOf('>') < xml.Length - 1)
            xml = xml.Remove(xml.LastIndexOf('>') + 1);
        m_Document.LoadXml(xml);

        root = m_Document.DocumentElement;

        if (!root.Name.Equals(PropertyNameMapper.TemplateElement))
            throw new ApplicationException("Invalid XML file!\nElement \"Template\" should be the root element!");

    }

    public XMLTemplateParser(XmlDocument document, XDocManager manager)
    {
        this.m_DocManager = manager;

        root = m_Document.DocumentElement;

        if (!root.Name.Equals(PropertyNameMapper.TemplateElement))
            throw new ApplicationException("Invalid XML file!\nElement \"Template\" should be the root element!");
    }

    #endregion

    #region Public methods

    public Collection<IXQuery> GetQueries()
    {
        Collection<IXQuery> queries = new Collection<IXQuery>();
        XmlNode queriesNode = root.SelectSingleNode(PropertyNameMapper.QueriesCollection);

        foreach (XmlNode queryNode in queriesNode.SelectNodes(PropertyNameMapper.QueryElementName))
        {
            IXQuery query = m_DocManager.NewQuery();
            query.Name = queryNode.Attributes[PropertyNameMapper.QueryName].Value;

            query.QueryText = queryNode.SelectSingleNode(PropertyNameMapper.QueryText).FirstChild is XmlCDataSection ? (queryNode.SelectSingleNode(PropertyNameMapper.QueryText).FirstChild as XmlCDataSection).Value : queryNode.SelectSingleNode(PropertyNameMapper.QueryText).Value;

            IXConnection conn = GetConnection(queryNode, query);

            conn.Save();

            query.ConnectionID = conn.ID;
            queries.Add(query);
        }

        return queries;
    }

    public int GetTemplateID()
    {
        XmlAttribute attrTemplateID = root.Attributes[PropertyNameMapper.TemplateID];
        if (attrTemplateID != null)
            return Convert.ToInt32(attrTemplateID.Value);
        else throw new MissingAttributeException(root, PropertyNameMapper.TemplateID);
    }

    /// <summary>
    /// Gets the string representing the content of the template from the corresponding xml tag
    /// </summary>
    /// <returns></returns>
    public string GetContent_old()
    {
        XmlNode node = root.SelectSingleNode(PropertyNameMapper.TemplateContent);
        if (node.FirstChild is XmlCDataSection)
            return (node.FirstChild as XmlCDataSection).Value;
        else return node.Value == null ? "" : node.Value;
    }
    public string GetContent()
    {
        XmlNode node = root.SelectSingleNode(PropertyNameMapper.TemplateContent);
        string s = node.InnerXml.Trim();
        s = s.Substring(0, s.Length - "]]>".Length);
        s = s.Substring("<![CDATA[".Length);
        return s;
    }

    /// <summary>
    /// Gets the template description from the corresponding xml tag
    /// </summary>
    /// <returns></returns>
    public string GetDescription()
    {
        XmlNode node = root.SelectSingleNode(PropertyNameMapper.TemplateDescriptionElementName);
        return node.Value == null ? "" : node.Value;
    }

    /// <summary>
    /// Gets the redirect string from the corresponding xml tag
    /// </summary>
    /// <returns></returns>
    public string GetRedirect()
    {
        XmlNode node = root.SelectSingleNode(PropertyNameMapper.TemplateToRedirect);
        if (node.FirstChild is XmlCDataSection)
            return (node.FirstChild as XmlCDataSection).Value;
        else return node.Value == null ? "" : node.Value;
    }

    /// <summary>
    /// Gets the template document type from the corresponding xml tag
    /// </summary>
    /// <returns></returns>
    public string GetDocType()
    {
        XmlAttribute attrDocType = root.Attributes[PropertyNameMapper.DocumentType];
        if (attrDocType != null)
            return attrDocType.Value;
        else throw new MissingAttributeException(root, PropertyNameMapper.DocumentType);
    }

    /// <summary>
    /// Gets the template name
    /// </summary>
    /// <returns></returns>
    public string GetTemplateName()
    {
        XmlAttribute attrTemplateName = root.Attributes[PropertyNameMapper.TemplateName];
        if (attrTemplateName != null)
            return attrTemplateName.Value;
        else throw new MissingAttributeException(root, PropertyNameMapper.TemplateName);
    }

    /// <summary>
    /// Gets the IsData value for the template
    /// </summary>
    /// <returns></returns>
    public bool GetIsData()
    {
        bool isData;

        XmlAttribute attrIsData = root.Attributes[PropertyNameMapper.IsDataTemplate];
        if (attrIsData != null)
            if (!bool.TryParse(attrIsData.Value, out isData))
                throw new InvalidAttributeValueException(root, PropertyNameMapper.IsDataTemplate);
            else
                return isData;
        else throw new MissingAttributeException(root, PropertyNameMapper.IsDataTemplate);
    }

    /// <summary>
    /// Gets the value of ToSave parameter for the template
    /// </summary>
    /// <returns></returns>
    public bool GetToSave()
    {
        bool toSave;

        XmlAttribute attrToSave = root.Attributes[PropertyNameMapper.ToSave];
        if (attrToSave != null)
            if (!bool.TryParse(attrToSave.Value, out toSave))
                throw new InvalidAttributeValueException(root, PropertyNameMapper.ToSave);
            else
                return toSave;
        else throw new MissingAttributeException(root, PropertyNameMapper.ToSave);
    }

    /// <summary>
    /// Gets the value of IsUnattended for the template
    /// </summary>
    /// <returns></returns>
    public bool GetIsUnattended()
    {
        bool unattended;

        XmlAttribute attrUnattended = root.Attributes[PropertyNameMapper.Unattended];
        if (attrUnattended != null)
            if (!bool.TryParse(attrUnattended.Value, out unattended))
                throw new InvalidAttributeValueException(root, PropertyNameMapper.Unattended);
            else
                return unattended;
        else throw new MissingAttributeException(root, PropertyNameMapper.Unattended);
    }

    #endregion

    #region Private Methods

    private IXConnection GetConnection(XmlNode queryNode, IXQuery query)
    {
        XmlNode connectionNode = queryNode.SelectSingleNode(PropertyNameMapper.ConnectionElementName);
        string name;
        string connectionString;

        if (connectionNode.Attributes[PropertyNameMapper.ConnectionString] != null)
            connectionString = connectionNode.Attributes[PropertyNameMapper.ConnectionString].Value;
        else
            connectionString = "";

        if (connectionNode.Attributes[PropertyNameMapper.ConnectionName] != null)
            name = connectionNode.Attributes[PropertyNameMapper.ConnectionName].Value;
        else
            name = "";

        IXConnection conn = null;
        IDName[] allConnections = m_DocManager.GetConnectionsList();
        if (allConnections != null)
            for (int i = 0; i < allConnections.Length; i++)
            {
                IXConnection currentConn = m_DocManager.LoadConnection(allConnections[i].ID);
                if (string.Compare(currentConn.ConnectionString, connectionString) == 0)
                    return currentConn;
            }

        conn = m_DocManager.NewConnection();
        conn.Name = name;
        conn.ConnectionString = connectionString;
        return conn;
    }

    #endregion

    public class PropertyNameMapper
    {
        //Template simple attributes
        /// <summary>
        /// The local name of the root element in an xml file representing an XDocTemplate
        /// </summary>
        public static string TemplateElement = "Template";
        public static string TemplateID = "ID";
        public static string TemplateName = "Name";
        public static string TemplateDescriptionElementName = "Description";
        public static string TemplateDescriptionValue = "value";
        public static string Unattended = "IsUnattended";
        public static string ToSave = "ToSave";
        public static string IsDataTemplate = "IsData";
        public static string DocumentType = "DocType";
        public static string TemplateToRedirect = "Redirect";
        public static string TemplateContent = "Content";

        //Query attributes
        public static string QueryElementName = "Query";
        public static string QueryID = "ID";
        public static string QueryName = "Name";
        public static string QueryText = "Text";
        public static string QueriesCollection = "Queries";

        //Connection attributes
        public static string ConnectionElementName = "Connection";
        public static string ConnectionID = "ID";
        public static string ConnectionString = "ConnectionString";
        public static string ConnectionName = "Name";

        //Role attributes
        public static string RoleElementName = "Role";
        public static string RoleValue = "value";
        public static string RolesCollection = "Roles";

    }
    public class MissingAttributeException : Exception
    {
        private XmlNode m_SourceNode;
        private string missingAttribute;
        public override string Message
        {
            get
            {
                return String.Format("Attribute {0} is missing for element {1}", missingAttribute, m_SourceNode.Name);
            }
        }

        public MissingAttributeException(XmlNode node, string missingAttribute)
        {
            this.m_SourceNode = node;
            this.missingAttribute = missingAttribute;
        }
    }

    public class InvalidAttributeValueException : Exception
    {
        private XmlNode sourceNode;
        private string missingAttribute;

        public override string Message
        {
            get
            {
                return String.Format("Attribute {0} value is invalid for element {1}", missingAttribute, sourceNode.Name);
            }
        }
        public InvalidAttributeValueException(XmlNode node, string missingAttribute)
        {
            this.sourceNode = node;
            this.missingAttribute = missingAttribute;
        }
    }

    public class MissingElementException : Exception
    {

        private string missingElement;

        public override string Message
        {
            get
            {
                return String.Format("Element {0} is missing!", missingElement);
            }
        }

        public MissingElementException(string elementName)
        {
            this.missingElement = elementName;
        }
    }

}
