using System;
using System.Collections.Generic;
using System.Text;
using XDataSourceModule;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Security.Principal;
using System.Data;
using System.Data.SqlClient;
using KubionLogNamespace;
using System.Web;
using System.IO;
using XHTMLMerge;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using System.Security;

namespace XDocuments
{
    public class XDocManager : IXManagerImportExport 
    {

        #region Private members

        private HttpContext m_HttpContext=null;

        private string
            m_attVirtualPath = null,
            m_attPath = null,
            m_userName = "anonim",
            m_userLogin = "",
            m_serverName = "",
            m_databaseName = "",
            m_includeLogFile = "";


        private const string
            UNIQUE_IDENTIFIER = "%UNIQUE%",
            UNIQUE_PARAMETER = "UNIQUE",
            DIV_ID = "DivXDoc",
            GET_CONTENT_FUNCTION = "GetDivContent",
            HIDDEN_CONTENT = "hiddenDivContent",
            START_COMMAND = "#STARTBLOCK#",
            END_COMMAND = "#ENDBLOCK#",
            START_BLOCK = "#STARTBLOCK#",
            END_BLOCK = "#ENDBLOCK#",
            START_COMMENT = "#STARTCOMMENT#",
            END_COMMENT = "#ENDCOMMENT#";

        //private const string
        //    Utils.CT_ENDCDATA = "}}>",
        //    Utils.CT_ENDCDATA2 = "}.}.>",
        //    Utils.CT_ENDCDATA3 = "}..}..>";

        private int
            m_userID = 0;

        private Connectors m_connectors = null;

        private ArrayList m_currentRoles = new ArrayList();

        private XProviderData m_dataProvider = null;

        private bool m_isWebApplication = true;
        private string m_LogRequest = "";
        public string LogRequest
        {
            get
            {
                if (m_LogRequest == "")
                {
                    string sLogRequest = System.Configuration.ConfigurationManager.AppSettings["LogRequest"];
                    if (sLogRequest != null)
                        m_LogRequest = sLogRequest;
                    else m_LogRequest = "0";
                }
                return m_LogRequest;
            }
            set
            {
                m_LogRequest = value;
            }
        }

        private string m_LogRequestData = "";
        public string LogRequestData
        {
            get
            {
                if (m_LogRequestData == "")
                {
                    string sLogRequestData = System.Configuration.ConfigurationManager.AppSettings["LogRequestData"];
                    if (sLogRequestData != null)
                        m_LogRequestData = sLogRequestData;
                    else m_LogRequestData = "1";
                }
                return m_LogRequestData;
            }
            set
            {
                m_LogRequestData = value;
            }
        }
        private string m_LogResponseData = "";
        public string LogResponseData
        {
            get
            {
                if (m_LogResponseData == "")
                {
                    string sLogResponseData = System.Configuration.ConfigurationManager.AppSettings["LogResponseData"];
                    if (sLogResponseData != null)
                        m_LogResponseData = sLogResponseData;
                    else m_LogResponseData = "1";
                }
                return m_LogResponseData;
            }
            set
            {
                m_LogResponseData = value;
            }
        }


        #endregion Private members


        #region Constructors

        public XDocManager() : this("", true)
        {
            m_dataProvider = m_connectors.SystemConnector;
            FillConnectionData();
            //MD load roles on request
            //GetCurrentUserAndRoles();
        }

        /// <summary>
        /// Creates a new instance providing the option of automatically loading the attachments paths
        /// </summary>
        /// <param name="loadPaths">If set to false, the system will expect the attachments paths to be provided through the exposed properties</param>
        public XDocManager(bool loadPaths): this ("", loadPaths)
        {

        }

        public XDocManager(string connectionString): this (connectionString, true)
        {
        }

        public XDocManager(string connectionString, bool loadPaths)
        {
            m_isWebApplication = HttpContext.Current != null;

            if (loadPaths)
                InitPaths();

            m_connectors = new Connectors();
            m_dataProvider = m_connectors.GetConnector(connectionString);
            FillConnectionData();
            //MD load roles on request
            //GetCurrentUserAndRoles();
        }
        public void Dispose()
        {
            if (m_connectors != null) m_connectors.Dispose();
        }
        #endregion Constructors


        #region Public properties

        private XDocCache m_xdocCache = null;
        public XDocCache DocCache
        {
            get { return m_xdocCache; }
            set { m_xdocCache = value; }
        }
        private XDocCacheTemplates m_xdocCacheTemplates = null;
        public XDocCacheTemplates DocCacheTemplates
        {
            get { return m_xdocCacheTemplates; }
            set { m_xdocCacheTemplates = value; }
        }

        public string IncludeLogFile
        {
            get { return m_includeLogFile; }
            set { m_includeLogFile = value; }
        }

        public bool IsWebApplication
        {
            get { return m_isWebApplication; }
        }

        public string AttachmentsVirtualFolder
        {
            get 
            {
                if (m_attVirtualPath == null)
                    throw new Exception("The attachments virtual path has not been specified!");
                return m_attVirtualPath; 
            }
            set 
            {
                if (m_isWebApplication)
                {
                    string path = HttpContext.Current.Server.MapPath(value);
                    if (path == null || path == "")
                        throw new Exception("The specified virtual path does not point to a valid physical folder");
                }

                m_attVirtualPath = value; 
            }
        }


        public string UserLogin
        {
            [SecurityCritical]
            get { return m_userLogin; }
        }

        public string UserName
        {
            [SecurityCritical]
            get { return m_userName; }
        }

        public int UserID
        {
            [SecurityCritical]
            get { return m_userID; }
        }

        public string ServerName
        {
            [SecurityCritical]
            get { return m_serverName; }
        }

        public string DatabaseName
        {
            [SecurityCritical]
            get { return m_databaseName; }
        }

        public object  myHttpContext
        {
            [SecurityCritical]
            get { return m_HttpContext; }
            [SecurityCritical]
            set { m_HttpContext = (HttpContext)value; }
        }
        public string AttachmentsFolder
        {
            get 
            {
                if (m_attPath == null)
                    throw new Exception("The attachments path has not been specified!");
                return m_attPath; 
            }
            set
            {
                if (!Directory.Exists(value))
                    throw new Exception("The specified path does not exist");
                m_attVirtualPath = value;
            }

        }

        public XProviderData AdminDataProvider
        {
            get { return m_connectors.SystemConnector; }
        }

        public Connectors Connectors
        {
            get { return m_connectors; }
        }

        public XProviderData CurrentDataProvider
        {
            get { return m_dataProvider; }
        }

        public ArrayList UserRoles
        {
            get {
                //MD load roles on request
                if (m_currentRoles.Count==0 ) GetCurrentUserAndRoles();
                return m_currentRoles;
            }
        }

        public static Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }

        public static string HTMLDocType
        {
            get { return ".HTM"; }
        }

        public static string AttKeyWord
        {
            get { return "%ATTPATH%"; }
        }

        public static string UniqueParameter
        {
            get { return UNIQUE_PARAMETER; }
        }

        #endregion Public properties


        #region Internal properties

        internal static string UserIDKey
        {
            get { return "USERID"; }
        }
        internal static string UserLoginKey
        {
            get { return "USERLOGIN"; }
        }
        internal static string UserNameKey
        {
            get { return "USERNAME"; }
        }
        internal static string ServerKey
        {
            get { return "SERVER"; }
        }
        internal static string DatabaseKey
        {
            get { return "DATABASE"; }
        }

        internal static string VersionKey
        {
            get { return "VERSION"; }
        }


        #endregion Internal properties


        #region Private methods

        protected void InitPaths()
        {
            try
            {
                if (! string.IsNullOrEmpty(ConfigurationManager.AppSettings["IncludeLogFile"]))
                    m_includeLogFile = ConfigurationManager.AppSettings["IncludeLogFile"];

                if (!m_isWebApplication)
                {
                    m_attPath = ConfigurationManager.AppSettings["XDocAttPath"];
                    if (m_attPath != null && m_attPath != "" && !Directory.Exists(m_attPath))
                        throw new Exception("The attachments path does not specify an existing directory!");
                    if (m_attPath == null || m_attPath == "")
                    {
                        string newPath = Path.Combine(Environment.CurrentDirectory, "Attachments");
                        if (!Directory.Exists(newPath))
                        {
                            try
                            {
                                Directory.CreateDirectory(newPath);
                            }
                            catch (Exception ex)
                            {
                                string message = "Could not create the Attachments folder!" + Environment.NewLine + ex.Message;
                                throw new Exception(message);
                            }
                        }
                        m_attPath = newPath;
                    }
                    m_attVirtualPath = ""; // set m_attVirtualPath != null to avoid throwing an exception when the property is accessed

                    if (m_includeLogFile != "")
                    {
                        string temp = "";
                        try
                        {
                            temp = Path.GetDirectoryName(m_includeLogFile);
                        }
                        catch { }
                        if (temp == "" || !Directory.Exists(temp))
                            throw new Exception("The IncludeLogFile key is not valid");
                    }
                        
                }
                else
                {
                    m_attVirtualPath = ConfigurationManager.AppSettings["XDocAttVirtualPath"];
                    if (m_attVirtualPath == null || m_attVirtualPath == "")
                        throw new Exception("The XDocAttVirtualPath key is not specified in the web.config file!");

                    m_attPath = MapPathFromRoot(ref m_attVirtualPath, true);

                    if (m_attVirtualPath != null && !m_attVirtualPath.EndsWith(Path.AltDirectorySeparatorChar.ToString()))
                        m_attVirtualPath += Path.AltDirectorySeparatorChar.ToString();

                    if (m_includeLogFile != "")
                    {
                        string temp = "";
                        try
                        {
                            temp = System.Web.HttpContext.Current.Server.MapPath(m_includeLogFile);
                        }
                        catch { }
                        if (temp == "" || !Directory.Exists(Path.GetDirectoryName(temp)))
                            throw new Exception("The IncludeLogFile key is not valid");
                        m_includeLogFile = temp;

                    }
                }
            }
            catch (Exception ex)
            {
                string message = "Errors occured while retrieving attachments path! Check the config file." + Environment.NewLine + ex.Message;
                KubionLog.WriteLine(message);
                throw new Exception(message);
            }
        }

        /// <summary>
        /// Removes any preceding characters of the virtual path ("." or "/") and returns the physical path to the folder
        /// </summary>
        public string MapPathFromRoot(ref string virtualPath, bool adjustVirtualPath)
        {
            int validPos = -1;
            for (int i = 0; i < virtualPath.ToCharArray().Length; i++)
            {
                if (char.IsSymbol(virtualPath[i]) || char.IsPunctuation(virtualPath[i]))
                    continue;
                else
                {
                    validPos = i;
                    break;
                }
            }
            if (validPos == -1)
                throw new Exception("The virtual path is not correctly specified in the config file!");

            string temp = virtualPath;
            temp = temp.Substring(validPos);

            if (adjustVirtualPath)
                virtualPath = temp;

            return System.Web.HttpContext.Current.Server.MapPath("~/" + temp);


        }
        void FillConnectionData()
        {
            try
            {
                string sUserName = WindowsIdentity.GetCurrent().Name;
                if (sUserName.IndexOf("\\") != -1)
                    sUserName = sUserName.Substring(sUserName.IndexOf("\\") + 1);
                m_userName = sUserName;
                //DataTable dt = m_dataProvider.GetDataTable("select @@servername, db_name()");
                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    m_serverName = Connectors.GetStringResult(dt.Rows[0][0], "");
                //    m_databaseName = Connectors.GetStringResult(dt.Rows[0][1], "");
                //}
            }
            catch (Exception)
            { }


        }
        void GetCurrentUserAndRoles()
        {
            //string sQueryStatement;
            try
            {
                string sUserName = WindowsIdentity.GetCurrent().Name;
                if (sUserName.IndexOf("\\") != -1)
                    sUserName = sUserName.Substring(sUserName.IndexOf("\\") + 1);
                IDataParameter[] parsCheck = new IDataParameter[]
                {
                    new SqlParameter("@" + K_USERS.SYSTABLENAME, K_USERS.SYSTABLENAME),
                    new SqlParameter("@" + K_ROLE_USERS.SYSTABLENAME, K_ROLE_USERS.SYSTABLENAME)
                };
                //sQueryStatement = SQL.CheckExistUserAndRoles;
                //sQueryStatement = sQueryStatement.Replace("@" + K_USERS.SYSTABLENAME,"'"+ K_USERS.SYSTABLENAME + "'");
                //sQueryStatement = sQueryStatement.Replace("@" + K_ROLE_USERS.SYSTABLENAME, "'" + K_ROLE_USERS.SYSTABLENAME + "'");
                DataTable dtCheck = m_dataProvider.GetDataTable(SQL.CheckExistUserAndRoles, parsCheck);
                //DataTable dtCheck = m_dataProvider.GetDataTable(sQueryStatement);

                if (dtCheck != null && dtCheck.Rows.Count == 1 && dtCheck.Rows[0][0] is int && (int)dtCheck.Rows[0][0] == 1)
                {
                    //sQueryStatement = SQL.GetCurrentUserAndRoles;
                    //sQueryStatement = sQueryStatement.Replace("@" + K_USERS.LOGONNAME, "'" + sUserName + "'");
                    DataTable dt = m_dataProvider.GetDataTable(SQL.GetCurrentUserAndRoles, new IDataParameter[] { new SqlParameter("@" + K_USERS.LOGONNAME, sUserName) });
                    //DataTable dt = m_dataProvider.GetDataTable(sQueryStatement);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string tempUserName = Connectors.GetStringResult(dt.Rows[0][K_USERS.LONGNAME], "");
                        string tempUserLogin = Connectors.GetStringResult(dt.Rows[0][K_USERS.LOGONNAME ], "");
                        int tempUserID = Connectors.GetInt32Result(dt.Rows[0][K_USERS.ID], -1);
                        if (tempUserID != -1 && tempUserName != "")
                        {
                            m_userID = tempUserID;
                            m_userName = tempUserName;
                            m_userLogin = tempUserLogin;
                            m_currentRoles.Clear();
                            foreach (DataRow row in dt.Rows)
                            {
                                int roleID = Connectors.GetInt32Result(row[K_ROLE_USERS.ROLEID], int.MinValue);
                                if (roleID != int.MinValue && !m_currentRoles.Contains(roleID))
                                    m_currentRoles.Add(roleID);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Could not load current user and roles" + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }

        int PathDepth(string physicalPath)
        {
            char sep = Path.DirectorySeparatorChar;
            if (physicalPath.EndsWith(sep.ToString()))
                physicalPath = physicalPath.Substring(0, physicalPath.Length - 1);
            int count = 0;
            foreach (char c in physicalPath.ToCharArray())
                if (c == sep)
                    count++;
            if (!physicalPath.Contains(Path.VolumeSeparatorChar.ToString()) && physicalPath.Length > 0)
                count++;
            return count;
        }

        /// <summary>
        /// Removes the preceding common string (considered a valid path) from two physical paths
        /// </summary>
        public void ReducePaths(ref string currentPhysicalPath, ref string attPath)
        {
            int index = -1;
            string tempCurrent = currentPhysicalPath;
            string tempAtt = attPath;
            if (!tempAtt.EndsWith(Path.DirectorySeparatorChar.ToString()))
                tempAtt += Path.DirectorySeparatorChar;
            if (!tempCurrent.EndsWith(Path.DirectorySeparatorChar.ToString()))
                tempCurrent += Path.DirectorySeparatorChar;


            for (int i = 0; i < tempAtt.ToCharArray().Length && i < tempCurrent.ToCharArray().Length; i++)
            {
                if (tempCurrent[i] == tempAtt[i])
                {
                    if (tempCurrent[i] == Path.DirectorySeparatorChar)
                        index = i;
                }
                else
                    break;
            }
            if (index != -1)
            {
                if (index + 1 <= tempCurrent.Length)
                {
                    tempCurrent = tempCurrent.Substring(index + 1);
                }
                else
                    tempCurrent = "";
                if (index + 1 <= tempAtt.Length)
                {
                    tempAtt = tempAtt.Substring(index + 1);
                }
                else
                    tempAtt = "";
            }

            if (tempAtt.EndsWith(Path.DirectorySeparatorChar.ToString()))
                tempAtt = tempAtt.Substring(0, tempAtt.Length - 1);
            if (tempCurrent.EndsWith(Path.DirectorySeparatorChar.ToString()))
                tempCurrent = tempCurrent.Substring(0, tempCurrent.Length - 1);

            attPath = tempAtt;
            currentPhysicalPath = tempCurrent;

        }

        /// <summary>
        /// Deletes a file or folder older than the specified interval.
        /// </summary>
        /// <param name="path">The full physical path to the file or folder.</param>
        /// <param name="isDirectory">Specifies whether the path is a folder or a file.</param>
        /// <param name="interval">Interval in minutes.</param>
        /// <param name="hasErrors">Indicates whether errors have occured.</param>
        /// <param name="error">Error message.</param>
        private void CleanEntry(string path, bool isDirectory, int interval, ref bool hasErrors, ref string error)
        {
            TimeSpan referenceSpan = new TimeSpan(0, interval, 0);
            DateTime dtLastAccessed = Directory.GetLastAccessTime(path);
            TimeSpan differenceSpan = DateTime.Now.Subtract(dtLastAccessed);
            if (differenceSpan.CompareTo(referenceSpan) >= 0)
            {
                try
                {
                    if (isDirectory && Directory.Exists(path))
                        Directory.Delete(path, true);
                    else if (!isDirectory && File.Exists(path))
                        File.Delete(path);
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    error += ex.Message + Environment.NewLine;
                    hasErrors = true;
                }
            }
        }

        internal void LogTemplateCall(Hashtable parameters, int indentation)
        {
            if (string.IsNullOrEmpty(m_includeLogFile))
                return;

            if (parameters == null)
                return;

            string toWrite = Environment.NewLine + Environment.NewLine;
            if (indentation == 0)
                toWrite += "(" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + ") " ;
            for (int i = 0; i < indentation; i++)
                toWrite += "  ";

            if (indentation > 0)
                toWrite += "-> ";

            string templateName = parameters["templatename"] as string;
            string templateID = parameters["templateid"] as string;

            if (templateName != null)
            {
                toWrite += "TemplateName=" + templateName;
                parameters.Remove("templatename");
            }
            else if (templateID != null)
            {
                toWrite += "TemplateID=" + templateID;
                parameters.Remove("templateid");
            }

            foreach (DictionaryEntry de in parameters)
                toWrite += "; " + de.Key.ToString() + "=" + de.Value.ToString();

            try
            {
                File.AppendAllText(m_includeLogFile, toWrite);
            }
            catch (Exception ex)
            {
                string message = "Could not write to the include log file, path: " + m_includeLogFile + ";" + Environment.NewLine;
                message += ex.Message;
                throw new Exception(message);
            }


        }

        #endregion Private methods


        #region Public Methods

        //public string ImportTemplate(int iConnID, string sTemplateName, string sTemplateXML)
        //{
        //    XDocManager xm;
        //    if (iConnID < 1) xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(iConnID);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    IXTemplate xt;
        //    try
        //    {
        //        xt = xm.LoadTemplate(sTemplateName);
        //    }
        //    catch (Exception ex)
        //    {
        //        xt = xm.NewTemplate();
        //    }
        //    try
        //    {

        //        XMLTemplateParser parser = new XMLTemplateParser(sTemplateXML, xm);

        //        xt.Name = sTemplateName;
        //        xt.Description = parser.GetDescription();
        //        xt.Content = parser.GetContent();

        //        xt.DocType = parser.GetDocType();
        //        xt.IsData = parser.GetIsData();
        //        xt.Redirect = parser.GetRedirect();
        //        xt.Roles = new Collection<int>();
        //        xt.ToSave = parser.GetToSave();
        //        xt.Unattended = parser.GetIsUnattended();
        //        Collection<IXQuery> m_Queries = parser.GetQueries();

        //        IDName[] o_Queries = xt.GetQueriesList();
        //        foreach (IDName queryname in o_Queries)
        //        {
        //            IXQuery xq = xm.LoadQuery(queryname.ID);
        //            xm.DeleteQuery(xq.ID);
        //        }

        //        xt.Save();
        //        foreach (IXQuery xq in m_Queries)
        //        {
        //            xq.TemplateID = xt.ID;
        //            xq.Save();
        //        }
        //        return xt.ID.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Import failed";
        //    }

        //}
        //public string ImportTemplate(int iConnID, int iTemplateID, string sTemplateXML)
        //{
        //    XDocManager xm;
        //    if (iConnID < 1) xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(iConnID);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    IXTemplate xt;
        //    try
        //    {
        //        xt = xm.LoadTemplate(iTemplateID);
        //    }
        //    catch (Exception ex)
        //    {
        //        xt = xm.NewTemplate();
        //    }
        //    try
        //    {

        //        XMLTemplateParser parser = new XMLTemplateParser(sTemplateXML, xm);

        //        xt.Name = parser.GetTemplateName();
        //        xt.Description = parser.GetDescription();
        //        xt.Content = parser.GetContent();

        //        xt.DocType = parser.GetDocType();
        //        xt.IsData = parser.GetIsData();
        //        xt.Redirect = parser.GetRedirect();
        //        xt.Roles = new Collection<int>();
        //        xt.ToSave = parser.GetToSave();
        //        xt.Unattended = parser.GetIsUnattended();
        //        Collection<IXQuery> m_Queries = parser.GetQueries();

        //        IDName[] o_Queries = xt.GetQueriesList();
        //        foreach (IDName queryname in o_Queries)
        //        {
        //            IXQuery xq = xm.LoadQuery(queryname.ID);
        //            xm.DeleteQuery(xq.ID);
        //        }

        //        xt.Save();
        //        foreach (IXQuery xq in m_Queries)
        //        {
        //            xq.TemplateID = xt.ID;
        //            xq.Save();
        //        }
        //        return xt.ID.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Import failed";
        //    }
        //}

        //<Template>
        //    <Name>Util_TransferMD</Name>
        //    <Content><![CDATA[]]></Content>
        //    <Description></Description>
        //    <ToSave>false</ToSave>
        //    <Redirect></Redirect>
        //    <DataTemplate>false</DataTemplate>
        //    <Unattended>false</Unattended>
        //    <Rights></Rights>
        //    <DocType>.HTM</DocType>
        //    <Queries>
        //        <Query>
        //            <Name>GetTemplates</Name>
        //            <Text><![CDATA[]]></Text>
        //        </Query>
        //    </Queries>
        //</Template>
        [SecurityCritical]
        string IXManagerImportExport.ImportTemplate(int iConnID, string sTemplateName, string sTemplateXML)
        {
            XDocManager xm;
            if (iConnID < 1) xm = this;
            else
            {
                IXConnection conn = LoadConnection(iConnID);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            try
            {
                XmlDocument xml_doc = new XmlDocument();
                xml_doc.LoadXml (sTemplateXML);
                if (sTemplateName == "ImportSet")
                {
                    string sResult = "";
                    sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
                    //System.Diagnostics.Debug.WriteLine("LD1:" + name + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));

                    IXTemplate xt;
                    XmlNodeList list = xml_doc.DocumentElement.ChildNodes;
                    foreach (XmlNode nodet in list)
                    {
                        sTemplateName = GetXMLNodeValue(nodet, "Name");
                        try
                        {
                            xt = xm.LoadTemplate(sTemplateName);
                        }
                        catch (Exception ex)
                        {
                            xt = xm.NewTemplate();
                        }

                        ImportFromXML(xt, xm, nodet, sTemplateName, "");
                        string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name; 
                        sResult += s + "\n";
                    }
                    sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
                    return sResult;
                }
                else
                {
                    if (sTemplateName == "fromXML") sTemplateName = GetXMLNodeValue(xml_doc.DocumentElement, "Name");
                    IXTemplate xt;
                    try
                    {
                        xt = xm.LoadTemplate(sTemplateName);
                    }
                    catch (Exception ex)
                    {
                        xt = xm.NewTemplate();
                    }

                    ImportFromXML(xt, xm, xml_doc, sTemplateName, "");
                    string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name;
                    return s;
                }
            }
            catch (Exception ex)
            {
                return "Error: Import failed. " + ex.Message ;
            }
            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
            finally
            {
                if (iConnID >= 1 && xm != null) xm.Dispose();
            }

        }
        [SecurityCritical]
        string IXManagerImportExport.ImportTemplate(int iConnID, int iTemplateID, string sTemplateXML)
        {
            XDocManager xm;
            if (iConnID < 1) xm = this;
            else
            {
                IXConnection conn = LoadConnection(iConnID);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            IXTemplate xt;
            try
            {
                xt = xm.LoadTemplate(iTemplateID);
            }
            catch (Exception ex)
            {
                xt = xm.NewTemplate();
            }
            try
            {
                XmlDocument xml_doc = new XmlDocument();
                xml_doc.LoadXml(sTemplateXML);
                ImportFromXML(xt, xm, xml_doc, "", "");
                return xt.ID.ToString();
            }
            catch (Exception ex)
            {
                return "Error: Import failed";
            }
            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
            finally
            {
                if (iConnID >= 1 && xm != null) xm.Dispose();
            }
        }
        [SecurityCritical]
        //LIGHT
        //string IXManagerImportExport.ImportTemplate(string sConn, string sTemplateName, string sTemplateXML)
        //{
        //    System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "import manager started");
        //    XDocManager xm;
        //    if (sConn == "") xm = this;
        //    else
        //    {
        //        IXConnection conn = LoadConnection(sConn);
        //        if (conn == null) return "Error: Invalid connection";
        //        xm = new XDocManager(conn.ConnectionString);
        //        if (xm == null) return "Error: Invalid connection";
        //    }
        //    try
        //    {
        //        XmlDocument xml_doc = new XmlDocument();
        //        xml_doc.LoadXml(sTemplateXML);
        //        System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "xml loaded");
        //        if (sTemplateName == "ImportSet")
        //        {
        //            string sResult = "";
        //            sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
        //            IXTemplate xt;
        //            XmlNodeList list = xml_doc.DocumentElement.ChildNodes;
        //            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "loop start");
        //            foreach (XmlNode nodet in list)
        //            {
        //                try
        //                {
        //                    System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item start");
        //                    sTemplateName = GetXMLNodeValue(nodet, "Name");
        //                    string sContent = GetXMLNodeValue(nodet, "Content");
        //                    sContent = sContent.Replace(Utils.CT_ENDCDATA, "]]>");
        //                    sContent = sContent.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
        //                    sContent = sContent.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
        //                    string sContentExpanded = new XTemplate(this).ExpandContent(sContent);
        //                    if (sContentExpanded == sContent) 
        //                        sContentExpanded = "{no_macros}";
        //                    sContent = sContent.Replace("'", "''");
        //                    sContentExpanded = sContentExpanded.Replace("'", "''");
        //                    string sSQL = "";
        //                    sSQL += "UPDATE S_TEMPLATES SET TEMPLATECONTENT='" + sContent + "' , TEMPLATECONTENTEXPANDED ='" + sContentExpanded + "'  WHERE TEMPLATENAME = '" + sTemplateName + "' ";
        //                    sSQL += "if @@ROWCOUNT=0 BEGIN ";
        //                    sSQL += "INSERT INTO S_TEMPLATES (TEMPLATENAME, TEMPLATEDESCRIPTION, TEMPLATECONTENT, TEMPLATECONTENTEXPANDED, TEMPLATETOSAVE, TEMPLATETOREDIRECT, UNATTENDED, DATATEMPLATE,RIGHTS, DOCTYPE)";
        //                    sSQL += "VALUES ( '" + sTemplateName + "', '', '" + sContent + "',  '" + sContentExpanded + "', 0, null, 0, 1, '', ''); SELECT SCOPE_IDENTITY() AS TEMPLATEID ";
        //                    sSQL += " END else SELECT TemplateID FROM S_TEMPLATES WHERE TEMPLATENAME = '" + sTemplateName + "'; ";
        //                    string sTemplateID = "-1";
        //                    DataTable dt = xm.m_dataProvider.GetDataTable(sSQL);
        //                    if (dt != null && dt.Rows.Count > 0)
        //                        sTemplateID = dt.Rows[0][0].ToString();

        //                    string s = "ID:" + sTemplateID + " Name:" + sTemplateName;
        //                    sResult += s + "\n";
        //                    System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item end");
        //                }
        //                catch (Exception ex)
        //                {
        //                    System.Diagnostics.Debug.WriteLine(ex.Message );
        //                }

        //            }
        //            sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
        //            return sResult;
        //        }
        //        else
        //        {
        //            if (sTemplateName == "fromXML") sTemplateName = GetXMLNodeValue(xml_doc.DocumentElement, "Name");
        //            IXTemplate xt;
        //            try
        //            {
        //                xt = xm.LoadTemplate(sTemplateName);
        //            }
        //            catch (Exception ex)
        //            {
        //                xt = xm.NewTemplate();
        //            }

        //            ImportFromXML(xt, xm, xml_doc, sTemplateName, "");
        //            string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name;
        //            return s;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error: Import failed. " + ex.Message;
        //    }
        //    /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
        //    finally
        //    {
        //        if ((sConn != "") && (xm != null)) xm.Dispose();
        //    }

        //}
            //MIXED
        string IXManagerImportExport.ImportTemplate(string sConn, string sTemplateName, string sTemplateXML)
        {
            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "import manager started");
            XDocManager xm;
            if (sConn == "") xm = this;
            else
            {
                IXConnection conn = LoadConnection(sConn);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            try
            {
                XmlDocument xml_doc = new XmlDocument();
                xml_doc.LoadXml(sTemplateXML);
                System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "xml loaded");
                if (sTemplateName == "ImportSet")
                {
                    string sResult = "";
                    sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
                    IXTemplate xt;
                    XmlNodeList list = xml_doc.DocumentElement.ChildNodes;
                    System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "loop start");
                    foreach (XmlNode nodet in list)
                    {
                        try
                        {
                            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item start");
                            sTemplateName = GetXMLNodeValue(nodet, "Name");
                            string sContent = GetXMLNodeValue(nodet, "Content");
                            sContent = sContent.Replace(Utils.CT_ENDCDATA, "]]>");
                            sContent = sContent.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
                            sContent = sContent.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
                            string sContentExpanded = new XTemplate(this).ExpandContent(sContent);
                            if (sContentExpanded == sContent)
                                sContentExpanded = "{no_macros}";
                            sContent = sContent.Replace("'", "''");
                            sContentExpanded = sContentExpanded.Replace("'", "''");
                            string sSQL = "";
                            sSQL += "UPDATE S_TEMPLATES SET TEMPLATECONTENT='" + sContent + "' , TEMPLATECONTENTEXPANDED ='" + sContentExpanded + "'  WHERE TEMPLATENAME = '" + sTemplateName + "' ";
                            sSQL += "if @@ROWCOUNT=0 BEGIN ";
                            sSQL += "INSERT INTO S_TEMPLATES (TEMPLATENAME, TEMPLATEDESCRIPTION, TEMPLATECONTENT, TEMPLATECONTENTEXPANDED, TEMPLATETOSAVE, TEMPLATETOREDIRECT, UNATTENDED, DATATEMPLATE,RIGHTS, DOCTYPE)";
                            sSQL += "VALUES ( '" + sTemplateName + "', '', '" + sContent + "',  '" + sContentExpanded + "', 0, null, 0, 1, '', ''); SELECT SCOPE_IDENTITY() AS TEMPLATEID ";
                            sSQL += " END else SELECT TemplateID FROM S_TEMPLATES WHERE TEMPLATENAME = '" + sTemplateName + "'; ";
                            string sTemplateID = "-1";
                            DataTable dt = xm.m_dataProvider.GetDataTable(sSQL);
                            if (dt != null && dt.Rows.Count > 0)
                                sTemplateID = dt.Rows[0][0].ToString();

                            string s = "ID:" + sTemplateID + " Name:" + sTemplateName;
                            sResult += s + "\n";
                            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item end");
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                        }

                    }
                    sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
                    return sResult;
                }
                else if (sTemplateName == "ImportSetFull")
                {
                    string sResultFull = "";
                    sResultFull += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
                    IXTemplate xt;
                    XmlNodeList list = xml_doc.DocumentElement.ChildNodes;
                    System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "loop start");
                    foreach (XmlNode nodet in list)
                    {
                        try
                        {
                            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item start");
                            sTemplateName = GetXMLNodeValue(nodet, "Name");
                            try
                            {
                                xt = xm.LoadTemplate(sTemplateName);
                            }
                            catch (Exception ex)
                            {
                                xt = xm.NewTemplate();
                            }
                            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "ImportFromXML start");
                            ImportFromXML(xt, xm, nodet, sTemplateName, "");
                            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "ImportFromXML end");
                            string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name;
                            sResultFull += s + "\n";
                            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item end");
                        }
                        catch (Exception)
                        { }

                    }
                    sResultFull += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
                    return sResultFull;
                }
                else
                {
                    if (sTemplateName == "fromXML") sTemplateName = GetXMLNodeValue(xml_doc.DocumentElement, "Name");
                    IXTemplate xt;
                    try
                    {
                        xt = xm.LoadTemplate(sTemplateName);
                    }
                    catch (Exception ex)
                    {
                        xt = xm.NewTemplate();
                    }

                    ImportFromXML(xt, xm, xml_doc, sTemplateName, "");
                    string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name;
                    return s;
                }
            }
            catch (Exception ex)
            {
                return "Error: Import failed. " + ex.Message;
            }
            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
            finally
            {
                if ((sConn != "") && (xm != null)) xm.Dispose();
            }

        }
//OLD/CLASIC
//        string IXManagerImportExport.ImportTemplate(string sConn, string sTemplateName, string sTemplateXML)
//        {
//System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "import manager started");
//            XDocManager xm;
//            if (sConn == "") xm = this;
//            else
//            {
//                IXConnection conn = LoadConnection(sConn);
//                if (conn == null) return "Error: Invalid connection";
//                xm = new XDocManager(conn.ConnectionString);
//                if (xm == null) return "Error: Invalid connection";
//            }
//            try
//            {
//                XmlDocument xml_doc = new XmlDocument();
//                xml_doc.LoadXml (sTemplateXML);
//System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "xml loaded");
//                if (sTemplateName == "ImportSet")
//                {
//                    string sResult = "";
//                    sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
//                    IXTemplate xt;
//                    XmlNodeList list = xml_doc.DocumentElement.ChildNodes;
//System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "loop start");
//                    foreach (XmlNode nodet in list)
//                    {
//                        try
//                        {
//System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item start");
//                            sTemplateName = GetXMLNodeValue(nodet, "Name");
//                            try
//                            {
//                                xt = xm.LoadTemplate(sTemplateName);
//                            }
//                            catch (Exception ex)
//                            {
//                                xt = xm.NewTemplate();
//                            }
//System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "ImportFromXML start");
//                            ImportFromXML(xt, xm, nodet, sTemplateName, "");
//System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "ImportFromXML end");
//                            string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name;
//                            sResult += s + "\n";
//System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "item end");
//                        }
//                        catch (Exception)
//                        { }

//                    }
//                    sResult += DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "\n";
//                    return sResult;
//                }
//                else
//                {
//                    if (sTemplateName == "fromXML") sTemplateName = GetXMLNodeValue(xml_doc.DocumentElement, "Name");
//                    IXTemplate xt;
//                    try
//                    {
//                        xt = xm.LoadTemplate(sTemplateName);
//                    }
//                    catch (Exception ex)
//                    {
//                        xt = xm.NewTemplate();
//                    }

//                    ImportFromXML(xt, xm, xml_doc, sTemplateName, "");
//                    string s = "ID:" + xt.ID.ToString() + " Name:" + xt.Name;
//                    return s;
//                }
//            }
//            catch (Exception ex)
//            {
//                return "Error: Import failed. " + ex.Message ;
//            }
//            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
//            finally
//            {
//                if ((sConn != "") && (xm != null)) xm.Dispose();
//            }

//        }
        [SecurityCritical]
        string IXManagerImportExport.ImportTemplate(string sConn, int iTemplateID, string sTemplateXML)
        {
            XDocManager xm;
            if (sConn == "") xm = this;
            else
            {
                IXConnection conn = LoadConnection(sConn);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            IXTemplate xt;
            try
            {
                xt = xm.LoadTemplate(iTemplateID);
            }
            catch (Exception ex)
            {
                xt = xm.NewTemplate();
            }
            try
            {
                XmlDocument xml_doc = new XmlDocument();
                xml_doc.LoadXml(sTemplateXML);
                ImportFromXML(xt, xm, xml_doc, "", "");
                return xt.ID.ToString();
            }
            catch (Exception ex)
            {
                return "Error: Import failed";
            }
            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
            finally
            {
                if ((sConn != "") && (xm != null)) xm.Dispose();
            }
        }
        [SecurityCritical]
        string IXManagerImportExport.ExportTemplatesLike(int iConnID, string sTemplatePattern, string sPostfix)
        {
            string sResult = "";
            XDocManager xm;
            if (iConnID < 1) xm = this;
            else
            {
                IXConnection conn = LoadConnection(iConnID);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            } 
            sResult = "<TemplateSet>";

            string sSQL = "SELECT TemplateName FROM [S_Templates] WHERE TemplateName LIKE '" + sTemplatePattern + "' ESCAPE '\\' ORDER BY TemplateName ASC ";
            DataTable dt = xm.Connectors.SystemConnector.GetDataTable(sSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string sTemplateName = Connectors.GetStringResult(row[0], "");
                    IXTemplate xt;
                    try
                    {
                        xt = xm.LoadTemplate(sTemplateName);
                    }
                    catch (Exception ex)
                    {
                        return "Error: Invalid template";
                    }
                    xt.Name = xt.Name + sPostfix;
                    XmlDocument xml_doc = new XmlDocument();
                    xml_doc = ExportToXML(xt, xm, true, true);
                    sResult += xml_doc.OuterXml;

                }
            }

            sResult += "</TemplateSet>";

            if (iConnID >= 1 && xm != null) xm.Dispose();
            return sResult;
        }
        [SecurityCritical]
        string IXManagerImportExport.ExportTemplate(int iConnID, string sTemplateName)
        {
            XDocManager xm;
            if (iConnID < 1) xm = this;
            else
            {
                IXConnection conn = LoadConnection(iConnID);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            IXTemplate xt;
            try
            {
                xt = xm.LoadTemplate(sTemplateName);
            }
            catch (Exception ex)
            {
                return "Error: Invalid template";
            }
            XmlDocument xml_doc = new XmlDocument();
            xml_doc = ExportToXML(xt, xm, true, true);

            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
            if (iConnID >= 1 && xm != null) xm.Dispose();

            return xml_doc.OuterXml ; 
        }
        [SecurityCritical]
        string IXManagerImportExport.ExportTemplate(int iConnID, int iTemplateID)
        {
            XDocManager xm;
            if (iConnID < 1) xm = this;
            else
            {
                IXConnection conn = LoadConnection(iConnID);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            IXTemplate xt;
            try
            {
                xt = xm.LoadTemplate(iTemplateID );
            }
            catch (Exception ex)
            {
                return "Error: Invalid template";
            }
            XmlDocument xml_doc = new XmlDocument();
            xml_doc = ExportToXML(xt, xm, true, true);

            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
            if (iConnID >= 1 && xm != null) xm.Dispose();

            return xml_doc.OuterXml;
        }
        [SecurityCritical]
        string IXManagerImportExport.ExportTemplatesLike(string sConn, string sTemplatePattern, string sPostfix)
        {
            string sResult = "";
            XDocManager xm;
            if (sConn == "") xm = this;
            else
            {
                IXConnection conn = LoadConnection(sConn);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            sResult = "<TemplateSet>";

            string sSQL = "SELECT TemplateName FROM [S_Templates] WHERE TemplateName LIKE '" + sTemplatePattern + "' ESCAPE '\\' ORDER BY TemplateName ASC ";
            DataTable dt = xm.Connectors.SystemConnector.GetDataTable(sSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string sTemplateName = Connectors.GetStringResult(row[0], "");
                    IXTemplate xt;
                    try
                    {
                        xt = xm.LoadTemplate(sTemplateName);
                    }
                    catch (Exception ex)
                    {
                        return "Error: Invalid template";
                    }
                    xt.Name = xt.Name + sPostfix;
                    XmlDocument xml_doc = new XmlDocument();
                    xml_doc = ExportToXML(xt, xm, true, true);
                    sResult += xml_doc.OuterXml; 

                }
            }

            sResult +=  "</TemplateSet>";

            if ((sConn != "") && (xm != null)) xm.Dispose();
            return sResult ;
        }
        [SecurityCritical]
        string IXManagerImportExport.ExportTemplate(string sConn, string sTemplateName)
        {
            XDocManager xm;
            if (sConn == "") xm = this;
            else
            {
                IXConnection conn = LoadConnection(sConn);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            IXTemplate xt;
            try
            {
                xt = xm.LoadTemplate(sTemplateName);
            }
            catch (Exception ex)
            {
                return "Error: Invalid template";
            }
            XmlDocument xml_doc = new XmlDocument();
            xml_doc = ExportToXML(xt, xm, true, true);

            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
            if ((sConn != "") && (xm != null)) xm.Dispose();

            return xml_doc.OuterXml;
        }
        [SecurityCritical]
        string IXManagerImportExport.ExportTemplate(string sConn, int iTemplateID)
        {
            XDocManager xm;
            if (sConn == "") xm = this;
            else
            {
                IXConnection conn = LoadConnection(sConn);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            IXTemplate xt;
            try
            {
                xt = xm.LoadTemplate(iTemplateID);
            }
            catch (Exception ex)
            {
                return "Error: Invalid template";
            }
            XmlDocument xml_doc = new XmlDocument();
            xml_doc = ExportToXML(xt, xm, true, true);

            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
            if ((sConn != "") && (xm != null)) xm.Dispose();

            return xml_doc.OuterXml;
        }

        [SecurityCritical]
        string IXManagerImportExport.RemoveTemplate(int iConnID, string sTemplateName)
        {
            XDocManager xm;
            if (iConnID < 1) xm = this;
            else
            {
                IXConnection conn = LoadConnection(iConnID);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            try
            {
                xm.DeleteTemplate(sTemplateName);
            }
            catch (Exception ex)
            {
                return "Error: Invalid template";
            }

            if (iConnID >= 1 && xm != null) xm.Dispose();

            return sTemplateName;
        }
        [SecurityCritical]
        string IXManagerImportExport.RemoveTemplate(int iConnID, int iTemplateID)
        {
            XDocManager xm;
            if (iConnID < 1) xm = this;
            else
            {
                IXConnection conn = LoadConnection(iConnID);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            try
            {
                xm.DeleteTemplate(iTemplateID);
            }
            catch (Exception ex)
            {
                return "Error: Invalid template";
            }

            if (iConnID >= 1 && xm != null) xm.Dispose();

            return iTemplateID.ToString () ;
        }
        [SecurityCritical]
        string IXManagerImportExport.RemoveTemplate(string sConn, string sTemplateName)
        {
            XDocManager xm;
            if (sConn == "") xm = this;
            else
            {
                IXConnection conn = LoadConnection(sConn);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            try
            {
                xm.DeleteTemplate (sTemplateName);
            }
            catch (Exception ex)
            {
                return "Error: Invalid template";
            }

            if ((sConn != "") && (xm != null)) xm.Dispose();

            return sTemplateName ;
        }
        [SecurityCritical]
        string IXManagerImportExport.RemoveTemplate(string sConn, int iTemplateID)
        {
            XDocManager xm;
            if (sConn == "") xm = this;
            else
            {
                IXConnection conn = LoadConnection(sConn);
                if (conn == null) return "Error: Invalid connection";
                xm = new XDocManager(conn.ConnectionString);
                if (xm == null) return "Error: Invalid connection";
            }
            try
            {
                xm.LoadTemplate(iTemplateID);
            }
            catch (Exception ex)
            {
                return "Error: Invalid template";
            }

            if ((sConn != "") && (xm != null)) xm.Dispose();

            return iTemplateID.ToString ();
        }


        private int ImportFromXML(IXTemplate xt, XDocManager xm, XmlDocument xmlDoc, string name, string content)
        {
            XmlNode root = xmlDoc.DocumentElement;
            if (name == "")
                xt.Name = GetXMLNodeValue(root, "Name");
            else
                xt.Name = name;
            if (content == "")
                xt.Content = GetXMLNodeValue(root, "Content");
            else
                xt.Content = content;
            xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA, "]]>");
            xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
            xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
            xt.Description = GetXMLNodeValue(root, "Description");
            xt.ToSave = XmlConvert.ToBoolean(GetXMLNodeValue(root, "ToSave","false"));
            xt.Redirect = GetXMLNodeValue(root, "Redirect"); 
            xt.Unattended = XmlConvert.ToBoolean(GetXMLNodeValue(root, "Unattended","false"));
            xt.IsData = XmlConvert.ToBoolean(GetXMLNodeValue(root, "DataTemplate", "true"));
            string sRights = GetXMLNodeValue(root, "Rights"); 
            string[] aRights = sRights.Split(';');
            int iRight;
            foreach (string sRight in aRights)
            {
                if (int.TryParse(sRight, out iRight))
                    if (iRight != 0) xt.Roles.Add(iRight);
            }
            xt.DocType = GetXMLNodeValue(root, "DocType"); 

            xt.Save();

            IDName[] queries = xt.GetQueriesList();
            foreach (IDName queryIDName in queries)
            {
                xm.DeleteQuery (queryIDName.ID);
            }

            XmlElement nodeQueries = (XmlElement)root.SelectSingleNode ("Queries");
            foreach (XmlElement nQuery in nodeQueries.ChildNodes)
            {
                IXQuery xq = xm.NewQuery();
                xq.TemplateID = xt.ID;
                xq.Name = GetXMLNodeValue(nQuery, "Name");
                xq.QueryText = GetXMLNodeValue(nQuery, "Text");
                xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA, "]]>");
                xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
                xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
                xq.ConnectionID = -1;
                XmlNodeList nConn = nQuery.GetElementsByTagName("Connection");
                if (nConn.Count > 0)
                {
                    string sConnectionText = GetXMLNodeValue(nConn[0], "ConnectionString");
                    string sConnectionName = GetXMLNodeValue(nConn[0], "Name");
                    try
                    {
                        IXConnection xc = xm.LoadConnection(sConnectionName);
                        xq.ConnectionID = xc.ID;
                    }
                    catch (Exception)
                    {
                        if (sConnectionName == "")
                            xq.ConnectionID = -1;
                        else
                        {
                            IXConnection xc = xm.NewConnection();
                            xc.Name = sConnectionName;
                            sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA, "]]>");
                            sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
                            sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
                            xc.ConnectionString = sConnectionText;
                            xc.Save();
                            xq.ConnectionID = xc.ID;
                        }
                    }
                }
                xq.Save();
            }
            return xt.ID;
        }
        private int ImportFromXML(IXTemplate xt, XDocManager xm, XmlNode root, string name, string content)
        {
            if (name == "")
                xt.Name = GetXMLNodeValue(root, "Name");
            else
                xt.Name = name;
            if (content == "")
                xt.Content = GetXMLNodeValue(root, "Content");
            else
                xt.Content = content;
            xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA, "]]>");
            xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
            xt.Content = xt.Content.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
            xt.Description = GetXMLNodeValue(root, "Description");
            xt.ToSave = XmlConvert.ToBoolean(GetXMLNodeValue(root, "ToSave", "false"));
            xt.Redirect = GetXMLNodeValue(root, "Redirect");
            xt.Unattended = XmlConvert.ToBoolean(GetXMLNodeValue(root, "Unattended", "false"));
            xt.IsData = XmlConvert.ToBoolean(GetXMLNodeValue(root, "DataTemplate", "true"));
            string sRights = GetXMLNodeValue(root, "Rights");
            string[] aRights = sRights.Split(';');
            int iRight;
            foreach (string sRight in aRights)
            {
                if (int.TryParse(sRight, out iRight))
                    if (iRight != 0) xt.Roles.Add(iRight);
            }
            xt.DocType = GetXMLNodeValue(root, "DocType");

            xt.Save();

            IDName[] queries = xt.GetQueriesList();
            foreach (IDName queryIDName in queries)
            {
                xm.DeleteQuery(queryIDName.ID);
            }

            XmlElement nodeQueries = (XmlElement)root.SelectSingleNode("Queries");
            foreach (XmlElement nQuery in nodeQueries.ChildNodes)
            {
                IXQuery xq = xm.NewQuery();
                xq.TemplateID = xt.ID;
                xq.Name = GetXMLNodeValue(nQuery, "Name");
                xq.QueryText = GetXMLNodeValue(nQuery, "Text");
                xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA, "]]>");
                xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
                xq.QueryText = xq.QueryText.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
                xq.ConnectionID = -1;
                XmlNodeList nConn = nQuery.GetElementsByTagName("Connection");
                if (nConn.Count > 0)
                {
                    string sConnectionText = GetXMLNodeValue(nConn[0], "ConnectionString");
                    string sConnectionName = GetXMLNodeValue(nConn[0], "Name");
                    try
                    {
                        IXConnection xc = xm.LoadConnection(sConnectionName);
                        xq.ConnectionID = xc.ID;
                    }
                    catch (Exception)
                    {
                        IXConnection xc = xm.NewConnection();
                        xc.Name = sConnectionName;
                        sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA, "]]>");
                        sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA);
                        sConnectionText = sConnectionText.Replace(Utils.CT_ENDCDATA3, Utils.CT_ENDCDATA2);
                        xc.ConnectionString = sConnectionText;
                        xc.Save();
                        xq.ConnectionID = xc.ID;

                    }
                }
                xq.Save();
            }
            return xt.ID;
        }

        private string GetXMLNodeValue(XmlNode queryNode, string childNode)
        {
            return GetXMLNodeValue(queryNode, childNode, "");
        }
        private string GetXMLNodeValue(XmlNode queryNode, string childNode, string defaultValue)
        {
            if (queryNode.SelectSingleNode(childNode) != null)
                return queryNode.SelectSingleNode(childNode).FirstChild is XmlCDataSection ?
                    (queryNode.SelectSingleNode(childNode).FirstChild as XmlCDataSection).Value.Replace("<.!.[.CDATA.[", "<![CDATA[").Replace("].].>", "]]>") :
                    queryNode.SelectSingleNode(childNode).InnerText ;
            else
                if (queryNode.Attributes[childNode] != null)
                    return queryNode.Attributes[childNode].Value;
                else return defaultValue;
        }

        private XmlDocument ExportToXML(IXTemplate xt, XDocManager xm, bool bComplete, bool bContent)
        {
            string sCDATAContent;
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("Template");
            doc.AppendChild(root);

            XmlCDataSection  elem_cData ;

            XmlElement elem = doc.CreateElement("Name");
            elem.InnerText = xt.Name;
            root.AppendChild(elem);


            if (bContent)
            {
                elem = doc.CreateElement("Content");
                //elem_cData = doc.CreateCDataSection(xt.Content.Replace("<![CDATA[","<.!.[.CDATA.[").Replace("]]>","].].>" ));
                sCDATAContent = xt.Content;
                sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
                sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
                sCDATAContent = sCDATAContent.Replace("]]>", Utils.CT_ENDCDATA);
                elem_cData = doc.CreateCDataSection(sCDATAContent);
                elem.AppendChild(elem_cData);
                root.AppendChild(elem);
            }

            elem = doc.CreateElement("Description");
            elem.InnerText = xt.Description;
            root.AppendChild(elem);

            elem = doc.CreateElement("ToSave");
            elem.InnerText = XmlConvert.ToString(xt.ToSave);
            root.AppendChild(elem);

            elem = doc.CreateElement("Redirect");
            elem.InnerText = xt.Redirect;
            root.AppendChild(elem);

            elem = doc.CreateElement("DataTemplate");
            elem.InnerText = XmlConvert.ToString(xt.IsData);
            root.AppendChild(elem);

            elem = doc.CreateElement("Unattended");
            elem.InnerText = XmlConvert.ToString(xt.Unattended);
            root.AppendChild(elem);

            if (bComplete)
            {
                string sRoles = "";
                foreach (int role in xt.Roles)
                    sRoles += ";" + role.ToString();
                if (sRoles.Length > 0) sRoles = sRoles.Substring(1);
                elem = doc.CreateElement("Rights");
                elem.InnerText = sRoles;
                root.AppendChild(elem);

                elem = doc.CreateElement("DocType");
                elem.InnerText = xt.DocType;
                root.AppendChild(elem);
            }

            XmlElement elemQueries = doc.CreateElement("Queries");
            root.AppendChild(elemQueries);

            IDName[] queries = xt.GetQueriesList();
            foreach (IDName queryIDName in queries)
            {
                XDocuments.IXQuery xq = xm.LoadQuery(queryIDName.ID);

                XmlElement elemQuery = doc.CreateElement("Query");

                elem = doc.CreateElement("Name");
                elem.InnerText = xq.Name;
                elemQuery.AppendChild(elem);

                elem = doc.CreateElement("Text");
                //elem_cData = doc.CreateCDataSection(xq.QueryText.Replace("<![CDATA[", "<.!.[.CDATA.[").Replace("]]>", "].].>"));
                sCDATAContent = xq.QueryText;
                sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
                sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
                sCDATAContent = sCDATAContent.Replace("]]>", Utils.CT_ENDCDATA);
                elem_cData = doc.CreateCDataSection(sCDATAContent);

                elem.AppendChild(elem_cData);
                elemQuery.AppendChild(elem);

                XmlElement elemConnection = doc.CreateElement("Connection");

                if (xq.ConnectionID != -1)
                {

                    XDocuments.IXConnection xc = xm.LoadConnection(xq.ConnectionID);

                    elem = doc.CreateElement("Name");
                    elem.InnerText = xc.Name;
                    elemConnection.AppendChild(elem);

                    if (bComplete)
                    {
                        elem = doc.CreateElement("ConnectionString");
//                        elem_cData = doc.CreateCDataSection(xc.ConnectionString.Replace("<![CDATA[", "<.!.[.CDATA.[").Replace("]]>", "].].>"));
                        sCDATAContent = xc.ConnectionString;
                        sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
                        sCDATAContent = sCDATAContent.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
                        sCDATAContent = sCDATAContent.Replace("]]>", Utils.CT_ENDCDATA);
                        elem_cData = doc.CreateCDataSection(sCDATAContent);
                        elem.AppendChild(elem_cData);
                        elemConnection.AppendChild(elem);
                    }
                    elemQuery.AppendChild(elemConnection);
                }

                elemQueries.AppendChild(elemQuery);


            }

            return doc;
        }

        public IDName[] GetDocumentList()
        {
            try
            {
                DataTable dtResults = m_dataProvider.GetDataTable(SQL.GetDocumentsList);
                if (dtResults == null)
                    throw new Exception("");
                ArrayList ret = new ArrayList();
                foreach (DataRow row in dtResults.Rows)
                {
                    int id = Connectors.GetInt32Result(row[S_DOCUMENTS.DOCID]);
                    string version = Connectors.GetInt32Result(row[S_DOCUMENTS.VERSION]).ToString();
                    string docType = Connectors.GetStringResult(row[S_DOCUMENTS.DOCTYPE]);
                    IDName item = new IDName(id, docType + ";" + version);
                    ret.Add(item);
                }

                return (IDName[])ret.ToArray(typeof(IDName));
            }
            catch (Exception ex)
            {
                string message = "Could not retrieve documents list" + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                throw new Exception(message);
            }
        }

        public IDName[] GetTemplatesList()
        {
            try
            {
                DataTable dtResults = m_dataProvider.GetDataTable(SQL.GetTemplatesList);
                if (dtResults == null)
                    throw new Exception("");
                ArrayList ret = new ArrayList();
                foreach (DataRow row in dtResults.Rows)
                {
                    int id = Connectors.GetInt32Result(row[S_TEMPLATES.TEMPLATEID]);
                    string name = Connectors.GetStringResult(row[S_TEMPLATES.TEMPLATENAME]);
                    IDName item = new IDName(id, name);
                    ret.Add(item);
                }

                return (IDName[])ret.ToArray(typeof(IDName));
            }
            catch (Exception ex)
            {
                string message = "Could not retrieve templates list" + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                throw new Exception(message);
            }
        }

        public IDName[] GetConnectionsList()
        {
            try
            {
                DataTable dtResults = m_dataProvider.GetDataTable(SQL.GetConnectionsList);
                if (dtResults == null)
                    throw new Exception("");
                ArrayList ret = new ArrayList();
                foreach (DataRow row in dtResults.Rows)
                {
                    int id = Connectors.GetInt32Result(row[S_CONNECTIONS.CONNID]);
                    string name = Connectors.GetStringResult(row[S_CONNECTIONS.CONNNAME]);
                    IDName item = new IDName(id, name);
                    ret.Add(item);
                }

                return (IDName[])ret.ToArray(typeof(IDName));
            }
            catch (Exception ex)
            {
                string message = "Could not retrieve templates list" + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                throw new Exception(message);
            }
        }

        public IDName[] GetQueriesList()
        {
            return GetQueriesList(-1);
        }

        public IDName[] GetQueriesList(int templateID)
        {
            //string sQueryStatement;

            try
            {
                //sQueryStatement = SQL.GetQueriesList;
                //sQueryStatement = sQueryStatement.Replace("@PNOTEMPLATE", templateID.ToString());
                //sQueryStatement = sQueryStatement.Replace("@" + S_TEMPLATES.TEMPLATEID, templateID.ToString());
                DataTable dtResults = m_dataProvider.GetDataTable(SQL.GetQueriesList, new IDataParameter[] { new SqlParameter("@PNOTEMPLATE", templateID), new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, templateID) });
                //DataTable dtResults = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResults == null)
                    throw new Exception("");
                ArrayList ret = new ArrayList();
                foreach (DataRow row in dtResults.Rows)
                {
                    int id = Connectors.GetInt32Result(row[S_QUERIES.QUERYID]);
                    string name = Connectors.GetStringResult(row[S_QUERIES.QUERYNAME]);
                    IDName item = new IDName(id, name);
                    ret.Add(item);
                }

                return (IDName[])ret.ToArray(typeof(IDName));
            }
            catch (Exception ex)
            {
                string message = "Could not retrieve templates list" + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                throw new Exception(message);
            }
        }


        /// <summary>
        /// Cleans all the files and folders in the attachments folder older than the specified interval.
        /// </summary>
        /// <param name="interval">Minimum age in minutes of files and folders that must be deleted. </param>
        public void CleanFiles(int interval)
        {
            string error = "";
            bool hasErrors = false;
            if (!Directory.Exists(AttachmentsFolder) || !m_isWebApplication)
                return;
            try
            {
                string[] directories = Directory.GetDirectories(AttachmentsFolder, "", SearchOption.TopDirectoryOnly);
                foreach (string directory in directories)
                    CleanEntry(directory, true, interval, ref hasErrors, ref error);

                string[] files = Directory.GetFiles(AttachmentsFolder, "", SearchOption.TopDirectoryOnly);
                foreach (string file in files)
                    CleanEntry(file, false, interval, ref hasErrors, ref error);

                if (hasErrors)
                    throw new Exception(error);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Error while cleaning files: " + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }

        /// <summary>
        /// Returns the current relative virtual path for attachments. 
        /// This method is used to access attachment URIs from an aspx page which is not found in the root of the Virtual Folder.
        /// </summary>
        public string GetCurrentAttachmentsVirtualPath()
        {
            string ret = "";

            string attPath = AttachmentsFolder;

            string currentPhysicalPath = Environment.CurrentDirectory;
            if (m_isWebApplication)
                currentPhysicalPath = HttpContext.Current.Server.MapPath("");

            ReducePaths(ref currentPhysicalPath, ref attPath);
            int depth = PathDepth(currentPhysicalPath);
            for (int i = 0; i < depth; i++)
                ret += "../";
            ret += attPath.Replace(Path.DirectorySeparatorChar, '/');

            return ret;
        }

        internal string ReplaceAttPath(string processedContent)
        {
            StringBuilder sb = new StringBuilder(processedContent);
            string toInsert = "file:///" + AttachmentsFolder.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

            if (m_isWebApplication)
                toInsert = GetCurrentAttachmentsVirtualPath();

            if (toInsert.EndsWith(Path.AltDirectorySeparatorChar.ToString()) || toInsert.EndsWith(Path.DirectorySeparatorChar.ToString()))
                toInsert = toInsert.Substring(0, toInsert.Length - 1);
            sb = sb.Replace(XDocManager.AttKeyWord, toInsert);
            processedContent = sb.ToString();

            return processedContent;
        }

        #endregion Public Methods


        #region Hierarchy Reader



        public string GetList(string parameters)
        {
            if (parameters == null || parameters == "")
                return "";

            try
            {
                string templateName = "GetList";
                Hashtable hParams = CollectionsUtil.CreateCaseInsensitiveHashtable();

                int iBr1 = parameters.IndexOf("(");
                if (iBr1 == -1)
                    throw new Exception("GetList command does not contain the opening bracket");
                int iBr2 = parameters.LastIndexOf(")!");
                if (iBr2 == -1)
                    throw new Exception("GetList command does not contain the closing bracket");

                string list = parameters.Substring(iBr1 + 1, iBr2 - iBr1 - 1);
                string[] listParams = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(list, '\'', ',', false, false));


                if (parameters.ToLower().StartsWith("!getitemslist"))
                {
                    if (listParams.Length != 2)
                        throw new Exception("GetItemsList does not contain two parameters");
                    hParams["Hierarchyname"] = RemoveQuote(listParams[0].Trim());
                    hParams["Levelname"] = RemoveQuote(listParams[1].Trim());
                }
                else if (parameters.ToLower().StartsWith("!gethierarchy"))
                {
                    foreach (string par in listParams)
                    {
                        string[] parts = par.Split('=');

                        if (par.TrimStart().ToLower().StartsWith("templatename"))
                        {
                            if (parts.Length == 2)
                                templateName = RemoveQuote(parts[1].Trim());
                            else
                                throw new Exception("The template name is not correctly specified");

                        }
                        else
                        {
                            hParams[parts[0].Trim()] = RemoveQuote(parts[1].Trim());
                        }
                    }
                }


                return GetHierarchy(templateName, hParams);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Could not get list " + parameters + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }

        string RemoveQuote(string text)
        {
            if (text.StartsWith("'"))
                text = text.Substring(1);
            if (text.EndsWith("'"))
                text = text.Substring(0, text.Length - 1);
            return text;
        }


        public string GetHierarchy(string templateName, Hashtable parameters)
        {
            string connectionString = ConfigurationManager.AppSettings["HierarchyConnectionString"];
            if (connectionString == null)
                connectionString = "";

            XDocManager hManager = new XDocManager(connectionString);

            IXTemplate template = hManager.LoadTemplate(templateName);
            IXDocument doc = template.ProcessTemplate(parameters);

            /* Michael 10 june 2011: if a new xdoc manager created here, need to dispose also to free connection pool */
            if (hManager != null) hManager.Dispose();

            return doc.Content;
        }

        #endregion Hierarchy Reader


        #region Public Static Methods

        public static Hashtable CaseInsensitiveHashtable()
        {
            return CaseInsensitiveHashtable(null);
        }

        public static Hashtable CaseInsensitiveHashtable(Hashtable oldHash)
        {
            Hashtable newHash = CollectionsUtil.CreateCaseInsensitiveHashtable();
            if (oldHash != null)
            {
                foreach (DictionaryEntry de in oldHash)
                    newHash.Add(de.Key, de.Value);
            }
            return newHash;
        }

        public static Hashtable HashtableFromQueryString(string queryString)
        {
            Hashtable newHash = CaseInsensitiveHashtable();
            if (queryString == null)
                return newHash;

            if (queryString.IndexOf("?") != -1)
            {
                if (queryString.EndsWith("?"))
                    return newHash;
                queryString = queryString.Substring(queryString.IndexOf("?") + 1);
            }
            string[] parameters = queryString.Split('&');
            foreach (string parameter in parameters)
            {
                string parName = "", parVal = "";

                int io1 = parameter.IndexOf("=");
                if (io1 == -1)
                    parName = parameter;
                else
                {
                    parName = parameter.Substring(0, io1);
                    if (io1 + 1 < parameter.Length)
                        parVal = parameter.Substring(io1 + 1);
                }

                if (parName != "")
                {
                    //string val = HttpUtility.UrlDecode(parVal);
                    string val = XDocUrlDecode(parVal);
                    if (newHash.ContainsKey(parName))
                    {
                        val = newHash[parName].ToString() + "," + val;
                        newHash[parName] = val;
                    }
                    else
                        newHash.Add(parName, val);
                }
            }

            return newHash;
        }

        public static string QueryStringFromHashtable(Hashtable parameters)
        {
            string ret = "";
            if (parameters == null)
                return ret;

            foreach (DictionaryEntry de in parameters)
            {
                ret += de.Key.ToString() + "=" + de.Value.ToString() + "&";
            }
            if (ret.EndsWith("&"))
                ret = ret.Remove(ret.Length - 1);

            return ret;
        }

        internal static string ReplaceUniqueID(string docHtml, string providedUniqueID, out string uniqueID)
        {
            string uniqueSequence = providedUniqueID;
            if (uniqueSequence == null || uniqueSequence.Trim() == "")
                uniqueSequence = "_" + Guid.NewGuid().ToString("N");
            uniqueID = uniqueSequence;
            if (uniqueSequence.Length > 0 && (!(char.IsLetter(uniqueSequence[0]) || uniqueSequence[0] == '_') && uniqueSequence != XDocManager.UNIQUE_IDENTIFIER))
                throw new Exception("Unique identifier " + uniqueSequence + " is not valid!");

            StringBuilder sb = new StringBuilder(docHtml);
            sb = sb.Replace(UNIQUE_IDENTIFIER, uniqueID);
            return sb.ToString();
        }

        internal static ContentPart[] SplitDocument(string docHtml, bool preserveBlockKeyWord)
        {// blocks startcommand endcommand
            int offsetStart = 0, offsetEnd = 0;
            if (!preserveBlockKeyWord)
            {
                offsetStart = START_COMMAND.Length;
                offsetEnd = END_COMMAND.Length;
            }

            ArrayList arr = new ArrayList();
            ContentPart cp = null;
            int ioStart = -1, ioEnd = 0;
            int offsetInterBlocks = 0;

            do
            {
                ioStart = docHtml.ToUpper().IndexOf(START_COMMAND, ioEnd);
                if (ioStart != -1)
                {
                    cp = new ContentPart(docHtml.Substring(ioEnd + offsetInterBlocks, ioStart - ioEnd - offsetInterBlocks), true);
                    arr.Add(cp);

                    ioEnd = docHtml.ToUpper().IndexOf(END_COMMAND, ioStart);
                    if (ioEnd == -1)
                        throw new Exception("Ignore block tags are used incorrectly!");
                    offsetInterBlocks = END_COMMAND.Length;

                    cp = new ContentPart(docHtml.Substring(ioStart + offsetStart, ioEnd - ioStart + END_COMMAND.Length - offsetStart - offsetEnd), false);
                    arr.Add(cp);
                }
            } while (ioStart != -1);

            cp = new ContentPart(docHtml.Substring(ioEnd + offsetInterBlocks, docHtml.Length - ioEnd - offsetInterBlocks), true);
            arr.Add(cp);

            return (ContentPart[])arr.ToArray(typeof(ContentPart));
        }



        //private static string XDocUrlDecode(string retVal)
        //{
        //    retVal = HttpUtility.UrlDecode(retVal, System.Text.Encoding.Default);
        //    return retVal;
        //}
        public static string XDocUrlDecode(string retVal)
        {
            string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
            if (sEncoding == null) sEncoding = "";
            if (sEncoding == "") sEncoding = "utf-8";
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
            if (enc != null)
                retVal = HttpUtility.UrlDecode(retVal, enc);
            else
                retVal = HttpUtility.UrlDecode(retVal, System.Text.Encoding.Default);

            return retVal;
        }
        public static string XDocUrlEncode(string retVal)
        {
            string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
            if (sEncoding == null) sEncoding = "";
            if (sEncoding == "") sEncoding = "utf-8";
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
            if (enc != null)
                retVal = HttpUtility.UrlEncode(retVal, enc);
            else
                retVal = HttpUtility.UrlEncode(retVal, System.Text.Encoding.Default);
            retVal = retVal.Replace("+", "%20");
            retVal = retVal.Replace("!", "%21");
            retVal = retVal.Replace("(", "%28");
            retVal = retVal.Replace(")", "%29");
            retVal = retVal.Replace("'", "%27");

            return retVal;
        }

        #endregion Public Static Methods


        #region Object Factory

        public IXTemplate NewTemplate()
        {
            XTemplate template = new XTemplate(this);
            return template;
        }

        public IXTemplate LoadTemplate(string name)
        {
            XTemplate template = new XTemplate(name, this);
            return template;
        }

        public IXTemplate LoadTemplate(int templateID)
        {
            XTemplate template = new XTemplate(templateID, this);
            return template;
        }

        public void DeleteTemplate(string name)
        {
            XTemplate.Delete(name, m_dataProvider);
        }
        public void DeleteTemplate(int templateID)
        {
            XTemplate.Delete(templateID, m_dataProvider);
        }

        public IXDocument NewDocument()
        {
            XDocument document = new XDocument(this);
            return document;
        }

        public IXDocument NewDocumentBasedOnTemplate(int templateID)
        {
            XTemplate templ = new XTemplate(templateID, this);
            return NewDocumentBasedOnTemplate(templ);
        }

        public IXDocument NewDocumentBasedOnTemplate(string templateName)
        {
            XTemplate templ = new XTemplate(templateName, this);
            return NewDocumentBasedOnTemplate(templ);
        }

        public IXDocument NewDocumentBasedOnTemplate(IXTemplate templ)
        {
            XDocument document = new XDocument(this);
            document.TemplateID = templ.ID;
            document.InitialRedirect = templ.Redirect;
            document.ToSave = templ.ToSave;
            document.DocType = templ.DocType;

            return document;
        }

        public IXDocument LoadDocument(int documentID)
        {
            XDocument document = new XDocument(documentID, this);
            return document;
        }

        public IXDocument LoadDocument(int documentID, int version)
        {
            XDocument document = new XDocument(documentID, version, this);
            return document;
        }

        public IXConnection NewConnection()
        {
            Connection conn = new Connection(m_dataProvider);
            return conn;
        }

        public IXConnection LoadConnection(int connectionID)
        {
            Connection conn = new Connection(connectionID, m_dataProvider);
            return conn;
        }

        public IXConnection LoadConnection(string name)
        {
            Connection conn = new Connection(name, m_dataProvider);
            return conn;
        }

        public void DeleteConnection(int connectionID)
        {
            Connection.Delete(connectionID, m_dataProvider);
        }

        public IXQuery NewQuery()
        {
            Query query = new Query(m_dataProvider);
            return query;
        }

        public IXQuery LoadQuery(string queryName, int templateID)
        {
            Query query = new Query(queryName, templateID, m_dataProvider);
            return query;
        }

        public IXQuery LoadQuery(int id)
        {
            Query query = new Query(id, m_dataProvider);
            return query;
        }

        public void DeleteQuery(int queryID)
        {
            Query.Delete(queryID, m_dataProvider);
        }

        #endregion Object Factory
        private string L_String(string strString)
        {
            if (strString == null) return "";
            else return strString.Replace("'", "''");
        }
        [SecurityCritical]
        void IXManagerImportExport.ReadSFile(string sKey, out string sFileName, out  string sFileExtension, out byte[] aFileContent, out string sFileContentType)
        {
            sFileName = "";
            sFileExtension = "";
            aFileContent = null;
            sFileContentType = "";
            try
            {
                string sQueryLog = @"";

                sQueryLog += " select FileName,FileExtension,FileContent,FileContentType from S_Files where FileID = '" + sKey + "'";
                DataTable dt = m_dataProvider.GetDataTable(sQueryLog);
                if (dt != null && dt.Rows.Count > 0)
                {
                    sFileName = dt.Rows[0][0].ToString();
                    sFileExtension = dt.Rows[0][1].ToString();
                    aFileContent = (byte[])dt.Rows[0][2];
                    sFileContentType = dt.Rows[0][3].ToString();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw;
            }
        }
        [SecurityCritical]
        string IXManagerImportExport.WriteSFile(string sKey, string sFileName, string sFileExtension, byte[] aFileContent, string sFileContentType)
        {
            string sFileLog = "";
            string sFileID = sKey;
            sFileName = sFileName.Replace("'", "''");
            sFileExtension = sFileExtension.Replace("'", "''");
            sFileContentType = sFileContentType.Replace("'", "''");
            try
            {
                if (sKey != "") sFileLog += " delete from S_Files where FileID = '" + sKey + "'";
                if (aFileContent != null)
                {
                    if (sKey != "")
                    {
                        sFileLog += " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[s_Files]') AND type in (N'U')) SET IDENTITY_INSERT s_Files on";
                        sFileLog += " insert into S_Files (FileID,FileName,FileExtension,FileContent,FileContentType) values (" + sKey + ",'"
                            + sFileName + "','" + sFileExtension + "',@FileContent,'" + sFileContentType + "')";
                        sFileLog += " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[s_Files]') AND type in (N'U')) SET IDENTITY_INSERT s_Files on";
                    }
                    else
                        sFileLog += " insert into S_Files (FileName,FileExtension,FileContent,FileContentType) values ('"
                        + sFileName + "','" + sFileExtension + "',@FileContent,'" + sFileContentType + "')";

                    sFileLog += " SELECT SCOPE_IDENTITY() fileid";
                }
                DataTable dt;
                if (aFileContent != null)
                    dt = m_dataProvider.GetDataTable(sFileLog, new IDataParameter[] { new SqlParameter("@FileContent", aFileContent) });
                else
                    dt = m_dataProvider.GetDataTable(sFileLog);
                if (dt != null && dt.Rows.Count > 0)
                    sFileID = dt.Rows[0][0].ToString();
                return sFileID;
            }
            catch (Exception ex)
            {
                //                System.Diagnostics.Debug.WriteLine(ex.Message);
                if (ex.Message.StartsWith("Invalid object name"))
                {
                    string sFileLog1 = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S_Files]') )
                        CREATE TABLE [dbo].[S_Files]([FileID] [int] IDENTITY(1,1) NOT NULL,	[FileName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, [FileExtension] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, [FileContent] [varbinary](max) NULL,	[FileContentType] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CONSTRAINT [PK_S_Files] PRIMARY KEY CLUSTERED (	[FileID] ASC )WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
                    try
                    {
                        m_dataProvider.ExecuteNonQuery(sFileLog1 + " " + sFileLog);
                    }
                    catch (Exception ex1) { throw; };
                }
                else
                    throw;

            }
            return "";
        }
        public void RequestLog(HttpContext context, DateTime dtStart, string postedData, string response)
        {
            string sRequestLog = "";
            try
            {

                //if ((context.Request.QueryString["LogRequest"] != null) && (context.Request.QueryString["LogRequest"] == "0")) return;
                //if ((context.Request.QueryString["LogRequestData"] != null) && (context.Request.QueryString["LogRequestData"] == "0")) postedData = "[no log]";
                //if ((context.Request.QueryString["LogResponseData"] != null) && (context.Request.QueryString["LogResponseData"] == "0")) response = "[no log]";
                //string sEncoding = System.Configuration.ConfigurationManager.AppSettings["LogRequest"];
                if (LogRequest == "0") return;
                if (LogRequestData == "0") postedData = "[no log]";
                if (LogResponseData == "0") response = "[no log]";

                sRequestLog += @" insert into S_RequestLog (RequestMethod,RequestURL,RequestData,ResponseData,LogDateTime,Duration,SessionID,UserName,UserHostAddress)  values ("
                    + "'" + L_String(context.Request.HttpMethod)
                    + "', '" + L_String(context.Request.Url.AbsoluteUri)
                    + "', '" + L_String(postedData)
                    + "', '" + L_String(response)
                    + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                    + "', " + ((int)DateTime.Now.Subtract(dtStart).TotalMilliseconds).ToString()
                    + ", '"  + L_String((string)context.Session["SESSIONID"])
                    + "', '" + L_String((string)context.Request.ServerVariables["REMOTE_USER"])
                    + "', '" + L_String(context.Request.UserHostAddress)
                    + "')";

                m_dataProvider.ExecuteNonQuery(sRequestLog);
            }
            catch (Exception ex)
            {
                //                System.Diagnostics.Debug.WriteLine(ex.Message);
                if (ex.Message.StartsWith("Invalid object name"))
                {
                    string sRequestLog1 = @"IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S_RequestLog]') AND type in (N'U'))
                CREATE TABLE [dbo].[S_RequestLog](
                    [RequestLogID] [int] IDENTITY(1,1) NOT NULL,
                    [RequestMethod] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                    [RequestURL] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                    [RequestData] [varchar](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                    [ResponseData] [varchar](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                    [LogDateTime] [datetime] NOT NULL,
                    [Duration] [int] NULL,
                   	[SessionID] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                    [UserName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                    [UserHostAddress] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                 CONSTRAINT [PK_S_RequestLog] PRIMARY KEY CLUSTERED ([RequestLogID] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
                    try
                    {
                        m_dataProvider.ExecuteNonQuery(sRequestLog1 + " " + sRequestLog);
                    }
                    catch (Exception ex1) { };
                }
            }
        }
        //        IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLog]') AND type in (N'U'))
        //CREATE TABLE [dbo].[S_RequestLog](
        //    [RequestLogID] [int] IDENTITY(1,1) NOT NULL,
        //    [RequestMethod] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        //    [RequestURL] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        //    [RequestXML] [varchar](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        //    [ResponseXML] [varchar](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        //    [LogDateTime] [datetime] NOT NULL,
        //    [Duration] [int] NULL,
        //    [UserName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        //    [UserHostAddress] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        // CONSTRAINT [PK_S_RequestLog] PRIMARY KEY CLUSTERED ([RequestLogID] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY]


        //        private void WriteLog(HttpContext context, string strTemplateName, string strParameters, string strMethod, string postedData, string strBody, DateTime dtStart, string strLogError)
        //        {
        //            if (!((context.Request.QueryString["LogRequest"] != null) && (context.Request.QueryString["LogRequest"] == "1")))
        //            {

        //                // why CASE in sql and not an if in C#??
        //                if (ConfigurationManager.AppSettings["Log"] == null || ConfigurationManager.AppSettings["Log"] != "1" || ConfigurationManager.AppSettings["XDocConnectionString"] == null || ConfigurationManager.AppSettings["XDocConnectionString"] == "")
        //                    return;
        //            }
        //            if ((context.Request.QueryString["LogRequest"] != null) && (context.Request.QueryString["LogRequest"] == "0"))
        //                return;
        //            if ((context.Request.QueryString["LogRequestXML"] != null) && (context.Request.QueryString["LogRequestXML"] == "0")) postedData = "[no log]";
        //            if ((context.Request.QueryString["LogResponseXML"] != null) && (context.Request.QueryString["LogResponseXML"] == "0")) strBody = "[no log]";

        //            try
        //            {
        //                /* LoginName, first try by parameter. If not set, try with integrated identity */
        //                string strLoginName = System.Web.HttpUtility.UrlDecode(this.GetParameterValue(strParameters, "LoginName"), context.Request.ContentEncoding);
        //                if (strLoginName == null) strLoginName = "";

        //                string strUserName = context.Request.ServerVariables["REMOTE_USER"];
        //                if (strUserName == null) strUserName = "";

        //                //string LogParameters = "";
        //                //LogParameters += "ApplicationName=" + this.GetParameterValue(strParameters, "ApplicationName");
        //                //LogParameters += "&LoginName=" + System.Web.HttpUtility.UrlEncode(strLoginName, System.Text.Encoding.Default);
        //                //LogParameters += "&UserName=" + System.Web.HttpUtility.UrlEncode(strUserName, System.Text.Encoding.Default);
        //                //LogParameters += "&RequestURL=" + System.Web.HttpUtility.UrlEncode(context.Request.Url.AbsoluteUri, System.Text.Encoding.Default);
        //                //LogParameters += "&RequestMethod=" + System.Web.HttpUtility.UrlEncode(strMethod, System.Text.Encoding.Default);
        //                //LogParameters += "&ResourceTemplate=" + System.Web.HttpUtility.UrlEncode(strTemplateName, System.Text.Encoding.Default);

        //                KubionDataNamespace.ClientData objClientData = new KubionDataNamespace.ClientData(ConfigurationManager.AppSettings["XDocConnectionString"]);
        //                string strQueryText = @"INSERT INTO tblLog(RequestMethod, RequestURL, RequestXML, ResponseXML, ApplicationName, StartTime, EndTime, Duration, LoginName, UserName, UserAgent, UserHostAddress, UserHostName, ResourceTemplate, Error)
        //	VALUES
        //	(
        //	  CASE WHEN '" + L_String(strMethod) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strMethod) + @"',1,10) END
        //	  , CASE WHEN '" + L_String(context.Request.Url.AbsoluteUri) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.Url.AbsoluteUri) + @"',1,500) END
        //	  , CASE WHEN '" + L_String(postedData) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(postedData) + @"',1,8000) END
        //	  , CASE WHEN '" + L_String(strBody) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strBody) + @"',1,8000) END
        //	  , CASE WHEN '" + L_String(System.Web.HttpUtility.UrlDecode(this.GetParameterValue(strParameters, "ApplicationName"), context.Request.ContentEncoding)) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(System.Web.HttpUtility.UrlDecode(this.GetParameterValue(strParameters, "ApplicationName"), context.Request.ContentEncoding)) + @"',1,50) END
        //	  , CASE WHEN '" + dtStart.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' = '' THEN NULL ELSE '" + dtStart.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' END
        //	  , CASE WHEN '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' = '' THEN NULL ELSE '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' END
        //	  , " + ((int)DateTime.Now.Subtract(dtStart).TotalMilliseconds).ToString() + @"
        //	  , CASE WHEN '" + L_String(strLoginName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strLoginName) + @"',1,50) END
        //	  , CASE WHEN '" + L_String(strUserName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strUserName) + @"',1,50) END
        //	  , CASE WHEN '" + L_String(context.Request.UserAgent) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.UserAgent) + @"',1,500) END
        //	  , CASE WHEN '" + L_String(context.Request.UserHostAddress) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.UserHostAddress) + @"',1,50) END
        //	  , CASE WHEN '" + L_String(context.Request.UserHostName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.UserHostName) + @"',1,50) END
        //	  , CASE WHEN '" + L_String(strTemplateName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strTemplateName) + @"',1,100) END
        //	  , CASE WHEN '" + L_String(strLogError) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strLogError) + @"',1,8000) END
        //	)";

        //                /* Run the query */
        //                string logResult = objClientData.ExecuteNonQuery(strQueryText);
        //                objClientData.Dispose();
        //            }
        //            catch (Exception e) { }
        //        }
        //        private string L_String(string strString)
        //        {
        //            if (strString == null) return "";
        //            else return strString.Replace("'", "''");
        //        }
    }
}
