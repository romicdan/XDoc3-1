using System;
using System.Collections.Generic;
using System.Text;

namespace XDocuments
{
    internal class ContentPart
    {
        string m_content = "";

        public string Content
        {
            get { return m_content; }
            set { m_content = value; }
        }
        bool m_parsable = false;

        public bool IsParsable
        {
            get { return m_parsable; }
            set { m_parsable = value; }
        }

        public ContentPart(string content, bool parsable)
        {
            m_content = content;
            m_parsable = parsable;
        }
        public ContentPart() { }

        public override string ToString()
        {
            return m_content;
        }

    }
}
