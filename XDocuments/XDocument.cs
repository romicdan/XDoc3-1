using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using XDataSourceModule;
using System.ComponentModel;
using XHTMLMerge;
using System.Collections;
using System.IO;
using KubionLogNamespace;
using System.Globalization;


namespace XDocuments
{
    internal class XDocument : IXDocument
    {
        #region Private members

        private int
            m_id = -1,
            m_templateID = -1,
            m_version = -1,
            
            m_newID = -1,
            m_newVersion = -1;

        private string
            m_content = "",
            m_docType = XDocManager.HTMLDocType,
            m_redirect = "",
            m_initialRedirect = "",
            m_uniqueID = "",

            m_newContent = "";

        private bool
            m_activeVersion = true,
            m_externalHTMLBody = false,
            m_downloadAttachments = false,
            m_toSave = true,
            m_newActiveVersion = true;

        private byte[] m_binaryContent = null;

        XProviderData m_dataProvider = null;

        private XDocManager m_manager = null;

        const string 
            DEFAULT_REDIRECT = "ViewXDocument.aspx?DocID=#PAR.DOCID#",
            RELOAD_KEYWORD = "RELOAD";

        #endregion Private members


        #region Const

        private readonly string newLine = Environment.NewLine;

        #endregion Const


        #region Constructors

        internal XDocument(XDocManager manager)
        {
            m_manager = manager;
            m_dataProvider = manager.CurrentDataProvider;
        }

        public XDocument(int id, XDocManager manager)
        {
            m_manager = manager;
            m_dataProvider = manager.CurrentDataProvider;
            Load(id);
        }

        public XDocument(int id, int version, XDocManager manager)
        {
            m_manager = manager;
            m_dataProvider = manager.CurrentDataProvider;
            Load(id, version);
        }

        #endregion Constructors


        #region Public properties

        /// <summary>
        /// The ID of the document
        /// </summary>
        public int ID
        {
            get { return m_id; }
        }

        /// <summary>
        /// Flag that indicates if the current version is active or not
        /// </summary>
        public bool ActiveVersion
        {
            get { return m_activeVersion; }
        }

        /// <summary>
        /// Indicates if the document can be stored in the database
        /// </summary>
        public bool ToSave
        {
            get { return m_toSave; }
            set { m_toSave = value; }
        }

        /// <summary>
        /// Indicates if the document is created using HTML text not generated with a template
        /// </summary>
        public bool ExternalHTMLBody
        {
            get { return m_externalHTMLBody; }
            set { m_externalHTMLBody = value; }
        }

        /// <summary>
        /// Indicates if the document attachments will be downloaded when its content will be requested.
        /// </summary>
        public bool DownloadAttachments
        {
            get { return m_downloadAttachments; }
            set { m_downloadAttachments = value; }
        }

        /// <summary>
        /// The ID of the template that generated the document
        /// </summary>
        public int TemplateID
        {
            get { return m_templateID; }
            set { m_templateID = value; }
        }

        /// <summary>
        /// The version of the document
        /// </summary>
        public int Version
        {
            get { return m_version; }
            set { m_version = value; } 
        }

        /// <summary>
        /// Returns the content of the document, if it supports string representation.
        /// The document attachments are only downloaded if the DownloadAttachments property is set to true.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Content
        {
            get 
            {
                if (m_content == "")
                {
                    try
                    {
                        m_content = XDocManager.Encoding.GetString(BinaryContent);
                    }
                    catch
                    { }
                }
                if (m_id != -1 && m_downloadAttachments)
                {
                    DownloadDocumentAttachments(m_id, m_version);
                }
                return m_content; 
            }
            set 
            {
                if (value == null)
                    throw new Exception("Document content cannot be null!");
                m_content = value;
                m_binaryContent = XDocManager.Encoding.GetBytes(m_content);
            }
        }

        /// <summary>
        /// The file extension of the document
        /// </summary>
        public string DocType
        {
            get { return m_docType; }
            set { m_docType = value; }
        }

        /// <summary>
        /// Redirect string after redirect processing
        /// </summary>
        public string Redirect
        {
            get { return m_redirect; }
            set { m_redirect = value; }
        }

        /// <summary>
        /// String to be processed in order to obtain the document redirect
        /// </summary>
        public string InitialRedirect
        {
            get { return m_initialRedirect; }
            set { m_initialRedirect = value; }
        }

        /// <summary>
        /// A unique identifier used for HTML element IDs and javascript function names in order to isolate this instance of the document in a web page
        /// </summary>
        public string UniqueID
        {
            get { return m_uniqueID; }
            set { m_uniqueID = value; }
        }

        /// <summary>
        /// The content of the document as a byte array (the
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public byte[] BinaryContent
        {
            get 
            {
                if (m_binaryContent == null)
                    LoadContent();
                return m_binaryContent; 
            }
            set 
            {
                if (value == null)
                    throw new Exception("Document content cannot be null!");
                m_binaryContent = value;
                try
                {
                    m_content = XDocManager.Encoding.GetString(m_binaryContent);
                }
                catch
                {
                    m_content = "";
                }
            }
        }


        #endregion Public properties


        #region Public methods

        /// <summary>
        /// Loads a document by its ID
        /// </summary>
        public void Load(int id)
        {
            //string sQueryStatement;
            try
            {
                IDataParameter[] pars = new IDataParameter[]
                {
                    new SqlParameter("@" + S_DOCUMENTS.DOCID, id)
                };
                //sQueryStatement = SQL.LoadDocumentByID;
                //sQueryStatement = sQueryStatement.Replace("@" + S_DOCUMENTS.DOCID, id.ToString());
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadDocumentByID, pars);
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception("Could not find document");
                Load(dtResult.Rows[0]);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Could not load document with ID = " + id.ToString() + newLine + ex.Message;
                throw new Exception(message);
            }
        }

        /// <summary>
        /// Loads a document by its ID and version
        /// </summary>
        public void Load(int id, int version)
        {
            //string sQueryStatement;
            try
            {
                IDataParameter[] pars = new IDataParameter[]
                {
                    new SqlParameter("@" + S_DOCUMENTS.DOCID, id),
                    new SqlParameter("@" + S_DOCUMENTS.VERSION, version)
                };
                //sQueryStatement = SQL.LoadDocumentByIDAndVersion;
                //sQueryStatement = sQueryStatement.Replace("@" + S_DOCUMENTS.DOCID, id.ToString());
                //sQueryStatement = sQueryStatement.Replace("@" + S_DOCUMENTS.VERSION, "'" + version + "'");

                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadDocumentByIDAndVersion, pars);
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception("Could not find document");
                Load(dtResult.Rows[0]);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Could not load document for DocID = " + id.ToString() + " and Version = " + version.ToString() + newLine + ex.Message;
                throw new Exception(message);
            }
        }

        /// <summary>
        /// Saves the document without performing updates
        /// </summary>
        public Hashtable Save()
        {
            return Save(false, XDocManager.CaseInsensitiveHashtable());
        }

        /// <summary>
        /// Saves the document and performs updates if indicated 
        /// </summary>
        /// <param name="performUpdates">Indicates if updates are performed</param>
        public Hashtable Save(bool performUpdates)
        {
            return Save(performUpdates, XDocManager.CaseInsensitiveHashtable());
        }

        /// <summary>
        /// Saves the document, performs updates if indicated and returns the new values for the provided parameters
        /// </summary>
        /// <param name="performUpdates">Indicates if updates are performed</param>
        /// <param name="parameters">Parameters to be updated</param>
        /// <returns>New parameter values</returns>
        public Hashtable Save(bool performUpdates, Hashtable parameters)
        {
            Hashtable returnParameters = XDocManager.CaseInsensitiveHashtable(parameters);

            // Add the UserID and UserName default parameters
            returnParameters[XDocManager.UserIDKey] = m_manager.UserID.ToString(CultureInfo.InvariantCulture);
            returnParameters[XDocManager.UserLoginKey] = m_manager.UserLogin;
            returnParameters[XDocManager.UserNameKey] = m_manager.UserName;
            returnParameters[XDocManager.ServerKey ] = m_manager.ServerName ;
            returnParameters[XDocManager.DatabaseKey ] = m_manager.DatabaseName ;

            try
            {
                CSubmitParser submitParser = new CSubmitParser();
                returnParameters = submitParser.GetNewParamValues(this.Content, returnParameters);

                m_dataProvider.BeginTransaction();

                if (m_id == -1)
                {
                    int newDocID = -1;
                    m_newContent = m_content;

                    if (m_toSave)
                    {
                        // new document
                        newDocID = GetNewDocID();

                        if (ExternalHTMLBody)// the document attachments are not present in the Attachments folder
                        {
                            // update the content with new guid-based file names without saving the attachments
                            // this is useful if the transaction fails
                            m_newContent = SaveDocAttachment(newDocID, 1, true, false);
                        }

                        IDataParameter[] pars = GetDocDataParameters(newDocID, 1, true);

                        m_dataProvider.ExecuteNonQuery(SQL.CreateNewDocument, pars);

                        SaveDocAttachment(newDocID, 1, m_externalHTMLBody, true);

                        SaveDocumentState(newDocID, 1, true);
                    }
                    if (performUpdates)
                    {
                        submitParser = new CSubmitParser();
                        string error;

                        if (!submitParser.PromoteUpdates(m_templateID, m_dataProvider, newDocID, m_newContent, ref returnParameters, out error))
                        {
                            string message = "Could not perform updates for templateID=" + m_templateID.ToString() + ";" + newLine + error;
                            throw new Exception(message);
                        }
                    }

                }
                else // update document
                {
                    if (m_toSave)
                    {
                        if (m_activeVersion)
                        {
                            //put the current document in history
                            UpdateDocumentStatus(m_id, m_version, false);

                            int newVersion = m_version + 1;

                            // create a new document with higher version
                            CreateNewDocumentVersion(m_id, newVersion);

                            SaveDocumentState(m_id, newVersion, true);
                        }
                        else// (m_activeVersion == false)
                        {
                            int newVersion = GetNewVersion(m_id);

                            //put the active document in history
                            UpdateDocumentStatus(m_id, newVersion - 1, false);

                            // create a new document with the highest version
                            CreateNewDocumentVersion(m_id, newVersion);

                            SaveDocumentState(m_id, newVersion, true);
                        }
                    }
                }

                m_dataProvider.Commit();
                UpdateDocumentState();

                //get document redirect
                m_redirect = GetDocumentRedirect(returnParameters);

                return returnParameters;
            }
            catch (Exception ex)
            {
                m_dataProvider.Rollback();
                string message = "Could not save document" + newLine + ex.Message;
                throw new Exception(message);
            }

        }


        #endregion Public methods


        #region Private methods


        internal void Load(DataRow row)
        {
            // load without content
            m_id = Connectors.GetInt32Result(row[S_DOCUMENTS.DOCID]);
            m_activeVersion = Connectors.GetBoolResult(row[S_DOCUMENTS.ACTIVE]);
            m_docType = Connectors.GetStringResult(row[S_DOCUMENTS.DOCTYPE]);
            m_uniqueID = Connectors.GetStringResult(row[S_DOCUMENTS.UNIQUEID]);
            m_templateID = Connectors.GetInt32Result(row[S_DOCUMENTS.TEMPLATEID]);
            m_version = Connectors.GetInt32Result(row[S_DOCUMENTS.VERSION]);
        }

        private void LoadContent()
        {
            //string sQueryStatement;
            if (m_id == -1)
                return;

            try
            {
                IDataParameter[] pars = new IDataParameter[]
                {
                    new SqlParameter("@" + S_DOCUMENTS.DOCID, m_id),
                    new SqlParameter("@" + S_DOCUMENTS.VERSION, m_version)
                };
                //sQueryStatement = SQL.LoadDocumentContent;
                //sQueryStatement = sQueryStatement.Replace("@" + S_DOCUMENTS.DOCID, m_id.ToString());
                //sQueryStatement = sQueryStatement.Replace("@" + S_DOCUMENTS.VERSION, "'" + m_version + "'");

                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadDocumentContent, pars);
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception("Could not find document");
                m_binaryContent = Connectors.GetBinaryResult(dtResult.Rows[0][S_DOCUMENTS.DOCBODY]);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Could not load content for document DocID = " + m_id.ToString() + ", version = " + m_version.ToString() + ", active version = " + m_activeVersion.ToString() + newLine + ex.Message;
                throw new Exception(message);
            }
        }

        /// <summary>
        /// Updates the status of an existing, stored document (active or inactive)
        /// </summary>
        void UpdateDocumentStatus(int docID, int version, bool active)
        {
            //string sQueryStatement;
            IDataParameter[] pars = new IDataParameter[]
                        {
                            new SqlParameter("@" + S_DOCUMENTS.ACTIVE, active),
                            new SqlParameter("@" + S_DOCUMENTS.VERSION, version),
                            new SqlParameter("@" + S_DOCUMENTS.DOCID, docID)
                        };
            //sQueryStatement = SQL.UpdateDocumentStatus;
            //sQueryStatement = sQueryStatement.Replace("@" + S_DOCUMENTS.DOCID, docID.ToString());
            //sQueryStatement = sQueryStatement.Replace("@" + S_DOCUMENTS.VERSION, "'" + version + "'");
            //sQueryStatement = sQueryStatement.Replace("@" + S_DOCUMENTS.ACTIVE, "'" + active + "'");
            m_dataProvider.ExecuteNonQuery(SQL.UpdateDocumentStatus, pars);
            //m_dataProvider.ExecuteNonQuery(sQueryStatement);
        }


        private void CreateNewDocumentVersion(int docID, int version)
        {
            //string sQueryStatement;
            IDataParameter[] pars = GetDocDataParameters(docID, version, true);
            m_dataProvider.ExecuteNonQuery(SQL.CreateNewDocument, pars);


            // save document attachments
            pars = new IDataParameter[]
                        {
                            new SqlParameter("@" + S_DOCATTACHMENTS.DOCVERSION, version),
                            new SqlParameter("@" + S_DOCATTACHMENTS.DOCID, m_id)
                        };
            //sQueryStatement = SQL.CopyDocumentAttachments;
            //sQueryStatement = sQueryStatement.Replace("@" + S_DOCATTACHMENTS.DOCID, m_id.ToString());
            //sQueryStatement = sQueryStatement.Replace("@" + S_DOCATTACHMENTS.DOCVERSION, "'" + version + "'");
            m_dataProvider.ExecuteNonQuery(SQL.CopyDocumentAttachments, pars);
            //m_dataProvider.ExecuteNonQuery(sQueryStatement);

            SaveDocumentState(m_id, version, true);

        }

        private int GetNewDocID()
        {
            try
            {
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.GetNewDocID);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception("Could not find max ID");
                int newDocID = Connectors.GetInt32Result(dtResult.Rows[0][S_DOCUMENTS.DOCID], 1);
                return newDocID;
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Could not get new DocID" + newLine + ex.Message;
                throw new Exception(message);
            }
        }


        private int GetNewVersion(int docID)
        {
            //string sQueryStatement;
            //sQueryStatement = SQL.GetNewDocVersion;
            //sQueryStatement = sQueryStatement.Replace("@" + S_DOCUMENTS.DOCID, docID.ToString());
            DataTable dtResult = m_dataProvider.GetDataTable(SQL.GetNewDocVersion, new IDataParameter[] { new SqlParameter("@" + S_DOCUMENTS.DOCID, docID) });
            //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement );
            if (dtResult == null || dtResult.Rows.Count == 0)
                throw new Exception("Could not find max version");
            int newVersion = Connectors.GetInt32Result(dtResult.Rows[0][S_DOCUMENTS.VERSION]);
            return newVersion;
        }


        IDataParameter[] GetDocDataParameters(int docID, int version, bool active)
        {
            byte[] binaryContent = m_binaryContent;
            if (m_newContent != "")
                binaryContent = XDocManager.Encoding.GetBytes(m_newContent);

            IDataParameter[] pars = new IDataParameter[]
            {
                new SqlParameter("@" + S_DOCUMENTS.ACTIVE, active),
                new SqlParameter("@" + S_DOCUMENTS.DOCBODY, binaryContent),
                new SqlParameter("@" + S_DOCUMENTS.DOCID, docID),
                new SqlParameter("@" + S_DOCUMENTS.VERSION, version),
                new SqlParameter("@" + S_DOCUMENTS.UNIQUEID, m_uniqueID),
                new SqlParameter("@" + S_DOCUMENTS.TEMPLATEID, m_templateID),
                new SqlParameter("@" + S_DOCUMENTS.DOCTYPE, m_docType),
            };

            return pars;
        }

        private string SaveDocAttachment(int docID, int version, bool isExternalBody, bool saveInDatabase)
        {
            string docBody = Content;
            if (docBody == "")
                return "";

            string retVal = docBody;

            try
            {
                CSubmitParser parser = new CSubmitParser();
                ArrayList filesAtt = parser.GetAttachmentsFileNames(docBody);
                foreach (string file in filesAtt)
                {
                    string fileName = Path.GetFileName(file);
                    string filePath = "";
                    if (m_manager.IsWebApplication)
                    {
                        try
                        {
                            filePath = System.Web.HttpContext.Current.Server.MapPath(file);
                        }
                        catch
                        {
                            // filePath is not a file 
                            continue;
                        }
                    }
                    else
                        filePath = Path.Combine(m_manager.AttachmentsFolder, file);

                    string fileForReplace = file;

                    string dummyGuid = Guid.NewGuid().ToString("N");
                    if (!isExternalBody)
                    {
                        if (Path.GetFileNameWithoutExtension(file).Length != dummyGuid.Length ||
                            !File.Exists(filePath))
                            continue;
                    }
                    else
                    {
                        string extension = Path.GetExtension(filePath);
                        fileName = dummyGuid + extension;
                        fileForReplace = XDocManager.AttKeyWord + "/" + fileName;
                    }

                    if (saveInDatabase)
                    {
                        FileStream fs = new FileStream(filePath, FileMode.Open);
                        byte[] img = new Byte[fs.Length];
                        fs.Read(img, 0, (int)fs.Length);
                        fs.Close();

                        IDataParameter[] pars = new IDataParameter[]
                        {
                            new SqlParameter("@" + S_DOCATTACHMENTS.DOCID, docID),
                            new SqlParameter("@" + S_DOCATTACHMENTS.DOCVERSION, version),
                            new SqlParameter("@" + S_DOCATTACHMENTS.NAME, fileName),
                            new SqlParameter("@" + S_DOCATTACHMENTS.ATTACHMENT, img)
                        };
                        m_dataProvider.ExecuteNonQuery(SQL.SQLInsertDocAttachment, pars);

                        img = null;
                    }
                    else if (isExternalBody)
                    {
                        File.Copy(filePath, Path.Combine(m_manager.AttachmentsFolder, fileName));
                    }

                    retVal = retVal.Replace(file, fileForReplace);

                }
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw new Exception("Error occured while saving document attachments: " + ex.Message);
            }
            return retVal;
        }


        private void DownloadDocumentAttachments(int documentID, int version)
        {
            //string sQueryStatement;
            IDataParameter[] pars = new IDataParameter[]
            {
                new SqlParameter("@" + S_DOCATTACHMENTS.DOCID, documentID),
                new SqlParameter("@" + S_DOCATTACHMENTS.DOCVERSION, version)
            };
            //sQueryStatement = SQL.SQLGetDocAttachment;
            //sQueryStatement = sQueryStatement.Replace("@" + S_DOCATTACHMENTS.DOCID, documentID.ToString());
            //sQueryStatement = sQueryStatement.Replace("@" + S_DOCATTACHMENTS.DOCVERSION,"'"+ version +"'");

            FileStream fs = null;

            try
            {
                DataTable dtDocAtt = m_dataProvider.GetDataTable(SQL.SQLGetDocAttachment, pars);
                //DataTable dtDocAtt = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtDocAtt == null)
                    throw new Exception("Could not load attachments information");

                // replace the AttPath keyword with the actual value
                if (dtDocAtt.Rows.Count > 0)
                    m_content = m_manager.ReplaceAttPath(m_content);
                    

                foreach (DataRow row in dtDocAtt.Rows)
                {
                    string strFileName = row[S_DOCATTACHMENTS.NAME].ToString().TrimEnd();
                    object oVal = row[S_DOCATTACHMENTS.ATTACHMENT];
                    string strTmpImageFile = Path.Combine(m_manager.AttachmentsFolder, strFileName);

                    byte[] stream = (byte[])oVal;
                    fs = File.Create(strTmpImageFile);
                    fs.Write(stream, 0, stream.Length);
                    fs.Close();
                    fs = null;
                }
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Error downloading attachments for docID = " + documentID.ToString() + " and version = " + version.ToString() + newLine + ex.Message;
                throw new Exception(message);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }

        }

        private string GetDocumentRedirect(Hashtable parameters)
        {
            try
            {
                //Add the VERSION parameter
                parameters[XDocManager.VersionKey] = m_version;

                if (m_initialRedirect == "" || m_initialRedirect == null)
                {
                    if (m_toSave)
                        m_initialRedirect = DEFAULT_REDIRECT;
                    else
                        return RELOAD_KEYWORD;
                }

                CParser parser = new CParser(null,m_templateID, m_dataProvider, m_initialRedirect, parameters);
                if (m_manager!= null) if (m_manager.DocCache != null) parser.DocCache = m_manager.DocCache;
                //parser.ParseForRedirect = true;
                string ret = parser.Parse();
                return ret;
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Error getting redirect" + newLine + ex.Message;
                throw new Exception(message);
            }
        }

        /// <summary>
        /// Temporarily saves members' new values (in case the transactions fail)
        /// </summary>
        private void SaveDocumentState(int docID, int version, bool active)
        {
            m_newActiveVersion = active;
            m_newID = docID;
            m_newVersion = version;
        }

        
        /// <summary>
        /// Updates the members with the new values, after the transactions succeded
        /// </summary>
        private void UpdateDocumentState()
        {
            m_activeVersion = m_newActiveVersion;
            m_id = m_newID;
            m_version = m_newVersion;
            if (m_newContent != "")
                Content = m_newContent;
        }


        #endregion Private methods
    }
}
