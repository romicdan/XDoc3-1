using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web;


namespace KubionLogNamespace
{

    public enum TraceLevelEnum
    {
        // values follow to be renamed
        None = 0,
        Level1 = 1,
        Level2 = 2,
        Level3 = 3,
        Level4 = 4,
        Level5 = 5,
        Level6 = 6,
        Level7 = 7,
        Level8 = 8,
        Level9 = 9,
        TemplateQueries = 10

    }

    public class KubionLog
    {

        static int m_traceLevel;
        static string m_fileName ;


        static KubionLog()
        {
            m_traceLevel = 0;
            m_fileName = "";


            string setting = ConfigurationManager.AppSettings["LogFile"];
            if (setting != null)
                m_fileName = setting;
            setting = ConfigurationManager.AppSettings["KubionLogFile"];
            if (setting != null)
                m_fileName = setting;

            setting = ConfigurationManager.AppSettings["LogTraceLevel"];
            if (setting != null)
                int.TryParse(setting, out m_traceLevel);
            setting = ConfigurationManager.AppSettings["KubionTraceLevel"];
            if (setting != null)
                int.TryParse(setting, out m_traceLevel);
        }

        //deprecated  = for backwards compatibility
        public static TraceLevelEnum TraceLevel
        {
            get { return (TraceLevelEnum)m_traceLevel; }
            set { m_traceLevel = (int)value; }
        }

        public static int KubionTraceLevel
        {
            get { return (int)m_traceLevel; }
            set { m_traceLevel = (int)value; }
        }


        public static void WriteLine(string text, int trace_level)
        {
            if (trace_level <= m_traceLevel) WriteLine(text);
        }
        public static void WriteLine(Exception ex, int trace_level)
        {
            if (trace_level <= m_traceLevel) WriteLine(ex);
        }

        public static void WriteLine(string text)
        {
            if (m_fileName == "" || m_fileName == null || text == null)
                return;

            string filePath = "";

            try
            {
                filePath = HttpContext.Current.Server.MapPath(Path.Combine("~", m_fileName));
            }
            catch
            {
                try
                {
                    string dir = Path.GetDirectoryName(m_fileName);
                    if ((dir!="")&&(!Directory.Exists(dir)))
                        return;
                    filePath = m_fileName;
                }
                catch
                {
                    return;
                }
            }

            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(filePath, true);
                sw.WriteLine(DateTime.Now.ToString("dd/MM - HH:mm:ss.fff") + "  |  " + text);
                sw.Flush();
            }
            catch { }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }

        public static void WriteLine(Exception ex)
        {
            if (ex == null)
                return;
            WriteLine(ex.ToString());
        }
    }
    public class Utils
    {
        public const string
    CT_ENDCDATA = "}}>",
    CT_ENDCDATA2 = "}.}.>",
    CT_ENDCDATA3 = "}..}..>";
        //--DBCC CHECKIDENT('s_templates', RESEED, 409)
        public static string[] MySplit(string sVal, char cSeparator)
        {
            //sVal = sVal.Replace("\\\\", "<_backslash_>");
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());
                //aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString()).Replace("<_backslash_>", "\\\\");
                //if (aData[i].EndsWith("\\\\")) 
                //    aData[i] = aData[i].Substring(0, aData[i].Length - 1);
            }
            return aData;
        }
        //public static string[] MySplit(string sVal, char cSeparator)
        //{
        //    sVal = sVal.Replace("\\\\", "<_backslash_>");
        //    sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
        //    string[] aData = sVal.Split(cSeparator);
        //    for (int i = 0; i < aData.Length; i++)
        //    {
        //        aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString()).Replace("<_backslash_>", "\\\\");
        //        if (aData[i].EndsWith("\\\\"))
        //            aData[i] = aData[i].Substring(0, aData[i].Length - 1);
        //    }
        //    return aData;
        //}

        public static string _JsonEscape(string aText)
        {
            char[] escapeChars = new char[] { '\\', '\"', '\n', '\r', '\t', '\b', '\f' };
            StringBuilder s = new StringBuilder();
            if (aText != null)
            {
                int j = 0;
                int i = aText.IndexOfAny(escapeChars);
                while (i != -1)
                {
                    if (i != j) s.Append(aText.Substring(j, i - j));
                    switch (aText[i])
                    {
                        case '\\': s.Append("\\\\"); break;
                        case '\"': s.Append("\\\""); break;
                        case '\n': s.Append("\\n"); break;
                        case '\r': s.Append("\\r"); break;
                        case '\t': s.Append("\\t"); break;
                        case '\b': s.Append("\\b"); break;
                        case '\f': s.Append("\\f"); break;
                    }
                    j = i + 1;
                    i = aText.IndexOfAny(escapeChars, j);
                }
                s.Append(aText.Substring(j));
            }
            return s.ToString();
        }

        public static string _JsonEscape_v3(string aText)
        {
            string result = "";
            if (aText != null)
                foreach (char c in aText)
                {
                    switch (c)
                    {
                        case '\\': result += "\\\\"; break;
                        case '\"': result += "\\\""; break;
                        case '\n': result += "\\n"; break;
                        case '\r': result += "\\r"; break;
                        case '\t': result += "\\t"; break;
                        case '\b': result += "\\b"; break;
                        case '\f': result += "\\f"; break;
                        default: result += c; break;
                    }
                }
            return result;
        }
        public static int myPathParamsRender(StringBuilder sJSON, string sPath)
        {
            int i = 0;
            sJSON.Append("{");
            string[] aPath = sPath.Split('/');
            string sIdsPath = "";
            string sIdsPath1 = "";
            sIdsPath = aPath[0];
            for (i = 0; i < aPath.Length; i++)
            {
                sJSON.Append("\"");
                sJSON.Append(_JsonEscape("Path" + (i + 1).ToString()));
                sJSON.Append("\":\"");
                sJSON.Append(_JsonEscape(aPath[i]));
                sJSON.Append("\",");
                if (i > 0)
                {
                    sJSON.Append("\"");
                    sJSON.Append(_JsonEscape(aPath[i - 1]));
                    sJSON.Append("\":\"");
                    sJSON.Append(_JsonEscape(aPath[i]));
                    sJSON.Append("\",");
                }
                if (i > 0)
                {
                    if (i % 2 == 0)
                    {
                        sIdsPath += "_" + aPath[i];
                        sIdsPath1 += "_ID";
                    }
                    else
                    {
                        sIdsPath += "_ID";
                        sIdsPath1 += "_" + aPath[i];
                    }

                }
            }
            if (i > 0)
            {
                sJSON.Append("\"PathLast\":\"");
                sJSON.Append(_JsonEscape(aPath[i - 1]));
                sJSON.Append("\",");
            }
            if (i > 1)
            {
                sJSON.Append("\"PathLast1\":\"");
                sJSON.Append(_JsonEscape(aPath[i - 2]));
                sJSON.Append("\",");
            }
            sJSON.Append("\"IDsPath\":\"");
            sJSON.Append(_JsonEscape(sIdsPath));
            sJSON.Append("\",");
            if (sIdsPath1.StartsWith("_")) sIdsPath1 = sIdsPath1.Substring(1);
            sJSON.Append("\"IDsPath1\":\"");
            sJSON.Append(_JsonEscape(sIdsPath1));
            sJSON.Append("\",");
            if (sIdsPath.EndsWith("_ID"))
            {
                sJSON.Append("\"IDsPath-1\":\"");
                sJSON.Append(_JsonEscape(sIdsPath.Substring(0, sIdsPath.Length - 3)));
                sJSON.Append("\",");
            }
            if (sIdsPath1.EndsWith("_ID"))
            {
                sJSON.Append("\"IDsPath1-1\":\"");
                sJSON.Append(_JsonEscape(sIdsPath1.Substring(0, sIdsPath1.Length - 3)));
                sJSON.Append("\",");
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(_JsonEscape(i.ToString()));
            sJSON.Append("\"");
            sJSON.Append("}");
            return i;
        }
        public static int myQueryRender(StringBuilder sJSON, string sQuery)
        {
            if (sQuery == null) return 0;
            if (sQuery.IndexOf("?") != -1)
            {
                if (sQuery.EndsWith("?")) return 0;
                sQuery = sQuery.Substring(sQuery.IndexOf("?") + 1);
            }
            string[] aParams = sQuery.Split('&');
            sJSON.Append("{");
            int i = 0;
            foreach (string sParam in aParams)
            {
                string parName = "", parVal = "";
                int io1 = sParam.IndexOf("=");
                if (io1 == -1)
                    parName = sParam;
                else
                {
                    parName = sParam.Substring(0, io1);
                    if (io1 + 1 < sParam.Length)
                        parVal = sParam.Substring(io1 + 1);
                }
                if (parName != "")
                {
                    //string val = HttpUtility.UrlDecode(parVal);
                    i++;
                    string val = XDocUrlDecode(parVal);
                    sJSON.Append("\"");
                    sJSON.Append(_JsonEscape(parName));
                    sJSON.Append("\":\"");
                    sJSON.Append(_JsonEscape(val));
                    sJSON.Append("\",");
                }
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(_JsonEscape(i.ToString()));
            sJSON.Append("\"");
            sJSON.Append("}");
            return i;
        }
        public static string Encode(string s, EncodeOption encOption)
        {
            string retVal = s;

            switch (encOption)
            {
                case EncodeOption.Encode:
                    retVal = XDocHtmlEncode(XDocUrlEncode(retVal));
                    //retVal = HttpUtility.HtmlEncode(XDocUrlEncode(retVal));
                    break;
                case EncodeOption.Decode:
                    retVal = XDocUrlDecode(XDocHtmlDecode(retVal));
                    //retVal = XDocUrlDecode(HttpUtility.HtmlDecode(retVal));
                    break;
                case EncodeOption.HTMLDecode:
                    retVal = XDocHtmlDecode(retVal);
                    //retVal = HttpUtility.HtmlDecode(retVal);
                    break;
                case EncodeOption.HTMLEncode:
                    retVal = XDocHtmlEncode(retVal);
                    //retVal = HttpUtility.HtmlEncode(retVal);
                    break;
                case EncodeOption.URLDecode:
                    retVal = XDocUrlDecode(retVal);
                    break;
                case EncodeOption.URLEncode:
                    retVal = XDocUrlEncode(retVal);
                    break;
                case EncodeOption.EndCDATA:
                    retVal = retVal.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
                    retVal = retVal.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
                    retVal = retVal.Replace("]]>", Utils.CT_ENDCDATA);
                    break;
                case EncodeOption.SQLEscape:
                    retVal = retVal.Replace("'", "''");
                    break;
                case EncodeOption.PAREscape:
                    retVal = retVal.Replace(".", "\\.");
                    retVal = retVal.Replace("#", "%23");
                    retVal = retVal.Replace("(", "%28");
                    retVal = retVal.Replace(")", "%29");
                    break;
                case EncodeOption.MACROEscape:
                    retVal = retVal.Replace(",", "\\,");
                    retVal = retVal.Replace(")", "\\)");
                    break;
                case EncodeOption.XDOCEncode:
                    retVal = retVal.Replace("#", "%23");
                    retVal = retVal.Replace("(", "%28");
                    retVal = retVal.Replace(")", "%29");
                    break;
                case EncodeOption.XDOCDecode:
                    retVal = retVal.Replace("%23", "#");
                    retVal = retVal.Replace("%28", "(");
                    retVal = retVal.Replace("%29", ")");
                    break;
                case EncodeOption.EncodeSQLEscape:
                    retVal = XDocHtmlEncode(XDocUrlEncode(retVal));
                    retVal = retVal.Replace("'", "''");
                    //retVal = HttpUtility.HtmlEncode(XDocUrlEncode(retVal));
                    break;
                case EncodeOption.DecodeSQLEscape:
                    retVal = XDocUrlDecode(XDocHtmlDecode(retVal));
                    retVal = retVal.Replace("'", "''");
                    //retVal = XDocUrlDecode(HttpUtility.HtmlDecode(retVal));
                    break;
                case EncodeOption.HTMLDecodeSQLEscape:
                    retVal = XDocHtmlDecode(retVal);
                    retVal = retVal.Replace("'", "''");
                    //retVal = HttpUtility.HtmlDecode(retVal);
                    break;
                case EncodeOption.HTMLEncodeSQLEscape:
                    retVal = XDocHtmlEncode(retVal);
                    retVal = retVal.Replace("'", "''");
                    //retVal = HttpUtility.HtmlEncode(retVal);
                    break;
                case EncodeOption.URLDecodeSQLEscape:
                    retVal = XDocUrlDecode(retVal);
                    retVal = retVal.Replace("'", "''");
                    break;
                case EncodeOption.URLEncodeSQLEscape:
                    retVal = XDocUrlEncode(retVal);
                    retVal = retVal.Replace("'", "''");
                    break;
                case EncodeOption.JSEscape:
                    retVal = retVal.Replace("\\", "\\\\");
                    retVal = retVal.Replace("'", "\\'");
                    retVal = retVal.Replace("\"", "\\\"");
                    retVal = retVal.Replace("`", "\\`");
                    retVal = retVal.Replace("\r", "\\r");
                    retVal = retVal.Replace("\n", "\\n");
                    break;
                case EncodeOption.HTMLEncodeJSEscape:
                    retVal = XDocHtmlEncode(retVal);
                    retVal = retVal.Replace("\\", "\\\\");
                    retVal = retVal.Replace("'", "\\'");
                    retVal = retVal.Replace("\"", "\\\"");
                    retVal = retVal.Replace("`", "\\`");
                    break;
                case EncodeOption.Text2HTML:
                    retVal = XDocHtmlEncode(retVal);
                    retVal = retVal.Replace("&#13;&#10;", "<br>");
                    break;
                case EncodeOption.Base64Encode:
                    byte[] binaryData = Encoding.UTF8.GetBytes(retVal);
                    retVal = Convert.ToBase64String(binaryData, 0, binaryData.Length);
                    break;
                case EncodeOption.Base64Decode:
                    byte[] binaryDataD = Convert.FromBase64String(retVal);
                    retVal = Encoding.UTF8.GetString(binaryDataD, 0, binaryDataD.Length);
                    break;
                case EncodeOption.JSONEscape:
                    retVal = Utils._JsonEscape(retVal);
                    break;
                case EncodeOption.None:
                    break;
                default:
                    break;
            }

            return retVal;
        }
        private static string XDocHtmlDecode(string retVal)
        {
            retVal = HttpUtility.HtmlDecode(retVal);
            return retVal;
        }
        private static string XDocHtmlEncode(string retVal)
        {
            retVal = HttpUtility.HtmlEncode(retVal);
            retVal = retVal.Replace("\r\n", "&#13;&#10;");
            retVal = retVal.Replace("\r", "&#13;&#10;");
            retVal = retVal.Replace("\n", "&#13;&#10;");
            return retVal;
            //retVal = HttpUtility.HtmlEncode(retVal);
            //retVal = retVal.Replace("\r\n|\r|\n", "&#13;&#10;", "g"); // regex should be with modifier global
            //return retVal;
        }

        public static string XDocUrlDecode(string retVal)
        {
            string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
            if (sEncoding == null) sEncoding = "";
            if (sEncoding == "") sEncoding = "utf-8";
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
            if (enc != null)
                retVal = HttpUtility.UrlDecode(retVal, enc);
            else
                retVal = HttpUtility.UrlDecode(retVal, System.Text.Encoding.Default);

            return retVal;
        }
        public static string XDocUrlEncode(string retVal)
        {
            string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
            if (sEncoding == null) sEncoding = "";
            if (sEncoding == "") sEncoding = "utf-8";
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
            if (enc != null)
                retVal = HttpUtility.UrlEncode(retVal, enc);
            else
                retVal = HttpUtility.UrlEncode(retVal, System.Text.Encoding.Default);
            retVal = retVal.Replace("+", "%20");
            retVal = retVal.Replace("!", "%21");
            retVal = retVal.Replace("(", "%28");
            retVal = retVal.Replace(")", "%29");
            retVal = retVal.Replace("'", "%27");

            return retVal;
        }
    }
    public enum EncodeOption
    {
        //this enum must not be modified. Its names are used to identify command keywords when parsing
        None,
        Encode,
        Decode,
        HTMLEncode,
        HTMLDecode,
        URLEncode,
        URLDecode,
        EndCDATA,
        EncodeSQLEscape,
        DecodeSQLEscape,
        HTMLEncodeSQLEscape,
        HTMLDecodeSQLEscape,
        URLEncodeSQLEscape,
        URLDecodeSQLEscape,
        SQLEscape,
        PAREscape,
        MACROEscape,
        XDOCEncode,
        XDOCDecode,
        JSEscape,
        HTMLEncodeJSEscape,
        Text2HTML,
        Base64Encode,
        Base64Decode,
        JSONEscape
    }


}
