using System;
using System.Collections.Generic;
using System.Text;
using XDocuments;
using System.Configuration;
using System.Collections;
using KubionLogNamespace;
//using XHTMLMerge;
using System.Collections.Specialized;

namespace HierarchyReaderModule
{
    public class HierarchyReader
    {
        XDocManager m_manager = null;

        public HierarchyReader()
        {
            m_manager = new XDocManager();
        }
        public  void Dispose()
        {
            if (m_manager != null)
            {
                m_manager.Dispose();
                m_manager = null;
            }
        }

        public string GetHierarchy(string templateName, string hierarchyName, string levelName)
        {
            Hashtable parameters = CollectionsUtil.CreateCaseInsensitiveHashtable();
            parameters["Hierarchyname"] = hierarchyName;
            parameters["Levelname"] = levelName;
            return m_manager.GetHierarchy(templateName, parameters);
        }

        public string GetItemsList(string hierarchyName, string levelName)
        {
            Hashtable parameters = CollectionsUtil.CreateCaseInsensitiveHashtable();
            parameters["Hierarchyname"] = hierarchyName;
            parameters["Levelname"] = levelName;
            return m_manager.GetHierarchy("GetList", parameters);
        }
    }
}
