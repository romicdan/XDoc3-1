using System;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Collections.Generic;


namespace XHTMLMerge
{

    public enum CommandType
    {
        REPCommand,
        QRYCommand,
        IFCommand,
        ElseIFCommand,
        FLDCommand,
        PARCommand,
        TEXTCommand,
        AttachCommand,
        IncludeCommand,
        IncludeOnceCommand,
        JSETCommand,
        JPARCommand,
        JDATACommand,
        JLOOPCommand,
        JPROPCommand,
        DefParCmd,
        DEFQRYCommand,
        MSGCommand,
        CountCommand,
        GenericCommand,
        XPATHCommand,
        SETVALCommand,
        SETFLDCommand,
        SETPARCommand,
        GETPARCommand,
        GETCFGPARCommand,
        ADDPARCommand,
        GETITEMPARCommand,
        FINDITEMPARCommand,
        DELITEMCommand,
        DELCommand,
        IMPORTCommand,
        EXPORTCommand,
        TRANSFERCommand,
        DELETETEMPLATECommand,
        CFGCommand,
        USEPARAMETERSCommand,
        FUNCCommand,
        THROWCommand,
        FILECommand

    }

    public enum AttachType
    {
        Database,
        File
    }

    /// <summary>
    /// Options for URL and HTML encoding available for FLD and PAR command
    /// </summary>
    //public enum EncodeOption
    //{
    //    //this enum must not be modified. Its names are used to identify command keywords when parsing
    //    None,
    //    Encode,
    //    Decode,
    //    HTMLEncode,
    //    HTMLDecode,
    //    URLEncode,
    //    URLDecode,
    //    EndCDATA,
    //    EncodeSQLEscape,
    //    DecodeSQLEscape,
    //    HTMLEncodeSQLEscape,
    //    HTMLDecodeSQLEscape,
    //    URLEncodeSQLEscape,
    //    URLDecodeSQLEscape,
    //    SQLEscape,
    //    PAREscape,
    //    MACROEscape,
    //    XDOCEncode,
    //    XDOCDecode,
    //    JSEscape,
    //    HTMLEncodeJSEscape,
    //    Text2HTML,
    //    Base64Encode,
    //    Base64Decode,
    //    JSONEscape
    //}

    public class ParseUtils
    {

        public static List<ListParameter> GetParameterList(string text, char quote, char separator, bool hasBrackets)
        {
            return GetParameterList(text, quote, separator, hasBrackets, true);
        }

        //public static List<ListParameter> GetParameterList(string text, char quote, char separator, bool hasBrackets, bool removeQuotes)
        //{

        //    List<ListParameter> rawParams = new List<ListParameter>();

        //    int indexOffset = 0;

        //    try
        //    {
        //        if (hasBrackets)
        //        {
        //            int fio = text.IndexOf('(');
        //            if (fio != -1)
        //            {
        //                int lio = text.LastIndexOf(')');
        //                if (lio != -1)
        //                {
        //                    text = text.Substring(fio + 1, lio - fio - 1);
        //                    indexOffset = fio + 1;
        //                }
        //                else throw new Exception("No closing bracket has been found!");
        //            }
        //            else
        //                throw new Exception("No opening bracket has been found!");

        //        }

        //        int firstIndex = 0;
        //        int lastIndex = 0;
        //        bool inQuote = false;
        //        bool escape = false;
        //        string cpar = "";
        //        for (int i = 0; i < text.Length; i++)
        //        {
        //            string c = text[i].ToString();
        //            string param = null;
        //            cpar = "";
        //            if (c == quote.ToString() && inQuote)
        //            {
        //                if (!escape)
        //                {
        //                    lastIndex = i;
        //                    param = text.Substring(firstIndex, lastIndex - firstIndex + 1);
        //                    i++;
        //                    inQuote = false;
        //                }
        //                else
        //                    escape = false;
        //            }
        //            else if (c == quote.ToString() && !inQuote && !escape)
        //            {
        //                inQuote = true;
        //            }
        //            else if (c == separator.ToString() && !inQuote)
        //            {
        //                if (i > 0)
        //                {
        //                    lastIndex = i - 1;
        //                    param = text.Substring(firstIndex, lastIndex - firstIndex + 1);
        //                }
        //                else
        //                    throw new Exception("The parameters list starts with a '");
        //            }
        //            else if (i == text.Length - 1)
        //            {
        //                if (inQuote)
        //                    throw new Exception("The parameters list does not use quotes correctly");
        //                lastIndex = i;
        //                param = text.Substring(firstIndex, lastIndex - firstIndex + 1);
        //            }

        //            escape = (c == "\\");

        //            if (param != null)
        //            {
        //                int index = indexOffset + firstIndex;
        //                if (param.StartsWith(quote.ToString()) && param.EndsWith(quote.ToString()) && removeQuotes)
        //                {
        //                    param = param.Substring(1);
        //                    param = param.Substring(0, param.Length - 1);
        //                    index++;
        //                }
        //                param = param.Replace("\\" + quote.ToString(), quote.ToString());
        //                rawParams.Add(new ListParameter(param, index));
        //                if (i < text.Length - 1)
        //                    firstIndex = i + 1;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string message = "Error parsing parameter list: " + text + Environment.NewLine + ex.Message;
        //        throw new Exception(message);
        //    }

        //    return rawParams;

        //}
        public static string[] MySplit(string sVal, char cSeparator)
        {
            sVal = sVal.Replace("\\\\" , "<_backslash_>");
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            string[] aData = sVal.Split(cSeparator );
            for (int i = 0; i < aData.Length; i++)
            {
                aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString()).Replace("<_backslash_>", "\\");
            }
            return aData;
        }
        public static List<ListParameter> GetParameterList(string text, char quote, char separator, bool hasBrackets, bool removeQuotes)
        {
            // MihaiD error in \. "FUNC..String_EndsWith..%url%.\\.%ext%"
            List<ListParameter> rawParams = new List<ListParameter>();

            int indexOffset = 0;
            text = text.Replace("\\" + separator.ToString (), "<tilda>");
            try
            {
                if (hasBrackets)
                {
                    int fio = text.IndexOf('(');
                    if (fio != -1)
                    {
                        int lio = text.LastIndexOf(')');
                        if (lio != -1)
                        {
                            text = text.Substring(fio + 1, lio - fio - 1);
                            indexOffset = fio + 1;
                        }
                        else throw new Exception("No closing bracket has been found!");
                    }
                    else
                        throw new Exception("No opening bracket has been found!");

                }

                int firstIndex = 0;
                int lastIndex = 0;
                bool inQuote = false;
                bool escape = false;

                for (int i = 0; i < text.Length; i++)
                {
                    string c = text[i].ToString();
                    string param = null;
                    if (c == quote.ToString() && inQuote)
                    {
                        if (!escape)
                        {
                            lastIndex = i;
                            param = text.Substring(firstIndex, lastIndex - firstIndex + 1);
                            i++;
                            inQuote = false;
                        }
                        else
                            escape = false;
                    }
                    else if (c == quote.ToString() && !inQuote && !escape)
                    {
                        inQuote = true;
                    }
                    else if (c == separator.ToString() && !inQuote)
                    {
                        if (i > 0)
                        {
                            lastIndex = i - 1;
                            param = text.Substring(firstIndex, lastIndex - firstIndex + 1);
                        }
                        else
                            throw new Exception("The parameters list starts with a '");
                    }
                    else if (i == text.Length - 1)
                    {
                        if (inQuote)
                            throw new Exception("The parameters list does not use quotes correctly");
                        lastIndex = i;
                        param = text.Substring(firstIndex, lastIndex - firstIndex + 1);
                    }

                    escape = (c == "\\");

                    if (param != null)
                    {
                        int index = indexOffset + firstIndex;
                        if (param.StartsWith(quote.ToString()) && param.EndsWith(quote.ToString()) && removeQuotes)
                        {
                            param = param.Substring(1);
                            param = param.Substring(0, param.Length - 1);
                            index++;
                        }
                        param = param.Replace("\\" + quote.ToString(), quote.ToString());
                        param = param.Replace("<tilda>", separator.ToString());  
                        rawParams.Add(new ListParameter(param, index));
                        if (i < text.Length - 1)
                            firstIndex = i + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                string message = "Error parsing parameter list: " + text + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }

            return rawParams;

        }

        public static string[] StringArrayFromParamList(List<ListParameter> list)
        {
            if (list == null)
                return new string[0];

            string[] ret = new string[list.Count];
            for (int i = 0; i < list.Count; i++)
                ret[i] = list[i].Text;

            return ret;
        }

        /// <summary>
        /// The default quote character is "
        /// </summary>
        public static string GetTagParameter(string tag, string param)
        {
            return GetTagParameter(tag, param, '\"');
        }

        public static string GetTagParameter(string tag, string param, char quote)
        {
            if (param == "" || param == null || tag == "" || tag == null)
                return "";
            string ret = "";

            int io1 = tag.ToLower().IndexOf(param.ToLower());
            if (io1 != -1)
            {
                int io2 = tag.IndexOf(quote, io1 + 1);
                if (io2 != -1)
                {
                    int io3 = tag.IndexOf(quote, io2 + 1);
                    if (io3 != -1)
                    {
                        ret = tag.Substring(io2 + 1, io3 - io2 - 1);
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// The default quote character is "
        /// </summary>
        public static Hashtable TagAttributes(string tag)
        {
            return TagAttributes(tag, '\"');
        }

        /// <summary>
        /// Returns all the attributes found in a tag
        /// </summary>
        public static Hashtable TagAttributes(string tag, char quote)
        {
            Hashtable hRet = CollectionsUtil.CreateCaseInsensitiveHashtable();
            string ex = @"(\w)+(\x20)*=(\x20)*" + quote.ToString() + "[^" + quote.ToString() + "]*" + quote.ToString();
            Regex regExAttributes = new Regex(ex);
            MatchCollection mc = regExAttributes.Matches(tag);
            foreach (Match m in mc)
            {
                int io1 = m.Value.IndexOf(quote);
                int io2 = m.Value.IndexOf(quote, io1 + 1);
                string attribute = m.Value.Substring(0, m.Value.IndexOf("=")).TrimStart().TrimEnd(); ;
                string value = m.Value.Substring(io1 + 1, io2 - io1 - 1);
                hRet[attribute] = value;
            }
            return hRet;
        }

        public static string GetTagAttribute(Hashtable tagAttributes, string attrName)
        {
            if (tagAttributes == null || tagAttributes.Count == 0 || attrName == null || attrName == "")
                return "";

            string ret = tagAttributes[attrName] as string;
            if (ret == null)
            {
                ret = tagAttributes[attrName.ToUpper()] as string;
                if (ret == null)
                {
                    ret = tagAttributes[attrName.ToLower()] as string;
                    if (ret == null)
                        ret = "";
                }
            }
            return ret;
        }

        public class ListParameter
        {
            string m_text = "";

            public string Text
            {
                get { return m_text; }
                set { m_text = value; }
            }
            int m_index = -1;

            public int Index
            {
                get { return m_index; }
                set { m_index = value; }
            }

            public ListParameter(string text, int index)
            {
                m_text = text;
                m_index = index;
            }

            public override string ToString()
            {
                return m_text;
            }
        }



        

    }

    public class IndexMappings
    {
        const string 
            START_COMMAND = "#STARTBLOCK#",
            END_COMMAND = "#ENDBLOCK#",
            START_BLOCK = "#STARTBLOCK#",
            END_BLOCK = "#ENDBLOCK#",
            START_COMMENT = "#STARTCOMMENT#",
            END_COMMENT = "#ENDCOMMENT#";

        public struct IndexRange
        {
            int m_startIndex;
            int m_endIndex;

            public int StartIndex
            {
                get { return m_startIndex; }
                set { m_startIndex = value; }
            }

            public int EndIndex
            {
                get { return m_endIndex; }
                set { m_endIndex = value; }
            }

            public int Length
            {
                get { return m_endIndex - m_startIndex; }
            }

            public IndexRange(int startIndex, int endIndex)
            {
                m_startIndex = startIndex;
                m_endIndex = endIndex;
            }

            public override string ToString()
            {
                string ret = "Undefined";
                if (m_startIndex != -1 && m_endIndex != -1)
                    ret = m_startIndex + " -> " + m_endIndex;
                return ret;
            }

        }

        List<IndexRange> m_ranges = new List<IndexRange>();

        public List<IndexRange> Ranges
        {
            get { return m_ranges; }
            set { m_ranges = value; }
        }

        public void AddRange(int startIndex, int endIndex)
        {
            IndexRange range = new IndexRange(startIndex, endIndex);
            m_ranges.Add(range);
        }

        public int GetOriginalIndex(int index)
        {
            int retIndex = index;
            int previousLengths = 0;
            int origPointer = 0;


            for (int i = 0; i < m_ranges.Count; i++)
            {
                if (i == 0)
                {
                    if (m_ranges[0].StartIndex > index)
                    {
                        return index;
                    }
                    origPointer += m_ranges[0].StartIndex;
                }
                else
                {
                    if (origPointer + m_ranges[i].StartIndex - m_ranges[i - 1].EndIndex > index)
                    {
                        return previousLengths + index;
                    }
                    else
                        origPointer += m_ranges[i].StartIndex - m_ranges[i - 1].EndIndex;

                }
                previousLengths += m_ranges[i].Length;
            }

            return previousLengths + index;

        }

        public string GetTextRange(int start, int end, string text, string originalText)
        {
            return "";
        }

        public static string StripDocument(string txt, out IndexMappings mappings)
        {
            int ioStart = -1, ioEnd = 0;
            mappings = new IndexMappings();
            string ret = "";

            do
            {
                ioStart = txt.ToUpper().IndexOf(START_COMMAND, ioEnd);
                if (ioStart != -1)
                {
                    int ioEndNew = txt.ToUpper().IndexOf(END_COMMAND, ioStart + 1);
                    if (ioEndNew == -1)
                        throw new Exception("Ignore block tags are used incorrectly!");

                    ret += txt.Substring(ioEnd, ioStart - ioEnd) + "{";

                    ioEnd = ioEndNew + END_COMMAND.Length;

                    mappings.AddRange(ioStart, ioEnd);
                }
            } while (ioStart != -1);

            ret += "{" + txt.Substring(ioEnd, txt.Length - ioEnd) + "}";

            return ret;
        }

        public override string ToString()
        {
            string ret = "No ranges";
            if (m_ranges != null)
                ret = m_ranges.Count.ToString() + " ranges";
            return ret;
        }

    }

    public class LogIncludeEventArgs : EventArgs
    {
        Hashtable m_includeParams = null;
        int m_indentation = 1;

        public int Indentation
        {
            get { return m_indentation; }
            set { m_indentation = value; }
        }

        public Hashtable IncludeParams
        {
            get { return m_includeParams; }
            set { m_includeParams = value; }
        }

        public LogIncludeEventArgs()
        {
        }

        public LogIncludeEventArgs(Hashtable includeParams, int indentation)
        {
            m_includeParams = includeParams;
            m_indentation = indentation;
        }

        public override string ToString()
        {
            string ret = "Indent: " + m_indentation.ToString();
            if (m_includeParams != null)
                ret += "; No of params: " + m_includeParams.Count.ToString();
            return ret;
        }
    }

}
