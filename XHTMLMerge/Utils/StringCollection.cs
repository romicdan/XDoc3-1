using System;
using System.Collections;

namespace XHTMLMerge
{
    public class StringCollection : CollectionBase
    {

        public string this[int index]
        {
            get
            {
                return ((string)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        public int Add(string value)
        {
            return (List.Add(value));
        }

        public int IndexOf(string value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, string value)
        {
            List.Insert(index, value);
        }

        public void Remove(string value)
        {
            List.Remove(value);
        }

        public bool Contains(string value)
        {
            // If value is not of type string, this will return false.
            return (List.Contains(value));
        }

        protected override void OnInsert(int index, Object value)
        {
            try
            {
                string temp = (string)value;
            }
            catch
            {
                throw new ArgumentException("value must be of type string.");
            }
        }

        protected override void OnRemove(int index, Object value)
        {
            try
            {
                string temp = (string)value;
            }
            catch
            {
                throw new ArgumentException("value must be of type string.");
            }
        }

        protected override void OnSet(int index, Object oldValue, Object newValue)
        {
            try
            {
                string temp = (string)newValue;
            }
            catch
            {
                throw new ArgumentException("value must be of type string.");
            }
        }

        protected override void OnValidate(Object value)
        {
            try
            {
                string temp = (string)value;
            }
            catch
            {
                throw new ArgumentException("value must be of type string.");
            }
        }

    }
}
