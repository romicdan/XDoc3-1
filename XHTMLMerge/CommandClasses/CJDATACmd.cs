﻿using System;
using System.Diagnostics;
using XDataSourceModule;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using KubionLogNamespace;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CJDATACmd : CCmd
    {
        # region Protected members
        // JDATA.[<par_name>].<_select_>.[<_conn_>].[<_error_>]
        protected string m_strParamName = "";
        protected string m_Select = "";
        protected string m_Conn = "0";
        protected string m_ErrorParamName = "ERROR";

        #endregion Protected members

        #region Public properties

        public string ParameterName
        {
            get { return m_strParamName; }
            set { m_strParamName = value; }
        }
        public string Select
        {
            get { return m_Select; }
            set { m_Select = value; }
        }
        public string Conn
        {
            get { return m_Conn; }
            set { m_Conn = value; }
        }
        public string ErrorParamName
        {
            get { return m_ErrorParamName; }
            set { m_ErrorParamName = value; }
        }
        protected string m_isParSource = "0";
        public string isParSource
        {
            get { return m_isParSource; }
            set { m_isParSource = value; }
        }


        #endregion Public properties

        public CJDATACmd(): base()
        {
            this.m_enType = CommandType.JDATACommand ;
            m_bIsBlockCommand = false;
        }

        public override string Execute(CParser m_parser)
        {
            //JSON.Net 3.5
            //XmlNote myXmlNode = JsonConvert.DeserializeXmlNode(myJsonString);
            //// or .DeserilizeXmlNode(myJsonString, "root"); // if myJsonString does not have a root
            //string jsonString = JsonConvert.SerializeXmlNode(myXmlNode);

            //// To convert an XML node contained in string xml into a JSON string   
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);
            //string jsonText = JsonConvert.SerializeXmlNode(doc);

            //// To convert JSON text contained in string json into an XML node
            //XmlDocument doc = JsonConvert.DeserializeXmlNode(json);

            //#JDATA.QData.%SEL%.API_Data.Err#

            //if (sConnIDName == "0") sConnIDName = "";
            //if (sConnIDName == "-1") sConnIDName = "";


            //if (sConnIDName != "")
            //{
            //    string sQueryStatement = SQL.SQLConnectionString;
            //    sQueryStatement = sQueryStatement.Replace("#P1#", sConnIDName);
            //    DataTable dtQueryc = connAdm.GetDataTable(sQueryStatement);
            //    if (dtQueryc == null || dtQueryc.Rows.Count == 0)
            //    {
            //        Error = "Cannot find requested connection (" + sConnIDName + ") for template id = " + Convert.ToString(m_templateID);
            //        ErrorVerbose = Error;
            //        return false;
            //    }
            //    strConn = Connectors.GetStringResult(dtQueryc.Rows[0][S_CONNECTIONS.CONNSTRING]);
            //}
            //if ((strConn == "") || (strConn == ConfigurationManager.AppSettings["XDocConnectionString"]))
            //    providerData = connAdm;
            //else
            //    providerData = new Connectors().GetConnector(strConn);

            //dtQueryResult = providerData.GetDataTable(strSQL, ref m_Response, ref m_OuterXml, ref m_InnerXml);


            string l_strParamName, l_Select, l_Conn, l_ErrorParamName;
            l_strParamName = m_parser.ReplaceParameters(m_strParamName);
            l_Select = m_parser.ReplaceParameters(m_Select);
            l_Conn = m_parser.ReplaceParameters(m_Conn);
            l_ErrorParamName = m_parser.ReplaceParameters(m_ErrorParamName);

            if (m_isParSource == "1") l_Select = (string)m_parser.TemplateParams[l_Select];

            string val = "";
            string sError, sErrorVerbose;

            try
            {
                if (l_Conn.StartsWith("SPLITXX"))
                {

                    val = JSON_SplitXX(l_Select, l_Conn[7], l_Conn[8],l_Conn .Substring (9));
                }
                else
                    val = m_parser.Evaluator.GetResponse(m_parser.DataProvider, l_Select, l_Conn);
                if (l_strParamName == "")
                    return val;
                else
                {
                    m_parser.TemplateParams[l_strParamName] = val;
                    return "";
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JDATA command";
                sErrorVerbose = ex.Message;
                sErrorVerbose = "Error executing JDATA command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
            }

            if (l_ErrorParamName != "")
            {
                //m_parser.ParamDefaults[l_ErrorParamName] = sError;
                m_parser.TemplateParams[l_ErrorParamName] = (m_parser.TemplateParams[l_ErrorParamName]==null?"": m_parser.TemplateParams[l_ErrorParamName] + "\r\n---\r\n") + sError;
                //m_parser.ParamDefaults[l_ErrorParamName + "Verbose"] = sErrorVerbose;
                m_parser.TemplateParams[l_ErrorParamName + "Verbose"] = (m_parser.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : m_parser.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n---\r\n") + sErrorVerbose;
            }

            return "";
        }

        string JSON_SplitXX(string sData, char cRowSep, char cColSep,string sHeader)
        {
            string[] sRows;                     //array of rows
            string[] sCols;                     //array of rows
            string[] sColNames;                 //array of column names
            int iCols;                          //number of columns
            int iRows;                          //number of rows
            StringBuilder sb = new StringBuilder();

            sb.Append("[");

            if (cColSep == ' ') cColSep = '|';
            if (cRowSep == ' ') cRowSep = '|';

            sRows = Utils.MySplit(sData, cRowSep);
            sColNames = Utils.MySplit(sHeader, cColSep);
            iCols = sColNames.GetLength(0);
            iRows = sRows.GetLength(0) ;

            for (int i = 0; i < iRows ; i++)     
            {
                if (i > 0) sb.Append(",");
                sb.Append("{");
                sb.Append("\"id\":\""); sb.Append((i+1).ToString()); sb.Append("\"");
                sb.Append(",\"id0\":\""); sb.Append(i.ToString()); sb.Append("\"");
                sCols = Utils.MySplit(sRows[i], cColSep);
                for (int j = 0; (j < iCols) && (j < sCols.Length); j++)
                {
                    sb.Append(",\""); sb.Append(sColNames[j]); sb.Append("\":\""); sb.Append(sCols[j]); sb.Append("\"");
                }
                sb.Append("}");
            }
            sb.Append("]");

            return sb.ToString();
        }

    }
}