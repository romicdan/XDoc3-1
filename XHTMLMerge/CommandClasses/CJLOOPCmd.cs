using System;
using System.Diagnostics;
using XDataSourceModule;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace XHTMLMerge
{
    [Serializable]
    public class CJLOOPCmd : CCmd
    {
        # region Protected members

        protected string m_ParamName = "";
        protected string m_Source = "";
        protected string m_JPath = "";
        protected string m_PageSize = "1000";
        protected string m_PageNr = "1";
        protected string m_ErrorParamName = "ERROR";

        #endregion Protected members

        #region Public properties

        public string ParameterName
        {
            get { return m_ParamName; }
            set { m_ParamName = value; }
        }
        public string Source
        {
            get { return m_Source; }
            set { m_Source = value; }
        }
        public string JPath
        {
            get { return m_JPath; }
            set { m_JPath = value; }
        }
        public string PageSize
        {
            get { return m_PageSize; }
            set { m_PageSize = value; }
        }
        public string PageNr
        {
            get { return m_PageNr; }
            set { m_PageNr = value; }
        }
        protected string m_isParSource = "0";
        public string isParSource
        {
            get { return m_isParSource; }
            set { m_isParSource = value; }
        }
        public string ErrorParamName
        {
            get { return m_ErrorParamName; }
            set { m_ErrorParamName = value; }
        }


        #endregion Public properties

        public CJLOOPCmd(): base()
        {
            this.m_enType = CommandType.JLOOPCommand ;
            m_bIsBlockCommand = true;
        }


        public override string Execute(CParser m_parser)
        {
            string l_ParamName, l_Source, l_JPath, l_PageSize, l_PageNr, l_ErrorParamName;
            l_ParamName = m_parser.ReplaceParameters(m_ParamName);
            l_Source = m_parser.ReplaceParameters(m_Source);
            l_JPath = m_parser.ReplaceParameters(m_JPath);
            l_PageSize = m_parser.ReplaceParameters(m_PageSize);
            l_PageNr = m_parser.ReplaceParameters(m_PageNr);
            l_ErrorParamName = m_parser.ReplaceParameters(m_ErrorParamName);

            if(m_isParSource=="1") l_Source =(string ) m_parser.TemplateParams[l_Source ];

            int i_PageSize = 10;
            int i_PageNr = 1;
            int.TryParse(l_PageSize, out i_PageSize);
            int.TryParse(l_PageNr, out i_PageNr);
            int start = (i_PageNr - 1) * i_PageSize;
            int end = i_PageNr * i_PageSize;

            string sError = "";
            string sErrorVerbose = "";
            string val;
            System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

            try
            {
                JArray ja;
                if (l_JPath == "")
                    ja = JArray.Parse(l_Source);
                else
                    ja = (JArray)(JObject.Parse(l_Source)).SelectToken(l_JPath);
                for (int i = start; i < end && i < ja.Count ; i++)
                {
                    JToken j = ja[i];
                    val = j.ToString();
                    m_parser.TemplateParams[l_ParamName] = val;
                    m_parser.TemplateParams[l_ParamName + "__count"] = ja.Count.ToString();
                    m_parser.TemplateParams[l_ParamName + "__fetchID"] = (i - start).ToString();
                    m_parser.TemplateParams[l_ParamName + "__fetchID1"] = (i - start + 1).ToString();
                    m_parser.TemplateParams[l_ParamName + "__oddEven"] = (i - start) % 2 == 1 ? "1" : "0";
                    m_parser.TemplateParams[l_ParamName + "__firstRow"] = (i == start) ? "1" : "0";
                    m_parser.TemplateParams[l_ParamName + "__lastRow"] = ((i == end - 1) || (i == ja.Count - 1)) ? "1" : "0";
                    foreach (JProperty jp in j.Children<JProperty>())
                        if (jp.Value.Type == JTokenType.Date)
                        {
                            DateTime d = (DateTime)jp.Value;
                            m_parser.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        }
                        else
                            m_parser.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value;
                    string output = base.Execute(m_parser);
                    sbRetVal.Append(output);
                    foreach (JProperty jp in j.Children<JProperty>())
                        m_parser.TemplateParams.Remove (l_ParamName + "_" + jp.Name);
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JLOOP command";
                sErrorVerbose = ex.Message;
                sErrorVerbose = "Error executing JLOOP command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
                sbRetVal.Clear(); 
            }

            if (l_ErrorParamName != "")
            {
                m_parser.ParamDefaults[l_ErrorParamName] = sError;
                m_parser.TemplateParams[l_ErrorParamName] = sError;
                m_parser.ParamDefaults[l_ErrorParamName + "Verbose"] = sErrorVerbose;
                m_parser.TemplateParams[l_ErrorParamName + "Verbose"] = sErrorVerbose;
            }
            return sbRetVal.ToString();


            //string sError = "";
            //string sErrorVerbose = "";
            //string sParameters = "";
            //double dDuration = 0;
            //string key;
            ////string retVal = "";
            //System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

            //bool bDefQry = (m_parser.Queries[m_strQueryName] != null);
            ////Array queryParameters = GetParamArray(m_parser, bDefQry);
            //Array queryParameters = GetOriginalParamArray(m_parser);
            //if (m_FromTemplate && queryParameters.Length > 0)
            //{
            //    string sTemplateName = (string)queryParameters.GetValue(0);
            //    sTemplateName = m_parser.ReplaceParameters(sTemplateName);
            //    m_parser.RequestTemplateIDForInclude(sTemplateName);
            //    m_parser.RequestTemplate(m_parser.IDForInclude);
            //    if (m_parser.TextForInclude == null)
            //        throw new Exception("Could not retrieve template content for TemplateID=" + m_parser.IDForInclude.ToString());
            //    string s_text = m_parser.TextForInclude;
            //    m_TemplateContent = s_text;
            //    queryParameters.SetValue(s_text, 0);
            //}

            //try
            //{

            //    m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser.parser_RequestHierarchy);

            //    m_Result = m_parser.Evaluator.EvaluateSourceResult(m_TemplateID, m_strQueryName, queryParameters, out sError, out sErrorVerbose, out sParameters, out dDuration , out key);
            //    m_parser.Context.Keys[this] = key;
            //    m_parser.Context.Results[this] = m_Result;
            //}
            //catch (Exception ex)
            //{
            //    Trace.WriteLine(ex);
            //    sError = ex.Message;
            //    sErrorVerbose = sError;
            //}
            //if (sError != "")
            //{
            //    //sErrorVerbose = "Error executing XPATH command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + sParameters + "; " + newLine + sErrorVerbose;
            //    sErrorVerbose = "Error executing XPATH command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
            //    m_parser.Message("QueryError", sErrorVerbose);

            //    string sErrorParameter = GetErrorParameter(queryParameters);
            //    string l_ParName = m_parser.ReplaceParameters(sErrorParameter);
            //    // set par 
            //    if (l_ParName != "")
            //    {
            //        m_parser.ParamDefaults[l_ParName] = sError;
            //        m_parser.TemplateParams[l_ParName] = sError;
            //        m_parser.ParamDefaults[l_ParName + "Verbose"] = sErrorVerbose;
            //        m_parser.TemplateParams[l_ParName + "Verbose"] = sErrorVerbose;
            //    }

            //}
            //m_resultsCount = 0;
            //if (m_Result != null) m_resultsCount = m_Result.GetCount();
            //if (m_parser.ParserQueryLog == "1") m_parser.QueryLog(m_strQueryName, sParameters, m_resultsCount, sError, sErrorVerbose,dDuration );

            //int pageNumber = -1;
            //int pageSize = -1;
            //int start = 0, end = m_resultsCount;

            //if (m_pageNumberCmd != null && m_pageSizeCmd != null)
            //{
            //    string strPgNum = m_pageNumberCmd.Execute(m_parser);
            //    string strPgSize = m_pageSizeCmd.Execute(m_parser);
            //    if (int.TryParse(strPgNum, out pageNumber))
            //        if (int.TryParse(strPgSize, out pageSize))
            //        {
            //            start = (pageNumber - 1) * pageSize;
            //            end = pageNumber * pageSize;
            //        }
            //}

            //if (start < 0)
            //    start = 0;
            //m_parser.Context.OSetQueryIndex(this, start);
            ////m_context.SetQueryIndex(m_strQueryName, start);

            //if (end == -1)
            //{
            //    if ((queryParameters.GetLength(0) >= 3 ? queryParameters.GetValue(2).ToString() : "") == "1")
            //    {
            //        m_resultsCount = 1; end = 1;
            //    }
            //}
            //for (int i = start; i < end; i++)
            //{
            //    if (i >= m_resultsCount)
            //        break;

            //    string output = base.Execute(m_parser);

            //    //retVal += output;
            //    sbRetVal.Append(output);
            //    m_parser.Context.OIncrementQIndex(this);
            //    //m_context.IncrementQIndex(m_strQueryName);
            //}
            ////return retVal;
            //return sbRetVal.ToString();
            return "";
        }
        //public override string Execute(CParser m_parser)
        //{
        //    string error = "";
        //    string sError = "";
        //    //string retVal = "";
        //    System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

        //    bool bDefQry = (m_parser.Queries[m_strQueryName] != null);
        //    //Array queryParameters = GetParamArray(m_parser, bDefQry);
        //    Array queryParameters = GetOriginalParamArray(m_parser);
        //    if (m_FromTemplate && queryParameters.Length > 0)
        //    {
        //        string sTemplateName = (string)queryParameters.GetValue(0);
        //        sTemplateName = m_parser.ReplaceParameters(sTemplateName);
        //        m_parser.RequestTemplateIDForInclude(sTemplateName);
        //        m_parser.RequestTemplate(m_parser.IDForInclude);
        //        if (m_parser.TextForInclude == null)
        //            throw new Exception("Could not retrieve template content for TemplateID=" + m_parser.IDForInclude.ToString());
        //        string s_text = m_parser.TextForInclude;
        //        m_TemplateContent = s_text;
        //        queryParameters.SetValue(s_text, 0);
        //    }

        //    try
        //    {

        //        m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser.parser_RequestHierarchy);

        //        m_Result = m_parser.Evaluator.EvaluateSourceResult(m_TemplateID, m_strQueryName, queryParameters, out error);
        //        if (error != "") sError = error;
        //    }
        //    catch (Exception ex)
        //    {
        //        Trace.WriteLine(ex);
        //        string parameters = "";
        //        Array queryParams = GetParamArray(m_parser);
        //        if (queryParameters != null && queryParameters.GetLength(0) != 0)
        //        {
        //            parameters = " with parameters (" + newLine;
        //            for (int i = 0; i < queryParameters.Length; i++)
        //            {
        //                parameters += (string)queryParameters.GetValue(i) + newLine;
        //                if (i != queryParameters.Length - 1)
        //                    parameters += ",";
        //            }
        //            parameters += ")";
        //        }

        //        m_parser.Message(1, "Error executing XPATH command at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + parameters + "; " + newLine + ex.Message);
        //        sError = ex.Message;
        //    }
        //    if (sError != "")
        //    {
        //        string sErrorParameter = GetErrorParameter(queryParameters);
        //        string l_ParName = m_parser.ReplaceParameters(sErrorParameter);
        //        // set par 
        //        if (l_ParName != "")
        //        {
        //            m_parser.ParamDefaults[l_ParName] = sError;
        //            m_parser.TemplateParams[l_ParName] = sError;
        //        }

        //    }
        //    m_resultsCount = 0;
        //    if (m_Result != null ) m_resultsCount = m_Result.GetCount();
        //    if (m_parser.ParserQueryLog == "1")
        //    {
        //        string sParameters = "";
        //        Array queryParams = GetParamArray(m_parser);
        //        if (queryParameters != null && queryParameters.GetLength(0) != 0)
        //        {
        //            for (int i = 0; i < queryParameters.Length; i++)
        //                sParameters += (string)queryParameters.GetValue(i) + newLine;
        //        }
        //        m_parser.QueryLog(m_strQueryName, sParameters, m_resultsCount, sError);
        //    }
        //    int pageNumber = -1;
        //    int pageSize = -1;
        //    int start = 0, end = m_resultsCount;

        //    if (m_pageNumberCmd != null && m_pageSizeCmd != null)
        //    {
        //        string strPgNum = m_pageNumberCmd.Execute(m_parser);
        //        string strPgSize = m_pageSizeCmd.Execute(m_parser);
        //        if (int.TryParse(strPgNum, out pageNumber))
        //            if (int.TryParse(strPgSize, out pageSize))
        //            {
        //                start = (pageNumber - 1) * pageSize;
        //                end = pageNumber * pageSize;
        //            }
        //    }

        //    if (start < 0)
        //        start = 0;
        //    m_context.SetQueryIndex(m_strQueryName, start);

        //    if (end == -1)
        //    {
        //        if ((queryParameters.GetLength(0) >= 3 ? queryParameters.GetValue(2).ToString() : "") == "1")
        //        {
        //            m_resultsCount = 1; end = 1;
        //        }
        //    }
        //    for (int i = start; i < end; i++)
        //    {
        //        if (i >= m_resultsCount)
        //            break;

        //        string output = base.Execute(m_parser);

        //        //retVal += output;
        //        sbRetVal.Append(output);
        //        m_context.IncrementQIndex(m_strQueryName);
        //    }
        //    //return retVal;
        //    return sbRetVal.ToString();
        //}

        //public override string Execute(CParser m_parser)
        //{

        //    string error = "";
        //    string retVal = "";
        //    //System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

        //    //string sTemplateName = pp.Value.Trim();
        //    ////sTemplateName = m_parser.ReplaceParameters(sTemplateName);
        //    //RequestTemplateIDForInclude(sTemplateName );
        //    //RequestTemplate(this.IDForInclude);
        //    //if (this.TextForInclude == null)
        //    //    throw new Exception("Could not retrieve template content for TemplateID=" + this.IDForInclude.ToString());
        //    //string s_text = this.TextForInclude;



        //    Array queryParameters = GetOriginalParamArray(m_parser );
        //    if (m_FromTemplate && queryParameters.Length >0 )
        //    {
        //        string sTemplateName = (string) queryParameters.GetValue (0);
        //        sTemplateName = m_parser.ReplaceParameters(sTemplateName);
        //        m_parser.RequestTemplateIDForInclude(sTemplateName);
        //        m_parser.RequestTemplate(m_parser.IDForInclude);
        //        if (m_parser.TextForInclude == null)
        //            throw new Exception("Could not retrieve template content for TemplateID=" + m_parser.IDForInclude.ToString());
        //        string s_text = m_parser.TextForInclude;
        //        m_TemplateContent = s_text;
        //        queryParameters.SetValue (  s_text,0);
        //    }

        //    try
        //    {

        //        //m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_evaluator_RequestHierarchy);
        //        m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser .parser_RequestHierarchy );
        //        //int count = m_parser.Evaluator.EvaluateSource(m_TemplateID, m_strQueryName, queryParameters, out error);
        //        //m_resultsCount = count;

        //        m_Result = m_parser.Evaluator.EvaluateSourceResult(m_TemplateID, m_strQueryName, queryParameters, out error);
        //        m_resultsCount = m_Result.GetCount();

        //        int pageNumber = -1;
        //        int pageSize = -1;
        //        int start = 0, end = m_resultsCount;

        //        if (m_pageNumberCmd != null && m_pageSizeCmd != null)
        //        {
        //            string strPgNum = m_pageNumberCmd.Execute(m_parser);
        //            string strPgSize = m_pageSizeCmd.Execute(m_parser);
        //            if (int.TryParse(strPgNum, out pageNumber))
        //                if (int.TryParse(strPgSize, out pageSize))
        //                {
        //                    start = (pageNumber - 1) * pageSize;
        //                    end = pageNumber * pageSize;
        //                }
        //        }

        //        if (start < 0)
        //            start = 0;
        //        m_context.SetQueryIndex(m_strQueryName, start);

        //        if(end==-1)
        //        {
        //            if ((queryParameters.GetLength(0) >= 3 ? queryParameters.GetValue(2).ToString() : "") == "1")
        //            {
        //                m_resultsCount = 1; end = 1;
        //            }
        //        }
        //        for (int i = start; i < end; i++)
        //        {
        //            if (i >= m_resultsCount)
        //                break;

        //            string output = base.Execute(m_parser);
        //            //if (output.StartsWith(Environment.NewLine))
        //            //    output = output.Substring(Environment.NewLine.Length);
        //            //if (output.EndsWith(Environment.NewLine))
        //            //    output = output.Substring(0, output.Length - Environment.NewLine.Length);

        //            retVal += output;
        //            //sbRetVal.Append(output);
        //            m_context.IncrementQIndex(m_strQueryName);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        retVal = "";
        //        Trace.WriteLine(ex);
        //        string parameters = "";
        //        Array queryParams = GetParamArray(m_parser);
        //        if (queryParameters != null && queryParameters.GetLength(0) != 0)
        //        {
        //            parameters = " with parameters (";
        //            for (int i = 0; i < queryParameters.Length; i++)
        //            {
        //                parameters += (string)queryParameters.GetValue(i);
        //                if (i != queryParameters.Length - 1)
        //                    parameters += ",";
        //            }
        //            parameters += ")";
        //        }

        //        throw new Exception("Error executing XPATH command at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + parameters + "; " + newLine + ex.Message);
        //    }
        //    return retVal;
        //    //return sbRetVal.ToString();

        //}

        //public Array GetParamArray(CParser m_parser)
        //{
        //    Array arrParams = Array.CreateInstance(typeof(string), m_Parameters.GetLength(0));
        //    for (int i = 0; i < m_Parameters.GetLength(0); i++)
        //    {
        //        CCmd cmd = m_Parameters[i];
        //        string strValue = cmd.Execute(m_parser);
        //        if (strValue.ToUpper().Contains("#PAR."))
        //            strValue = m_parser.Parameters2Values(strValue);
        //        arrParams.SetValue(strValue, i);
        //    }
        //    return arrParams;

        //}

//        public Array GetParamArray(CParser m_parser)
//        {
//            return new Array();
////            Array arrParams = Array.CreateInstance(typeof(string), 4);

//            //Array arrParams = Array.CreateInstance(typeof(string), (m_Parameters.GetLength(0) + 1 < 4 ? 4 : m_Parameters.GetLength(0) + 1));
//            //arrParams.SetValue("0", 2);
//            //for (int i = 0; i < m_Parameters.GetLength(0) && i < 3; i++)
//            //{
//            //    if ((i==0) && m_FromTemplate)
//            //        arrParams.SetValue(m_TemplateContent, 0);
//            //    else
//            //    {
//            //        CCmd cmd = m_Parameters[i];
//            //        string strValue = cmd.Execute(m_parser);
//            //        arrParams.SetValue(strValue, i);
//            //    }
//            //}
//            //arrParams.SetValue("Provider=XPATH;", 3);
//            //for (int i = 3; i < m_Parameters.GetLength(0); i++)
//            //{
//            //    ////CCmd cmd = m_Parameters[i];
//            //    ////string strValue = cmd.Execute(m_parser);
//            //    CCmd cmd = m_Parameters[i];
//            //    string strValue = cmd.Execute(m_parser);
//            //    if (strValue.ToUpper().Contains("#PAR."))
//            //        strValue = m_parser.Parameters2Values(strValue);
//            //    arrParams.SetValue(strValue, i + 1);
//            //}

//            //return arrParams;
//        }
//        public Array GetOriginalParamArray(CParser m_parser)
//        {
//            //Array arrParams = Array.CreateInstance(typeof(string), (m_Parameters.GetLength(0) + 1 < 4 ? 4 : m_Parameters.GetLength(0) + 1));
//            //arrParams.SetValue("0", 2);
//            //for (int i = 0; i < m_Parameters.GetLength(0) && i < 3; i++)
//            //{
//            //    CCmd cmd = m_Parameters[i];
//            //    string strValue = cmd.Execute(m_parser);
//            //    arrParams.SetValue(strValue, i);
//            //}
//            //arrParams.SetValue("Provider=XPATH;", 3);

//            //for (int i = 3; i < m_Parameters.GetLength(0) ; i++)
//            //{
//            //    ////CCmd cmd = m_Parameters[i];
//            //    ////string strValue = cmd.Execute(m_parser);
//            //    CCmd cmd = m_Parameters[i];
//            //    string strValue = cmd.Execute(m_parser);
//            //    if (strValue.ToUpper().Contains("#PAR."))
//            //        strValue = m_parser.Parameters2Values(strValue);
//            //    arrParams.SetValue(strValue, i + 1);
//            //}
//            //return arrParams;
//        }

        public SourceResult GetResult(CParser m_parser)
        {
            return (SourceResult)m_parser.Context.Results[this];
            //return m_Result;
        }
        //public Array GetOriginalParamArray_1(CParser m_parser)
        //{
        //    //Array arrParams = Array.CreateInstance(typeof(string), 4);
        //    //arrParams.SetValue("0", 2);
        //    //for (int i = 0; i < m_Parameters.GetLength(0) && i < 3; i++)
        //    //{
        //    //    CCmd cmd = m_Parameters[i];
        //    //    string strValue = cmd.Execute(m_parser);
        //    //    arrParams.SetValue(strValue, i);
        //    //}
        //    //arrParams.SetValue("Provider=XPATH;", 3);
        //    //return arrParams;
        //}

    }
}