using System;
using System.Web;
using System.Text;
using System.Web.UI;
using System.IO;
using System.Net;
using KubionLogNamespace;

namespace XHTMLMerge
{
    [Serializable]
    public class CFILECmd : CCmd
    {
        string m_InType = "";
        string m_InVal = "";
        string m_OutType = "";
        string m_OutVal = "";

        string m_Error = "";
        string m_FileName = "";
        string m_FileExtension = "";
        byte[] m_FileContent;
        string m_FileContentType = "";
        string m_FileID = "";
        [NonSerialized]
        CParser m_parser;

        public string InType
        {
            get { return m_InType; }
            set { m_InType = value; }
        }
        public string InVal
        {
            get { return m_InVal; }
            set { m_InVal = value; }
        }
        public string OutType
        {
            get { return m_OutType; }
            set { m_OutType = value; }
        }
        public string OutVal
        {
            get { return m_OutVal; }
            set { m_OutVal = value; }
        }

        public CFILECmd(): base()
        {
            m_enType = CommandType.FILECommand ;
            this.m_bIsBlockCommand = false;
        }


        public override string Execute(CParser l_parser)
        {
            string l_InType = "";
            string l_InVal = "";
            string l_OutType = "";
            string l_OutVal = "";

            m_parser = l_parser;
            l_InType = m_parser.ReplaceParameters(m_InType).ToUpper ();
            l_InVal = m_parser.ReplaceParameters(m_InVal);

            m_Error = "";
            m_FileID = "";
//            m_parser.SetSV(l_VarName, l_ContextName, l_Val, l_ID);
            switch (l_InType)
            {
                case "UPLOAD":
                    ReadUploadFile(l_InVal);
                    break;
                case "NFS":
                    ReadNFSFile(l_InVal);
                    break;
                case "URI":
                    ReadURIFile(l_InVal);
                    break;
                case "HTTP":
                    ReadHTTPFile(l_InVal);
                    break;
                case "TEMP":
                    ReadNFSFile(HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_InVal);
                    //ReadURIFile(".\\Attachments\\" + l_InVal);
                    break;
                case "DB":
                    ReadDBFile(l_InVal);
                    break;
                case "BLOB":
                    ReadBlobFile(l_InVal);
                    break;
                case "PAR":
                    ReadPARFile(l_InVal);
                    break;
                case "PAR64":
                    ReadPAR64File(l_InVal);
                    break;
                case "DEL":
                    m_FileName = "";
                    m_FileExtension = "";
                    m_FileContent = null;
                    break;
                default:
                    m_Error = "Invalid source type";
                    break;
            }
            if (m_Error == "")
            {
                SetPar("FileName", m_FileName);
                SetPar("FileExtension", m_FileExtension);
                SetPar("FileContentType", m_FileContentType);

                l_OutType = m_parser.ReplaceParameters(m_OutType).ToUpper();
                l_OutVal = m_parser.ReplaceParameters(m_OutVal);
                l_OutVal = Utils.Encode(l_OutVal, EncodeOption.XDOCDecode);

                if (l_OutType == "") { l_OutType = "PAR"; l_OutVal = "File"; }
                switch (l_OutType)
                {
                    case "RESPONSE":
                        string sInline = "1";
                        sInline = GetPar("FileResponseInline");
                        if (sInline != "0") sInline = "1";
                        WriteResponseFile(l_OutVal, sInline );
                        break;
                    case "NFS":
                        WriteNFSFile(l_OutVal);
                        break;
                    case "URI":
                        WriteURIFile(l_OutVal);
                        break;
                    case "HTTP":
                        WriteHTTPFile(l_OutVal);
                        break;
                    case "TEMP":
                        WriteNFSFile(HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_OutVal);
                        SetPar("FilePath", HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_OutVal);
                        break;
                    case "DB":
                        WriteDBFile(l_OutVal);
                        break;
                    case "BLOB":
                        WriteBlobFile(l_OutVal);
                        break;
                    case "PAR":
                        WritePARFile(l_OutVal);
                        break;
                    case "PAR64":
                        WritePAR64File(l_OutVal);
                        break;
                    default:
                        m_Error = "Invalid target type";
                        break;
                }
                SetPar("FileID", m_FileID);
                SetPar("FileError", "");
            }

            if (m_Error != "")
            {
                SetPar("FileID", "-1");
                SetPar("FileName", m_FileName);
                SetPar("FileExtension", m_FileExtension);
                SetPar("FileContentType", m_FileContentType );
                SetPar("FileError", m_Error);
            }
            else
                SetPar("FileError", "");

            return "";
        }
        private void ReadUploadFile(string sIndex)
        {
            try
            {
                if (sIndex == "") sIndex = "0";
                int iIndex = Convert.ToInt16(sIndex);
                HttpContext m_HttpContext = (HttpContext)m_parser.Manager.myHttpContext;
                HttpPostedFile objHttpPostedFile = m_HttpContext.Request.Files[iIndex];
                if (objHttpPostedFile == null)
                    m_Error = "Error reading file upload (" + sIndex + "): " + " not found";
                else
                {
                    string strFullFileName = objHttpPostedFile.FileName;
                    m_FileContentType = objHttpPostedFile.ContentType;
                    int intLength = objHttpPostedFile.ContentLength;
                    m_FileName = System.IO.Path.GetFileName(strFullFileName); /* only the attched file name not its path */
                    m_FileExtension  = System.IO.Path.GetExtension(strFullFileName).ToLower();
                    m_FileContent = new byte[intLength];
                    objHttpPostedFile.InputStream.Read(m_FileContent, 0, intLength);
                    SetPar("UploadFileName", m_FileName);
                    SetPar("UploadFileExtension", m_FileExtension);
                    SetPar("UploadFileContentType", m_FileContentType);

                    objHttpPostedFile = null;
                }
            }
            catch (Exception err)
            {
                m_Error = "Error reading file upload (" + sIndex + "): " + err.Message;
            }

        }
        private void ReadNFSFile(string sFile)
        {
            int iMaxAttempts = 3;
            try
            {
				//StreamReader sr = File.OpenText(fileFullPath);
				//output.Write(sr.ReadToEnd());
				//sr.Close();

                m_FileName = System.IO.Path.GetFileName(sFile); 
                m_FileExtension = System.IO.Path.GetExtension(sFile).ToLower();
                m_FileContentType = "";
                int iAttempt = 1;
				while (m_FileContent == null)
				{
					try
					{
						m_FileContent = File.ReadAllBytes(sFile);
					}
					catch (Exception ex)
					{
						if (iAttempt >= iMaxAttempts) throw ;
						else iAttempt++;
					}
				}

            }
            catch (Exception err)
            {
                m_Error = "Error reading file nfs (" + sFile + "): " + err.Message;
            }

        }
        private void ReadURIFile(string sURI)
        {
            int iMaxAttempts = 3;
            try
            {
                WebClient client = new WebClient();

                m_FileName = System.IO.Path.GetFileName(sURI);
                m_FileExtension = System.IO.Path.GetExtension(sURI).ToLower();
                m_FileContentType = "";
                int iAttempt = 1;
                while (m_FileContent == null)
                {
                    try
                    {
                        m_FileContent = client.DownloadData(sURI);
                    }
                    catch (Exception ex)
                    {
                        if (iAttempt >= iMaxAttempts) throw ;
                        else iAttempt++;
                    }
                }
            }
            catch (Exception err)
            {
                m_Error = "Error reading file uri (" + sURI + "): " + err.Message;
            }

        }
        private void ReadHTTPFile(string sURI)
        {
            int iMaxAttempts = 3;
            try
            {
                string strUserName = GetPar("FileUserName");
                string strPassword = GetPar("FileUserPassword");
                string strDomain = GetPar("FileUserDomain");
                string strProxyUserName = GetPar("FileProxyUserName");
                string strProxyPassword = GetPar("FileProxyUserPassword");
                string strProxyDomain = GetPar("FileProxyUserDomain");
                string strProxyUrl = GetPar("FileProxyUrl");

                m_FileName = System.IO.Path.GetFileName(sURI);
                m_FileExtension = System.IO.Path.GetExtension(sURI).ToLower();
                m_FileContentType = "";

                int iAttempt = 1;
                while (m_FileContent == null)
                {
                    try
                    {
                        System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURI);
                        request.Method = "GET";

                        if ((strUserName == "" || strUserName == null) && (strPassword == "" || strPassword == null) && (strDomain == "" || strDomain == null))
                        {
                            /* Set the credentials to be like impersonate */
                            request.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                        else
                        {
                            request.Credentials = new System.Net.NetworkCredential(strUserName, strPassword, strDomain);
                        }

                        if (strProxyUrl != "" && strProxyUrl != null)
                        {

                            WebProxy proxy = new WebProxy();
                            proxy.Address = new Uri(strProxyUrl);
                            if (strProxyUserName != "" && strProxyUserName != null)
                            {
                                proxy.Credentials = new NetworkCredential(strProxyUserName, (strProxyPassword == null ? "" : strProxyPassword), (strProxyDomain == null ? "" : strProxyDomain));
                            }
                            request.Proxy = proxy;
                        }
                        WebResponse response = null;
                        try { response = request.GetResponse(); }
                        catch (WebException webex)
                        {
                            String data = String.Empty;
                            if (webex.Response != null)
                            {
                                StreamReader r = new StreamReader(webex.Response.GetResponseStream());
                                data = r.ReadToEnd();
                                r.Close();
                            }
                            throw new Exception(webex.Message + "\r\n" + data);
                        }
                        int intLength = (int)response.ContentLength;
                        m_FileContent = new byte[intLength];
                        response.GetResponseStream ().Read(m_FileContent, 0, intLength);

                    }
                    catch (Exception ex)
                    {
                        if (iAttempt >= iMaxAttempts) throw ;
                        else iAttempt++;
                    }
                }
            }
            catch (Exception err)
            {
                m_Error = "Error reading file http (" + sURI + "): " + err.Message;
            }
        }
        private void ReadDBFile(string sKey)
        {
            try
            {
                m_parser.ReadSFile(sKey, out m_FileName, out m_FileExtension, out m_FileContent, out m_FileContentType);
            }
            catch (Exception err)
            {
                m_Error = "Error reading file db (" + sKey + "): " + err.Message;
            }
        }
        private void ReadBlobFile(string sKey)
        {
            try
            {
                m_parser.ReadBlobFile (sKey, out m_FileName, out m_FileExtension, out m_FileContent, out m_FileContentType);
            }
            catch (Exception err)
            {
                m_Error = "Error reading file blob (" + sKey + "): " + err.Message;
            }
        }
        private void ReadPARFile(string sPar)
        {
            try
            {
                m_FileName = GetPar(sPar + "FileName");
                m_FileExtension = GetPar(sPar + "FileExtension");
                string sContent = GetPar(sPar);
                m_FileContent = Encoding.UTF8.GetBytes(sContent );
            }
            catch (Exception err)
            {
                m_Error = "Error reading file par (" + sPar + "): " + err.Message;
            }
        }
        private void ReadPAR64File(string sPar)
        {
            try
            {
                m_FileName = GetPar(sPar + "FileName");
                m_FileExtension = GetPar(sPar + "FileExtension");
                string sContent = GetPar(sPar);
                //                m_FileContent = Encoding.UTF8.GetBytes(sContent );
                m_FileContent = Convert.FromBase64String(sContent);
            }
            catch (Exception err)
            {
                m_Error = "Error reading file par (" + sPar + "): " + err.Message;
            }
        }


        private void WriteResponseFile(string sContentType,string sInline)
        {
            try
            {
                m_FileID = m_FileName;
                HttpContext m_HttpContext = (HttpContext)m_parser.Manager.myHttpContext;
                /* Clear Response buffer, set type and write data into Output stream */
                m_HttpContext.Response.Clear();
                m_HttpContext.Response.ContentType = sContentType ;

                /*
                 * Abount disposition:
                 * type = attachment -> let the user save
                 * type = inline -> try to show the content in the browser
                 */

                if (sInline == "1")
                {
                    /* Setting the filename with type=inline only makes sense when the browser falls back to type attachment (when it cannot show the file inline) */
                    m_HttpContext.Response.AppendHeader("Content-Disposition", "inline; filename=" + m_FileName );
                }
                else
                {
                    m_HttpContext.Response.AppendHeader("Content-Disposition", "attachment; filename=" + m_FileName );
                }

                m_HttpContext.Response.OutputStream.Write(m_FileContent , 0, m_FileContent.Length  );
                m_HttpContext.Response.OutputStream.Flush ();
                m_HttpContext.ApplicationInstance.CompleteRequest();
                //m_HttpContext.Response.End();
            }
            catch (Exception err)
            {
                m_Error = "Error writing file response (" + sContentType + "): " + err.Message;
            }
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        private void WriteNFSFile(string sFile)
        {
            FileStream fs = null;
            BinaryWriter bw = null;
            try
            {
                //if (m_FileContent == null)
                //{
                    if (File.Exists(sFile))
                    {
                        File.Delete(sFile);
                    }
                //}
                //else
                //{
                    string sTempFile = Path.GetTempFileName();

                    fs = File.Open(sTempFile, FileMode.OpenOrCreate);
                    bw = new BinaryWriter(fs);

                    bw.Write(m_FileContent);
                    bw.Close();
                    fs.Close();
                    File.Move(sTempFile, sFile);
                //}
                    m_FileID = m_FileName;
            }
            catch (Exception err)
            {
                m_Error = "Error writing file nfs (" + sFile + "): " + err.Message;
            }
            finally
            {
                if (bw != null)
                    bw.Close();
                if (fs != null)
                    fs.Close();
            }

        }
        private void WriteURIFile(string sURI)
        {
            try
            {
                WebClient client = new WebClient();
                client.UploadData (sURI,"POST",m_FileContent);
                m_FileID = "";
            }
            catch (Exception err)
            {
                m_Error = "Error writing file uri (" + sURI + "): " + err.Message;
            }
        }
        private void WriteHTTPFile(string sURI)
        {
            try
            {
                m_FileID = "";
                string strUserName = GetPar("FileUserName");
                string strPassword = GetPar("FileUserPassword");
                string strDomain = GetPar("FileUserDomain");
                string strProxyUserName = GetPar("FileProxyUserName");
                string strProxyPassword = GetPar("FileProxyUserPassword");
                string strProxyDomain = GetPar("FileProxyUserDomain");
                string strProxyUrl = GetPar("FileProxyUrl");

                string strContentType = GetPar("FileContentType");
                if (strContentType == "") strContentType = "application/octet-stream";
                if (m_FileContentType != "") strContentType = m_FileContentType;

                System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURI);
                if ((strUserName == "" || strUserName == null) && (strPassword == "" || strPassword == null) && (strDomain == "" || strDomain == null))
                    request.Credentials = System.Net.CredentialCache.DefaultCredentials;
                else
                    request.Credentials = new System.Net.NetworkCredential(strUserName, strPassword, strDomain);

                if (strProxyUrl != "" && strProxyUrl != null)
                {
                    WebProxy proxy = new WebProxy();
                    proxy.Address = new Uri(strProxyUrl);
                    if (strProxyUserName != "" && strProxyUserName != null)
                    {
                        proxy.Credentials = new NetworkCredential(strProxyUserName, (strProxyPassword == null ? "" : strProxyPassword), (strProxyDomain == null ? "" : strProxyDomain));
                    }
                    request.Proxy = proxy;
                }
                Encoding encoding = Encoding.UTF8 ;
                //string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
                string boundary = String.Format("----------{0:N}", Guid.NewGuid());

                Stream formDataStream = new System.IO.MemoryStream();
                formDataStream.Write(encoding.GetBytes("\r\n"), 0, encoding.GetByteCount("\r\n"));
                string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\";\r\nContent-Type: {3}\r\n\r\n",boundary,m_FileName,m_FileName,strContentType);
                formDataStream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));
                formDataStream.Write(m_FileContent, 0, m_FileContent.Length);
                string footer = "\r\n--" + boundary + "--\r\n";
                formDataStream.Write(encoding.GetBytes(footer), 0, encoding.GetByteCount(footer));

                formDataStream.Position = 0;
                byte[] formData = new byte[formDataStream.Length];
                formDataStream.Read(formData, 0, formData.Length);
                formDataStream.Close();

                request.Method = "POST";
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                request.KeepAlive = true;
                request.ContentLength = formData.Length;

                //byte[] buffer = Encoding.ASCII.GetBytes(boundary + Environment.NewLine);
                //requestStream.Write(buffer, 0, buffer.Length);
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(formData, 0, formData.Length);
                requestStream.Close();
                requestStream = null;

                WebResponse response = null;
                try
                {
                    response = request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    StreamReader sr = new StreamReader(responseStream);
                    m_FileID = sr.ReadToEnd();
                }
                catch (WebException webex)
                {
                    String data = String.Empty;
                    if (webex.Response != null)
                    {
                        StreamReader r = new StreamReader(webex.Response.GetResponseStream());
                        data = r.ReadToEnd();
                        r.Close();
                    }
                    throw new Exception(webex.Message + "\r\n" + data);
                }
                finally
                {
                    if (response != null)
                    {
                        response.Close();
                        response = null;
                    }
                    response = null;
                }
            }
            catch (Exception err)
            {
                m_Error = "Error writing file http (" + sURI + "): " + err.Message;
            }
            //m_FileName = System.IO.Path.GetFileName(sURI);
            //m_FileExtension = System.IO.Path.GetExtension(sURI).ToLower();
        }
        private void WriteDBFile(string sKey)
        {
            try
            {
                m_FileID = m_parser.WriteSFile(sKey, m_FileName, m_FileExtension, m_FileContent, m_FileContentType);
            }
            catch (Exception err)
            {
                m_Error = "Error writing file db (" + sKey + "): " + err.Message;
            }
        }
        private void WriteBlobFile(string sKey)
        {
            try
            {
                m_FileID = m_parser.WriteBlobFile(sKey, m_FileName, m_FileExtension, m_FileContent, m_FileContentType);
            }
            catch (Exception err)
            {
                m_Error = "Error writing file blob (" + sKey + "): " + err.Message;
            }
        }
        private void WritePARFile(string sPar)
        {
            try
            {
                string sOutVal = Encoding.UTF8.GetString(m_FileContent);
                SetPar(sPar, sOutVal);
                SetPar(sPar + "FileName", m_FileName);
                SetPar(sPar + "FileExtension", m_FileExtension);
                m_FileID = m_FileName;
            }
            catch (Exception err)
            {
                m_Error = "Error writing file par (" + sPar + "): " + err.Message;
            }
        }
        private void WritePAR64File(string sPar)
        {
            try
            {
                //string sOutVal = Encoding.UTF8.GetString(m_FileContent);
                string sOutVal = Convert.ToBase64String(m_FileContent, 0, m_FileContent.Length);
                SetPar(sPar, sOutVal);
                SetPar(sPar + "FileName", m_FileName);
                SetPar(sPar + "FileExtension", m_FileExtension);
                m_FileID = m_FileName;
            }
            catch (Exception err)
            {
                m_Error = "Error writing file par (" + sPar + "): " + err.Message;
            }
        }

        private string GetPar(string l_ParName)
        {
            string retVal = "";
             retVal = m_parser.GetParameterValue(l_ParName);
            return retVal;
        }
        private void SetPar(string l_ParName,string l_ParValue)
        {
            m_parser.ParamDefaults[l_ParName] = l_ParValue;
            m_parser.TemplateParams[l_ParName] = l_ParValue;
        }
        private string GetResponse(string strUrl, string strUserName, string strPassword, string strDomain, string strProxyUrl, string strProxyUserName, string strProxyPassword, string strProxyDomain, string strMethod, string strXml, string strContentType)
        {
            /* Post the XML */
            System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strUrl);
            request.Method = strMethod;

            if ((strUserName == "" || strUserName == null) && (strPassword == "" || strPassword == null) && (strDomain == "" || strDomain == null))
            {
                /* Set the credentials to be like impersonate */
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }
            else
            {
                request.Credentials = new System.Net.NetworkCredential(strUserName, strPassword, strDomain);
            }

            if (strProxyUrl != "" && strProxyUrl != null)
            {

                WebProxy proxy = new WebProxy();
                proxy.Address = new Uri(strProxyUrl);
                if (strProxyUserName != "" && strProxyUserName != null)
                {
                    proxy.Credentials = new NetworkCredential(strProxyUserName, (strProxyPassword == null ? "" : strProxyPassword), (strProxyDomain == null ? "" : strProxyDomain));
                }
                request.Proxy = proxy;
            }

            /* Send the XML content */
            if (strXml != "")
            {
                string sEncoding = null;
                Encoding objEncoding = null;

                if (strContentType.IndexOf("charset=") != -1)
                {
                    int intStart = strContentType.IndexOf("charset=") + 8;
                    int intEnd = (strContentType + ";").IndexOf(";", intStart);
                    if (intEnd != -1) sEncoding = strContentType.Substring(intStart, intEnd - intStart);
                    try { objEncoding = Encoding.GetEncoding(sEncoding); }
                    catch (Exception e) { sEncoding = null; }
                }

                if (objEncoding == null)
                {
                    sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
                    if (sEncoding == null || sEncoding == "") sEncoding = "utf-8";
                    try { objEncoding = Encoding.GetEncoding(sEncoding); }
                    catch (Exception e) { sEncoding = null; }
                }
                byte[] postData = (objEncoding == null ? Encoding.Default.GetBytes(strXml) : objEncoding.GetBytes(strXml));

                request.ContentType = strContentType;
                request.ContentLength = postData.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(postData, 0, postData.Length);
                requestStream.Close();
            }

            /* Read the response XML */
            WebResponse response = null;
            try { response = request.GetResponse(); }
            catch (WebException webex)
            {
                /* Need some special attention to get the error message from the response */
                String data = String.Empty;
                if (webex.Response != null)
                {
                    StreamReader r = new StreamReader(webex.Response.GetResponseStream());
                    data = r.ReadToEnd();
                    r.Close();
                }
                throw new Exception(webex.Message + "\r\n" + data + "\r\nURL (decoded): " + strUrl + "\r\nPosted data (decoded): " + System.Web.HttpUtility.UrlDecode(strXml, Encoding.Default));
            }
            Stream responseStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(responseStream);
            return sr.ReadToEnd();
        }

        //private byte[] GetEncodedBytes()
        //{
        //    Encoding enc = null;
        //    if (Content[0] == 0xef && Content[1] == 0xbb && Content[2] == 0xbf)
        //        enc = new UTF8Encoding();
        //    else if (Content[0] == 0xff && Content[1] == 0xfe)
        //        enc = new UnicodeEncoding();
        //    else if (Content[0] == 0 && Content[1] == 0 && Content[2] == 0xfe && Content[3] == 0xff)
        //        enc = new UTF32Encoding();
        //    else if (this.DefaultEncoding.ToLower() == "utf7")
        //        enc = new UTF7Encoding();
        //    else if (this.DefaultEncoding.ToLower() == "utf8")
        //        enc = new UTF8Encoding();
        //    else if (this.DefaultEncoding.ToLower() == "unicode")
        //        enc = new UnicodeEncoding();
        //    else if (this.DefaultEncoding.ToLower() == "utf32")
        //        enc = new UTF32Encoding();
        //    else
        //        enc = new ASCIIEncoding();

        //    if (enc != null)
        //        return enc.GetBytes(Content);
        //    else
        //        return null;
        //}
    }
}

