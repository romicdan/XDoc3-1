using System;

namespace XHTMLMerge
{
    [Serializable]
    public class CDEFQRYCmd: CCmd
    {
        protected string
            m_strQueryName = "",
            m_strQueryPar = "";

        public string QueryName
        {
            get { return m_strQueryName; }
            set { m_strQueryName = value; }
        }
        public string QueryPar
        {
            get { return m_strQueryPar; }
            set { m_strQueryPar = value; }
        }

        public CDEFQRYCmd(): base()
		{
			m_enType = CommandType.DEFQRYCommand;
			this.m_bIsBlockCommand = false;
		}

        public override string Execute(CParser m_parser)
        {
            string l_strQueryName, l_strQueryPar;
            l_strQueryName = m_parser.ReplaceParameters(m_strQueryName);
            l_strQueryPar = m_parser.ReplaceParameters(m_strQueryPar);

            m_parser.Queries[l_strQueryName] = l_strQueryPar;

			return "";
		}


    }
}


