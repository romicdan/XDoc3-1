using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using KubionLogNamespace;

namespace XHTMLMerge
{
    [Serializable]
    public class CIncludeCmd : CCmd
    {

        protected CmdCollection m_paramCmds = null;

        string m_ParName = "";
        string m_udfTemplateName = "";
        bool m_isUDF = false;
        bool m_IgnoreMissing = false;
        bool m_NoParse = false;

        const int MAX_INCLUDES = 150;

        public bool IsUDF
        {
            get { return m_isUDF; }
            set { m_isUDF = value; }
        }
        public bool IgnoreMissing
        {
            get { return m_IgnoreMissing; }
            set { m_IgnoreMissing = value; }
        }

        public bool NoParse
        {
            get { return m_NoParse; }
            set { m_NoParse = value; }
        }

        public CmdCollection ParamCmds
        {
            get { return m_paramCmds; }
            set { m_paramCmds = value; }
        }

        public string UDFTemplateName
        {
            get { return m_udfTemplateName; }
            set { m_udfTemplateName = value; }
        }
        public string ParName
        {
            get { return m_ParName; }
            set { m_ParName = value; }
        }


        public CIncludeCmd()
            : base()
        {
            m_bIsBlockCommand = false;
            m_enType = CommandType.IncludeCommand;
            m_paramCmds = new CmdCollection();
        }

        public override string Execute(CParser m_parser)
        {
            string templateIDstr = "[unknown]";
            try
            {
                string parameterList = "";
                foreach (CCmd cmd in m_paramCmds)
                    parameterList += cmd.Execute(m_parser);

                char sep = '&';
                if (parameterList.IndexOf(sep) == -1)
                    sep = ';';

                Hashtable hashParams = CollectionsUtil.CreateCaseInsensitiveHashtable();

                if (m_isUDF)
                {
                    hashParams["TemplateName"] = m_udfTemplateName;
                    string[] parameters = parameterList.Split(sep);
                    for (int i = 0; i < parameters.Length; i++)
                        hashParams[string.Format("P{0}", i + 1)] = parameters[i].Trim();

                }
                else
                {
                    string[] parameters = parameterList.Split(sep);
                    foreach (string parameter in parameters)
                    {
                        string[] parts = parameter.Split('=');
                        string paramName = parts[0];
                        string paramValue = parts[1];
                        if (paramValue.EndsWith("\r\n")) paramValue = paramValue.Substring(0, paramValue.Length - 2);

                        if (hashParams.Contains(paramName))
                            throw new Exception("Parameter '" + paramName + "' is specified more than once;");
                        hashParams[paramName] = Utils.XDocUrlDecode(paramValue);
                    }
                }

                m_parser.PreviousTemplateIDs.Add(m_parser.TemplateID);

                Hashtable logIncludeParams = hashParams.Clone() as Hashtable;

                if (hashParams.Contains("Ignore"))
                    if (hashParams["Ignore"].ToString() == "1")
                        IgnoreMissing = true;
                if (hashParams.Contains("noparse"))
                    if (hashParams["noparse"].ToString() == "1")
                        NoParse = true;


                if (hashParams.Contains("templateid"))
                {
                    int idForInclude;
                    if (!int.TryParse(hashParams["templateid"].ToString(), out idForInclude))
                    {
                        throw new Exception("Parameter TemplateID is incorrectly specified : '" + hashParams["templateid"].ToString() + "';");
                    }
                    else
                    {
                        m_parser.IDForInclude = idForInclude;
                        hashParams.Remove("templateid");
                    }
                }
                else if (hashParams.Contains("templatename"))
                {
                    string templateName = hashParams["templatename"].ToString();
                    hashParams.Remove("templatename");
                    //templateName = m_parser.ReplaceParameters(templateName);
                    try
                    {

                        m_parser.RequestTemplateIDForInclude(templateName);

                    }
                    catch (Exception)
                    {
                        if (IgnoreMissing)
                        {
                            string sError = "Template " + templateName + " not found";
                            m_parser.TemplateParams["ERROR"] = sError;
                            string sErrorVerbose = "Error executing INCLUDE command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sError;
                            m_parser.TemplateParams["ERRORVERBOSE"] = sErrorVerbose;
                            return "";
                        }
                        else
                            throw new Exception("Could not retrieve template content for TemplateName=" + templateName);
                    }
                }

                if (m_parser.IDForInclude != -1)
                {
                    templateIDstr = m_parser.IDForInclude.ToString();

                    if (m_parser.PreviousTemplateIDs.Count > MAX_INCLUDES)
                    {
                        string message = "Template inclusions reached the maximum number of " + MAX_INCLUDES.ToString();
                        throw new Exception(message);
                    }
                }
                else
                    throw new Exception("Parameter TemplateID or TemplateName is missing or is not correctly specified");

                try
                {

                    m_parser.RequestTemplate(m_parser.IDForInclude);

                }
                catch (Exception)
                {
                    if (IgnoreMissing)
                    {
                        string sError = "Template " + m_parser.IDForInclude.ToString () + " not found";
                        m_parser.TemplateParams["ERROR"] = sError;
                        string sErrorVerbose = "Error executing INCLUDE command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sError;
                        m_parser.TemplateParams["ERRORVERBOSE"] = sErrorVerbose;
                        return "";
                    }
                    else
                        throw new Exception("Could not retrieve template content for TemplateID=" + m_parser.IDForInclude.ToString());
                }

                if (m_parser.AdditionalIncludeParams != null)
                    foreach (DictionaryEntry de in m_parser.AdditionalIncludeParams)
                        hashParams[de.Key] = de.Value;
                hashParams["INCLUDE"] = 1;
                string sSessionID = "";
                object retVal = null;
                m_parser.Evaluator.GetValue("SessionID", out retVal);
                if (retVal != null) sSessionID = retVal.ToString();
                hashParams["SESSIONID"] = sSessionID;

                string returnVal;
                if (!NoParse)
                {
                    //CParser parser = new CParser(m_parser.Manager, m_parser.IDForInclude, m_parser.DataProvider, m_parser.TextForInclude, hashParams, m_parser.VirtualPath, m_parser.OutputPath);
                    CParser parser = new CParser(m_parser.Manager, m_parser.IDForInclude, m_parser.Evaluator, m_parser.TextForInclude, hashParams, m_parser.VirtualPath, m_parser.OutputPath);
                    parser.GlobalParams = m_parser.GlobalParams;
                    parser.PreviousTemplateIDs = m_parser.PreviousTemplateIDs;
                    parser.RequestTemplateText += new CParser.RequestTemplateText_Handler(m_parser.parser_RequestTemplateText);
                    parser.RequestTemplateID += new CParser.RequestTemplateID_Handler(m_parser.parser_RequestTemplateID);
                    //                parser.LogInclude += new CParser.LogInclude_Handler(parser_LogInclude);
                    parser.LogInclude += new CParser.LogInclude_Handler(m_parser.parser_LogInclude);
                    parser.IncludeLogIndentation = m_parser.IncludeLogIndentation + 1;
                    //parser.SessionVariables = m_parser.SessionVariables; 

                    //LogInclude
                    m_parser.RequestLogInclude(logIncludeParams, m_parser.IncludeLogIndentation);

                    returnVal = parser.Parse();
                }
                else returnVal = m_parser.TextForInclude;

                string l_ParName;
                l_ParName = m_parser.ReplaceParameters(m_ParName);
                if (l_ParName == "")
                    return returnVal;
                else
                {
                    m_parser.ParamDefaults[l_ParName] = returnVal;
                    m_parser.TemplateParams[l_ParName] = returnVal;
                    return "";
                }
            }
            catch (Exception ex)
            {
                string commandName = "INCLUDE";
                if (m_isUDF)
                    commandName = "UDF";
                string message = "Error executing " + commandName + " comand at line " + m_parser.GetLine(this.StartIndex).ToString() + " for TemplateID = " + templateIDstr + ";" + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
            finally
            {
                if (m_parser.PreviousTemplateIDs.Count > 0 && 
                    Convert.ToInt32(m_parser.PreviousTemplateIDs[m_parser.PreviousTemplateIDs.Count -1]) == m_parser.TemplateID)
//                    m_parser.PreviousTemplateIDs.Remove(m_parser.TemplateID);
//                        m_parser.PreviousTemplateIDs.Remove(m_parser.PreviousTemplateIDs[m_parser.PreviousTemplateIDs.Count - 1]);
                    m_parser.PreviousTemplateIDs.RemoveAt(m_parser.PreviousTemplateIDs.Count - 1);
            }
        }

        //void parser_LogInclude(CParser sender, LogIncludeEventArgs e)
        //{
        //    l_parser.RequestLogInclude(e.IncludeParams, e.Indentation);
        //}

        //void parser_RequestTemplateID(CParser sender, string templateName)
        //{
        //    l_parser.RequestTemplateIDForInclude(templateName);
        //    sender.IDForInclude = l_parser.IDForInclude;
        //}

        //void parser_RequestTemplateText(CParser sender, int templateID)
        //{
        //    l_parser.RequestTemplate(templateID);
        //    sender.TextForInclude = l_parser.TextForInclude;
        //    if (l_parser.AdditionalIncludeParams != null)
        //        foreach (DictionaryEntry de in l_parser.AdditionalIncludeParams)
        //            sender.AdditionalIncludeParams[de.Key] = de.Value;
        //}
    }
}
