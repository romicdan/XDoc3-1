using KubionLogNamespace;
using System;
using System.Globalization;
using XDataSourceModule;

namespace XHTMLMerge
{
    [Serializable]
    public class CSETFLDCmd : CCmd
    {
        bool m_OnlyNull = false;
        bool m_Append = false;
        string m_AppendChar = "";
        string m_strQueryName = "";
        string m_strFieldName = "";
        string m_VarName = "";
        string m_ContextName = "";
        string m_ID = "ID";
        string m_Format = "";
        string m_EncodeOption = "";

        //protected CContext m_context = null;
        //protected XDataSourceModule.IXDataSource m_evaluator = null;
        protected Array m_parameters = null;
        protected CCmd m_parent = null;
        protected bool m_isFetchID = false;
        protected bool m_isFetchID1 = false;
        protected bool m_oddEven = false;
        protected bool m_isCount = false;
        protected bool m_isFirstRow = false;
        protected bool m_isLastRow = false;

        public string QueryName
        {
            get { return m_strQueryName; }
            set { m_strQueryName = value; }
        }
        public string FieldName
        {
            get { return m_strFieldName; }
            set { m_strFieldName = value; }
        }
        public bool OnlyNull
        {
            get { return m_OnlyNull; }
            set { m_OnlyNull = value; }
        }
        public bool Append
        {
            get { return m_Append; }
            set { m_Append = value; }
        }
        public string AppendChar
        {
            get { return m_AppendChar; }
            set { m_AppendChar = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }
        public string ContextName
        {
            get { return m_ContextName; }
            set { m_ContextName = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string Format
        {
            get { return m_Format; }
            set { m_Format = value; }
        }
        public string EncodeOption
        {
            get { return m_EncodeOption; }
            set { m_EncodeOption = value; }
        }

        public bool IsCount
        {
            get { return m_isCount; }
            set { m_isCount = value; }
        }
        public bool IsFirstRow
        {
            get { return m_isFirstRow; }
            set { m_isFirstRow = value; }
        }
        public bool IsLastRow
        {
            get { return m_isLastRow; }
            set { m_isLastRow = value; }
        }

        public bool OddEven
        {
            get { return m_oddEven; }
            set { m_oddEven = value; }
        }
        public bool IsFetchID
        {
            get { return m_isFetchID; }
            set { m_isFetchID = value; }
        }
        public bool IsFetchID1
        {
            get { return m_isFetchID1; }
            set { m_isFetchID1 = value; }
        }
        //public CContext Context
        //{
        //    get { return m_context; }
        //    set { m_context = value; }
        //}

        //public XDataSourceModule.IXDataSource Evaluator
        //{
        //    get { return m_evaluator; }
        //    set { m_evaluator = value; }
        //}

        public Array Parameters
        {
            get { return m_parameters; }
            set { m_parameters = value; }
        }

        public CCmd Parent
        {
            get { return m_parent; }
            set { m_parent = value; }
        }

        public CSETFLDCmd()
            : base()
        {
            m_enType = CommandType.SETFLDCommand;
            this.m_bIsBlockCommand = false;
        }


        public override string Execute(CParser m_parser)
        {
            string l_VarName, l_ContextName, l_ID, l_strFieldName, l_Val, l_EncodeOption, l_Format;
            //cannot get in runtime queryname: m_strQueryName = m_parser.ReplaceParameters(m_strQueryName);
            l_strFieldName = m_parser.ReplaceParameters(m_strFieldName);

            SourceResult parentResult = null;
            int queryIndex = m_parser.Context.OGetQueryIndex(m_parent);
            //int queryIndex = m_context.GetQueryIndex(m_strQueryName);
			object oVal = "";
			string retVal = "";
            try
            {
                int parentResultsCount = -1;
                if (m_parent is CREPCmd)
                {
                    parentResult = ((CREPCmd)m_parent).GetResult(m_parser);
                    parentResultsCount = ((CREPCmd)m_parent).ResultsCount;
                }
                else if (m_parent is CXPATHCmd)
                {
                    parentResult = ((CXPATHCmd)m_parent).GetResult(m_parser);
                    parentResultsCount = ((CXPATHCmd)m_parent).ResultsCount;
                }
                else if (m_parent is CQRYCmd)
                {
                    parentResult = ((CQRYCmd)m_parent).GetResult(m_parser);
                    parentResultsCount = ((CQRYCmd)m_parent).ResultsCount;
                }

                string error = "";



                if (m_isFetchID)
                    retVal = queryIndex.ToString();
                else if (m_isFetchID1)
                    retVal = (queryIndex + 1).ToString();
                else if (m_isFirstRow)
                    retVal = ((queryIndex == 0) && (parentResultsCount >0)? 1 : 0).ToString();
                else if (m_isLastRow)
                    retVal = ((queryIndex == parentResultsCount -1)?1:0).ToString();
                else if (m_oddEven)
                    retVal = (queryIndex % 2).ToString();
                else if (m_isCount)
                {
                    int retResCount = parentResultsCount;
                    retVal = retResCount.ToString();
                }
                else
                {
                    if (parentResult != null)
                    if (parentResult.GetFieldValue(l_strFieldName, queryIndex, out oVal))
                        retVal = oVal.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error executing FLD command for query " + m_strQueryName + " and field " 
                    + m_strFieldName + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + ex.Message);
            }

            l_VarName = m_parser.ReplaceParameters(m_VarName);
            l_ContextName = m_parser.ReplaceParameters(m_ContextName);
            l_ID = m_parser.ReplaceParameters(m_ID);
            l_EncodeOption = m_parser.ReplaceParameters(m_EncodeOption);
            l_Format = m_parser.ReplaceParameters(m_Format);

            retVal = m_parser.ApplyFormat(retVal, l_Format);

            //if (l_Format != "")
            //{

            //    if (l_Format.StartsWith("@"))
            //    {
            //        CCmd cmd = new CPARCmd();
            //        ((CPARCmd)cmd).ParameterName = l_Format.Substring(1);
            //        cmd.Parser = m_parser;
            //        l_Format = cmd.Execute();
            //    }
            //    bool b_isU = false;
            //    l_Format = l_Format.Replace("%dash%", "#");
            //    CultureInfo ci = CultureInfo.InvariantCulture;
            //    if (l_Format.StartsWith("NL"))
            //    {
            //        l_Format = l_Format.Substring(2);
            //        ci = CultureInfo.CreateSpecificCulture("nl-NL");
            //    }
            //    if (l_Format.StartsWith("US"))
            //    {
            //        l_Format = l_Format.Substring(2);
            //        ci = CultureInfo.CreateSpecificCulture("en-US");
            //    }
            //    if (l_Format.StartsWith("U"))
            //    {
            //        b_isU = true;
            //        l_Format = l_Format.Substring(1);
            //    }
            //    if (l_Format != "")
            //    {
            //        try
            //        {
            //            double dVal = Convert.ToDouble(retVal);
            //            retVal = dVal.ToString(l_Format, ci);
            //        }
            //        catch
            //        {
            //            try
            //            {
            //                DateTime dVal = Convert.ToDateTime(retVal);
            //                retVal = dVal.ToString(l_Format, ci);
            //            }
            //            catch
            //            {
            //            }
            //        }

            //    }
            //    if (b_isU)
            //    {
            //        retVal = retVal.Replace(',', '.');
            //    }
            //}
            EncodeOption t_encodeOption = CParser.GetEncodeOption(l_EncodeOption);
            retVal = Utils.Encode(retVal, t_encodeOption);

            l_Val = retVal;

            if ((Append) || (OnlyNull))
            {
                string sPar = m_parser.GetSV(l_VarName, l_ContextName, l_ID, "");
                if ((OnlyNull) && (sPar != "")) return "";
                if (Append) l_Val = sPar + (sPar == "" ? "" : AppendChar) + l_Val;
            }
            m_parser.SetSV(l_VarName, l_ContextName, l_Val, l_ID);

            return "";
		}

    }
}

