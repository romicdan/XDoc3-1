using System;
using System.Diagnostics;
using XDataSourceModule;

namespace XHTMLMerge
{
    [Serializable]
    public class CXPATHCmd : CCmd
    {
        # region Protected members

        protected int m_TemplateID = -1;
        protected string m_strQueryName = "";
        protected CCmd[] m_Parameters = null;
        //protected CContext m_context = null;
        //protected XDataSourceModule.IXDataSource m_evaluator = null;
        protected CCmd m_pageNumberCmd = null;
        protected CCmd m_pageSizeCmd = null;
        private int m_resultsCount = -1;
        [NonSerialized]
        protected SourceResult m_Result = null;

        protected bool m_FromTemplate = false;
        protected bool m_JPath = false;
        protected string m_TemplateContent = "";
        protected bool m_DataTable = false;



        #endregion Protected members

        #region Public properties

        public int TemplateID
        {
            get { return m_TemplateID; }
            set { m_TemplateID = value; }
        }

        public int ResultsCount
        {
            get { return m_resultsCount; }
            set { m_resultsCount = value; }
        }

        public CCmd PageNumberCmd
        {
            get { return m_pageNumberCmd; }
            set { m_pageNumberCmd = value; }
        }

        public CCmd PageSizeCmd
        {
            get { return m_pageSizeCmd; }
            set { m_pageSizeCmd = value; }
        }


        public string QueryName
        {
            get { return m_strQueryName; }
            set { m_strQueryName = value; }
        }

        public CCmd[] Parameters
        {
            get { return m_Parameters; }
            set { m_Parameters = value; }
        }

        //public CContext Context
        //{
        //    get { return m_context; }
        //    set { m_context = value; }
        //}

        //public XDataSourceModule.IXDataSource Evaluator
        //{
        //    get { return m_evaluator; }
        //    set { m_evaluator = value; }
        //}

        public bool FromTemplate
        {
            get { return m_FromTemplate; }
            set { m_FromTemplate = value; }
        }

        public bool JPath
        {
            get { return m_JPath; }
            set { m_JPath = value; }
        }

        public bool DataTable
        {
            get { return m_DataTable; }
            set { m_DataTable = value; if (m_DataTable) m_bIsBlockCommand = false; }
        }

        #endregion Public properties

        public CXPATHCmd()            : base()
        {
            this.m_enType = CommandType.XPATHCommand;
            m_bIsBlockCommand = true;
        }

        private string GetErrorParameter(Array m_parameters)
        {
            string sError = "Error";

            for (int index = 0; index < m_parameters.GetLength(0); index++)
            {
                if (m_parameters.GetValue(index) != null)
                {
                    string sValue = m_parameters.GetValue(index).ToString();
                    if (sValue.Contains("="))
                    {
                        string sParName = sValue.Substring(0, sValue.IndexOf("="));
                        string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
                        if (sParName.ToUpper() == "ERROR") sError = sValue1;
                    }
                }
            }
            return sError;
        }

        public override string Execute(CParser m_parser)
        {
            string sError = "";
            string sErrorVerbose = "";
            string sParameters = "";
            double dDuration = 0;
            string key;
            //string retVal = "";
            System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

            bool bDefQry = (m_parser.Queries[m_strQueryName] != null);
            //Array queryParameters = GetParamArray(m_parser, bDefQry);
            Array queryParameters = GetOriginalParamArray(m_parser);
            if (m_FromTemplate && queryParameters.Length > 0)
            {
                string sTemplateName = (string)queryParameters.GetValue(0);
                sTemplateName = m_parser.ReplaceParameters(sTemplateName);
                m_parser.RequestTemplateIDForInclude(sTemplateName);
                m_parser.RequestTemplate(m_parser.IDForInclude);
                if (m_parser.TextForInclude == null)
                    throw new Exception("Could not retrieve template content for TemplateID=" + m_parser.IDForInclude.ToString());
                string s_text = m_parser.TextForInclude;
                m_TemplateContent = s_text;
                queryParameters.SetValue(s_text, 0);
            }

            try
            {

                m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser.parser_RequestHierarchy);

                m_Result = m_parser.Evaluator.EvaluateSourceResult(m_TemplateID, m_strQueryName, queryParameters, out sError, out sErrorVerbose, out sParameters, out dDuration , out key);
                m_parser.Context.Keys[this] = key;
                m_parser.Context.Results[this] = m_Result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                sError = ex.Message;
                sErrorVerbose = sError;
            }
            string sErrorParameter = GetErrorParameter(queryParameters);
            string l_ErrorParName = m_parser.ReplaceParameters(sErrorParameter);
            if (sError != "")
            {
                //sErrorVerbose = "Error executing COUNT command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + sParameters + "; " + newLine + sErrorVerbose;
                sErrorVerbose = "Error executing " + (m_JPath ? "JPATH" : "XPATH") + " command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
                m_parser.Message("QueryError", sErrorVerbose);
            }
            else
            {
                sErrorVerbose = "";
            }
            // set par 
            if (l_ErrorParName != "")
            {
                m_parser.ParamDefaults[l_ErrorParName] = sError;
                m_parser.TemplateParams[l_ErrorParName] = sError;
                m_parser.ParamDefaults[l_ErrorParName + "Verbose"] = sErrorVerbose;
                m_parser.TemplateParams[l_ErrorParName + "Verbose"] = sErrorVerbose;
            }

            m_resultsCount = 0;
            if (m_Result != null) m_resultsCount = m_Result.GetCount();
            if (m_parser.ParserQueryLog == "1") m_parser.QueryLog(m_strQueryName, sParameters, m_resultsCount, sError, sErrorVerbose,dDuration );

            int pageNumber = -1;
            int pageSize = -1;
            int start = 0, end = m_resultsCount;

            if (m_pageNumberCmd != null && m_pageSizeCmd != null)
            {
                string strPgNum = m_pageNumberCmd.Execute(m_parser);
                string strPgSize = m_pageSizeCmd.Execute(m_parser);
                if (int.TryParse(strPgNum, out pageNumber))
                    if (int.TryParse(strPgSize, out pageSize))
                    {
                        start = (pageNumber - 1) * pageSize;
                        end = pageNumber * pageSize;
                    }
            }

            if (start < 0)
                start = 0;
            m_parser.Context.OSetQueryIndex(this, start);
            //m_context.SetQueryIndex(m_strQueryName, start);

            if (end == -1)
            {
                if ((queryParameters.GetLength(0) >= 3 ? queryParameters.GetValue(2).ToString() : "") == "1")
                {
                    m_resultsCount = 1; end = 1;
                }
            }

            if (m_DataTable)
            {
                string retVal = "";
                if (m_Result != null) retVal = m_Result.GetJSON(start,end);
                m_parser.TemplateParams[m_strQueryName + "Data"] = retVal;
                return "";
            }

            for (int i = start; i < end; i++)
            {
                if (i >= m_resultsCount)
                    break;

                string output = base.Execute(m_parser);

                //retVal += output;
                sbRetVal.Append(output);
                m_parser.Context.OIncrementQIndex(this);
                //m_context.IncrementQIndex(m_strQueryName);
            }
            //return retVal;
            return sbRetVal.ToString();
        }


        public Array GetParamArray(CParser m_parser)
        {
//            Array arrParams = Array.CreateInstance(typeof(string), 4);
            Array arrParams = Array.CreateInstance(typeof(string), (m_Parameters.GetLength(0) + 1 < 4 ? 4 : m_Parameters.GetLength(0) + 1));
            arrParams.SetValue("0", 2);
            for (int i = 0; i < m_Parameters.GetLength(0) && i < 3; i++)
            {
                if ((i==0) && m_FromTemplate)
                    arrParams.SetValue(m_TemplateContent, 0);
                else
                {
                    CCmd cmd = m_Parameters[i];
                    string strValue = cmd.Execute(m_parser);
                    arrParams.SetValue(strValue, i);
                }
            }
            if (m_JPath)
                arrParams.SetValue("Provider=JPATH;", 3);
            else
                arrParams.SetValue("Provider=XPATH;", 3);
            for (int i = 3; i < m_Parameters.GetLength(0); i++)
            {
                ////CCmd cmd = m_Parameters[i];
                ////string strValue = cmd.Execute(m_parser);
                CCmd cmd = m_Parameters[i];
                string strValue = cmd.Execute(m_parser);
                if (strValue.ToUpper().Contains("#PAR."))
                    strValue = m_parser.Parameters2Values(strValue);
                arrParams.SetValue(strValue, i + 1);
            }

            return arrParams;
        }
        public Array GetOriginalParamArray(CParser m_parser)
        {
            Array arrParams = Array.CreateInstance(typeof(string), (m_Parameters.GetLength(0) + 1 < 4 ? 4 : m_Parameters.GetLength(0) + 1));
            arrParams.SetValue("0", 2);
            for (int i = 0; i < m_Parameters.GetLength(0) && i < 3; i++)
            {
                CCmd cmd = m_Parameters[i];
                string strValue = cmd.Execute(m_parser);
                arrParams.SetValue(strValue, i);
            }
            if (m_JPath)
                arrParams.SetValue("Provider=JPATH;", 3);
            else
                arrParams.SetValue("Provider=XPATH;", 3);

            for (int i = 3; i < m_Parameters.GetLength(0) ; i++)
            {
                ////CCmd cmd = m_Parameters[i];
                ////string strValue = cmd.Execute(m_parser);
                CCmd cmd = m_Parameters[i];
                string strValue = cmd.Execute(m_parser);
                if (strValue.ToUpper().Contains("#PAR."))
                    strValue = m_parser.Parameters2Values(strValue);
                arrParams.SetValue(strValue, i + 1);
            }
            return arrParams;
        }

        public SourceResult GetResult(CParser m_parser)
        {
            return (SourceResult)m_parser.Context.Results[this];
            //return m_Result;
        }
        //public Array GetOriginalParamArray_1(CParser m_parser)
        //{
        //    Array arrParams = Array.CreateInstance(typeof(string), 4);
        //    arrParams.SetValue("0", 2);
        //    for (int i = 0; i < m_Parameters.GetLength(0) && i < 3; i++)
        //    {
        //        CCmd cmd = m_Parameters[i];
        //        string strValue = cmd.Execute(m_parser);
        //        arrParams.SetValue(strValue, i);
        //    }
        //    arrParams.SetValue("Provider=XPATH;", 3);
        //    return arrParams;
        //}

    }
}