using KubionLogNamespace;
using System;
using System.Globalization;

namespace XHTMLMerge
{
    [Serializable]
    public class CSETPARCmd : CCmd
	{
        string m_ParName = "";
        bool m_OnlyNull = false;
        string m_VarName = "";
        string m_ContextName = "";
        string m_ID = "ID";
        string m_Format = "";
        string  m_EncodeOption = "";

        public string ParName
        {
            get { return m_ParName; }
            set { m_ParName = value; }
        }
        public bool OnlyNull
        {
            get { return m_OnlyNull; }
            set { m_OnlyNull = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }
        public string ContextName
        {
            get { return m_ContextName; }
            set { m_ContextName = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string Format
        {
            get { return m_Format; }
            set { m_Format = value; }
        }
        public string EncodeOption
        {
            get { return m_EncodeOption; }
            set { m_EncodeOption = value; }
        }

        public CSETPARCmd(): base()
		{
			m_enType = CommandType.SETPARCommand ;
			this.m_bIsBlockCommand = false;
		}


        public override string Execute(CParser m_parser)
        {
            string l_VarName, l_ContextName, l_ID, l_ParName, l_EncodeOption, l_Format;

            l_ParName = m_parser.ReplaceParameters(m_ParName);
            l_EncodeOption = m_parser.ReplaceParameters(m_EncodeOption);
            l_Format = m_parser.ReplaceParameters(m_Format);
            l_VarName = m_parser.ReplaceParameters(m_VarName);
            l_ContextName = m_parser.ReplaceParameters(m_ContextName);
            l_ID = m_parser.ReplaceParameters(m_ID);

            if (OnlyNull)
            {
                string sPar = m_parser.GetSV(l_VarName, l_ContextName, l_ID, "");
                if (sPar != "") return "";
            }

            string retVal = "";
            //if (m_parser.TemplateParams[l_ParName] != null) retVal = (string)m_parser.TemplateParams[l_ParName];
            //else if (m_parser.ParamDefaults[l_ParName] != null) retVal = (string)m_parser.ParamDefaults[l_ParName];
            //else retVal = m_parser.GetParameterValue(l_ParName);
            retVal =  m_parser.GetParameterValue (l_ParName );

            retVal = m_parser.ApplyFormat(retVal, l_Format);

            //if (l_Format != "")
            //{

            //    if (l_Format.StartsWith("@"))
            //    {
            //        CCmd cmd = new CPARCmd();
            //        ((CPARCmd)cmd).ParameterName = l_Format.Substring(1);
            //        cmd.Parser = m_parser;
            //        l_Format = cmd.Execute();
            //    }
            //    bool b_isU = false;
            //    l_Format = l_Format.Replace("%dash%", "#");
            //    CultureInfo ci = CultureInfo.InvariantCulture;
            //    if (l_Format.StartsWith("NL"))
            //    {
            //        l_Format = l_Format.Substring(2);
            //        ci = CultureInfo.CreateSpecificCulture("nl-NL");
            //    }
            //    if (l_Format.StartsWith("US"))
            //    {
            //        l_Format = l_Format.Substring(2);
            //        ci = CultureInfo.CreateSpecificCulture("en-US");
            //    }
            //    if (l_Format.StartsWith("U"))
            //    {
            //        b_isU = true;
            //        l_Format = l_Format.Substring(1);
            //    }
            //    if (l_Format != "")
            //    {
            //        try
            //        {
            //            double dVal = Convert.ToDouble(retVal);
            //            retVal = dVal.ToString(l_Format, ci);
            //        }
            //        catch
            //        {
            //            try
            //            {
            //                DateTime dVal = Convert.ToDateTime(retVal);
            //                retVal = dVal.ToString(l_Format, ci);
            //            }
            //            catch
            //            {
            //            }
            //        }

            //    }
            //    if (b_isU)
            //    {
            //        retVal = retVal.Replace(',', '.');
            //    }
            //}
            EncodeOption t_encodeOption = CParser.GetEncodeOption(l_EncodeOption);
            retVal = Utils.Encode(retVal, t_encodeOption);

            m_parser.SetSV(l_VarName, l_ContextName, retVal, l_ID);
            return "";
        }

	}
}

