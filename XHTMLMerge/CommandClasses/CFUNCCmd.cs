using System;
using System.Collections;
using System.Diagnostics;
using System.Web;
using System.Globalization;
using System.Reflection;
using System.Text;
using KubionLogNamespace;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Xml;
using System.Linq;
using System.Security.Cryptography.Xml;
//using System.Security.Cryptography.Xml;
//using Security.Cryptography;

namespace XHTMLMerge
{
    [Serializable]
    public class CFUNCCmd : CCmd
	{
		#region Protected members

		protected string 
            m_strParamName = "",
            m_strOperation = "",
            m_strFormat = "",
            m_strValue1="",
            m_strValue2="",
            m_strValue3="",
            m_strValue4="",
            m_strValue5="",
            m_strValue6="";

		
		#endregion Protected members

		#region Public properties

		public string ParameterName
		{
			get { return m_strParamName; }
			set { m_strParamName = value; }
		}

		public string Operation
		{
			get { return m_strOperation; }
			set { m_strOperation = value; }
		}

        public string Format
        {
            get { return m_strFormat; }
            set { m_strFormat = value; }
        }

		public string Value1
		{
			get { return m_strValue1; }
			set { m_strValue1 = value; }
		}
		public string Value2
		{
			get { return m_strValue2; }
			set { m_strValue2 = value; }
		}
		public string Value3
		{
			get { return m_strValue3; }
			set { m_strValue3 = value; }
		}
		public string Value4
		{
			get { return m_strValue4; }
			set { m_strValue4 = value; }
		}
		public string Value5
		{
			get { return m_strValue5; }
			set { m_strValue5 = value; }
		}
        public string Value6
		{
			get { return m_strValue6; }
			set { m_strValue6 = value; }
        }

#endregion Public properties


		public CFUNCCmd():base()
		{
			m_enType = CommandType.FUNCCommand ;
			m_bIsBlockCommand = false;
		}



        //[SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "RSAPKCS", Justification = "This casing is to match the existing RSAPKCS1SHA1SignatureDescription type")]
        //[SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SHA", Justification = "This casing is to match the use of SHA throughout the framework")]
	public sealed class RSAPKCS1SHA256SignatureDescription : SignatureDescription
	{
		/// <summary>
		///     Construct an RSAPKCS1SHA256SignatureDescription object. The default settings for this object
		///     are:
		///     <list type="bullet">
		///         <item>Digest algorithm - <see cref="SHA256Managed" /></item>
		///         <item>Key algorithm - <see cref="RSACryptoServiceProvider" /></item>
		///         <item>Formatter algorithm - <see cref="RSAPKCS1SignatureFormatter" /></item>
		///         <item>Deformatter algorithm - <see cref="RSAPKCS1SignatureDeformatter" /></item>
		///     </list>
		/// </summary>
		public RSAPKCS1SHA256SignatureDescription()
		{
			KeyAlgorithm = typeof(RSACryptoServiceProvider).FullName;
			DigestAlgorithm = typeof(SHA256Managed).FullName;   // Note - SHA256CryptoServiceProvider is not registered with CryptoConfig
			FormatterAlgorithm = typeof(RSAPKCS1SignatureFormatter).FullName;
			DeformatterAlgorithm = typeof(RSAPKCS1SignatureDeformatter).FullName;
		}

		/// <summary>
		/// Create deformatter
		/// </summary>
		/// <param name="key">The key</param>
		/// <returns>Formatter Info</returns>
		public override AsymmetricSignatureDeformatter CreateDeformatter(AsymmetricAlgorithm key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}

			RSAPKCS1SignatureDeformatter deformatter = new RSAPKCS1SignatureDeformatter(key);
			deformatter.SetHashAlgorithm("SHA256");
			return deformatter;
		}

		/// <summary>
		/// Create formatter
		/// </summary>
		/// <param name="key">The key</param>
		/// <returns>Signature Formatter</returns>
		public override AsymmetricSignatureFormatter CreateFormatter(AsymmetricAlgorithm key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}

			RSAPKCS1SignatureFormatter formatter = new RSAPKCS1SignatureFormatter(key);
			formatter.SetHashAlgorithm("SHA256");
			return formatter;
		}
	}

        // #FUNC..xml_sign..%MyXML%.c:\cert\.p12.myPass# => xml:string
		public static string FileCertSignXml(string strXml, string strCertificateLocation, string strCertificatePassword)
		{
			X509Certificate2 certificate = new X509Certificate2(strCertificateLocation, strCertificatePassword, X509KeyStorageFlags.Exportable);
			CspParameters cspParams = new CspParameters(24);
			cspParams.KeyContainerName = "XML_DISG_RSA_KEY";
			RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);
            if (certificate.PrivateKey == null) throw new Exception("Certificate contains no PrivateKey");
            rsaKey.FromXmlString(certificate.PrivateKey.ToXmlString(true));

			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.PreserveWhitespace = true;
			xmlDoc.LoadXml(strXml);

			CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
			SignedXml signedXml = new SignedXml(xmlDoc);
			signedXml.SigningKey = rsaKey;
			signedXml.SignedInfo.CanonicalizationMethod = "http://www.w3.org/2001/10/xml-exc-c14n#";
			signedXml.SignedInfo.SignatureMethod = @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

			// Create a reference to be signed.
			Reference reference = new Reference();
			reference.Uri = "";
			reference.DigestMethod = @"http://www.w3.org/2001/04/xmlenc#sha256";
			XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
			reference.AddTransform(env);
			signedXml.AddReference(reference);

			KeyInfo keyInfo = new KeyInfo();
			KeyInfoName kin = new KeyInfoName();
			kin.Value = certificate.Thumbprint;
			keyInfo.AddClause(kin);
			signedXml.KeyInfo = keyInfo;

			signedXml.ComputeSignature();
			XmlElement xmlDigitalSignature = signedXml.GetXml();
			xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
			return xmlDoc.OuterXml;
		}
		// #FUNC..xml_verify.%MyXML%.c:\cert\.cer# => 0/1:string
		public static string FileCertVerifyXml(string strXmlSigned, string strCertificateLocation) //(XmlDocument Doc, RSA Key)
		{
			X509Certificate2 certificate = new X509Certificate2(strCertificateLocation);
			RSACryptoServiceProvider rsaKey = (RSACryptoServiceProvider)certificate.PublicKey.Key;

			XmlDocument Doc = new XmlDocument();
			Doc.PreserveWhitespace = true;
			Doc.LoadXml(strXmlSigned);

			CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
			SignedXml signedXml = new SignedXml(Doc);

			XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");
			if (nodeList.Count != 1)
			{
				throw new CryptographicException("Verification failed: No, or more than one, signature(s) was found in the document.");
			}
			signedXml.LoadXml((XmlElement)nodeList[0]);

			return signedXml.CheckSignature(rsaKey) ? "1" : "0";
		}
        // #FUNC..xml_sign..%MyXML%.Sto# => xml:string
        // #FUNC..xml_signstore.%MyXML%.%storeName%.%storeLocation%.%certKey%..%storeFindBy%# =>  xml:string
        public static string StoreCertSignXml(string strXml, string strStoreName, string strStoreLocation, string strCertificateKey, string strStoreFindBy) 
        {
            X509Certificate2 certificate = FindCertificateInStore(strStoreName,strStoreLocation,strCertificateKey,strStoreFindBy);
            if (certificate == null)
                throw new Exception("Certificate not found");
            CspParameters cspParams = new CspParameters(24);
            cspParams.KeyContainerName = "XML_DISG_RSA_KEY";
            RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);
            if (certificate.PrivateKey == null) throw new Exception("Certificate contains no PrivateKey");
            rsaKey.FromXmlString(certificate.PrivateKey.ToXmlString(true));

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.LoadXml(strXml);

            CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
            SignedXml signedXml = new SignedXml(xmlDoc);
            signedXml.SigningKey = rsaKey;
            signedXml.SignedInfo.CanonicalizationMethod = "http://www.w3.org/2001/10/xml-exc-c14n#";
            signedXml.SignedInfo.SignatureMethod = @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

            // Create a reference to be signed.
            Reference reference = new Reference();
            reference.Uri = "";
            reference.DigestMethod = @"http://www.w3.org/2001/04/xmlenc#sha256";
            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(env);
            signedXml.AddReference(reference);

            KeyInfo keyInfo = new KeyInfo();
            KeyInfoName kin = new KeyInfoName();
            kin.Value = certificate.Thumbprint;
            keyInfo.AddClause(kin);
            signedXml.KeyInfo = keyInfo;

            signedXml.ComputeSignature();
            XmlElement xmlDigitalSignature = signedXml.GetXml();
            xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
            return xmlDoc.OuterXml;
        }
        // #FUNC..xml_verifystore.%MyXMLSigned%.%storeName%.%storeLocation%.%certKey%..%storeFindBy%# => 0/1:string
        // xmlsigned, storeName, storeLocation, CertKey/CertThumbprint, FindMethod = SubjectName
        public static string StoreCertVerifyXml(string strXmlSigned, string strStoreName, string strStoreLocation, string strCertificateKey,  string strStoreFindBy) 
        {
            X509Certificate2 certificate = FindCertificateInStore(strStoreName,strStoreLocation,strCertificateKey,strStoreFindBy);
            if (certificate == null)
                throw new Exception("Certificate not found");
            RSACryptoServiceProvider rsaKey = (RSACryptoServiceProvider)certificate.PublicKey.Key;

            XmlDocument Doc = new XmlDocument();
            Doc.PreserveWhitespace = true;
            Doc.LoadXml(strXmlSigned);

            CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
            SignedXml signedXml = new SignedXml(Doc);

            XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");
            if (nodeList.Count != 1)
            {
                throw new CryptographicException("Verification failed: No, or more than one, signature(s) was found in the document.");
            }
            signedXml.LoadXml((XmlElement)nodeList[0]);

            return signedXml.CheckSignature(rsaKey) ? "1" : "0";
        }
        private static X509Certificate2 FindCertificateInStore(string strStoreName, string strStoreLocation, string strCertificateKey, string strStoreFindBy)
        {
            StoreName sn = StoreName.My;
            StoreLocation sl = StoreLocation.LocalMachine;
            X509FindType ft = X509FindType.FindBySubjectName;
            switch ( strStoreName.ToLower()){
                case "addressbook":
                    sn = StoreName.AddressBook;
                    break;
                case "authroot":
                    sn = StoreName.AuthRoot;
                    break;
                case "ca":
                    sn = StoreName.CertificateAuthority;
                    break;
                case "certificateauthority":
                    sn = StoreName.CertificateAuthority;
                    break;
                case "disallowed":
                    sn = StoreName.Disallowed;
                    break;
                case "my":
                    sn = StoreName.My;
                    break;
                case "root":
                    sn = StoreName.Root;
                    break;
                case "trustedpeople":
                    sn = StoreName.TrustedPeople;
                    break;
                case "trustedpublisher":
                    sn = StoreName.TrustedPublisher;
                    break;
                case "":
                    sn = StoreName.My;
                    break;
                default:
                    throw new Exception("Invalid certificate StoreName: " + strStoreName);
                    break;
            }
            switch ( strStoreLocation.ToLower()){
                case "localmachine":
                    sl = StoreLocation.LocalMachine;
                    break;
                case "currentuser":
                    sl = StoreLocation.CurrentUser;
                    break;
                case "":
                    sl = StoreLocation.LocalMachine;
                    break;
                default:
                    throw new Exception("Invalid certificate StoreLocation: " + strStoreLocation);
                    break;
            }
            strStoreFindBy=strStoreFindBy.ToLower();
            if(strStoreFindBy.StartsWith("findby")) strStoreFindBy = strStoreFindBy.Substring(6);
            switch ( strStoreFindBy){
                case "applicationpolicy":
                     ft = X509FindType.FindByApplicationPolicy;
                    break;
                case "certificatepolicy":
                     ft = X509FindType.FindByCertificatePolicy;
                    break;
                case "extension":
                     ft = X509FindType.FindByExtension;
                    break;
                case "issuerdistinguishedname":
                     ft = X509FindType.FindByIssuerDistinguishedName;
                    break;
                case "issuername":
                     ft = X509FindType.FindByIssuerName;
                    break;
                case "keyusage":
                     ft = X509FindType.FindByKeyUsage;
                    break;
                case "serialnumber":
                     ft = X509FindType.FindBySerialNumber;
                    break;
                case "subjectdistinguishedname":
                     ft = X509FindType.FindBySubjectDistinguishedName;
                    break;
                case "subjectkeyidentifier":
                     ft = X509FindType.FindBySubjectKeyIdentifier;
                    break;
                case "subjectname":
                     ft = X509FindType.FindBySubjectName;
                    break;
                case "templatename":
                     ft = X509FindType.FindByTemplateName;
                    break;
                case "thumbprint":
                     ft = X509FindType.FindByThumbprint;
                    break;
                case "timeexpired":
                     ft = X509FindType.FindByTimeExpired;
                    break;
                case "timenotyetvalid":
                     ft = X509FindType.FindByTimeNotYetValid;
                    break;
                case "timevalid":
                     ft = X509FindType.FindByTimeValid;
                    break;
                case "":
                    ft = X509FindType.FindByThumbprint;
                    break;
                default:
                    throw new Exception("Invalid certificate StoreFindType: " + strStoreFindBy);
                    break;
            }
            if (strCertificateKey=="") return null;
            X509Store certStore = new X509Store(sn, sl);
            certStore.Open(OpenFlags.ReadOnly);
            var certCollection = certStore.Certificates.Find(ft, strCertificateKey, false);
            certStore.Close();
            if (certCollection.Count > 0)
            {
                return certCollection[0];
            }
            else
            {
                return null;
            }
        }



        public override string Execute(CParser m_parser)
        {
            string strFormat = m_strFormat;
            string l_ParName = "";
            string l_Value1 = "", l_Value2 = "", l_Value3 = "", l_Value4 = "", l_Value5 = "", l_Value6 = "";
            string retVal = "";
            StringBuilder sJSON;
            double doubleVal, dVal;
            DateTime datetimeVal, datetimeVal2;
            TimeSpan timespanVal2;
            int i_Value = 0, iVal2 = 0, iVal3 = 0, iVal4 = 0, iVal5 = 0, iVal6 = 0;

            l_ParName = m_parser.ReplaceParameters(m_strParamName);
            l_Value1 = m_parser.ReplaceParameters(m_strValue1);
            l_Value2 = m_parser.ReplaceParameters(m_strValue2);
            l_Value3 = m_parser.ReplaceParameters(m_strValue3);
            l_Value4 = m_parser.ReplaceParameters(m_strValue4);
            l_Value5 = m_parser.ReplaceParameters(m_strValue5);
            l_Value6 = m_parser.ReplaceParameters(m_strValue6);

            CultureInfo ci_in = CultureInfo.InvariantCulture;
            CultureInfo ci = CultureInfo.InvariantCulture;
            strFormat = m_parser.ParseFormat(strFormat,out  ci_in, out ci);


            switch (m_strOperation.ToLower())
            {
                case "guid":
                    retVal = Guid.NewGuid().ToString();
                    break;
                case "plus":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal + dVal;
                    if (l_Value3 != "")
                    {
                        if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value3: " + l_Value3 + " cannot evaluate as double;");
                        doubleVal = doubleVal + dVal;
                    }
                    if (l_Value4 != "")
                    {
                        if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value4: " + l_Value4 + " cannot evaluate as double;");
                        doubleVal = doubleVal + dVal;
                    }
                    if (l_Value5 != "")
                    {
                        if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value5: " + l_Value5 + " cannot evaluate as double;");
                        doubleVal = doubleVal + dVal;
                    }
                    if (l_Value6 != "")
                    {
                        if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value6: " + l_Value6 + " cannot evaluate as double;");
                        doubleVal = doubleVal + dVal;
                    }
                    retVal = doubleVal.ToString(strFormat, ci);
                    break;
                case "minus":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal - dVal;
                    if (l_Value3 != "")
                    {
                        if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value3: " + l_Value3 + " cannot evaluate as double;");
                        doubleVal = doubleVal - dVal;
                    }
                    if (l_Value4 != "")
                    {
                        if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value4: " + l_Value4 + " cannot evaluate as double;");
                        doubleVal = doubleVal - dVal;
                    }
                    if (l_Value5 != "")
                    {
                        if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value5: " + l_Value5 + " cannot evaluate as double;");
                        doubleVal = doubleVal - dVal;
                    }
                    if (l_Value6 != "")
                    {
                        if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value6: " + l_Value6 + " cannot evaluate as double;");
                        doubleVal = doubleVal - dVal;
                    }
                    retVal = doubleVal.ToString(strFormat, ci);
                    break;
                case "mult":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal * dVal;
                    if (l_Value3 != "")
                    {
                        if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value3: " + l_Value3 + " cannot evaluate as double;");
                        doubleVal = doubleVal * dVal;
                    }
                    if (l_Value4 != "")
                    {
                        if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value4: " + l_Value4 + " cannot evaluate as double;");
                        doubleVal = doubleVal * dVal;
                    }
                    if (l_Value5 != "")
                    {
                        if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value5: " + l_Value5 + " cannot evaluate as double;");
                        doubleVal = doubleVal * dVal;
                    }
                    if (l_Value6 != "")
                    {
                        if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value6: " + l_Value6 + " cannot evaluate as double;");
                        doubleVal = doubleVal * dVal;
                    }
                    retVal = doubleVal.ToString(strFormat, ci);
                    break;
                case "div":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal / dVal;
                    if (l_Value3 != "")
                    {
                        if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value3: " + l_Value3 + " cannot evaluate as double;");
                        doubleVal = doubleVal / dVal;
                    }
                    if (l_Value4 != "")
                    {
                        if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value4: " + l_Value4 + " cannot evaluate as double;");
                        doubleVal = doubleVal / dVal;
                    }
                    if (l_Value5 != "")
                    {
                        if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value5: " + l_Value5 + " cannot evaluate as double;");
                        doubleVal = doubleVal / dVal;
                    }
                    if (l_Value6 != "")
                    {
                        if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value6: " + l_Value6 + " cannot evaluate as double;");
                        doubleVal = doubleVal / dVal;
                    }
                    retVal = doubleVal.ToString(strFormat, ci);
                    break;
                case "mod":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal % dVal;
                    retVal = doubleVal.ToString(strFormat, ci);
                    break;
                case "string_concat":
                    retVal = l_Value1 + l_Value2 + l_Value3 + l_Value4 + l_Value5 + l_Value6;
                    break;
                case "string_length":
                    doubleVal = l_Value1.Length;
                    retVal = doubleVal.ToString(strFormat, ci);
                    break;
                case "string_trim":
                    l_Value1 = l_Value1.Trim();
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_trimend":
                    l_Value1 = l_Value1.TrimEnd();
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_trimstart":
                    l_Value1 = l_Value1.TrimStart();
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_toupper":
                    l_Value1 = l_Value1.ToUpper();
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_tolower":
                    l_Value1 = l_Value1.ToLower();
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_substring":
                    if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    if (l_Value3 != "")
                    {
                        if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value3 + " cannot evaluate as int;");
                        l_Value1 = l_Value1.Substring(iVal2, iVal3);
                    }
                    else
                        l_Value1 = l_Value1.Substring(iVal2);
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_startswith":
                    l_Value1 = (l_Value1.StartsWith(l_Value2)) ? "1" : "0";
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_endswith":
                    l_Value1 = (l_Value1.EndsWith(l_Value2)) ? "1" : "0";
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_replace":
                    l_Value1 = l_Value1.Replace(l_Value2, l_Value3);
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_contains":
                    l_Value1 = (l_Value1.Contains(l_Value2)) ? "1" : "0";
                    retVal = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_indexof":
                    i_Value = l_Value1.IndexOf(l_Value2);
                    if (l_Value3 != "")
                    {
                        if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value3: " + l_Value3 + " cannot evaluate as int;");
                        i_Value = l_Value1.IndexOf(l_Value2, iVal3);
                    }
                    if (l_Value4 != "")
                    {
                        if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
                            throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value4: " + l_Value4 + " cannot evaluate as int;");
                        i_Value = l_Value1.IndexOf(l_Value2, iVal3, iVal4);
                    }
                    retVal = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_day":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    i_Value = datetimeVal.Day;
                    retVal = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_month":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Month;
                    retVal = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_year":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Year;
                    retVal = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_hour":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Hour;
                    retVal = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_minute":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Minute;
                    retVal = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_second":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Second;
                    retVal = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_millisecond":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Millisecond;
                    retVal = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_ticks":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    retVal = datetimeVal.Ticks.ToString(strFormat, ci);
                    break;
                case "datetime_subtract":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!TimeSpan.TryParse(l_Value2, out timespanVal2))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as timespan;");
                    datetimeVal = datetimeVal.Subtract(timespanVal2);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_add":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!TimeSpan.TryParse(l_Value2, out timespanVal2))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as timespan;");
                    datetimeVal = datetimeVal.Add(timespanVal2);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_subtractduration":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    try{
                    timespanVal2 = System.Xml.XmlConvert.ToTimeSpan(l_Value2);
                    }
                    catch(Exception e)
                    {
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as duration;");
                    }
                    datetimeVal = datetimeVal.Subtract(timespanVal2);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addduration":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    try{
                    timespanVal2 = System.Xml.XmlConvert.ToTimeSpan(l_Value2);
                    }
                    catch(Exception e)
                    {
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as duration;");
                    }
                    datetimeVal = datetimeVal.Add(timespanVal2);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addmilliseconds":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddMilliseconds(dVal);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addseconds":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddSeconds(dVal);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addminutes":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddMinutes(dVal);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addhours":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddHours(dVal);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_adddays":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddDays(dVal);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addmonths":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddMonths(iVal2);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addyears":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddYears(iVal2);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addworkminutes":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    if (l_Value3 == "") l_Value3 = "0";
                    if (l_Value4 == "") l_Value4 = "0";
                    if (l_Value5 == "") l_Value5 = "0";
                    if (l_Value6 == "") l_Value6 = "0";
                    if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value3: " + l_Value3 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value4: " + l_Value4 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out iVal5))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value5: " + l_Value5 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out iVal6))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value6: " + l_Value6 + " cannot evaluate as int;");
                    datetimeVal = DateTime_AddWorkMinutes(datetimeVal, iVal2, iVal3, iVal4, iVal5, iVal6);
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_diffworkminutes":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!DateTime.TryParse(l_Value2, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal2))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (l_Value3 == "") l_Value3 = "0";
                    if (l_Value4 == "") l_Value4 = "0";
                    if (l_Value5 == "") l_Value5 = "0";
                    if (l_Value6 == "") l_Value6 = "0";
                    if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value3: " + l_Value3 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value4: " + l_Value4 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out iVal5))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value5: " + l_Value5 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out iVal6))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value6: " + l_Value6 + " cannot evaluate as int;");
                    i_Value = DateTime_CalcWorkMinutes(datetimeVal, datetimeVal2, iVal3, iVal4, iVal5, iVal6);
                    retVal = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_diffdatetime":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!DateTime.TryParse(l_Value2, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal2))
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " for parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    timespanVal2 = datetimeVal.Subtract(datetimeVal2);
                    datetimeVal = new DateTime( timespanVal2.Ticks );
                    retVal = datetimeVal.ToString(strFormat, ci);
                    break;
                case "json_path":
                    sJSON = new StringBuilder();
                    Utils.myPathParamsRender(sJSON, l_Value1);
                    retVal = sJSON.ToString();
                    sJSON.Clear();
                    sJSON = null;
                    break;
                case "json_query":
                    sJSON = new StringBuilder();
                    Utils.myQueryRender(sJSON, l_Value1);
                    retVal = sJSON.ToString();
                    sJSON.Clear();
                    sJSON = null;
                    break;
                case "xml_sign":
                    //xml, certFilePath, CertPassword
                    try
                    {
                        retVal = FileCertSignXml(l_Value1, l_Value2, l_Value3);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " error: " +e.Message );
                    }
                    break;
                case "xml_verify":
                    //xmlsigned, certFilePath
                    try
                    {
                        retVal = FileCertVerifyXml(l_Value1, l_Value2);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " error: " +e.Message );
                    }
                    break;
                case "xml_signstore":
                    // xml, storename=My, storeLocation=LocalMachine, CertKey/CertThumbprint, FindMethod = SubjectName
                    try
                    {
                        retVal = StoreCertSignXml(l_Value1, l_Value2, l_Value3, l_Value4, l_Value5);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " error: " +e.Message );
                    }
                    break;
                case "xml_verifystore":
                    // xmlsigned, storename=My, storeLocation=LocalMachine, CertKey/CertThumbprint, FindMethod = SubjectName
                    try
                    {
                        retVal = StoreCertVerifyXml(l_Value1, l_Value2, l_Value3, l_Value4, l_Value5);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("FUNC command " + m_strOperation + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " error: " +e.Message );
                    }
                    break;
            }


            if (l_ParName != "")
            {
                //m_parser.ParamDefaults[l_ParName] = retVal;
                m_parser.TemplateParams[l_ParName] = retVal;
                return "";
            }
            else
                return retVal;


        }

        private static DateTime DateTime_AddWorkMinutes(DateTime now, int iMinutes, int iStartHour, int iEndHour, int iStartHourFriday, int iEndHourFriday)
        {
            DateTime later = now;
            int iDayMinutes = 0;

            if (iStartHour == 0) iStartHour = 9;
            if (iEndHour == 0) iEndHour = 17;
            if (iStartHourFriday == 0) iStartHourFriday = iStartHour;
            if (iEndHourFriday == 0) iEndHourFriday = iEndHour;

            if (iStartHour > iEndHour) throw new Exception("Startuur van de dag moet groter zijn dan einduur");
            if (iStartHourFriday > iEndHourFriday) throw new Exception("Startuur van de vrijdag moet groter zijn dan einduur");

            int iMinutesPerDay = (iEndHour - iStartHour) * 60;
            int iMinutesPerFriday = (iEndHourFriday - iStartHourFriday) * 60;

            while (iMinutes >= iMinutesPerDay)
            {
                if (later.DayOfWeek == DayOfWeek.Friday)
                    iDayMinutes = iMinutesPerFriday;
                else
                    iDayMinutes = iMinutesPerDay;
                later = later.AddDays(1);
                if (later.DayOfWeek == DayOfWeek.Saturday)
                {
                    later = later.AddDays(2);
                }
                iMinutes -= iDayMinutes;
            }

            later = later.AddMinutes(iMinutes);
            if (later.Hour > 17)
            {
                later = later.AddHours(15);
            }
            if (later.DayOfWeek == DayOfWeek.Saturday)
            {
                later = later.AddDays(2);
            }
            else if (later.DayOfWeek == DayOfWeek.Sunday)
            {
                later = later.AddDays(1);
            }

            return later;
        }

        private static int DateTime_CalcWorkMinutes(DateTime start, DateTime end, int iStartHour, int iEndHour, int iStartHourFriday, int iEndHourFriday)
        {
            if (iStartHour == 0) iStartHour = 9;
            if (iEndHour == 0) iEndHour = 17;
            if (iStartHourFriday == 0) iStartHourFriday = iStartHour;
            if (iEndHourFriday == 0) iEndHourFriday = iEndHour;

            if (iStartHour > iEndHour) throw new Exception("Startuur van de dag moet groter zijn dan einduur");
            if (iStartHourFriday > iEndHourFriday) throw new Exception("Startuur van de vrijdag moet groter zijn dan einduur");
            if (start > end) throw new Exception("Start datum moet kleiner zijn dan einddatum");

            int count = 0;
            for (DateTime i = start; i < end; i = i.AddMinutes(1))
            {
                if (i.DayOfWeek != DayOfWeek.Saturday && i.DayOfWeek != DayOfWeek.Sunday)
                {
                    if (i.DayOfWeek == DayOfWeek.Friday)
                    {
                        if (i.TimeOfDay.Hours >= iStartHourFriday && i.TimeOfDay.Hours < iEndHourFriday)
                            count++;
                    }
                    else if (i.TimeOfDay.Hours >= iStartHour && i.TimeOfDay.Hours < iEndHour)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

	}
}
