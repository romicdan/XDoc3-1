
using System;

namespace XHTMLMerge
{
    [Serializable]
    public class CUseParameters : CCmd
    {

        public CUseParameters()            : base()
        {
            m_enType = CommandType.USEPARAMETERSCommand ;
            this.m_bIsBlockCommand = false;
        }


        public override string Execute(CParser m_parser)
        {
            m_parser.m_UseParameters =true ;
            return "";
        }

    }
}

