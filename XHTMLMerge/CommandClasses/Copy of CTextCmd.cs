using System;

namespace XHTMLMerge
{
	public class CTextCmd : CCmd
	{
        string m_text = "";
        bool m_forcedText = false;

        public string Text
        {
            get { return m_text; }
            set { m_text = value; }
        }

		public CTextCmd():base()
		{
			m_enType = CommandType.TEXTCommand;
			this.m_bIsBlockCommand = false;
		}

        public void ForceText(string text)
        {
            m_forcedText = true;
            m_text = text;
        }


		public override string Execute()
		{
            if (m_forcedText == true)
                return m_text;

            int originalStartIndex = m_parser.Mappings.GetOriginalIndex(StartIndex);
            int originalEndIndex = m_parser.Mappings.GetOriginalIndex(EndIndex);

            //if (m_parser.OriginalText[originalStartIndex] == '{')
            //    originalStartIndex++;
            //if (m_parser.OriginalText[originalEndIndex] == '}')
            //    originalEndIndex--;
            
            string ret = m_parser.OriginalText.Substring(originalStartIndex, originalEndIndex - originalStartIndex);

            return ret;
		}

	}
}
