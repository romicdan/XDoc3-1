using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace XHTMLMerge
{
    [Serializable]
    public class CIFCmd : CCmd
	{
		protected CIFCmd m_ElseIfCmd = null;
		protected CmdCollection m_conditionCmds = null;

		public CmdCollection ConditionCmds
		{
			get { return m_conditionCmds; }
			set { m_conditionCmds = value; }
		}

		public CIFCmd ElseIfCommand
		{
			get { return m_ElseIfCmd; }
			set { m_ElseIfCmd = value; }
		}

		public CIFCmd():base()
		{
			m_bIsBlockCommand = true;
			m_enType = CommandType.IFCommand;
			m_conditionCmds = new CmdCollection();
		}
        private bool RegExMatch(string sRegEx, string sValue, bool bIgnoreCase)
        {
            Regex objPattern;
            if(bIgnoreCase )
                objPattern = new Regex(sRegEx,RegexOptions.IgnoreCase   );
            else
                objPattern = new Regex(sRegEx);

            return objPattern.IsMatch(sValue);
        }
        public override string Execute(CParser m_parser)
        {
            string condition = "";
                foreach (CCmd condCmd in this.m_conditionCmds)
                    condition += condCmd.Execute(m_parser);
                bool condResult = false;
                if (condition == "")
                    condResult = true;
                else if (condition.Contains("===="))
                {
                    int i = condition.IndexOf("====");
                    if (RegExMatch(condition.Substring(i + 4).Trim(), condition.Substring(0, i).Trim(), false))
                        condResult = true;
                }
                else if (condition.Contains("!==="))
                {
                    int i = condition.IndexOf("!===");
                    if (!RegExMatch(condition.Substring(i + 4).Trim(), condition.Substring(0, i).Trim(), false))
                        condResult = true;
                }
                else if (condition.Contains("==="))
                {
                    int i = condition.IndexOf("===");
                    if (RegExMatch(condition.Substring(i + 3).Trim(), condition.Substring(0, i).Trim(), true))
                        condResult = true;
                }
                else if (condition.Contains("!=="))
                {
                    int i = condition.IndexOf("!==");
                    if (!RegExMatch(condition.Substring(i + 3).Trim(), condition.Substring(0, i).Trim(), true))
                        condResult = true;
                }
                else if (condition.Contains("=="))
                {
                    int i = condition.IndexOf("==");
                    if (condition.Substring(i + 2).Trim() == condition.Substring(0, i).Trim())
                        condResult = true;
                }
                else if (condition.Contains("!="))
                {
                    int i = condition.IndexOf("!=");
                    if (condition.Substring(i + 2).Trim() != condition.Substring(0, i).Trim())
                        condResult = true;
                }
                else if (condition.Contains(">>>"))
                {
                    int i = condition.IndexOf(">>>");
                    double do1, do2;
                    if (double.TryParse(condition.Substring(0, i).Trim(), out do1))
                        if (double.TryParse(condition.Substring(i + 3).Trim(), out do2))
                            if (do1 > do2)
                                condResult = true;
                }
                else if (condition.Contains(">>"))
                {
                    int i = condition.IndexOf(">>");
                    if (condition.Substring(0, i).Trim().CompareTo(condition.Substring(i + 2).Trim()) > 0)
                        condResult = true;
                }
                else
                {
                    DataTable dt = m_parser.DataProvider.GetDataTable("SELECT 1 WHERE " + condition);
                    if (dt.Rows.Count > 0)
                        condResult = true;
                }
                if (condResult == true)
                    return base.Execute(m_parser);
                else if (condResult == false && m_ElseIfCmd != null)
                    return m_ElseIfCmd.Execute(m_parser);

                return "";
        }

//        public override string Execute(CParser m_parser)
//        {
//            string condition = "";
//            try
//            {
//                foreach (CCmd condCmd in this.m_conditionCmds)
//                    condition += condCmd.Execute(m_parser);
//                bool condResult = false;
//                if (condition == "")
//                    condResult = true;
//                else if (condition.Contains("===="))
//                {
//                    int i = condition.IndexOf("====");
//                    if (RegExMatch(condition.Substring(i + 4).Trim(), condition.Substring(0, i).Trim(), false))
//                        condResult = true;
//                }
//                else if (condition.Contains("!==="))
//                {
//                    int i = condition.IndexOf("!===");
//                    if (!RegExMatch(condition.Substring(i + 4).Trim(), condition.Substring(0, i).Trim(), false))
//                        condResult = true;
//                }
//                else if (condition.Contains("==="))
//                {
//                    int i = condition.IndexOf("===");
//                    if (RegExMatch(condition.Substring(i + 3).Trim() ,condition.Substring(0, i).Trim(),true ))
//                        condResult = true;
//                }
//                else if (condition.Contains("!=="))
//                {
//                    int i = condition.IndexOf("!==");
//                    if (!RegExMatch(condition.Substring(i + 3).Trim(), condition.Substring(0, i).Trim(),true ))
//                        condResult = true;
//                }
//                else if (condition.Contains("=="))
//                {
//                    int i = condition.IndexOf("==");
//                    if (condition.Substring(i + 2).Trim() == condition.Substring(0, i).Trim())
//                        condResult = true;
//                }
//                else if (condition.Contains("!="))
//                {
//                    int i = condition.IndexOf("!=");
//                    if (condition.Substring(i + 2).Trim() != condition.Substring(0, i).Trim())
//                        condResult = true;
//                }
//                else if (condition.Contains(">>>"))
//                {
//                    int i = condition.IndexOf(">>>");
//                    double do1, do2;
//                    if (double.TryParse(condition.Substring(0, i).Trim(), out do1))
//                        if (double.TryParse(condition.Substring(i + 3).Trim(), out do2))
//                            if (do1 > do2)
//                                condResult = true;
//                }
//                else if (condition.Contains(">>"))
//                {
//                    int i = condition.IndexOf(">>");
//                    if (condition.Substring(0, i).Trim().CompareTo(condition.Substring(i + 2).Trim()) > 0)
//                        condResult = true;
//                }
//                else
//                {
//                    DataTable dt = m_parser.DataProvider.GetDataTable("SELECT 1 WHERE " + condition);
//                    if (dt.Rows.Count > 0)
//                        condResult = true;
//                }
//                if (condResult == true)
//                    return base.Execute(m_parser);
//                else if (condResult == false && m_ElseIfCmd != null)
//                    return m_ElseIfCmd.Execute(m_parser);

//                return "";
//            }
//            catch (Exception ex)
//            {
////                Trace.WriteLine(ex);
////                throw new Exception("IF command at line " + m_parser.GetLine(StartIndex).ToString() + " cannot evaluate the condition " + condition + "; " + newLine + ex.Message);
////                throw new Exception(ex.Message);
//                throw ex;
//            }
//            finally
//            {
//                //if (connector != null)
//                //    connector.Dispose();
//            }
//        }

	}
}
