CREATE TABLE tblPersoon
(
  PersoonID int IDENTITY(1,1) NOT NULL
  , PersoonNaam varchar(50) NULL
  ,	PersoonLeeftijd int NULL
  , CONSTRAINT PK_tblPersoon PRIMARY KEY CLUSTERED ( PersoonID ASC )WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE tblPersoonAdres
(
  AdresID int IDENTITY(1,1) NOT NULL
  , PersoonID int NULL
  ,	AdresStraat varchar(50) NULL
  ,	AdresHuisNr int NULL
  , CONSTRAINT PK_tblPersoonAdres PRIMARY KEY CLUSTERED ( AdresID ASC ) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO