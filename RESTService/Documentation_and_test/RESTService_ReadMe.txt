To install a new instance of the RESTService:

- Create a IIS virtual Directory. In IIS 6.0 go to the properties, tab "Virtual Directory", click on "Configuration...", tab "Mappings", click on "Insert" at the section "Wildcard application maps(....)", enter "C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\aspnet_isapi.dll" as executable and uncheck "Verify that file exists". Press 3x OK to save your changes.

- In the web.config, you need at least:

<configuration>
	<appSettings>
		<add key="XDocConnectionString" value="..." />
		<add key="XDocAttVirtualPath" value="Attachments"/>
		<add key="PassRequest" value="0" />
	</appSettings>
</configuration>

The key PassRequest can be set to 1 to pass the XML (posted to the service) to the target template as parameter #PAR.REQUEST#