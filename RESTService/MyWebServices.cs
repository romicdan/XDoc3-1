using System;
using System.Web;
using System.Xml;
using System.IO;
using XDocuments;
using XDataSourceModule;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;

namespace MyWebServices
{
    public class exampleRestService : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        private string sCache = "";
        public string Cache
        {
            get
            {
                if (sCache == "")
                {
                    string strCache = System.Configuration.ConfigurationManager.AppSettings["Cache"];
                    if (strCache != null)
                        sCache = strCache;
                    else sCache = "1";
                }
                return sCache;
            }
            set
            {
                sCache = value;
            }
        }

        private string GetParameterValue(string strParameters, string strKey)
        {
            //function used only by WriteLog to read LoginName and ApplicationName !
            strParameters = strParameters.Replace('?', '&');
            int intStart = strParameters.IndexOf("&" + strKey, StringComparison.CurrentCultureIgnoreCase);
            if (intStart == -1) return null;
            intStart = intStart + strKey.Length + 1 + 1; /* plus 1 for the ampersand and plus 1 for index fix */

            int intEnd = strParameters.IndexOf('&', intStart);
            if (intEnd == -1) intEnd = strParameters.Length;

            return strParameters.Substring(intStart, intEnd - intStart);
        }

        private string GetTemplateContentUC(string strTemplateName, Hashtable htParameters, ref string strDocType)
        {
            XDocumentsWebControls.XDoc xdocwebcontrol = new XDocumentsWebControls.XDoc();
            XDocManager xm = new XDocManager();
            XDocCache m_xdocCache = null;
            XDocCacheTemplates m_xdocCacheTemplates = null;
            if ((Cache != "0") && (HttpContext.Current != null) && (HttpContext.Current.Session != null)) m_xdocCache = (XDocCache)HttpContext.Current.Session["XDocCache"];
            if (Cache != "0") if (HttpContext.Current != null) if (HttpContext.Current.Application != null) m_xdocCacheTemplates = (XDocCacheTemplates)HttpContext.Current.Application["XDocCacheTemplates"];
            if (System.Configuration.ConfigurationManager.AppSettings["CacheTemplate"] != null) m_xdocCacheTemplates = (XDocCacheTemplates)HttpContext.Current.Application["XDocCacheTemplates"]; 
            if (m_xdocCache == null) m_xdocCache = new XDocCache();
            if (m_xdocCache != null) { xm.DocCache = m_xdocCache; }
            if (m_xdocCacheTemplates != null) { xm.DocCacheTemplates = m_xdocCacheTemplates; }
            //xm.DocCache =new XDocCache();
            if (HttpContext.Current != null) xm.DocCache.FillCookies(HttpContext.Current.Request.Cookies);
            strDocType = xm.LoadTemplate(strTemplateName).DocType;
            string result = xdocwebcontrol.GetHTML(strTemplateName, htParameters, HttpContext.Current);
            m_xdocCacheTemplates = xm.DocCacheTemplates;
            m_xdocCache = xm.DocCache;
            if (HttpContext.Current != null) if (HttpContext.Current.Application != null) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            if (HttpContext.Current != null) if (xm.DocCache != null) m_xdocCache.WriteCookies(HttpContext.Current.Response.Cookies);
            if ((HttpContext.Current != null) && (HttpContext.Current.Session != null)) HttpContext.Current.Session["XDocCache"] = m_xdocCache;
            xm.Dispose();
            return result;
        }
        private string GetTemplateContent(string strTemplateName, Hashtable htParameters, ref string strDocType)
        {
            //System.Diagnostics.Debug.WriteLine("LT1:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            XDocManager xm = new XDocManager();
            XDocCache m_xdocCache = null;
            XDocCacheTemplates m_xdocCacheTemplates = null;
            if ((Cache != "0") && (HttpContext.Current != null) && (HttpContext.Current.Session != null)) m_xdocCache = (XDocCache)HttpContext.Current.Session["XDocCache"];
            if (Cache != "0") if (HttpContext.Current != null) if (HttpContext.Current.Application != null) m_xdocCacheTemplates = (XDocCacheTemplates)HttpContext.Current.Application["XDocCacheTemplates"];
            if (m_xdocCache == null) m_xdocCache = new XDocCache();
            if (m_xdocCache != null) { xm.DocCache = m_xdocCache; }
            if (m_xdocCacheTemplates != null) { xm.DocCacheTemplates = m_xdocCacheTemplates; }
//            xm.DocCache = new XDocCache();
            if (HttpContext.Current != null) xm.DocCache.FillCookies(HttpContext.Current.Request.Cookies);
            //System.Diagnostics.Debug.WriteLine("LT2:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            IXTemplate xt = xm.LoadTemplate(strTemplateName);
            //System.Diagnostics.Debug.WriteLine("LT3:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            IXDocument xd = xt.ProcessTemplate(htParameters);
            //System.Diagnostics.Debug.WriteLine("LT4:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            strDocType = xt.DocType;
            m_xdocCacheTemplates = xm.DocCacheTemplates;
            m_xdocCache = xm.DocCache;
            if (HttpContext.Current != null) if (HttpContext.Current.Application != null) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            if (HttpContext.Current != null) if (xm.DocCache != null) xm.DocCache.WriteCookies(HttpContext.Current.Response.Cookies);
            if ((HttpContext.Current != null) && (HttpContext.Current.Session != null)) HttpContext.Current.Session["XDocCache"] = m_xdocCache;
            xm.Dispose();
            return xd.Content;
        }

		//private string GetResourceTemplateRoot(HttpRequest objHttpRequest)
		//{
		//    string strRequestUrl = objHttpRequest.RawUrl.Substring(objHttpRequest.ApplicationPath.Length).Replace('?', '&');
		//    string strTemplateName = "";
		//    string strPart = "";
		//    int intStart, intEnd, i;
		//    i = 1;

		//    while (strRequestUrl.IndexOf('/') != -1)
		//    {
		//        intStart = strRequestUrl.IndexOf('/');
		//        intEnd = strRequestUrl.IndexOf('/', intStart + 1);
		//        if (intEnd == -1) intEnd = strRequestUrl.Length;
		//        strPart = strRequestUrl.Substring(intStart + 1, intEnd - intStart - 1);
		//        if (strPart.IndexOf('&') != -1)
		//        {
		//            intEnd = strRequestUrl.IndexOf('&');
		//            strPart = strRequestUrl.Substring(intStart + 1, intEnd - intStart - 1);
		//        }
		//        if (i % 2 == 1 && strPart != "") strTemplateName += (strTemplateName != "" ? "_" : "") + strPart;
		//        else if (strPart != "") strTemplateName += "_ID";

		//        i++;
		//        strRequestUrl = strRequestUrl.Substring(intEnd - intStart);
		//    }
		//    return strTemplateName;
		//}

		private string GetURLParameters(HttpRequest objHttpRequest)
		{
            //function not used 
			string strRequestUrl = objHttpRequest.RawUrl.Substring(objHttpRequest.ApplicationPath.Length).Replace('?', '&');
			string strParameters = "";
			string strPart = "";
			int intStart, intEnd, i;
			i = 1;

			while (strRequestUrl.IndexOf('/') != -1)
			{
				intStart = strRequestUrl.IndexOf('/');
				intEnd = strRequestUrl.IndexOf('/', intStart + 1);
				if (intEnd == -1) intEnd = strRequestUrl.Length;
				strPart = strRequestUrl.Substring(intStart + 1, intEnd - intStart - 1);
				if (strPart.IndexOf('&') != -1)
				{
					intEnd = strRequestUrl.IndexOf('&');
					strPart = strRequestUrl.Substring(intStart + 1, intEnd - intStart - 1);
				}
				if (i % 2 == 1) strParameters += (i == 1 ? "" : "&") + strPart;
				else strParameters += "=" + strPart;
				i++;
				strRequestUrl = strRequestUrl.Substring(intEnd - intStart);
			}
			if (strRequestUrl.IndexOf('&') != -1) strParameters += strRequestUrl.Substring(strRequestUrl.IndexOf('&'));
			return strParameters;
		}

		private String GetVal(Hashtable objHashtable, String strKey)
		{
			if (objHashtable[strKey] == null)
				return "";
			else
				return objHashtable[strKey].ToString();
		}

        //private String GetQueryStringFromHashtable(Hashtable objHashtable, System.Text.Encoding enc)
        //{
        //    //System.Web.HttpUtility.UrlEncode(, context.Request.ContentEncoding)

        //    //string strReturn = "";
        //    //foreach (string strKey in objHashtable.Keys)
        //    //{
        //    //    if (strKey != "TemplateName")
        //    //        strReturn += "&" + System.Web.HttpUtility.UrlEncode(strKey,enc) + "=" + System.Web.HttpUtility.UrlEncode(GetVal(objHashtable,strKey),enc);
        //    //}
        //    //return strReturn;

        //    StringBuilder strReturn = new StringBuilder();
        //    foreach (string strKey in objHashtable.Keys)
        //    {
        //        if (strKey != "TemplateName")
        //            strReturn.Append("&" + System.Web.HttpUtility.UrlEncode(strKey, enc) + "=" + System.Web.HttpUtility.UrlEncode(GetVal(objHashtable, strKey), enc));
        //    }
        //    return strReturn.ToString();
        //}

		private Hashtable GetParameters(HttpRequest objHttpRequest)
		{
			/*
			 * Path				/RESTService/Handler
			 * QueryString		{ ApplicationName=tinyget }
			 * ApplicationPath	/RESTService
			 */
			Hashtable objHashtable = new Hashtable(StringComparer.InvariantCultureIgnoreCase);
			
			string strKey = "";

			string strTemplateName = "";

			foreach (string strPathPart in objHttpRequest.Path.Substring(objHttpRequest.ApplicationPath.Length).Split('/'))
			{
				if (strKey == "")
				{
					strKey = strPathPart;
					strTemplateName += (strTemplateName != "" ? "_" : "") + strPathPart;
				}
				else
				{
					objHashtable.Remove(strKey);
					objHashtable.Add(strKey, strPathPart);
					strTemplateName += "_ID";
					strKey = "";
				}
			}
			if (strKey != "")
			{
				objHashtable.Remove(strKey);
				objHashtable.Add(strKey, "");
			}

			objHashtable.Remove("TemplateName");
			objHashtable.Add("TemplateName", strTemplateName);

			foreach (string strQueryKey in objHttpRequest.QueryString.Keys)
			{
				if (strQueryKey != null && strQueryKey != "")
				{
					objHashtable.Remove(strQueryKey);
					objHashtable.Add(strQueryKey, objHttpRequest.QueryString[strQueryKey]);
				}
			}
			//objHttpRequest.QueryString.CopyTo(objHashtable, 0);
			

			return objHashtable;
		}



        //private DataTable DTPostedData(StreamReader postedData)
        //{
        //    if (postedData != null )
        //    {
        //        XmlReaderSettings settings = new XmlReaderSettings();
        //        settings.ProhibitDtd = true; /* true to disable inline DTDs completely */
        //        settings.XmlResolver = null; /* XmlResolver objects are used to resolve external references, including external entities */
        //        XmlReader reader = XmlReader.Create(postedData, settings);

        //        /* Put the response in an XML document */
        //        XmlDocument objXmlDocumentPosted = new XmlDocument();
        //        objXmlDocumentPosted.Load(reader);

        //        /* Build a DataTable from the XML and return it */
        //        DataSet objDataSet = new DataSet();
        //        using (XmlReader objXmlReader = new XmlNodeReader(objXmlDocumentPosted.DocumentElement))
        //        {
        //            objDataSet.ReadXml(objXmlReader);
        //            objXmlReader.Close();
        //        }

        //        if (objDataSet.Tables.Count > 0)
        //            return objDataSet.Tables[0];
        //    }
        //    return null;
        //}


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        private DataTable DTPostedData(String postedData)
		{
			if (postedData != "")
			{

				/* Put the response in an XML document */
				XmlDocument objXmlDocumentPosted = new XmlDocument();
				objXmlDocumentPosted.LoadXml(postedData);

				/* Build a DataTable from the XML and return it */
				DataSet objDataSet = new DataSet();
				using (XmlReader objXmlReader = new XmlNodeReader(objXmlDocumentPosted.DocumentElement))
				{
					objDataSet.ReadXml(objXmlReader);
					objXmlReader.Close();
				}

				if (objDataSet.Tables.Count > 0)
					return objDataSet.Tables[0];
			}
			return null;
		}

		private string GetContentType(string strDocType, string strView)
		{
			/* Determine the content type */
			/* ?view= should affect the content-type returned, set by the web.config */
			/* Can only set the contentType after calling this.GetTemplateContentUC, because adding XDoc uc to the current context, it will set the contentType */

			/* DocType is comming from the template attributes */
			if (strView == null && strDocType != null && strDocType != "")
			{
				if (strDocType.StartsWith(".")) strDocType = strDocType.Substring(1);
				strView = strDocType.ToLower();
			}

			if (strView == null) strView = "default";

			/* Read the content type from the web.config */
			if (ConfigurationManager.AppSettings[strView] != null && ConfigurationManager.AppSettings[strView] != "")
			{
				return ConfigurationManager.AppSettings[strView].ToString();
			}
			else
			{
				return "text/xml; charset=utf-8";
			}
		}

		private string GetEncodingError(string strResponseContentType, System.Text.Encoding objResponseEncoding)
		{
			if (strResponseContentType.IndexOf("charset=") != -1)
			{
				int intStart = strResponseContentType.IndexOf("charset=") + 8;
				int intEnd = (strResponseContentType + ";").IndexOf(";", intStart);
				if (intEnd != -1)
				{
					string strCharSet = strResponseContentType.Substring(intStart, intEnd - intStart);
					System.Text.Encoding charsetEncoding = null;
					try
					{
						charsetEncoding = System.Text.Encoding.GetEncoding(strCharSet);
					}
					catch (Exception e)
					{
					}

					if (!objResponseEncoding.Equals(charsetEncoding))
					{
						return "Response charset " + strCharSet + " does not match content encoding " + objResponseEncoding.WebName;
					}
				}
			}
			return null;
		}

		public void ProcessRequest(HttpContext context)
		{
			//string strResourceTemplateRoot = this.GetResourceTemplateRoot(context.Request);

			Hashtable htParams = this.GetParameters(context.Request);

			/* check for batch operation on put method */
			if (context.Request.HttpMethod.Equals("put", StringComparison.CurrentCultureIgnoreCase) && ! GetVal(htParams,"TemplateName").EndsWith("_id", StringComparison.CurrentCultureIgnoreCase))
			{
				this.ProcessRequestBatch(context, htParams);
			}
			else
			{
				this.ProcessRequestSingle(context, htParams);
			}
			//optional for batch check: 
			//if (objXmlDocumentPosted.DocumentElement.Name.Equals("servicerequestlist", StringComparison.CurrentCultureIgnoreCase))
		}

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public void ProcessRequestSingle(HttpContext context, Hashtable htParams)
		{
			string strTemplateName = GetVal(htParams,"TemplateName") + "_" + context.Request.HttpMethod;

            //System.Diagnostics.Debug.WriteLine("BEG:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            /* Needed for logging: */
			DateTime dtStart = DateTime.Now;
			string strLogError = "";

			string strBody = "";
			string strDocType = "";

			/**
			 * 
			 * Template structure:
			 * GET      /{object}/                  call get{object}List()
			 * GET      /{object}/{id}              call get{object}({id})
			 * ?? GET      /{object1}/{id}/{object2}/  call get{object2}ListFor{object1}({id}) - information should be given in previous request already
			 * POST     /{object}/                  call insert{object}(postData)
			 * POST     /{object}/{id}              call update{object}({id}, postData)
			 * DELETE   /{object}/{id}              call delete{object}({id})
			 * 
			 **/

			string strMethod = context.Request.HttpMethod;

			/* Get all parameters */
			//string strParameters = this.GetURLParameters(context.Request);

			/* Read posted data into parameters */
			StreamReader reader = new StreamReader(context.Request.InputStream, context.Request.ContentEncoding);
            string postedData = reader.ReadToEnd().Trim();
            //string postedData = "";
            DataTable objDataTable = null;
			try
			{
                objDataTable = this.DTPostedData(postedData);
                //objDataTable = this.DTPostedData(reader );
            }
			catch (Exception e)
			{
				strBody = this.GetErrorXml("Failed to create an XML document. Error: " + e.Message, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);
				strLogError = "Failed to create an XML document. Error: " + e.Message;
				this.WriteLog(context, strTemplateName, htParams, strMethod, postedData, strBody, dtStart, strLogError);
				context.Response.Write(strBody);
				return;
			}
			if (objDataTable!=null)
			{
				foreach (DataRow objDataRow in objDataTable.Rows)
				{
					for (int colNr = 0; colNr < objDataRow.ItemArray.Length; colNr++)
					{
						//strParameters += "&" + System.Web.HttpUtility.UrlEncode(objDataTable.Columns[colNr].ColumnName, context.Request.ContentEncoding) + "=" + System.Web.HttpUtility.UrlEncode(objDataRow.ItemArray[colNr].ToString(), context.Request.ContentEncoding);
						htParams.Remove(objDataTable.Columns[colNr].ColumnName);
						htParams.Add(objDataTable.Columns[colNr].ColumnName, objDataRow.ItemArray[colNr].ToString());
					}
				}
			}

			if (ConfigurationManager.AppSettings["PassRequest"] != null && ConfigurationManager.AppSettings["PassRequest"].ToString() == "1")
			{
				//strParameters += "&REQUESTMETHOD=" + System.Web.HttpUtility.UrlEncode(strMethod, context.Request.ContentEncoding);
				//strParameters += "&REQUESTURL=" + System.Web.HttpUtility.UrlEncode(context.Request.Url.AbsoluteUri, context.Request.ContentEncoding);

				htParams.Remove("REQUESTMETHOD");
				htParams.Add("REQUESTMETHOD", strMethod);
				htParams.Remove("REQUESTURL");
				htParams.Add("REQUESTURL", context.Request.Url.AbsoluteUri);

				string strXml = "";
				if (objDataTable != null)
				{
					XmlDocument doc = new XmlDocument();
					using (XmlWriter writer = doc.CreateNavigator().AppendChild())
					{
						objDataTable.WriteXml(writer, XmlWriteMode.IgnoreSchema);
						writer.Close();
					}
					XmlNode objXmlNodeMethod = doc.DocumentElement.SelectSingleNode("method");
					if (objXmlNodeMethod != null)
						objXmlNodeMethod.ParentNode.RemoveChild(objXmlNodeMethod);
					strXml = doc.DocumentElement.InnerXml;
				}
				//strParameters += "&REQUESTXML=" + System.Web.HttpUtility.UrlEncode(strXml, context.Request.ContentEncoding);
				htParams.Remove("REQUESTXML");
				htParams.Add("REQUESTXML", strXml);
			}

			//strParameters += "&ENCODING=" + context.Response.ContentEncoding.WebName;
			htParams.Remove("ENCODING");
			htParams.Add("ENCODING", context.Response.ContentEncoding.WebName);

            //System.Diagnostics.Debug.WriteLine("BTC:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));

			try
			{
				if (GetVal(htParams, "uc").ToLower() == "true" || GetVal(htParams,"uc") == "1")
				{
                    strBody = this.GetTemplateContentUC(strTemplateName, htParams, ref strDocType);
				}
				else
				{
					strBody = this.GetTemplateContent(strTemplateName, htParams, ref strDocType);
				}
			}
			catch (Exception e)
			{
				strLogError = e.Message;
				strBody = this.GetErrorXml(e.Message, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);
			}
            //System.Diagnostics.Debug.WriteLine("GTC:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            context.Response.ContentType = this.GetContentType(strDocType, GetVal(htParams, "view"));
			string strEncodingError = this.GetEncodingError(context.Response.ContentType, context.Response.ContentEncoding);
			if(strEncodingError!=null)
				strBody = this.GetErrorXml(strEncodingError, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);

			this.WriteLog(context, strTemplateName, htParams, strMethod, postedData, strBody, dtStart, strLogError);
			context.Response.Write(strBody);
            //System.Diagnostics.Debug.WriteLine("END:" + strTemplateName + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public void ProcessRequestBatch(HttpContext context, Hashtable htParams)
		{
			/* Needed for logging: */
			DateTime dtStart = DateTime.Now;
			string strLogError = "";

			string strBody = "";
			string strDocType = "";

			/**
			 * 
			 * Template structure:
			 * GET      /{object}/                  call get{object}List()
			 * GET      /{object}/{id}              call get{object}({id})
			 * ?? GET      /{object1}/{id}/{object2}/  call get{object2}ListFor{object1}({id}) - information should be given in previous request already
			 * POST     /{object}/                  call insert{object}(postData)
			 * POST     /{object}/{id}              call update{object}({id}, postData)
			 * DELETE   /{object}/{id}              call delete{object}({id})
			 * 
			 **/
			string strMethod = context.Request.HttpMethod;
			//string strParameters = this.GetURLParameters(context.Request);

			StreamReader reader = new StreamReader(context.Request.InputStream, context.Request.ContentEncoding);
			string postedData = reader.ReadToEnd().Trim();

			DataTable objDataTable = null;
			try
			{
				objDataTable = this.DTPostedData(postedData);
			}
			catch (Exception e)
			{
				strBody = this.GetErrorXml("Failed to create an XML document. Error: " + e.Message, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);
				strLogError = "Failed to create an XML document. Error: " + e.Message;

				this.WriteLog(context, GetVal(htParams,"TemplateName"), htParams, strMethod, postedData, strBody, dtStart, strLogError);
				context.Response.Write(strBody);
				return;
			}

			if (objDataTable != null)
			{
				foreach (DataRow objDataRow in objDataTable.Rows)
				{
					//string strCommandParameters = ""; //setting to strParameters gives problem for adding resourcenames
					//strCommandParameters = strParameters;
					Hashtable htCommandParameters = (Hashtable)htParams.Clone();


					for (int colNr = 0; colNr < objDataRow.ItemArray.Length; colNr++)
					{
						//if (("&" + strParameters + "=").IndexOf("&" + objDataTable.Columns[colNr].ColumnName + "=", StringComparison.InvariantCultureIgnoreCase) != -1)
						//    strParameters.Replace("&" + objDataTable.Columns[colNr].ColumnName + "=", "&_" + objDataTable.Columns[colNr].ColumnName + "=");

						//strCommandParameters += "&" + System.Web.HttpUtility.UrlEncode(objDataTable.Columns[colNr].ColumnName, context.Request.ContentEncoding) + "=" + System.Web.HttpUtility.UrlEncode(objDataRow.ItemArray[colNr].ToString(), context.Request.ContentEncoding);
						if (!objDataRow.IsNull(colNr))
						{
							htCommandParameters.Remove(objDataTable.Columns[colNr].ColumnName);
							htCommandParameters.Add(objDataTable.Columns[colNr].ColumnName, objDataRow.ItemArray[colNr].ToString());
						}

					}

					//string[] arrParameters = 

					//string strRequestMethod = this.GetParameterValue(strCommandParameters, "method");
					string strRequestMethod = GetVal(htCommandParameters,"method");
					string strTemplateName = GetVal(htParams,"TemplateName") + "_" + strRequestMethod;

					if (ConfigurationManager.AppSettings["PassRequest"] != null && ConfigurationManager.AppSettings["PassRequest"].ToString() == "1")
					{
						//strCommandParameters += "&REQUESTMETHOD=" + System.Web.HttpUtility.UrlEncode(strRequestMethod, context.Request.ContentEncoding);
						//strCommandParameters += "&REQUESTURL=" + System.Web.HttpUtility.UrlEncode(context.Request.Url.AbsoluteUri, context.Request.ContentEncoding);
						htCommandParameters.Remove("REQUESTMETHOD");
						htCommandParameters.Add("REQUESTMETHOD", strRequestMethod);
						htCommandParameters.Remove("REQUESTURL");
						htCommandParameters.Add("REQUESTURL", context.Request.Url.AbsoluteUri);

						string strXml = "";
						if (objDataTable != null)
						{
							DataTable objCommandDataTable = objDataTable.Clone();
							objCommandDataTable.ImportRow(objDataRow);
							XmlDocument doc = new XmlDocument();
							using (XmlWriter writer = doc.CreateNavigator().AppendChild())
							{
								objCommandDataTable.WriteXml(writer, XmlWriteMode.IgnoreSchema);
								writer.Close();
							}
							XmlNode objXmlNodeMethod = doc.DocumentElement.FirstChild.SelectSingleNode("method");
							if (objXmlNodeMethod != null)
								objXmlNodeMethod.ParentNode.RemoveChild(objXmlNodeMethod);
							strXml = doc.DocumentElement.FirstChild.InnerXml;
						}
						//strCommandParameters += "&REQUESTXML=" + System.Web.HttpUtility.UrlEncode(strXml, context.Request.ContentEncoding);
						htCommandParameters.Remove("REQUESTXML");
						htCommandParameters.Add("REQUESTXML", strXml);
					}

					//strCommandParameters += "&ENCODING=" + context.Response.ContentEncoding.WebName;
					htCommandParameters.Remove("ENCODING");
					htCommandParameters.Add("ENCODING", context.Response.ContentEncoding.WebName);

					try
					{
						if (GetVal(htCommandParameters, "uc").ToLower() == "true" || GetVal(htCommandParameters,"uc") == "1")
						{
                            strBody += this.GetTemplateContentUC(strTemplateName, htCommandParameters, ref strDocType);
						}
						else
						{
							strBody += this.GetTemplateContent(strTemplateName, htCommandParameters, ref strDocType);
						}
					}
					catch (Exception e)
					{
						strLogError = e.Message;
						strBody += this.GetErrorXml(e.Message, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, false);
					}
				}
			}

			context.Response.ContentType = this.GetContentType(strDocType, GetVal(htParams,"view"));
			string strEncodingError = this.GetEncodingError(context.Response.ContentType, context.Response.ContentEncoding);
			if (strEncodingError != null)
				strBody = this.GetErrorXml(strEncodingError, strMethod, context.Request.Url.AbsoluteUri, postedData, context.Response.ContentEncoding.WebName, true);


			this.WriteLog(context, GetVal(htParams,"TemplateName"), htParams, strMethod, postedData, strBody, dtStart, strLogError);
			context.Response.Write("<serviceresponseList>" + strBody + "</serviceresponseList>");
		}

        //private static string ReplaceI(string source, string oldValue, string newValue)
        //{
        //    //function not used
        //    if (newValue == null) newValue = "";
        //    return System.Text.RegularExpressions.Regex.Replace(source, System.Text.RegularExpressions.Regex.Escape(oldValue), newValue, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //}

		private void WriteLog(HttpContext context, string strTemplateName, Hashtable htParameters, string strMethod, string postedData, string strBody, DateTime dtStart, string strLogError)
		{
            if (!((context.Request.QueryString["LogRequest"] != null) && (context.Request.QueryString["LogRequest"] == "1")))
            {

                // why CASE in sql and not an if in C#??
                if (ConfigurationManager.AppSettings["Log"] == null || ConfigurationManager.AppSettings["Log"] != "1" || ConfigurationManager.AppSettings["XDocConnectionString"] == null || ConfigurationManager.AppSettings["XDocConnectionString"] == "")
                    return;
            }
            if ((context.Request.QueryString["LogRequest"] != null) && (context.Request.QueryString["LogRequest"] == "0")) 
                return;
            if ((context.Request.QueryString["LogRequestXML"] != null) && (context.Request.QueryString["LogRequestXML"] == "0")) postedData = "[no log]";
            if ((context.Request.QueryString["LogResponseXML"] != null) && (context.Request.QueryString["LogResponseXML"] == "0")) strBody = "[no log]";

			try
			{
				/* LoginName, first try by parameter. If not set, try with integrated identity */
                string strLoginName = System.Web.HttpUtility.UrlDecode(this.GetVal(htParameters, "LoginName"), context.Request.ContentEncoding);
				if (strLoginName == null) strLoginName = "";

				string strUserName = context.Request.ServerVariables["REMOTE_USER"];
				if (strUserName == null) strUserName = "";

                //string LogParameters = "";
                //LogParameters += "ApplicationName=" + this.GetParameterValue(strParameters, "ApplicationName");
                //LogParameters += "&LoginName=" + System.Web.HttpUtility.UrlEncode(strLoginName, System.Text.Encoding.Default);
                //LogParameters += "&UserName=" + System.Web.HttpUtility.UrlEncode(strUserName, System.Text.Encoding.Default);
                //LogParameters += "&RequestURL=" + System.Web.HttpUtility.UrlEncode(context.Request.Url.AbsoluteUri, System.Text.Encoding.Default);
                //LogParameters += "&RequestMethod=" + System.Web.HttpUtility.UrlEncode(strMethod, System.Text.Encoding.Default);
                //LogParameters += "&ResourceTemplate=" + System.Web.HttpUtility.UrlEncode(strTemplateName, System.Text.Encoding.Default);

				KubionDataNamespace.ClientData objClientData = new KubionDataNamespace.ClientData(ConfigurationManager.AppSettings["XDocConnectionString"]);
				string strQueryText = @"INSERT INTO tblLog(RequestMethod, RequestURL, RequestXML, ResponseXML, ApplicationName, StartTime, EndTime, Duration, LoginName, UserName, UserAgent, UserHostAddress, UserHostName, ResourceTemplate, Error)
	VALUES
	(
	  CASE WHEN '" + L_String(strMethod) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strMethod) + @"',1,10) END
	  , CASE WHEN '" + L_String(context.Request.Url.AbsoluteUri) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.Url.AbsoluteUri) + @"',1,500) END
	  , CASE WHEN '" + L_String(postedData) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(postedData) + @"',1,8000) END
	  , CASE WHEN '" + L_String(strBody) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strBody) + @"',1,8000) END
	  , CASE WHEN '" + L_String(System.Web.HttpUtility.UrlDecode(this.GetVal(htParameters, "ApplicationName"), context.Request.ContentEncoding)) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(System.Web.HttpUtility.UrlDecode(this.GetVal(htParameters, "ApplicationName"), context.Request.ContentEncoding)) + @"',1,50) END
	  , CASE WHEN '" + dtStart.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' = '' THEN NULL ELSE '" + dtStart.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' END
	  , CASE WHEN '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' = '' THEN NULL ELSE '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' END
	  , " + ((int)DateTime.Now.Subtract(dtStart).TotalMilliseconds).ToString() + @"
	  , CASE WHEN '" + L_String(strLoginName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strLoginName) + @"',1,50) END
	  , CASE WHEN '" + L_String(strUserName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strUserName) + @"',1,50) END
	  , CASE WHEN '" + L_String(context.Request.UserAgent) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.UserAgent) + @"',1,500) END
	  , CASE WHEN '" + L_String(context.Request.UserHostAddress) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.UserHostAddress) + @"',1,50) END
	  , CASE WHEN '" + L_String(context.Request.UserHostName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(context.Request.UserHostName) + @"',1,50) END
	  , CASE WHEN '" + L_String(strTemplateName) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strTemplateName) + @"',1,100) END
	  , CASE WHEN '" + L_String(strLogError) + "' = '' THEN NULL ELSE SUBSTRING('" + L_String(strLogError) + @"',1,8000) END
	)";

				/* Run the query */
				string logResult = objClientData.ExecuteNonQuery(strQueryText);
				objClientData.Dispose();
			}
			catch (Exception e) { }
		}

		private string L_String(string strString)
		{
			if (strString == null) return "";
			else return strString.Replace("'", "''");
		}

        //what is this?
        public bool IsReusable 
         {
            get { return false; }
        }

        private string GetErrorXml(string strError, string strMethod, string strUrl, string strRequestXml, string strEncoding, bool blnXmlDeclaration)
        {
            //must be an much easyer way to build that string
			try
			{
				StringWriter sw = new StringWriter();

				XmlDocument objXmlDocument = new XmlDocument();
				if(blnXmlDeclaration)
					objXmlDocument.AppendChild(objXmlDocument.CreateXmlDeclaration("1.0", strEncoding, null));

				XmlNode node_response = objXmlDocument.CreateElement("serviceresponse");
				objXmlDocument.AppendChild(node_response);

				XmlNode node_message = objXmlDocument.CreateElement("message");
				node_message.AppendChild(objXmlDocument.CreateTextNode("Method not implemented"));
				node_response.AppendChild(node_message);

				XmlNode node_error = objXmlDocument.CreateElement("error");
				node_error.AppendChild(objXmlDocument.CreateTextNode(strError));
				node_response.AppendChild(node_error);

                if (ConfigurationManager.AppSettings["PassRequest"] != null && ConfigurationManager.AppSettings["PassRequest"].ToString() == "1")
                {
                    XmlNode node_method = objXmlDocument.CreateElement("method");
                    node_method.AppendChild(objXmlDocument.CreateTextNode(strMethod));
                    node_response.AppendChild(node_method);

                    XmlNode node_url = objXmlDocument.CreateElement("url");
                    node_url.AppendChild(objXmlDocument.CreateTextNode(strUrl));
                    node_response.AppendChild(node_url);

                    XmlNode node_xml = objXmlDocument.CreateElement("requestxml");
                    //node_xml.AppendChild(objXmlDocument.CreateCDataSection(strRequestXml));
                    node_xml.AppendChild(objXmlDocument.CreateTextNode(HttpUtility.HtmlEncode(strRequestXml)));
                    node_response.AppendChild(node_xml);
                }

				XmlTextWriter xw = new XmlTextWriter(sw);
				xw.Formatting = Formatting.Indented;
				objXmlDocument.WriteTo(xw);
				return sw.ToString();
			}
			catch (Exception e)
			{
				StringWriter sw2 = new StringWriter();

				XmlDocument objXmlDocument = new XmlDocument();
				objXmlDocument.AppendChild(objXmlDocument.CreateXmlDeclaration("1.0", "utf-8", null));

				XmlNode node_response = objXmlDocument.CreateElement("serviceresponse");
				objXmlDocument.AppendChild(node_response);

				XmlNode node_error = objXmlDocument.CreateElement("error");
				node_error.AppendChild(objXmlDocument.CreateTextNode("Error in creating Error XML: " + HttpUtility.HtmlEncode(e.Message)));
				node_response.AppendChild(node_error);

				XmlTextWriter xw = new XmlTextWriter(sw2);
				xw.Formatting = Formatting.Indented;
				objXmlDocument.WriteTo(xw);
				return sw2.ToString();
			}
        }
	}
}