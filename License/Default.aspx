﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Kubion License</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="Label1" runat="server" Height="14px" Text="License" Width="146px"></asp:Label>
        <asp:TextBox ID="txtLicense" runat="server" Width="427px">...klant... - IRIS</asp:TextBox><br />
        <asp:Label ID="Label2" runat="server" Height="14px" Text="Connection" Width="146px"></asp:Label>
        <asp:TextBox ID="txtConnString" runat="server" Width="1200px"></asp:TextBox><br />
        <asp:Label ID="Label3" runat="server" Height="14px" Text="XDocuments" Width="146px"></asp:Label>
        <asp:TextBox ID="txtEncrypt" runat="server" Width="1200px"></asp:TextBox><br />
        <hr />
        <asp:Button ID="btnEncrypt" runat="server" Text="Encrypt" OnClick="btnEncrypt_Click" />
        <asp:Button ID="btnDecrypt" runat="server" Text="Decrypt" OnClick="btnDecrypt_Click" /><br />
        <hr />
        <asp:TextBox ID="txtResult" runat="server" Height="277px" Width="100%" Rows="5" TextMode="MultiLine"></asp:TextBox></div>
        <hr />
        
        
    </form>
</body>
</html>
