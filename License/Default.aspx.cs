using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Security.Cryptography;
using KubionDataNamespace;
using System.Security.Principal;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    //public string Show()
    //{
    //    Console.ReadLine();
    //        string source = "Password=12345;Persist Security Info=True;User ID=sa;Initial Catalog=IRIS3_admin_Apps_dev2;Data Source=IRISONTWIKKEL";
    //        string m_strLicense = "Kubion - ONTWIKKEL";
    //        string sKey = "Kubion" + m_strLicense + "Chessm@sterChessm@ster";
    //        sKey = sKey.Substring(3, 24);
    //        string m_strConnString = Encrypt(source, sKey);
    //        Console.WriteLine(@"<add key=""XDocuments"" value=""{0}""/>", m_strConnString);
    //        System.Diagnostics.Debug.WriteLine("");
    //        System.Diagnostics.Debug.WriteLine("<add key=\"XDocuments\" value=\"" + m_strConnString + "\"/>");
    //        System.Diagnostics.Debug.WriteLine("");
    //        Console.ReadLine();

    //        string source1 = "User ID=sa;Password=12345;Persist Security Info=True;Initial Catalog=IRIS3_admin_Apps_dev2;Data Source=IRISONTWIKKEL";
    //        string skey = "kubion123456789123456789";
    //        Console.WriteLine("1.{0} ", source);
    //        string source2 = Encrypt(source, skey);
    //        Console.WriteLine("2.{0} ", source2);
    //        string source3 = Decrypt(source2, skey);
    //        Console.WriteLine("3.{0} ", source3);
    //        Console.WriteLine("1.{0} ", source1);
    //        source2 = Encrypt(source1, skey);
    //        Console.WriteLine("2.{0} ", source2);
    //        source3 = Decrypt(source2, skey);
    //        Console.WriteLine("3.{0} ", source3);
    //        Console.ReadLine();

    //    }

   public static string Encrypt(string input, string key)
   {
      byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
      TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
      tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
      tripleDES.Mode = CipherMode.ECB;
      tripleDES.Padding  = PaddingMode.PKCS7;
      ICryptoTransform cTransform = tripleDES.CreateEncryptor();
      byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
      tripleDES.Clear();
      return Convert.ToBase64String(resultArray, 0, resultArray.Length);
   }

    public void Log(string op,string lic, string con, string enc)
    {
        string m_strConnString;
        string m_strLicense, m_strConnStringEncrypted;
            
        m_strLicense = ConfigurationManager.AppSettings["License"];
        m_strConnStringEncrypted = ConfigurationManager.AppSettings["XDocuments"];
        if (m_strLicense != null && m_strLicense != "" && m_strConnStringEncrypted != null && m_strConnStringEncrypted != "")
        {
            string sKey = "Kubion" + m_strLicense + "Chessm@sterChessm@ster";
            sKey = sKey.Substring(3, 24);
            m_strConnString = Decrypt(m_strConnStringEncrypted, sKey);
        }
        else
        {
            m_strConnString = ConfigurationManager.AppSettings["XDocConnectionString"];
            if (m_strConnString == null || m_strConnString == "")
                throw new ApplicationException("The XDocuments connection string is not present in the config file");
        }
        string strUserName = Request.ServerVariables["REMOTE_USER"];
        if (strUserName == null) strUserName = "";


        ClientData m_clientData = new ClientData(m_strConnString);
        string sTrace = @"IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LLog]') AND type in (N'U'))
CREATE TABLE [dbo].[LLog](
	[LLogID] [int] IDENTITY(1,1) NOT NULL,
	[operation] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[license] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[connection] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[encryption] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserName] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TraceDateTime] [datetime] NULL CONSTRAINT [DF_LLog_TraceDateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_LLog] PRIMARY KEY CLUSTERED 
(
	[LLogID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]";

        sTrace += " insert into LLog (operation,license,connection,encryption, username) values ('" + op + "','" + lic.Replace("'", "''") + "','" + con.Replace("'", "''") + "','" + enc.Replace("'", "''") + "','" +  strUserName + "' )";
        m_clientData.ExecuteNonQuery(sTrace);

    }
   public static string Decrypt(string input, string key)
   {
      byte[] inputArray = Convert.FromBase64String(input);
      TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
      tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
      tripleDES.Mode = CipherMode.ECB;
      tripleDES.Padding = PaddingMode.PKCS7;
      ICryptoTransform cTransform = tripleDES.CreateDecryptor();
      byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
      tripleDES.Clear();
      return UTF8Encoding.UTF8.GetString(resultArray);
   }

    protected void btnEncrypt_Click(object sender, EventArgs e)
    {
        string source = "Integrated Security=SSPI;Initial Catalog=IRIS3_Client_Apps_Dev2;Data Source=irisontwikkel";
        string strLicense = "Kubion - ONTWIKKEL";
        strLicense = txtLicense.Text;
        source = txtConnString.Text;
        string sKey = "Kubion" + strLicense + "Chessm@sterChessm@ster";
        sKey = sKey.Substring(3, 24);
        string strEncrypt = Encrypt(source, sKey);
        txtEncrypt.Text = strEncrypt;
        txtResult.Text = "<add key=\"License\" value=\"" + strLicense + "\"/>";
        txtResult.Text += "\r\n";
        txtResult.Text += "<add key=\"XDocuments\" value=\"" + strEncrypt + "\"/>";
        Log("encrypt", strLicense,  source,strEncrypt );
    }
    protected void btnDecrypt_Click(object sender, EventArgs e)
    {
        string source = "Ed3e8aPCJO08gMd2RN0R7mwvdCVNPbZBXjNuch3boTa4nUZs5w2WMO3DPA5XkTA09bQ4VpjjtgrLmKdd10DcWnwF8eA+DwsxAgVQcseff6+rToq11JzUrlV+USqS9auo";
        string strLicense = "Kubion - ONTWIKKEL";
        strLicense = txtLicense.Text;
        source = txtEncrypt.Text;
        string sKey = "Kubion" + strLicense + "Chessm@sterChessm@ster";
        sKey = sKey.Substring(3, 24);
        string strDecrypt = Decrypt(source, sKey);
        txtConnString.Text = strDecrypt;
        txtResult.Text = strDecrypt;
        Log("decrypt", strLicense, strDecrypt, source);

    }
}
