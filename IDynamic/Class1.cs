using System;
using System.Collections;

namespace IDynamic
{

    public interface IDynamicUC
    {
        void SetParameters(Hashtable parameters);
        event DynamicUCEventHandler DynamicUCEvent;
    }

    public class DynamicUCEventArgs : EventArgs
    {
        string m_eventName = "";
        object m_eventParameter = null;

        public string EventName
        {
            get { return m_eventName; }
            set { m_eventName = value; }
        }

        public object EventParameter
        {
            get { return m_eventParameter; }
            set { m_eventParameter = value; }
        }

        public DynamicUCEventArgs() { }

        public DynamicUCEventArgs(string eventName, object eventParameter)
        {
            m_eventName = eventName;
            m_eventParameter = eventParameter;
        }

    }

    public delegate void DynamicUCEventHandler(object sender, DynamicUCEventArgs e);

}
