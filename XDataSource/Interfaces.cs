#region Using
using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
#endregion

namespace XDataSourceModule
{
	/// <sumary>
	/// IXDataSource interface 
	/// Provides mechanism to interogate a certain filed value for a given context
	/// </sumary>
	public interface IXDataSource
	{
		/// <summary>
		/// Initizalize IXDataSource
		/// </summary>
		/// <param name="templateParams">Values of template parameters</param>
        void InitializeEvaluator(int templateID, XProviderData dataProvider, Hashtable templateParams);

        XDocCache DocCache
        {
            get;
            set;
        }
        XDocCacheTemplates DocCacheTemplates
        {
            get;
            set;
        }

        XProviderData DataProvider
        {
            get;
            set;
        }
        string ParserTrace
        {
            get;
            set;
        }
        string ParserQueryLog
        {
            get;
            set;
        }


        /// <summary>
		/// Evaluate a query and return number of rows returned by given query and parameters
		/// </summary>
		/// <param name="SourceName">Query Name</param>
		/// <param name="Params">Array of parameters' values</param>
		/// <returns></returns>
        string GetResponse(XProviderData dataProvider, string strSQL, string connName);
        int EvaluateSource(int TemplateID, string SourceName, Array Params, out string sError, out string sErrorVerbose, out string sParameters, out double dDuration, out string key);
        SourceResult EvaluateSourceResult(int TemplateID, string SourceName, Array Params, out string sError, out string sErrorVerbose, out string sParameters, out double dDuration, out string key);

		/// <summary>
		/// Return value of a field
		/// </summary>
		/// <param name="fieldSource">Field Name from template</param>
		/// <param name="source">Source name (query)</param>
		/// <param name="sourceContext">Array with source parameters values.</param>
		/// <param name="Index">Index of value required</param>
		/// <param name="Value">Returned field' value</param>
		bool    GetValue(string fieldSource, string source, Array sourceContext, int Index, out object Value, out string error);

		/// <summary>
		/// Return value of a field that is not depending on context(Template Parameters)
		/// </summary>
		/// <param name="fieldSource">Field Name</param>
		/// <param name="Value">Return field' value</param>
		void    GetValue(string fieldSource, out object Value);

        string GetIncludeOnce();
        bool RequestIncludeOnce(string sTemplateName);
        void ClearIncludeOnce();
        void ClearCache();
        void ClearTemplatesCache();
        string GetCfg(string sVarName, string sDefault);
        string GetSV(string sVarName, string sContextName, string sDefault);
        void SetSV(string sVarName, string sContextName, string sVal);
        void DelSV(string sVarName, string sContextName, bool defrom);
        void CommitSV();

        Object RequestTemplateTree(int templateID, ref string sTemplateText);
        void AddTemplateTree(int templateID, Object templateRoot, string sTemplateText);
        string RequestTemplateText(int templateID);
        void AddTemplateText(int templateID, string sTemplateText);
        int RequestTemplateIDForInclude(string templateName);
        void AddTemplateIDForInclude(string templateName, int  iTemplateID); 

        event SourceResult.RequestHierarcyEventHandler RequestHierarchy;
	}

    //public interface IXDocCache
    //{

    //    string GetCfg(string sVarName, string sDefault);
    //    string GetSV(string sVarName, string sContextName, string sDefault);
    //    void SetSV(string sVarName, string sContextName, string sVal);
    //    void DelSV(string sVarName, string sContextName, bool defrom);
    //    void CommitSV();

    //    string RequestTemplateText(int templateID);
    //    void AddTemplateText(int templateID, string sTemplateText);
    //    int RequestTemplateIDForInclude(string templateName);
    //    void AddTemplateIDForInclude(string templateName, int iTemplateID);

    //}

	/// <sumary>
	/// IXDataUpdate interface 
	/// </sumary>
	public interface IXDataUpdate
	{
		/// <summary>
		/// Initialize IXDataUpdate
		/// </summary>
        /// <param name="returnParameters">Map of initial parameter values</param>
		void Initialize(int templateID, XProviderData dataProvider, Hashtable returnParameters);
		
		/// <summary>
		/// Add a field to update command
		/// </summary>
		/// <param name="updateName">Update command name</param>
		/// <param name="arrConditions">Context of update(parameters)</param>
		/// <param name="arrUpdParams">Array of parameters that should be updated</param>
		/// <param name="fieldName">Update field name</param>
		/// <param name="fieldValue">Value of field</param>
		/// <returns>true-success; false-failed</returns>
		bool AddUpdateField(string updateName, string[] arrConditions, string[] arrUpdParams, string fieldName, string fieldValue);

		/// <summary>
		/// Execute updates 
		/// </summary>
		/// <param name="sqlTransaction">Transaction from container application</param>
		/// <returns>true-success; false-failed</returns>
		bool Execute(IDBConnector conn, out string error);

		/// <summary>
		/// Calculated updated parameters values
		/// </summary>
		/// <param name="newParamsValues"></param>
		/// <returns>true-success; false-failed</returns>
		bool GetNewParameters(out Hashtable newParamsValues);
	}

    public interface IDBConnector : IDisposable
    {
        // TODO: must declare all methods

        int ExecuteNonQuery(string sql, bool trace);
        DataTable GetDataTable(string sql, bool trace);
        DataTable GetDataTable(string sqlString, IDataParameter[] arrParams);
        string GetConnectionKey();
    }
}