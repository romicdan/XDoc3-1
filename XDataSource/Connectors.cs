using System;
using System.Collections;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;


namespace XDataSourceModule
{
	
	public  class Connectors 
	{
        Hashtable m_connectors = new Hashtable();
        bool inTransaction = false;



        public Connectors()
        {
            m_connectors = new Hashtable();
        }

        public void Dispose()
        {
            if (m_connectors != null)
            {
                foreach (DictionaryEntry myDE in m_connectors)
                    (myDE.Value as XProviderData).Dispose();
                m_connectors.Clear();
                m_connectors = null;
            }
        }

        public bool InTransaction
        {
            get { return inTransaction; }
        }

        public XProviderData SystemConnector
        {
            get { return GetConnector(""); }
        }
        public static  string Encrypt(string input, string key)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static  string Decrypt(string input, string key)
        {
            byte[] inputArray = Convert.FromBase64String(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

		public XProviderData GetConnector(string m_strConnString)
        {
            string m_strLicense, m_strConnStringEncrypted;
            if (m_strConnString == "" || m_strConnString == null)
            {
                m_strLicense = ConfigurationManager.AppSettings["License"];
                m_strConnStringEncrypted = ConfigurationManager.AppSettings["XDocuments"];
                if (m_strLicense != null && m_strLicense != "" && m_strConnStringEncrypted != null && m_strConnStringEncrypted != "")
                {
                    string sKey = "Kubion" + m_strLicense + "Chessm@sterChessm@ster";
                    sKey = sKey.Substring(3, 24); 
                    m_strConnString = Decrypt(m_strConnStringEncrypted, sKey );
                }
                else
                {
                    m_strConnString = ConfigurationManager.AppSettings["XDocConnectionString"];
                    if (m_strConnString == null || m_strConnString == "")
                        throw new Exception("The XDocuments connection string is not present in the config file");
                }
            }
            if (m_connectors.Contains(m_strConnString))
                return (XProviderData)m_connectors[m_strConnString];
            else
            {
                XProviderData pd = new XProviderData(m_strConnString);
                if (inTransaction) 
                    pd.BeginTransaction();
                m_connectors.Add(m_strConnString, pd);
                return pd;
            }
		}

        public void BeginTransaction()
        {
            if (!inTransaction)
            {
                if (m_connectors != null)
                    foreach (DictionaryEntry myDE in m_connectors)
                        (myDE.Value as XProviderData).BeginTransaction();

                inTransaction = true;
            }
        }
        public void Commit()
        {
            if (inTransaction)
            {
                if (m_connectors != null)
                    foreach (DictionaryEntry myDE in m_connectors)
                        (myDE.Value as XProviderData).Commit();

                inTransaction = false;
            }
        }
        public void Rollback()
        {
            if (inTransaction)
            {
                if (m_connectors != null)
                    foreach (DictionaryEntry myDE in m_connectors)
                        (myDE.Value as XProviderData).Rollback();
                inTransaction = false;
            }
        }
        
        #region GetResult

        public static bool GetBoolResult(object obj)
        {
            return GetBoolResult(obj, false);
        }
        public static bool GetBoolResult(object obj, bool defaultValue)
        {
            int i;
            //if (obj != null && obj is bool)
            //    return (bool)obj;
            if (obj != null && bool.TryParse(obj.ToString(), out  defaultValue))
                return defaultValue;
            if (obj != null && Int32.TryParse(obj.ToString(), out  i))
                return (i!=0) ;

            return defaultValue;
        }

        public static string GetStringResult(object obj, string defaultValue)
        {
            if (obj != null)
                if (obj is string)
                    return ((string)obj).TrimEnd();
                else
                    return obj.ToString();
            return defaultValue;
        }
        public static string GetStringResult(object obj)
        {
            return GetStringResult(obj, "");
        }

        public static int GetInt32Result(object obj, int defaultValue)
        {
//            if (obj != null && obj is int)
//            return (int)obj;
            if (obj != null && Int32.TryParse(obj.ToString(),out  defaultValue))
                return defaultValue;

            return defaultValue;
        }

        public static int GetInt32Result(object obj)
        {
            return GetInt32Result(obj, -1);
        }

        public static double GetDoubleResult(object obj, double defaultValue)
        {
            if (obj != null && obj is double)
                return (double)obj;
            return defaultValue;
        }

        public static double GetDoubleResult(object obj)
        {
            return GetDoubleResult(obj, -1);
        }

        public static DateTime GetDateTimeResult(object obj, DateTime defaultValue)
        {
            if (obj != null && obj is DateTime)
                return (DateTime)obj;
            return defaultValue;
        }
        public static DateTime GetDateTimeResult(object obj)
        {
            return GetDateTimeResult(obj, (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
        }

        public static byte[] GetBinaryResult(object obj)
        {
            if (obj != null && obj != DBNull.Value && obj is byte[])
                return (byte[])obj;
            return null;
        }

        #endregion GetResult
    }
}
