using System;
using System.Data;
using System.Collections;
using System.Data.OleDb;
using System.Diagnostics;
using System.Configuration;
using System.Text.RegularExpressions;
using KubionLogNamespace;


public class XOleDbData
{
    OleDbConnection m_conn = null;
    OleDbTransaction m_trans = null;

    public XOleDbData(string connectionString)
    {

        try
        {
            m_conn = new OleDbConnection(connectionString);
        }
        catch (Exception ex)
        {
            //no trace message because we try to open SQL connection first, then OLEDB
            throw (ex);
        }
    }

    public void Dispose()
    {
        if (m_conn != null && m_conn.State != ConnectionState.Closed)
        {
            try
            {
                m_conn.Close();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw (ex);
            }
        }
    }


    public bool ConnOpen()
    {
        OpenConnection();
        return (m_conn.State == ConnectionState.Open);
    }

    private void OpenConnection()
    {
        if (m_conn.State != ConnectionState.Open)
        {
            try
            {
                m_conn.Open();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw (ex);
            }
        }
    }

    public DataTable GetDataTable(string sqlString)
    {
        if (!ConnOpen())
            return null;
        bool bLocalTrans = false;
        OleDbCommand command = new OleDbCommand(sqlString, m_conn);
        if (m_trans == null)
        {
            BeginTransaction();
            bLocalTrans = true;
        }
        command.Transaction = m_trans;

        DataSet ds = new DataSet();
        OleDbDataAdapter da = new OleDbDataAdapter(command);
        if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
            KubionLog.WriteLine(">>GetDataTable(\"" + sqlString + "\")");
        try
        {
            da.Fill(ds);
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }

        DataTable dt = null;
        if (ds.Tables.Count > 0)
            dt = ds.Tables[0];
        return dt;
    }


    public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
    {
        if (!ConnOpen())
            return null;

        bool bLocalTrans = false;
        DataSet ds = new DataSet();

        IDataParameter[] newParameters = null;
        sqlString = ParseForUnnamedParams(sqlString, arrParams, out newParameters);

        OleDbCommand comm = new OleDbCommand(sqlString, m_conn);
        if (m_trans == null)
        {
            BeginTransaction();
            bLocalTrans = true;
        }
        comm.Transaction = m_trans;
        DataTable dt = null;
        string traceString = ">>GetDataTable(\"" + sqlString + "\"";
        try
        {
            foreach (IDataParameter paramValue in newParameters)
            {
                if (paramValue != null)
                {
                    if (paramValue == null)
                        throw new ApplicationException("Parsing for unnamed parameters returned null parameters");
                    comm.Parameters.AddWithValue(paramValue.ParameterName, paramValue.Value);
                    string val = paramValue.Value == null ? "<NULL>" : paramValue.Value.ToString();
                    traceString += ", " + val;
                }
            }

            OleDbDataAdapter da = new OleDbDataAdapter(comm);
            traceString += ")";
            if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
                KubionLog.WriteLine(traceString);
            da.Fill(ds);
            if (bLocalTrans)
                Commit();
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (new Exception(ex.Message + ";" + Environment.NewLine + sqlString));
        }
        finally
        {
            bLocalTrans = false;
        }

        return dt;
    }


    public int ExecuteNonQuery(string sqlString)
    {
        if (!ConnOpen())
            return -1;
        bool bLocalTrans = false;
        OleDbCommand comm = new OleDbCommand(sqlString, m_conn);
        if (m_trans == null)
        {
            BeginTransaction();
            bLocalTrans = true;
        }
        comm.Transaction = m_trans;
        int rowsAffected = -1;
        if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
            KubionLog.WriteLine(">>ExecuteNonQuery (\"" + sqlString + "\")");
        try
        {
            rowsAffected = comm.ExecuteNonQuery();
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }
        return rowsAffected;
    }

    public int ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
    {
        if (!ConnOpen())
            return -1;
        bool bLocalTrans = false;

        IDataParameter[] newParameters = null;
        sqlString = ParseForUnnamedParams(sqlString, arrParams, out newParameters);

        OleDbCommand comm = new OleDbCommand(sqlString, m_conn);
        if (m_trans == null)
        {
            BeginTransaction();
            bLocalTrans = true;
        }
        comm.Transaction = m_trans;
        int rowsAffected = -1;
        string traceString = ">>ExecuteNonQuery (\"" + sqlString + "\"";

        try
        {
            foreach (IDataParameter paramValue in newParameters)
            {
                if (paramValue == null)
                    throw new ApplicationException("Parsing for unnamed parameters returned null parameters");
                comm.Parameters.AddWithValue(paramValue.ParameterName, paramValue.Value);
                string val = paramValue.Value == null ? "<NULL>" : paramValue.Value.ToString();
                traceString += ", " + val;
            }
            traceString += ")";
            if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
                KubionLog.WriteLine(traceString);
            rowsAffected = comm.ExecuteNonQuery();
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }

        return rowsAffected;
    }


    public DataSet GetDataSet(string sqlString)
    {
        if (!ConnOpen())
            return null;
        bool bLocalTrans = false;
        DataSet ds = new DataSet();
        OleDbCommand command = new OleDbCommand(sqlString, m_conn);
        if (m_trans == null)
        {
            BeginTransaction();
            bLocalTrans = true;
        }
        command.Transaction = m_trans;

        OleDbDataAdapter da = new OleDbDataAdapter(command);
        if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
            KubionLog.WriteLine(">>GetDataSet(\"" + sqlString + "\")");
        try
        {
            da.Fill(ds);
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }
        return ds;
    }


    public DataSet GetDataSet(string sqlString, IDataParameter[] arrParams)
    {
        if (!ConnOpen())
            return null;
        bool bLocalTrans = false;

        IDataParameter[] newParameters = null;
        sqlString = ParseForUnnamedParams(sqlString, arrParams, out newParameters);

        DataSet ds = new DataSet();
        OleDbCommand comm = new OleDbCommand(sqlString, m_conn);
        if (m_trans == null)
        {
            BeginTransaction();
            bLocalTrans = true;
        }
        comm.Transaction = m_trans;
        string traceString = ">>GetDataSet(\"" + sqlString + "\"";

        try
        {
            foreach (IDataParameter paramValue in newParameters)
            {
                if (paramValue == null)
                    throw new ApplicationException("Parsing for unnamed parameters returned null parameters");
                comm.Parameters.AddWithValue(paramValue.ParameterName, paramValue.Value);
                string val = paramValue.Value == null ? "<NULL>" : paramValue.Value.ToString();
                traceString += ", " + val;
            }
            OleDbDataAdapter da = new OleDbDataAdapter(comm);
            traceString += ")";
            if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
                KubionLog.WriteLine(traceString);

            da.Fill(ds);
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }

        return ds;
    }


    public void BeginTransaction()
    {
        OpenConnection();
        try
        {
            m_trans = m_conn.BeginTransaction(IsolationLevel.ReadUncommitted);
        }
        catch (Exception e)
        {
        }

    }
    public void Commit()
    {
        try
        {
            m_trans.Commit();
            m_trans = null;
        }
        catch (Exception e)
        {
        }
    }
    public void Rollback()
    {
        try
        {
            m_trans.Rollback();
            m_trans = null;
        }
        catch (Exception e)
        {
        }
    }

    #region Private Methods

    private string ParseForUnnamedParams(string sqlString, IDataParameter[] arrParams, out IDataParameter[] newParams)
    {
        newParams = new IDataParameter[arrParams.GetLength(0)];
        string retString = sqlString;
        Regex regEx = new Regex(@"@[\w]+");
        MatchCollection mc = regEx.Matches(sqlString);
        int index = 0;
        foreach (Match m in mc)
        {
            foreach (IDataParameter par in arrParams)
                if (par.ParameterName.ToUpper().Replace("@", "") == m.Value.ToUpper().Replace("@", ""))
                {
                    bool exists = false;
                    foreach (IDataParameter p in newParams)
                        if (p != null && p.ParameterName == par.ParameterName)
                            exists = true;
                    if (!exists)
                    {
                        newParams[index] = par;
                        index++;
                    }
                    break;
                }


            int nIndex = retString.IndexOf(m.Value);
            retString = retString.Remove(nIndex, m.Value.Length);
            retString = retString.Insert(nIndex, " ? ");

            //retString = retString.Replace(m.Value, " ? ");
        }

        return retString;
    }

    #endregion Private Methods

}
