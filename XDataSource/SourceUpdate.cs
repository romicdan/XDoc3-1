using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace XDataSourceModule
{
	/// <summary>
	/// Summary description for SourceUpdate.
	/// </summary>
	public class SourceUpdate
	{
		#region Protected Members
		protected Hashtable m_NewParamsValues = null; 
		protected Hashtable m_FieldsValues= null;
		protected Array     m_Parameters = null;
		protected string    m_KeyName = "";
		protected string    m_UpdateName = "";
		protected int		m_templateID = -1;
        protected string    m_templateName = "";

        Regex rxFindParams = new Regex("#[^#]*?#");
		#endregion

		#region Consts
		protected const string DummySqlStr = "";
		protected const string UpdateSqlStr = "UPDATE";
		protected const string InsertSqlStr = "INSERT";
		protected const string SelectSqlStr = "SELECT";
        protected const string DeleteSqlStr = "DELETE";
		#endregion

		#region protected enums 
		protected enum UpdateSqlType
		{
			DummySql,
			UpdateSql,
			InsertSql,
			SelectSql,
            DeleteSql,
            XMLSql
		}	
		#endregion

		#region Public methods
   		public SourceUpdate()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string Key
		{
			get{ return m_KeyName; }
		}

		public bool Initialize(int templateID, string templateName, string UpdateName, string[] Params)
		{
			try
			{
                m_templateName = templateName;
				m_templateID = templateID;
				m_FieldsValues= CollectionsUtil.CreateCaseInsensitiveHashtable();
				m_Parameters = Params;
				m_UpdateName = UpdateName;
                m_NewParamsValues = CollectionsUtil.CreateCaseInsensitiveHashtable();
				
				m_KeyName = SourceUpdate.GetUpdateKey(UpdateName, Params, templateName);

			}
			catch(Exception ex)
			{
                //Trace.WriteLine(ex);
				throw;
			}

			return false;
		}

		public bool AddFieldValue(string fieldName, string fieldValue, string[] arrUpdParams)
		{
			m_FieldsValues[fieldName] = fieldValue;

			for(int index = 0; (arrUpdParams != null) && (index < arrUpdParams.GetLength(0)); index++)
			{
				m_NewParamsValues[arrUpdParams.GetValue(index).ToString().TrimEnd().ToUpper()] = "";
			}

			return true;
		}

        public bool Execute(XProviderData dataProvider, out string error)
        {
            error = "";
            string strSQL = "";
            string strConn = "";
            XProviderData ClientData = null;
            //string sQueryStatement;

            try
            {
                XProviderData connAdm = dataProvider;

                int templateID = m_templateID;
                if (m_templateName != null && m_templateName != "")
                {
                    //sQueryStatement = SQL.SQLGetTemplateID;
                    //sQueryStatement = sQueryStatement.Replace("@TEMPLATENAME", "'" + m_templateName + "'");
                    DataTable dt = connAdm.GetDataTable(SQL.SQLGetTemplateID, new IDataParameter[] { new SqlParameter("@TEMPLATENAME", m_templateName) });
                    //DataTable dt = connAdm.GetDataTable(sQueryStatement);
                    if (dt != null && dt.Rows.Count > 0)
                        templateID = Connectors.GetInt32Result(dt.Rows[0]["TEMPLATEID"]);
                }

                //sQueryStatement = SQL.SQLGetQueryStatement;
                //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYNAME, "'" + m_UpdateName + "'");
                //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.TEMPLATEID, templateID.ToString ());
                //DataTable dtQuery = connAdm.GetDataTable(sQueryStatement);

                IDataParameter[] pars = new IDataParameter[2];
                pars[0] = new SqlParameter("@QUERYNAME", m_UpdateName);
                pars[1] = new SqlParameter("@TEMPLATEID", templateID);
                DataTable dtQuery = connAdm.GetDataTable(SQL.SQLGetQueryStatement, pars);
                if (dtQuery.Rows.Count == 0)
                {
                    error = "Update " + m_UpdateName + " was not found!";
                    return false;
                }

                strSQL = dtQuery.Rows[0][S_QUERIES.QUERYTEXT].ToString();
                strConn = dtQuery.Rows[0][S_CONNECTIONS.CONNSTRING].ToString();

                UpdateSqlType sqlType = UpdateSqlType.DummySql;
                string sConnIDName = ParseQueryStatement(ref strSQL, out sqlType);

                if (sConnIDName != "")
                {
                    string sQueryStatement = SQL.SQLConnectionString;
                    sQueryStatement = sQueryStatement.Replace("#P1#", "'" + sConnIDName + "'");
                    DataTable dtQueryc = connAdm.GetDataTable(sQueryStatement);
                    if (dtQueryc == null || dtQueryc.Rows.Count == 0)
                    {
                        error = "Cannot find requested connection (" + sConnIDName + ") for template id = " + Convert.ToString(m_templateID);
                        return false;
                    }
                    strConn = Connectors.GetStringResult(dtQueryc.Rows[0][S_CONNECTIONS.CONNSTRING]);
                }

                if ((strConn == "") || (strConn == ConfigurationManager.AppSettings["XDocConnectionString"]))
                    ClientData = connAdm;
                else
                    ClientData = new Connectors().GetConnector(strConn);
                    //ClientData = m_Connectors.GetConnector(strConn);

                if (sqlType == UpdateSqlType.UpdateSql || sqlType == UpdateSqlType.InsertSql || sqlType == UpdateSqlType.DeleteSql)
                    sqlType = UpdateSqlType.SelectSql;

                string retUpdateValue = "";
                if (sqlType == UpdateSqlType.UpdateSql || sqlType == UpdateSqlType.InsertSql || sqlType == UpdateSqlType.DeleteSql)
                {
                    retUpdateValue = ClientData.ExecuteNonQuery(strSQL).ToString();
                }
                else if (sqlType == UpdateSqlType.SelectSql)
                {
                    DataTable dtRetVal = null;
                    dtRetVal = ClientData.GetDataTable(strSQL);
                    if (dtRetVal != null && dtRetVal.Rows.Count > 0 && dtRetVal.Columns.Count > 0)
                        retUpdateValue = dtRetVal.Rows[0][0].ToString().TrimEnd();
                }
                else if (sqlType == UpdateSqlType.XMLSql )
                {
                    string sResponse="", sOuter="", sInner="";
                    ClientData.ExecuteNonQuery(strSQL,ref sResponse,ref  sOuter, ref sInner);
                    retUpdateValue = sInner;
                }


                SetNewParametersValue(retUpdateValue);

                return true;
            }
            catch (Exception ex)
            {
                //Trace.WriteLine(ex);
                string parameters = "";
                if (m_Parameters != null && m_Parameters.GetLength(0) != 0)
                {
                    parameters = " with parameters (";
                    for (int i = 0; i < m_Parameters.Length; i++)
                    {
                        parameters += (string)m_Parameters.GetValue(i);
                        if (i != m_Parameters.Length - 1)
                            parameters += ",";
                    }
                    parameters += ")";
                }

                error = "Could not execute update " + m_UpdateName + parameters + ": " + ex.Message;
                return false;
            }
            finally
            {
            }
        }

		public bool GetNewParameters(ref Hashtable newParamsValues)
		{
			try
			{
				IDictionaryEnumerator en = m_NewParamsValues.GetEnumerator();
				while(en.MoveNext())
				{
                    bool keyExists = false;
                    foreach (DictionaryEntry de in newParamsValues)
                    {
                        if (de.Key.ToString().ToUpper() == en.Key.ToString().ToUpper())
                        {
                            keyExists = true;
                            break;
                        }
                    }

                    if (!keyExists)
                        newParamsValues[en.Key] = en.Value;
				}
				return true;
			}
			catch(Exception ex)
			{
                //Trace.WriteLine(ex);
				throw ;
			}
		}

		#endregion

		#region Protected Methods
		protected void SetNewParametersValue(string newValue)
		{
			
			Array arrKeys = Array.CreateInstance(typeof(object), m_NewParamsValues.Count);

			m_NewParamsValues.Keys.CopyTo(arrKeys, 0);
			for(int index =0; index < arrKeys.GetLength(0); index++)
				m_NewParamsValues[arrKeys.GetValue(index)] = newValue;
				
		}

		protected string ParseQueryStatement(ref string strQuery, out UpdateSqlType sqlType)
		{
            string sReturn = "";
			sqlType = UpdateSqlType.DummySql;
			try
			{
				
				for(int index = 0; m_Parameters != null &&  index < m_Parameters.GetLength(0); index++)
				{
                    string strParamKey = "";
                    strParamKey = String.Format("#P{0}#", index + 1);
                    object Value = m_Parameters.GetValue(index);
                    string sValue = Value.ToString();
                    bool bReplaced = false;
                    if (sValue.Contains("="))
                    {
                        string sParName = sValue.Substring(0, sValue.IndexOf("=") );
                        string sValue1 = sValue.Substring(sValue.IndexOf("=")+1);
                        if (strQuery.Contains("#" + sParName + "#"))
                        {
                            strQuery = strQuery.Replace("#" + sParName + "#", sValue1);
                            bReplaced = true;
                        }
                        if (sParName.ToUpper () == "CONN") sReturn = sValue1;
                    }
                    if (!bReplaced) strQuery = strQuery.Replace(strParamKey, sValue);
                }

				if(m_FieldsValues != null)
				{
					IDictionaryEnumerator en  = m_FieldsValues.GetEnumerator();
                    MatchCollection mc = rxFindParams.Matches(strQuery);
					while(en.MoveNext())
					{
						string strParamKey = "";
						strParamKey = String.Format("#{0}#", en.Key).ToUpper();

                        foreach (Match m in mc)
                        {
                            if (m.Value.ToUpper() == strParamKey)
                            {
                                string Value = en.Value.ToString();
                                Value = Value.Replace("\'", "\'\'");
                                strQuery = strQuery.Replace(m.Value, Value.ToString());
                            }
                        }
					}
				}

                strQuery += " ";
                int pos = strQuery.IndexOf(" ", 0);
                string strSqlType = strQuery.Substring(0, pos);
                strSqlType = strSqlType.ToUpper();

                strQuery = " " + strQuery;
                sqlType = UpdateSqlType.XMLSql;
                int iSELECT = strQuery.ToUpper().Replace("\r\n", " ").IndexOf(" SELECT ");
                int iUPDATE = strQuery.ToUpper().Replace("\r\n", " ").IndexOf(" UPDATE ");
                int iINSERT = strQuery.ToUpper().Replace("\r\n", " ").IndexOf(" INSERT ");
                int iDELETE = strQuery.ToUpper().Replace("\r\n", " ").IndexOf(" DELETE ");
                if (strQuery.ToUpper().Replace("\r\n", " ").Contains(" SELECT ")) sqlType = UpdateSqlType.SelectSql;
                if (strQuery.ToUpper().Replace("\r\n", " ").Contains(" UPDATE ")) sqlType = UpdateSqlType.UpdateSql;
                if (strQuery.ToUpper().Replace("\r\n", " ").Contains(" INSERT ")) sqlType = UpdateSqlType.InsertSql ;
                if (strQuery.ToUpper().Replace("\r\n", " ").Contains(" DELETE ")) sqlType = UpdateSqlType.DeleteSql ;
                if (iSELECT > -1) 
                    if (sqlType != UpdateSqlType.SelectSql)
                    {
                        if ((sqlType == UpdateSqlType.UpdateSql) && (iSELECT > iUPDATE)) sqlType = UpdateSqlType.SelectSql;
                        if ((sqlType == UpdateSqlType.InsertSql) && (iSELECT > iINSERT )) sqlType = UpdateSqlType.SelectSql;
                        if ((sqlType == UpdateSqlType.DeleteSql) && (iSELECT > iDELETE )) sqlType = UpdateSqlType.SelectSql;
                    }

                //switch (strSqlType)
                //{
                //    case UpdateSqlStr:
                //        sqlType = UpdateSqlType.UpdateSql;
                //        break;
                //    case InsertSqlStr:
                //        sqlType = UpdateSqlType.InsertSql;
                //        break;
                //    case SelectSqlStr:
                //        sqlType = UpdateSqlType.SelectSql;
                //        break;
                //    case DeleteSqlStr:
                //        sqlType = UpdateSqlType.DeleteSql;
                //        break;
                //    default:
                //        sqlType = UpdateSqlType.XMLSql;
                //        break;
                //}

								
				return sReturn ;
			}
			catch(Exception ex)
			{
                //Trace.WriteLine(ex);
				throw;
			}
		}

		#endregion

		#region Static Methods
		public static string GetUpdateKey(string UpdateName, string[] Params, string templateName)
		{

			string key = UpdateName;
			for(int Index = 0; Params != null && Index < Params.GetLength(0); Index++)
			{
				key += Params.GetValue(Index).ToString(); 
			}
            if (templateName != null && templateName != "")
                key += "_" + templateName.ToLower().Trim();
			return key;
		}
		#endregion
		
	}
}
