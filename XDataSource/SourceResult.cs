using System;
using System.Collections;
using System.Data;
//using System.Text.RegularExpressions;
//using XDataSourceModule;
using System.Configuration;
using System.Data.SqlClient;
using KubionLogNamespace;
using System.Collections.Specialized;
using System.Xml;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace XDataSourceModule
{
	/// <summary>
	/// Summary description for SourceResult.
	/// </summary>
	public class SourceResult
	{
        public delegate void RequestHierarcyEventHandler(SourceResult sourceResult, string parameters);
        public event RequestHierarcyEventHandler RequestHierarchy;

		#region Protected Members
        private DataTable dtQueryResult = null;

        protected Hashtable m_FieldsResults = null;
        protected string[] m_FieldsIndex = null;
        protected int m_FieldsIndexOffset = 0;
        protected Array m_parameters = null;
        protected string m_keyName = "";
		protected int       m_size = -1;
		protected string    m_sourceName = "";
		protected int		m_templateID = -1;
        protected XProviderData m_dataProvider = null;
        protected string m_Response;
        protected string m_OuterXml;
        protected string m_InnerXml;
        private string m_requestedList = "";
        protected string m_sError = "";
        protected string m_sErrorVerbose = "";

        protected bool bDefQryParameter = false;
        protected bool bExceptionParameter = false;
        protected string sFilter = "", sSort = "", sDistinct = "", sColumns = "";
        protected string sErrorRegexParameter = "";
        protected string m_sParameters = "";
        protected Connectors m_Connectors = null; 

        public string RequestedList
        {
            get { return m_requestedList; }
            set { m_requestedList = value; }
        }
        public string Error
        {
            get { return m_sError; }
            set { m_sError = value; }
        }
        public string ErrorVerbose
        {
            get { return m_sErrorVerbose; }
            set { m_sErrorVerbose = value; }
        }
        public string Parameters
        {
            get { return m_sParameters; }
            set { m_sParameters = value; }
        }

		#endregion

		public SourceResult()
		{
		}
        public SourceResult(Connectors p_Connectors)
        {
            m_Connectors = p_Connectors;
        }
		#region protected methods

		protected bool AddValue(string fieldName, int Index, object Value)
		{
			fieldName = fieldName.ToUpper();
			FieldValues fieldValues = (FieldValues)m_FieldsResults[fieldName];
			if(fieldValues == null)
				fieldValues = new FieldValues();
			

			fieldValues.AddValue(Index, Value);
			m_FieldsResults[fieldName] = fieldValues;
			
			return true;
		}

        protected string  ParseQueryStatement(ref string strQuery)
        {
            string sReturn = "";
            try
            {
                if (m_parameters == null)
                    return sReturn;

                for (int index = 0; index < m_parameters.GetLength(0); index++)
                {
                    string strParamKey = "";
                    strParamKey = String.Format("#P{0}#", index + 1);
                    object Value = m_parameters.GetValue(index);
                    if (Value != null)
                    {
                        string sValue = Value.ToString();
                        bool bReplaced = false;
                        if (sValue.Contains("="))
                        {
                            string sParName = sValue.Substring(0, sValue.IndexOf("="));
                            string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
                            if (strQuery.Contains("#" + sParName + "#"))
                            {
                                strQuery = strQuery.Replace("#" + sParName + "#", sValue1);
                                bReplaced = true;
                            }
                            if (sParName.ToUpper() == "CONN") sReturn = sValue1;
                        }
                        if (!bReplaced) strQuery = strQuery.Replace(strParamKey, sValue);
                    }
                }

                strQuery = ParseForHierarchy(strQuery);

                return sReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing the query text: " + ex.Message);
            }
            
        }

        string ParseForHierarchy(string query)
        {
            if (query == null || query == "")
                return "";



            int i1 = query.ToLower().IndexOf("!getitemslist");
            if (i1 == -1)
                i1 = query.ToLower().IndexOf("!gethierarchy");
            if (i1 != -1)
            {
                int i2 = query.IndexOf(")!", i1);
                if (i2 != -1)
                {
                    string parameters = query.Substring(i1, i2-i1+2);
                    m_requestedList = "";
                    if (RequestHierarchy != null)
                        RequestHierarchy(this, parameters);
                    if (m_requestedList != "")
                    {
                        string ret = "";
                        if (i1 > 0)
                            ret = query.Substring(0, i1);
                        ret += m_requestedList;
                        if (i2 < query.Length - 1)
                            ret += query.Substring(i2 + 2);

                        return ret;
                    }
                }
            }

            return query;

        }


        DataTable JPath_GetDataTable(string sJSON, string sJPath, string sQRY,string sFields, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            DataTable ret = new DataTable();

//string json = "[{"clientID":"1788","projectID":"19"},{"clientID":"1789","projectID":"24"},{"clientID":"1790","projectID":"24"},{"clientID":"1790","projectID":"23"},{"clientID":"1790","projectID":"21"}]";

//DataTable tester = (DataTable) JsonConvert.DeserializeObject(json, (typeof(DataTable)));

            m_Response = sJSON;
            m_InnerXml = "";
            m_OuterXml = "";
            if (sJSON == "") sJSON = "{}";

            try
            {
                JArray ja;
                if (sJPath == "")
                    ja = JArray.Parse(sJSON);
                else
                    ja = (JArray)(JObject.Parse(sJSON)).SelectToken(sJPath);
                try
                {
                    string[] fa = sFields.Split('|');
                    foreach (string f in fa)
                    {
                        if (f != "") ret.Columns.Add(f, System.Type.GetType("System.String"));
                    }
                    ret.Columns.Add("JSON", System.Type.GetType("System.String"));
                    foreach (JToken j in ja)
                    {
                        DataRow dr = ret.NewRow();
                        foreach (string f in fa)
                        {
                            if (f != "") if (j.SelectToken(f) != null) dr[f] = j.SelectToken(f).ToString();
                        }
                        dr["JSON"] = j.ToString();
                        ret.Rows.Add(dr);
                    }
                    m_InnerXml = ja.ToString();
                    m_OuterXml = ja.ToString();
                }
                catch (Exception e) { throw new Exception("Failed to loop JSON array. Error: " + e.Message + "\r\nJPath: " + sJPath + "\r\nJSON: " + sJSON); }
            }
            catch (Exception e) { throw new Exception("Failed to create an JSON array. Error: " + e.Message + "\r\nJPath: " + sJPath + "\r\nJSON: " + sJSON); }

            //foreach (DataColumn dc in ret.Columns)
            //{
            //    if (dc.ColumnMapping == MappingType.SimpleContent)
            //    {
            //        dc.ColumnMapping = MappingType.Element;
            //        dc.ColumnName = dc.ColumnName.Replace("_Text", "");
            //    }
            //};

            if (sQRY == "1")
            {
                DataTable ret1 = ret.Clone();
                if (ret.Rows.Count >0)
                    ret1.ImportRow(ret.Rows[0]);
                else
                    ret1.LoadDataRow(new string[0], true);
                return ret1;
            }
            else 
                return ret;
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        DataTable XPath_GetDataTable(string sXml, string sXPath, string sQRY, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            string strNs = "ns";
            DataTable ret = new DataTable();
            DataTable ret1 = new DataTable();

            m_Response = sXml;
            m_InnerXml = "";
            m_OuterXml = "";
            if (sXml == "") sXml = "<root></root>";

            sXml = sXml.Replace(" xmlns=", " xml_ns=");
            XmlDocument objXmlDocument = new XmlDocument();
            try { objXmlDocument.LoadXml(sXml); }
            catch (Exception e) { throw new Exception("Failed to create an XML document. Error: " + e.Message + "\r\nXml: " + sXml); }

            XmlNamespaceManager objXmlNamespaceManager = new XmlNamespaceManager(objXmlDocument.NameTable);
            objXmlNamespaceManager.AddNamespace(strNs, objXmlDocument.DocumentElement.NamespaceURI);


            /* Select the nodes given by the XPath and NS */
            XmlNodeList objXmlNodeList = objXmlDocument.SelectNodes(sXPath, objXmlNamespaceManager);
            if (objXmlNodeList.Count > 0)
            {
                XmlNode objXmlParentNode = objXmlNodeList[0].ParentNode;

                if (objXmlParentNode == null) objXmlParentNode = objXmlNodeList[0];
                /*Michael*/
                /* objXmlParentNode can be null if objXmlNodeList[0] is the rootnode */
                if (objXmlParentNode != null && objXmlParentNode.ChildNodes.Count != objXmlNodeList.Count)
                {
                    objXmlParentNode.RemoveAll();
                    foreach (XmlNode node in objXmlNodeList)
                        objXmlParentNode.AppendChild(node);
                }

                if (objXmlParentNode != null)
                {
                    m_InnerXml = objXmlParentNode.InnerXml;
                    m_OuterXml = objXmlParentNode.OuterXml;
                }

                DataSet objDataSet = new DataSet();
                using (XmlReader reader = new XmlNodeReader(objXmlParentNode))
                {
                    objDataSet.ReadXml(reader);
                    reader.Close();
                }

                if (objDataSet.Tables.Count > 0)
                {
                    if (objDataSet.Tables.IndexOf (  objXmlNodeList[0].Name)>-1)
                        ret = objDataSet.Tables[objXmlNodeList[0].Name];
                    else
                        ret = objDataSet.Tables[0];
                    if (ret == null) ret = objDataSet.Tables[0];
                }

                foreach (DataColumn dc in ret.Columns)
                {
                    if (dc.ColumnMapping == MappingType.SimpleContent)
                    {
                        dc.ColumnMapping = MappingType.Element;
                        dc.ColumnName = dc.ColumnName.Replace("_Text", "");
                    }
                    //else if (dc.ColumnMapping == MappingType.Hidden)
                    //{
                    //    if (dc.ColumnName != objXmlNodeList[0].Name + "_Id")
                    //    {
                    //        for (int iRow = 0; iRow < ret.Rows.Count; iRow++)
                    //            if (ret.Rows[iRow][dc].ToString() != "") { ret.Rows.Remove(ret.Rows[iRow]); iRow--; }
                    //    }
                    //}
                    if (dc.ColumnName.ToLower() == "xml") dc.ColumnName = "requestxml";
                }

                //foreach (DataColumn dc in ret.Columns)
                //{
                //    if (dc.ColumnMapping == MappingType.SimpleContent)
                //    {
                //        dc.ColumnMapping = MappingType.Element;
                //        dc.ColumnName = dc.ColumnName.Replace("_Text", "");
                //    }
                //}
 
                ret.Columns.Add("XML", System.Type.GetType("System.String"));

                int i = 0;
                foreach (XmlNode objXmlNode in objXmlNodeList)
                {
                    if (ret.Rows.Count < i + 1) ret.LoadDataRow(new string[0], true);
                    ret.Rows[i]["XML"] = objXmlNode.OuterXml;
                    i++;
                }
            }

            if (sQRY == "1")
            {
                ret1 = ret.Clone();
                if (ret.Rows.Count >0)
                    ret1.ImportRow(ret.Rows[0]);
                else
                    ret1.LoadDataRow(new string[0], true);
                return ret1;
            }
            else 
                return ret;


            //if (objXmlNodeList.Count > 0)
            //{
            //    //if (objXmlNodeList.Count == 0) throw new Exception("XPath " + strXPath + " could not be found in the response.\r\nResponse: " + refResponse);
            //    //if (objXmlNodeList.Count > 1) throw new Exception("XPath " + sXPath + " found to many time in the response.\r\nXml: " + sXml);
            //    m_InnerXml = objXmlNodeList[0].InnerXml;
            //    m_OuterXml = objXmlNodeList[0].OuterXml;

            //    /* Build a DataTable from the XML and return it */
            //    DataSet objDataSet = new DataSet();
            //    using (XmlReader reader = new XmlNodeReader(objXmlNodeList[0]))
            //    {
            //        objDataSet.ReadXml(reader);
            //        reader.Close();
            //    }
            //    if (objDataSet.Tables.Count == 0) return null;
            //    else
            //    {
            //        if (sQRY == "1")
            //        {
            //            DataRow dr = objDataSet.Tables[0].Rows[0];
            //            ret.ImportRow(dr);
            //            return ret;

            //        }
            //        else
            //            return objDataSet.Tables[0];
            //    }
            //}
            //if (sQRY == "1") ret.LoadDataRow(new string[0], true);
            //return ret;

        }
        DataTable SplitXX_GetDataTable(string sData, string sRowSep, string sColSep)
        {
            DataTable dtRet = new DataTable();  //Datatable that will be returned            
            char[] cColSep = new char [1];                     //The column seperators
            char[] cRowSep = new char [1];                     //The row seperator
            string[] sRows;                     //array of rows
            string[] sCols;                     //array of rows
            string[] sColNames;                 //array of column names
            int iCols;                          //number of columns
            int iRows;                          //number of rows
            string[] sNewRow;                   //new row to be added to datatable            
            int i, j, k;                          //counters in loops
            string strColName;                  //next column name to be added to datatable

            try
            {
                if (sColSep == "") 
                    cColSep = new char[1];
                else
                    cColSep[0] = sColSep[0];//.ToCharArray(); //The seperators (more than one are automatically allowed)
                if (sRowSep == "") sColSep = "|";
                cRowSep[0] = sRowSep[0];//.ToCharArray();

                if (!CheckDoubleSeperators(ref cRowSep, ref cColSep))
                {   //If any of the Columnseperators occur in the Rowseperators, empty the columnseperators
                    cColSep = new char[0];  //This results in having only one column and using only the rowseperator
                }

                
                //sData = sData.Replace ("\\" + cRowSep[0], "<tilda>");  
                //sRows = sData.Split(cRowSep);           //array holding all rows
                //sRows[0] = sRows[0].Replace("<tilda>", "" + cRowSep[0]);
                sRows = Utils.MySplit(sData, cRowSep[0]); 
                //sRows[0] = sRows[0].Replace("\\" + cColSep[0], "<tilda>");
                //sColNames = sRows[0].Split(cColSep);   //Array holding column names
                sColNames = Utils.MySplit(sRows[0], cColSep[0]); 
                iCols = sColNames.GetLength(0);
                iRows = sRows.GetLength(0) - 1;   //-1 since first element are the rownames


                dtRet.Columns.Add(new DataColumn("ID", typeof(string)));    //Add columns ID and ID0
                dtRet.Columns.Add(new DataColumn("ID0", typeof(string)));

                for (i = 0; i < iCols; i++)     //Loop throug sColNames to add all columns to datatable
                {
                    //sColNames[i] = sColNames[i].Replace("<tilda>", "" + cColSep[0]);
                    strColName = sColNames[i].Trim();
                    if (string.IsNullOrEmpty(strColName) || dtRet.Columns.Contains(strColName))  //empty or existing column name are not allowed
                    {   //The columnname might exist, if the user does something like this SplitXX("Column2,,Column3|a,b,c","|",",")
                        strColName = "Column" + (i + 1).ToString(); //Results in "Column1" etcetera.
                        k = 2;
                        while (dtRet.Columns.Contains(strColName))     //Do this until we find a columnname that does not exist yet. 
                        {   //We always get a unique, non-empty Columnname
                            strColName = "Column" + (i + k).ToString();   //Increase name by one, until we reach a non-existing name
                            k += 1;
                        }
                    }
                    dtRet.Columns.Add(new DataColumn(strColName, typeof(string)));
                }

                for (i = 1; i < iRows + 1; i++)     //Loop throug rows to add all rows to datatable
                {
                    sNewRow = new string[iCols + 2];    //An array of string of the row to be added to the datatable. Will be empty each time around.
                    sNewRow[0] = i.ToString();          //Add the ID and ID0 values
                    sNewRow[1] = (i - 1).ToString();    //Add the ID and ID0 values

                    //sRows[i] = sRows[i].Replace("<tilda>", "" + cRowSep[0]);
                    /*
                    sRows[i] = sRows[i].Replace("\\" + cColSep[0], "<tilda>");
                    sCols = sRows[i].Split(cColSep);
                    */
                    sCols = Utils.MySplit(sRows[i], cColSep[0]);
  
                    for (j = 0; (j < iCols) && (j < sCols .Length ); j++)
                    {
                        //sCols[j] = sCols[j].Replace("<tilda>", "" + cColSep[0]);
                        sNewRow[j + 2] = sCols[j];    //Add it to row cell by cell                            
                    }
                    dtRet.Rows.Add(sNewRow);                                //Add row to datatable
                }
            }
            catch (Exception ex)
            {
                dtRet = new DataTable();    //Create new datatable, because we don't know at which point it crashed
                dtRet.Columns.Add(new DataColumn("ID", typeof(string)));
                dtRet.Columns.Add(new DataColumn("ID0", typeof(string)));
                dtRet.Columns.Add(new DataColumn("Column1", typeof(string)));
                string[] sErrorRow = { "1", "0", "Error in SplitXX function!" + ex.Message };
                dtRet.Rows.Add(sErrorRow);
            }

            return dtRet;
        }

        private static bool CheckDoubleSeperators(ref char[] cColSep, ref char[] cRowSep)  //Check if the rowseperators are not overlapping the columnseperators
        {
            bool retBln = true;

            foreach (char cc in cColSep)
            {
                foreach (char cr in cRowSep)
                {
                    if (cr == cc)
                    {
                        retBln = false; //One of the RowSeperators is equal to one of the Columnseperators
                        break;
                    }
                }
                if (!retBln)
                {
                    break;
                }
            }

            return retBln;
        }


        protected bool GetQueryResult( out DataTable dtQueryResult, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            Error  = "";
            ErrorVerbose = "";
            dtQueryResult = null;

            string strSQL = "";
            string strConn = "";
            XProviderData providerData = null;

            try
            {
                if (m_sourceName.ToUpper() == "ERR")
                {
                    string sErr = "Message|";
                    string sData = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
                    if (sData == "invalid") sData = "Fout! Ongeldige parameter: ";
                    else if (sData == "missing") sData = "Fout! Ontbrekende parameter: ";
                    sErr += sData + " ";
                    sData = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "");
                    sErr += sData + ".";
                    sData = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
                    sErr += sData + ".";
                    sData = (m_parameters.GetLength(0) >= 4 ? m_parameters.GetValue(3).ToString() : "");
                    sErr += sData + ".";
                    sData = (m_parameters.GetLength(0) >= 5 ? m_parameters.GetValue(4).ToString() : "");
                    sErr += sData + " ";
                    sData = (m_parameters.GetLength(0) >= 6 ? m_parameters.GetValue(5).ToString() : "");
                    sErr += sData + " ";
                    dtQueryResult = SplitXX_GetDataTable(sErr, "|", "|");
                }
                else if (m_sourceName.ToUpper() == "SPLITXX")
                {
                    string sData = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
                    string sSep1 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : ",");
                    string sSep2 = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
                    string sHeader = (m_parameters.GetLength(0) >= 4 ? m_parameters.GetValue(3).ToString() : "");
                    if (sHeader != "") sData = sHeader + sSep1 + sData;
                    if (sSep1 == "") sSep1 = "|";
                    while (sData.EndsWith(sSep1))
                        sData = sData.Substring(0, sData.Length - sSep1.Length);
                    dtQueryResult = SplitXX_GetDataTable(sData, sSep1, sSep2);
                }
                else if (m_sourceName.ToUpper() == "SPLITXX2")
                {
                    string sData = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
                    string sSep1 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : ",");
                    string sSep2 = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
                    string sHeader = (m_parameters.GetLength(0) >= 4 ? m_parameters.GetValue(3).ToString() : "");
                    if (sHeader != "") sData = sHeader + sSep1 + sData;
                    if (sSep1 == "") sSep1 = "||";
                    while (sData.EndsWith(sSep1))
                        sData = sData.Substring(0, sData.Length - sSep1.Length);
                    dtQueryResult = SplitXX_GetDataTable(sData, sSep1, sSep2);
                }
                else if (m_sourceName.ToUpper() == "SPLITXX3")
                {
                    string sData = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
                    string sSep1 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : ",");
                    string sSep2 = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
                    string sHeader = (m_parameters.GetLength(0) >= 4 ? m_parameters.GetValue(3).ToString() : "");
                    if (sHeader != "") sData = sHeader + sSep1 + sData;
                    if (sSep1 == "") sSep1 = "||";
                    while (sData.EndsWith(sSep1))
                        sData = sData.Substring(0, sData.Length - sSep1.Length);
                    dtQueryResult = SplitXX_GetDataTable(sData, sSep1, sSep2);
                }
                else if (m_sourceName.ToUpper() == "FOR")
                {
                    string s1 = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "1");
                    string s2 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "1");
                    string sFor = "";
                    int iRows = 0;
                    int iCols = 0;
                    int.TryParse(s1, out iRows);
                    int.TryParse(s2, out iCols);
                    sFor += "R,R0,C,C0";
                    for (int iR = 0; iR < iRows; iR++)
                        for (int iC = 0; iC < iCols; iC++)
                            sFor += "|" + (iR + 1).ToString() + "," + iR.ToString() + "," + (iC + 1).ToString() + "," + iC.ToString();
                    dtQueryResult = SplitXX_GetDataTable(sFor, "|", ",");
                }
                else if (m_sourceName.ToUpper() == "FOR2")
                {
                    string s1 = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "1");
                    string s2 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "1");
                    string sFor = "";
                    int iRows = 0;
                    int iCols = 0;
                    int.TryParse(s1, out iRows);
                    int.TryParse(s2, out iCols);
                    sFor += "R,R0,C,C0";
                    for (int iR = 0; iR < iRows; iR++)
                        for (int iC = 0; iC < iCols; iC++)
                            sFor += "|" + (iR + 1).ToString() + "," + iR.ToString() + "," + (iC + 1).ToString() + "," + iC.ToString();
                    dtQueryResult = SplitXX_GetDataTable(sFor, "|", ",");
                }
                else if (m_sourceName.ToUpper() == "FOR3")
                {
                    string s1 = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "1");
                    string s2 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "1");
                    string sFor = "";
                    int iRows = 0;
                    int iCols = 0;
                    int.TryParse(s1, out iRows);
                    int.TryParse(s2, out iCols);
                    sFor += "R,R0,C,C0";
                    for (int iR = 0; iR < iRows; iR++)
                        for (int iC = 0; iC < iCols; iC++)
                            sFor += "|" + (iR + 1).ToString() + "," + iR.ToString() + "," + (iC + 1).ToString() + "," + iC.ToString();
                    dtQueryResult = SplitXX_GetDataTable(sFor, "|", ",");
                }

                else if ((m_parameters.GetLength(0) >= 4) && (m_parameters.GetValue(3)!=null) && (m_parameters.GetValue(3).ToString() == "Provider=XPATH;"))
                {
                    string sXml = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
                    string sXPath = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "");
                    string sQRY = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
                    string sRootPar = (m_parameters.GetLength(0) >= 5 ? m_parameters.GetValue(4).ToString() : "");
                    string sRoot = "";
                    if (sRootPar.StartsWith("Root=")) sRoot = sRootPar.Substring(5);
                    sRootPar = (m_parameters.GetLength(0) >= 6 ? m_parameters.GetValue(5).ToString() : "");
                    if (sRootPar.StartsWith("Root=")) sRoot = sRootPar.Substring(5);
                    if (sRoot != "")
                    {
                        sXml = HttpUtility.HtmlDecode(sXml);
                        int i1 = sXml.IndexOf("<" + sRoot + ">");
                        int i2 = sXml.IndexOf("</" + sRoot + ">", i1 + 1);
                        if ((i1 > -1) && (i2 > -1)) sXml = sXml.Substring(i1, i2 - i1 + 2 + sRoot.Length + 1);
                    }
                    dtQueryResult = XPath_GetDataTable(sXml, sXPath, sQRY, ref m_Response, ref m_OuterXml, ref m_InnerXml);
                }
                else if ((m_parameters.GetLength(0) >= 4) && (m_parameters.GetValue(3) != null) && (m_parameters.GetValue(3).ToString() == "Provider=JPATH;"))
                {
                    string sJSON = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
                    string sJPath = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "");
                    string sQRY = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
                    string sFieldsPar = (m_parameters.GetLength(0) >= 5 ? m_parameters.GetValue(4).ToString() : "");
                    string sFields = "";
                    if (sFieldsPar.StartsWith("Fields=")) sFields = sFieldsPar.Substring(7);
                    sFieldsPar = (m_parameters.GetLength(0) >= 6 ? m_parameters.GetValue(5).ToString() : "");
                    if (sFieldsPar.StartsWith("Fields=")) sFields = sFieldsPar.Substring(7);

                    dtQueryResult = JPath_GetDataTable(sJSON, sJPath, sQRY, sFields , ref m_Response, ref m_OuterXml, ref m_InnerXml);
                }
                else if (bDefQryParameter)
                {
                    XProviderData connAdm = m_dataProvider;
                    strSQL = "#Query#";
                    strConn = "";
                    string sConnIDName = ParseQueryStatement(ref strSQL);
                    /* todo md: cache connections by name */
                    if (sConnIDName == "0") sConnIDName = "";
                    if (sConnIDName == "-1") sConnIDName = "";


                    if (sConnIDName != "")
                    {
                        string sQueryStatement = SQL.SQLConnectionString;
                        sQueryStatement = sQueryStatement.Replace("#P1#", sConnIDName);
                        DataTable dtQueryc = connAdm.GetDataTable(sQueryStatement);
                        if (dtQueryc == null || dtQueryc.Rows.Count == 0)
                        {
                            Error = "Cannot find requested connection (" + sConnIDName + ") for template id = " + Convert.ToString(m_templateID);
                            ErrorVerbose = Error;
                            return false;
                        }
                        strConn = Connectors.GetStringResult(dtQueryc.Rows[0][S_CONNECTIONS.CONNSTRING]);
                    }

                    if ((strConn == "") || (strConn == ConfigurationManager.AppSettings["XDocConnectionString"]))
                        providerData = connAdm;
                    else
                        //providerData = new Connectors().GetConnector(strConn);
                        providerData = m_Connectors.GetConnector(strConn);

                    dtQueryResult = providerData.GetDataTable(strSQL, ref m_Response, ref m_OuterXml, ref m_InnerXml);
                    

                }
                else
                {
                    XProviderData connAdm = m_dataProvider;

                    IDataParameter[] pars = new IDataParameter[2];
                    pars[0] = new SqlParameter("@" + S_QUERIES.QUERYNAME, m_sourceName);
                    pars[1] = new SqlParameter("@" + S_QUERIES.TEMPLATEID, m_templateID);

                    //string sQueryStatement = SQL.SQLGetQueryStatement;
                    //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYNAME, "'" + m_sourceName + "'");
                    //sQueryStatement = sQueryStatement.Replace ("@" + S_QUERIES.TEMPLATEID, m_templateID.ToString ());

                    DataTable dtQuery = connAdm.GetDataTable(SQL.SQLGetQueryStatement, pars);
                    //DataTable dtQuery = connAdm.GetDataTable(sQueryStatement);
                    if (dtQuery == null || dtQuery.Rows.Count == 0)
                    {
                        Error = "Cannot find requested query (" + m_sourceName + ") for template id = " + Convert.ToString(m_templateID);
                        ErrorVerbose = Error;
                        return false;
                    }

                    strSQL = Connectors.GetStringResult(dtQuery.Rows[0][S_QUERIES.QUERYTEXT]);
                    strConn = Connectors.GetStringResult(dtQuery.Rows[0][S_CONNECTIONS.CONNSTRING]);

                    string sConnIDName = ParseQueryStatement(ref strSQL);
                    if (sConnIDName == "0") sConnIDName = "";
                    if (sConnIDName == "-1") sConnIDName = "";

                    if (sConnIDName != "")
                    {
                        string sQueryStatement = SQL.SQLConnectionString;
                        sQueryStatement = sQueryStatement.Replace("#P1#", sConnIDName);
                        DataTable dtQueryc = connAdm.GetDataTable(sQueryStatement);
                        if (dtQueryc == null || dtQueryc.Rows.Count == 0)
                        {
                            Error = "Cannot find requested connection (" + sConnIDName + ") for template id = " + Convert.ToString(m_templateID);
                            ErrorVerbose = Error;
                            return false;
                        }
                        strConn = Connectors.GetStringResult(dtQueryc.Rows[0][S_CONNECTIONS.CONNSTRING]);
                    }

                    if ((strConn == "") || (strConn == ConfigurationManager.AppSettings["XDocConnectionString"]))
                        providerData = connAdm;
                    else
                        //providerData = new Connectors().GetConnector(strConn);
                        providerData = m_Connectors.GetConnector(strConn);

                    dtQueryResult = providerData.GetDataTable(strSQL, ref m_Response, ref m_OuterXml, ref m_InnerXml);
                }
                //here md regex
                //Regex re = new Regex(@"<error\b[^>]*>(<!\[CDATA\[.*?]]>|.*?)</error>", RegexOptions.IgnoreCase);
                //mc = re.Matches(sourcestring1);
                //for (matchIndex = 0; matchIndex < mc.Count; matchIndex++)
                //{
                //    m = mc[matchIndex];
                //    Console.WriteLine("\t\t{0} ", m.Groups[1].Value);
                //}
                if (m_Response != null)
                {
                    if (sErrorRegexParameter == "") sErrorRegexParameter = @"<message>(Method.*?)</message>|<error\b[^>]*></error>|<error\b[^>]*>(<!\[CDATA\[.+?]]>|.+?)</error>";
                    Regex re = new Regex(sErrorRegexParameter, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    MatchCollection mc = re.Matches(m_Response);
                    Match m;
                    if (mc.Count > 0)
                    {
                        Error = "";
                        for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
                        {
                            m = mc[matchIndex];
                            string sErr = m.Groups[1].Value + m.Groups[2].Value + m.Groups[3].Value + m.Groups[4].Value + m.Groups[5].Value + m.Groups[6].Value + m.Groups[7].Value + m.Groups[8].Value + m.Groups[9].Value + m.Groups[10].Value;
                            if(sErr != "") Error += sErr + Environment.NewLine;
                        }
                        if (Error != "")
                        {
                            ErrorVerbose = "Could not execute query " + m_sourceName + m_sParameters + "; " + Environment.NewLine + Error;
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                ErrorVerbose = "Could not execute query " + m_sourceName + m_sParameters + "; " + Environment.NewLine  + Error;
                return false;
            }
            finally
            {
                if (providerData != null)
                    providerData.Dispose();
            }

        }

        //protected bool GetQueryResult(out string error, out DataTable dtQueryResult, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        //{
        //    error = "";
        //    dtQueryResult = null;

        //    string strSQL = "";
        //    string strConn = "";
        //    XProviderData providerData = null;

        //    try
        //    {
        //        if (m_sourceName.ToUpper () == "ERR")
        //        {
        //            string sErr = "Message|";
        //            string sData = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
        //            if (sData == "invalid") sData = "Fout! Ongeldige parameter: ";
        //            else if (sData == "missing") sData = "Fout! Ontbrekende parameter: ";
        //            sErr += sData + " ";
        //            sData = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "");
        //            sErr += sData + ".";
        //            sData = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
        //            sErr += sData + ".";
        //            sData = (m_parameters.GetLength(0) >= 4 ? m_parameters.GetValue(3).ToString() : "");
        //            sErr += sData + ".";
        //            sData = (m_parameters.GetLength(0) >= 5 ? m_parameters.GetValue(4).ToString() : "");
        //            sErr += sData + " ";
        //            sData = (m_parameters.GetLength(0) >= 6 ? m_parameters.GetValue(5).ToString() : "");
        //            sErr += sData + " ";
        //            dtQueryResult = SplitXX_GetDataTable(sErr, "|", "|");
        //        }
        //        else if (m_sourceName.ToUpper() == "SPLITXX")
        //        {
        //            string sData = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
        //            string sSep1 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : ",");
        //            string sSep2 = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
        //            string sHeader = (m_parameters.GetLength(0) >= 4 ? m_parameters.GetValue(3).ToString() : "");
        //            if (sHeader != "") sData = sHeader + sSep1 + sData;
        //            if (sSep1 == "") sSep1 = "|";
        //            while (sData.EndsWith(sSep1))
        //                sData = sData.Substring(0, sData.Length - sSep1.Length);
        //            dtQueryResult = SplitXX_GetDataTable(sData, sSep1, sSep2);
        //        }
        //        else if (m_sourceName.ToUpper() == "SPLITXX2")
        //        {
        //            string sData = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
        //            string sSep1 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : ",");
        //            string sSep2 = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
        //            string sHeader = (m_parameters.GetLength(0) >= 4 ? m_parameters.GetValue(3).ToString() : "");
        //            if (sHeader != "") sData = sHeader + sSep1 + sData;
        //            if (sSep1 == "") sSep1 = "||";
        //            while (sData.EndsWith(sSep1))
        //                sData = sData.Substring(0, sData.Length - sSep1.Length);
        //            dtQueryResult = SplitXX_GetDataTable(sData, sSep1, sSep2);
        //        }
        //        else if (m_sourceName.ToUpper() == "SPLITXX3")
        //        {
        //            string sData = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
        //            string sSep1 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : ",");
        //            string sSep2 = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
        //            string sHeader = (m_parameters.GetLength(0) >= 4 ? m_parameters.GetValue(3).ToString() : "");
        //            if (sHeader != "") sData = sHeader + sSep1 + sData;
        //            if (sSep1 == "") sSep1 = "||";
        //            while (sData.EndsWith(sSep1))
        //                sData = sData.Substring(0, sData.Length - sSep1.Length);
        //            dtQueryResult = SplitXX_GetDataTable(sData, sSep1, sSep2);
        //        }
        //        else if (m_sourceName.ToUpper() == "FOR")
        //        {
        //            string s1 = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "1");
        //            string s2 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "1");
        //            string sFor = "";
        //            int iRows = 0;
        //            int iCols = 0;
        //            int.TryParse(s1, out iRows);
        //            int.TryParse(s2, out iCols);
        //            sFor += "R,R0,C,C0";
        //            for (int iR = 0; iR < iRows; iR++)
        //                for (int iC = 0; iC < iCols; iC++)
        //                    sFor += "|" + (iR + 1).ToString() + "," + iR.ToString() + "," + (iC + 1).ToString() + "," + iC.ToString();
        //            dtQueryResult = SplitXX_GetDataTable(sFor, "|", ",");
        //        }
        //        else if (m_sourceName.ToUpper() == "FOR2")
        //        {
        //            string s1 = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "1");
        //            string s2 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "1");
        //            string sFor = "";
        //            int iRows = 0;
        //            int iCols = 0;
        //            int.TryParse(s1, out iRows);
        //            int.TryParse(s2, out iCols);
        //            sFor += "R,R0,C,C0";
        //            for (int iR = 0; iR < iRows; iR++)
        //                for (int iC = 0; iC < iCols; iC++)
        //                    sFor += "|" + (iR + 1).ToString() + "," + iR.ToString() + "," + (iC + 1).ToString() + "," + iC.ToString();
        //            dtQueryResult = SplitXX_GetDataTable(sFor, "|", ",");
        //        }
        //        else if (m_sourceName.ToUpper() == "FOR3")
        //        {
        //            string s1 = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "1");
        //            string s2 = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "1");
        //            string sFor = "";
        //            int iRows = 0;
        //            int iCols = 0;
        //            int.TryParse(s1, out iRows);
        //            int.TryParse(s2, out iCols);
        //            sFor += "R,R0,C,C0";
        //            for (int iR = 0; iR < iRows; iR++)
        //                for (int iC = 0; iC < iCols; iC++)
        //                    sFor += "|" + (iR + 1).ToString() + "," + iR.ToString() + "," + (iC + 1).ToString() + "," + iC.ToString();
        //            dtQueryResult = SplitXX_GetDataTable(sFor, "|", ",");
        //        }
                  
        //        else if ((m_parameters.GetLength(0) >= 4) && (m_parameters.GetValue(3).ToString() == "Provider=XPATH;"))
        //        {
        //            string sXml = (m_parameters.GetLength(0) >= 1 ? m_parameters.GetValue(0).ToString() : "");
        //            string sXPath = (m_parameters.GetLength(0) >= 2 ? m_parameters.GetValue(1).ToString() : "");
        //            string sQRY = (m_parameters.GetLength(0) >= 3 ? m_parameters.GetValue(2).ToString() : "");
        //            string sRootPar = (m_parameters.GetLength(0) >= 5 ? m_parameters.GetValue(4).ToString() : "");
        //            string sRoot = "";
        //            if (sRootPar.StartsWith("Root=")) sRoot = sRootPar.Substring(5); 
        //            sRootPar = (m_parameters.GetLength(0) >= 6 ? m_parameters.GetValue(5).ToString() : "");
        //            if (sRootPar.StartsWith("Root=")) sRoot = sRootPar.Substring(5);
        //            if (sRoot != "")
        //            {
        //                sXml = HttpUtility.HtmlDecode(sXml);
        //                int i1 = sXml.IndexOf("<" + sRoot + ">");
        //                int i2 = sXml.IndexOf("</" + sRoot + ">",i1+1);
        //                if ((i1 > -1) && (i2 > -1)) sXml = sXml.Substring(i1, i2 - i1 + 2 + sRoot.Length + 1);
        //            }
        //            dtQueryResult = XPath_GetDataTable(sXml, sXPath, sQRY, ref m_Response, ref m_OuterXml, ref m_InnerXml);
        //        }
        //        else
        //        {
        //            XProviderData connAdm = m_dataProvider;

        //            IDataParameter[] pars = new IDataParameter[2];
        //            pars[0] = new SqlParameter("@" + S_QUERIES.QUERYNAME, m_sourceName);
        //            pars[1] = new SqlParameter("@" + S_QUERIES.TEMPLATEID, m_templateID);

        //            //string sQueryStatement = SQL.SQLGetQueryStatement;
        //            //sQueryStatement = sQueryStatement.Replace("@" + S_QUERIES.QUERYNAME, "'" + m_sourceName + "'");
        //            //sQueryStatement = sQueryStatement.Replace ("@" + S_QUERIES.TEMPLATEID, m_templateID.ToString ());

        //            DataTable dtQuery = connAdm.GetDataTable(SQL.SQLGetQueryStatement, pars);
        //            //DataTable dtQuery = connAdm.GetDataTable(sQueryStatement);
        //            if (dtQuery == null || dtQuery.Rows.Count == 0)
        //            {
        //                error = "Cannot find requested query (" + m_sourceName + ") for template id = " + Convert.ToString(m_templateID);
        //                return false;
        //            }

        //            strSQL = Connectors.GetStringResult(dtQuery.Rows[0][S_QUERIES.QUERYTEXT]);
        //            strConn = Connectors.GetStringResult(dtQuery.Rows[0][S_CONNECTIONS.CONNSTRING]);

        //            string sConnIDName = ParseQueryStatement(ref strSQL);

        //            if (sConnIDName != "")
        //            {
        //                string sQueryStatement = SQL.SQLConnectionString;
        //                sQueryStatement = sQueryStatement.Replace("#P1#",  sConnIDName );
        //                DataTable dtQueryc = connAdm.GetDataTable( sQueryStatement);
        //                if (dtQueryc == null || dtQueryc.Rows.Count == 0)
        //                {
        //                    error = "Cannot find requested connection (" + sConnIDName + ") for template id = " + Convert.ToString(m_templateID);
        //                    return false;
        //                }
        //                strConn = Connectors.GetStringResult(dtQueryc.Rows[0][S_CONNECTIONS.CONNSTRING]);
        //            }

        //            if ((strConn == "") || (strConn == ConfigurationManager.AppSettings["XDocConnectionString"]))
        //                providerData = connAdm;
        //            else
        //                providerData = new Connectors().GetConnector(strConn);

        //            dtQueryResult = providerData.GetDataTable(strSQL, ref m_Response, ref m_OuterXml, ref m_InnerXml);
        //        }
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        string parameters = "";
        //        if (m_parameters != null && m_parameters.GetLength(0) != 0)
        //        {
        //            parameters = " with parameters (";
        //            for (int i = 0; i < m_parameters.Length; i++)
        //            {
        //                parameters += (string)m_parameters.GetValue(i);
        //                if (i != m_parameters.Length - 1)
        //                    parameters += ",";
        //            }
        //            parameters += ")";
        //        }

        //        error = "Could not execute query " + m_sourceName + parameters + "; " + Environment.NewLine + ex.Message;
        //        return false;
        //    }
        //    finally
        //    {
        //        if (providerData != null)
        //            providerData.Dispose();
        //    }

        //}

        //protected bool GetQueryResult(out string error, out DataTable dtQueryResult, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        //{
        //    error = "";
        //    dtQueryResult = null;

        //    string strSQL = "";
        //    string strConn = "";
        //    XProviderData providerData = null;

        //    try
        //    {
        //        XProviderData connAdm = m_dataProvider;
        //        string sQueryStatement = SQL.SQLGetQueryStatement;

        //        IDataParameter[] pars = new IDataParameter[2];
        //        pars[0] = new SqlParameter("@" + S_QUERIES.QUERYNAME, m_sourceName);
        //        pars[1] = new SqlParameter("@" + S_QUERIES.TEMPLATEID, m_templateID);

        //        DataTable dtQuery = connAdm.GetDataTable(SQL.SQLGetQueryStatement, pars);
        //        if (dtQuery == null || dtQuery.Rows.Count == 0)
        //        {
        //            error = "Cannot find requested query (" + m_sourceName + ") for template id = " + Convert.ToString(m_templateID);
        //            return false;
        //        }

        //        strSQL = Connectors.GetStringResult(dtQuery.Rows[0][S_QUERIES.QUERYTEXT]);
        //        strConn = Connectors.GetStringResult(dtQuery.Rows[0][S_CONNECTIONS.CONNSTRING]);
        //        if ((strConn == "") || (strConn == ConfigurationManager.AppSettings["XDocConnectionString"]))
        //            providerData =  connAdm;
        //        else
        //            providerData = new Connectors().GetConnector(strConn);

        //        ParseQueryStatement(ref strSQL);

        //        dtQueryResult = providerData.GetDataTable(strSQL, ref m_Response, ref m_OuterXml, ref m_InnerXml);

        //        return true;
        //    }
        //    catch(Exception ex)
        //    {
        //        string parameters = "";
        //        if (m_parameters != null && m_parameters.GetLength(0) != 0)
        //        {
        //            parameters = " with parameters (";
        //            for (int i = 0; i < m_parameters.Length; i++)
        //            {
        //                parameters += (string)m_parameters.GetValue(i);
        //                if (i != m_parameters.Length - 1)
        //                    parameters += ",";
        //            }
        //            parameters += ")";
        //        }
                
        //        error = "Could not execute query " + m_sourceName + parameters + "; " + Environment.NewLine + ex.Message;
        //        return false;
        //    }
        //    finally
        //    {
        //        if (providerData != null)
        //            providerData.Dispose();
        //    }

        //}

        private void GetSpecialParameters()
        {
            m_sParameters = "";
            if (m_parameters != null && m_parameters.GetLength(0) != 0)
            {
                m_sParameters = " (";
                for (int index = 0; index < m_parameters.GetLength(0); index++)
                {
                    if (m_parameters.GetValue(index) != null)
                    {
                        string sValue = m_parameters.GetValue(index).ToString();
                        m_sParameters += sValue;
                        if (index != m_parameters.Length - 1) m_sParameters += ",";
                        if (sValue.Contains("="))
                        {
                            string sParName = sValue.Substring(0, sValue.IndexOf("="));
                            string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
                            if (sParName.ToUpper() == "MEMFILTER") sFilter = sValue1;
                            if (sParName.ToUpper() == "MEMSORT") sSort = sValue1;
                            if (sParName.ToUpper() == "MEMDISTINCT") sDistinct = sValue1;
                            if (sParName.ToUpper() == "MEMCOLUMNS") sColumns = sValue1;
                            if (sParName.ToUpper() == "DEFQRY") if (sValue1 == "1") bDefQryParameter = true;
                            if (sParName.ToUpper() == "EXCEPTION") if (sValue1 == "1") bExceptionParameter = true;
                            if (sParName.ToUpper() == "ERRORREGEX") sErrorRegexParameter = sValue1;
                        }
                    }
                }
                m_sParameters += ")";
            }
        }

        //private bool GetDataViewParameters(out string sFilter, out string sSort, out string sDistinct, out string sColumns)
        //{
        //    bool bReturn = false;
        //    sFilter = "";
        //    sSort = "";
        //    sDistinct = "";
        //    sColumns = "";

        //    for (int index = 0; index < m_parameters.GetLength(0); index++)
        //    {
        //        if (m_parameters.GetValue(index) != null)
        //        {
        //            string sValue = m_parameters.GetValue(index).ToString();
        //            if (sValue.Contains("="))
        //            {
        //                string sParName = sValue.Substring(0, sValue.IndexOf("="));
        //                string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
        //                if (sParName.ToUpper() == "MEMFILTER") { sFilter = sValue1; bReturn = true; }
        //                if (sParName.ToUpper() == "MEMSORT") { sSort = sValue1; bReturn = true; }
        //                if (sParName.ToUpper() == "MEMDISTINCT") { sDistinct = sValue1; bReturn = true; }
        //                if (sParName.ToUpper() == "MEMCOLUMNS") { sColumns = sValue1; bReturn = true; }
        //            }
        //        }
        //    }
        //    return bReturn;
        //}
        //private bool GetDefQryParameter()
        //{
        //    bool bDefQry = false;

        //    for (int index = 0; index < m_parameters.GetLength(0); index++)
        //    {
        //        if (m_parameters.GetValue(index) != null)
        //        {
        //            string sValue = m_parameters.GetValue(index).ToString();
        //            if (sValue.Contains("="))
        //            {
        //                string sParName = sValue.Substring(0, sValue.IndexOf("="));
        //                string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
        //                if (sParName.ToUpper() == "DEFQRY") if (sValue1 == "1") bDefQry = true;
        //            }
        //        }
        //    }
        //    return bDefQry;
        //}
        //private string GetExceptionParameter()
        //{
        //    string sError = "";

        //    for (int index = 0; index < m_parameters.GetLength(0); index++)
        //    {
        //        if (m_parameters.GetValue(index) != null)
        //        {
        //            string sValue = m_parameters.GetValue(index).ToString();
        //            if (sValue.Contains("="))
        //            {
        //                string sParName = sValue.Substring(0, sValue.IndexOf("="));
        //                string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
        //                if (sParName.ToUpper() == "EXCEPTION") sError = sValue1; 
        //            }
        //        }
        //    }
        //    return sError;
        //}

		protected bool ExecuteAndStoreQuery()
		{
            //////DataTable dtQueryResult = null;

            if (!GetQueryResult(out dtQueryResult, ref m_Response, ref m_OuterXml, ref m_InnerXml))
            {
                return false;
            }
            //dtQueryResult.Columns[2].DataType  
            if (dtQueryResult.Columns.Count > 0)
            {
                //if (GetDataViewParameters(out  sFilter, out  sSort, out  sDistinct, out  sColumns))
                //{
                if (sSort != "")
                {
                    Regex rexSortField = new Regex(@"(datetime~\w+)|(datetimeNL~\w+)|(decimal~\w+)|(decimalNL~\w+)");
                    MatchCollection mc = rexSortField.Matches(sSort);
                    for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
                    {
                        string sField = mc[matchIndex].Value;
                        if (sField.StartsWith("decimal~"))
                        {
                            string sColName = sField.Substring("decimal~".Length);
                            try { dtQueryResult.Columns.Add(new DataColumn("decimal_" + sColName, typeof(decimal), sColName)); }
                            catch { };
                            sSort = sSort.Replace("decimal~" + sColName, "decimal_" + sColName);
                        }
                        if (sField.StartsWith("decimalNL~"))
                        {
                            string sColName = sField.Substring("decimalNL~".Length);
                            try { dtQueryResult.Columns.Add(new DataColumn("decimalNL_" + sColName, typeof(decimal), sColName)); }
                            catch { };
                            sSort = sSort.Replace("decimalNL~" + sColName, "decimalNL_" + sColName);
                        }
                        if (sField.StartsWith("datetime~"))
                        {
                            string sColName = sField.Substring("datetime~".Length);
                            try { dtQueryResult.Columns.Add(new DataColumn("datetime_" + sColName, typeof(DateTime), sColName)); }
                            catch { };
                            sSort = sSort.Replace("datetime~" + sColName, "datetime_" + sColName);
                        }
                        if (sField.StartsWith("datetimeNL~"))
                        {
                            string sColName = sField.Substring("datetimeNL~".Length);
                            try { dtQueryResult.Columns.Add("datetimeNL_" + sColName, typeof(DateTime), "substring(" + sColName + ",4,2)+substring(" + sColName + ",3,1)+substring(" + sColName + ",1,2)+substring(" + sColName + ",6,len(" + sColName + ")-5)"); }
                            catch (Exception ex1)
                            {
                                //                                    throw (ex1);
                            }
                            //try { dtQueryResult.Columns.Add(new DataColumn("datetimeNL_" + sColName, typeof(DateTime), "convert(datetime," + sColName + ",105)")); }
                            //catch { };
                            sSort = sSort.Replace("datetimeNL~" + sColName, "datetimeNL_" + sColName);
                        }
                    }

                    dtQueryResult.DefaultView.Sort = sSort;
                }
                if (sFilter != "")
                    dtQueryResult.DefaultView.RowFilter = sFilter;
                if (sColumns != "")
                {
//                    string[] aColumns = sColumns.Split('|');
                    string[] aColumns = Utils .MySplit ( sColumns,'|');
                    dtQueryResult = dtQueryResult.DefaultView.ToTable(sDistinct == "1", aColumns);
                }
                else
                    dtQueryResult = dtQueryResult.DefaultView.ToTable();
                //}

            }
			m_size = dtQueryResult.Rows.Count;
            m_FieldsIndex = new string[dtQueryResult.Columns.Count];
            m_FieldsIndexOffset = 0;
            int i=0;
			foreach(DataColumn col in dtQueryResult.Columns)
			{
				string fieldName = col.ColumnName.TrimEnd().ToUpper();
				FieldValues fieldValues = new FieldValues();
				fieldValues.Initialize(fieldName, m_size);
				m_FieldsResults.Add(fieldName, fieldValues);
                m_FieldsIndex[i] = fieldName;
                if (fieldName.EndsWith("[MEMBER_CAPTION]")) m_FieldsIndexOffset++;
                i++;
			}
			
			for(int index = 0; index < dtQueryResult.Rows.Count; index++ )
			{
                DataRow rowResult = dtQueryResult.Rows[index];
                foreach (DataColumn col in dtQueryResult.Columns)
				{						
					string fieldName = col.ColumnName.TrimEnd().ToUpper();
					object Value = rowResult[col];
					if(!AddValue(fieldName, index, Value))
						return false;
				}
			}


			return true;
		}
		#endregion

		#region Public Methods 
        public string GetResponse(XProviderData dataProvider, string strSQL, string connName)
        {
            XProviderData connAdm = dataProvider;
            XProviderData providerData = null;
            string strConn = "";
            string sConnIDName = connName;
            /* todo md: cache connections by name */
            if (sConnIDName == "0") sConnIDName = "";
            if (sConnIDName == "-1") sConnIDName = "";


            if (sConnIDName != "")
            {
                string sQueryStatement = SQL.SQLConnectionString;
                sQueryStatement = sQueryStatement.Replace("#P1#", sConnIDName);
                DataTable dtQueryc = connAdm.GetDataTable(sQueryStatement);
                if (dtQueryc == null || dtQueryc.Rows.Count == 0)
                {
                    Error = "Cannot find requested connection (" + sConnIDName + ") for template id = " + Convert.ToString(m_templateID);
                    ErrorVerbose = Error;
                    return "";
                }
                strConn = Connectors.GetStringResult(dtQueryc.Rows[0][S_CONNECTIONS.CONNSTRING]);
            }

            if ((strConn == "") || (strConn == ConfigurationManager.AppSettings["XDocConnectionString"]))
                providerData = connAdm;
            else
                //providerData = new Connectors().GetConnector(strConn);
                providerData = m_Connectors.GetConnector(strConn);

            string ret = providerData.GetResponse(strSQL);
            return ret;
        }

        public bool Initialize(int templateID, XProviderData dataProvider, string queryName, Array queryParams, out string sError, out string sErrorVerbose, out string sParameters)
		{
            bool bResult = true;
            Error  = "";
            ErrorVerbose = "";
            sError = "";
            sErrorVerbose = "";
            sParameters = "";
            try
            {
                m_templateID = templateID;
                m_FieldsResults = CollectionsUtil.CreateCaseInsensitiveHashtable();
                m_parameters = queryParams;
                m_sourceName = queryName;
                m_dataProvider = dataProvider;
                GetSpecialParameters();
                sParameters = m_sParameters;
                //sExceptionParameter = GetExceptionParameter();
                //m_DefQryParameter = GetDefQryParameter();

                m_keyName = SourceResult.GetSourceKey(queryName, queryParams);

                bResult = ExecuteAndStoreQuery();
                sError = Error;
                sErrorVerbose = ErrorVerbose;
                if (!bResult)
                    if (bExceptionParameter)
                        throw (new Exception(ErrorVerbose));
                return bResult;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                ErrorVerbose = Error;
                if (bExceptionParameter)
                    throw;
                else
                    return false;
                //throw(ex);
            }
		}

        public bool GetFieldValue(string fieldSource, int Index, out object Value)
        {
            if (fieldSource.ToUpper() == "DATATABLE")
            {
                Value = Error;
                //return true;
                return (Value != null);
            }
            if (fieldSource.ToUpper() == "EXCEPTION")
            {
                Value = Error;
                //return true;
                return (Value != null);
            }
            if (fieldSource.ToUpper() == "EXCEPTIONVERBOSE")
            {
                Value = ErrorVerbose;
                //return true;
                return (Value != null);
            }
            if (fieldSource.ToUpper() == "RESPONSE")
            {
                Value = m_Response;
                //return true;
                return (Value != null);
            }
            if (fieldSource.ToUpper() == "OUTERXML")
            {
                Value = m_OuterXml;
                //return true;
                return (Value != null);
            }
            if (fieldSource.ToUpper() == "INNERXML")
            {
                Value = m_InnerXml ;
                //Value = (m_InnerXml != null) ? m_InnerXml : "";
                //return true;
                return (Value != null);
            }
            if (fieldSource.ToUpper() == "FIRSTCOLUMN")
            {
                Value = -1 * m_FieldsIndexOffset;
                //return true;
                return (Value != null);
            }
            if (fieldSource.ToUpper() == "LASTCOLUMN")
            {
                Value = m_FieldsIndex.GetUpperBound(0) -  m_FieldsIndexOffset;
                //return true;
                return (Value != null);
            }
            try
            {
                //byid
                int ifieldSource;
                if(Int32.TryParse (fieldSource,out ifieldSource))
                //= Convert.ToInt32(fieldSource);
                    if ((ifieldSource + m_FieldsIndexOffset >= 0) && (ifieldSource + m_FieldsIndexOffset <= m_FieldsIndex.GetUpperBound(0)))
                        fieldSource = m_FieldsIndex[ifieldSource + m_FieldsIndexOffset].ToString();
                    else
                    {
                        Value = "error";
                        return true;
                    }
            }
            catch (Exception)
            {
            }

            Value = null;
            try
            {
                fieldSource = fieldSource.ToUpper();
                FieldValues fieldValues = (FieldValues)m_FieldsResults[fieldSource];
                if (fieldValues == null)
                    return false;

                return fieldValues.GetValue(Index, out Value);
            }
            catch (Exception ex)
            {
                //Trace.WriteLine(ex);
                throw;
            }
        }

		public string GetSourceKey()
		{
			return m_keyName;
		}

		public int GetCount()
		{
			return m_size;
		}
        private static string ComputeHash(byte[] objectAsBytes)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            try
            {
                byte[] result = md5.ComputeHash(objectAsBytes);

                // Build the final string by converting each byte
                // into hex and appending it to a StringBuilder
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < result.Length; i++)
                {
                    sb.Append(result[i].ToString("X2"));
                }

                // And return it
                return sb.ToString();
            }
            catch (ArgumentNullException ane)
            {
                //If something occurred during serialization, 
                //this method is called with a null argument. 
                Console.WriteLine("Hash has not been generated.");
                return null;
            }
        }
        private static readonly Object locker = new Object();
        private static byte[] ObjectToByteArray(object objectToSerialize)
        {
            using (MemoryStream fs = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                //Here's the core functionality! One Line!
                //To be thread-safe we lock the object
                lock (locker)
                {
                    formatter.Serialize(fs, objectToSerialize);
                }
                return fs.ToArray();
            }
        }

		public static string GetSourceKey(string QueryName, Array Params)
		{
            string sHash =  ComputeHash(ObjectToByteArray(Params));

			string key = QueryName;
            //for(int Index = 0; Index < Params.GetLength(0); Index++)
            //{
            //    if (Params.GetValue(Index)!= null) key += Params.GetValue(Index).ToString(); 
            //}
            key = key + sHash;
			return key;
		}


        public string GetJSON(int start, int end)
        {
            string ret="";
            if (end == 0)
            {
                if (dtQueryResult.Rows.Count > 0)
                {
                    DataTable dtn = dtQueryResult.Clone();
                    dtn.ImportRow(dtQueryResult.Rows[0]);
                    ret = JsonConvert.SerializeObject(dtn);
                    ret = ret.Substring(1, ret.Length - 2);
                }
                else
                    ret = "{}";
            }
            else
            {
                DataTable dtn = dtQueryResult.Clone();
                for (int index = start; index < end; index++)
                {
                    dtn.ImportRow(dtQueryResult.Rows[index]);
                    //                DataRow rowResult = dtQueryResult.Rows[index];
                }
                ret = JsonConvert.SerializeObject(dtn);
            }
            return ret;
        }
    //    fill dataset with datatables and relations....
    //    XmlDocument doc = new XmlDocument();
    //doc.LoadXml(resultSet.GetXml());
    //string jsonText = JsonConvert.SerializeXmlNode(doc).Replace("null", "\"\"").Replace("'", "\'");
    //return jsonText;


		#endregion

     }
}
