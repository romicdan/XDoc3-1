
#region using
using System;
using System.Data;
using System.Collections;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Web;
using System.Text;
using System.Configuration;
using KubionLogNamespace;
#endregion

namespace XDataSourceModule
{
	/// <summary>
	/// 
	/// </summary>
    [Serializable]
    public class XDocCache 
	{
		#region Protected Members
        // protected XProviderData m_dataProvider = null;
        protected Hashtable m_ConfigVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_ContextSessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_ContextSessionVariablesString = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected string sSessionID = "";
        //protected Hashtable m_TemplateTree = CollectionsUtil.CreateCaseInsensitiveHashtable();
        //protected Hashtable m_TemplateText = CollectionsUtil.CreateCaseInsensitiveHashtable();
        //protected Hashtable m_TemplateIDForInclude = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_TemplateOnce = CollectionsUtil.CreateCaseInsensitiveHashtable();
        private bool bReplaceEmptySV = false;
        #endregion

        public XDocCache()
        {
            bReplaceEmptySV = ((string)ConfigurationManager.AppSettings["ReplaceEmptySV"]=="1");
        }

        public string GetIncludeOnce()
        {
            string sVal = "";
            //Creates vector object
            SortedList vecKeys1 = new SortedList();

            foreach (string key in m_TemplateOnce.Keys)
            {
                vecKeys1.Add(key, key);
            }

            foreach (string key in vecKeys1.Values)
            {
                sVal += key + "|";
            }

            return sVal;
        }
        public bool RequestIncludeOnce(string sTemplateName)
        {
            //System.Diagnostics.Debug.WriteLine(sTemplateName);
            if (m_TemplateOnce.ContainsKey(sTemplateName)) return false;
            //System.Diagnostics.Debug.WriteLine(sTemplateName + " OK");
            m_TemplateOnce[sTemplateName] = 1;
            return true;
        }
        public void ClearIncludeOnce()
        {
            m_TemplateOnce.Clear();
        }
        public void FillCookies(HttpCookieCollection col)
        {
            DelSV("", "cookies_", false);
            foreach (string skey in col)
            {
                HttpCookie key = col[skey];
                SetSV(key.Name, "cookies_", key.Value);
            }
        }
        public void WriteCookies(HttpCookieCollection col)
        {
            string sContextName = "cookies_";
            Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];
            foreach (string key in m_SessionVariables.Keys)
            {
                if (key.EndsWith("__value"))
                {
                    try
                    {
                        string name = key.Substring(0, key.Length - "__value".Length);
                        HttpCookie cookie = new HttpCookie("");
                        cookie.Name = name;
                        cookie.Value = m_SessionVariables[key].ToString();
                        if (m_SessionVariables.ContainsKey(name + "__domain"))
                            cookie.Domain = m_SessionVariables[name + "__domain"].ToString();
                        if (m_SessionVariables.ContainsKey(name + "__path"))
                            cookie.Path = m_SessionVariables[name + "__path"].ToString();
                        if (m_SessionVariables.ContainsKey(name + "__expires"))
                            cookie.Expires = Convert.ToDateTime(m_SessionVariables[name + "__expires"].ToString());
                        if (m_SessionVariables.ContainsKey(name + "__httponly"))
                            cookie.HttpOnly = (m_SessionVariables[name + "__httponly"].ToString() == "1");
                        if (m_SessionVariables.ContainsKey(name + "__secure"))
                            cookie.Secure = (m_SessionVariables[name + "__secure"].ToString() == "1");
                        if (m_SessionVariables.ContainsKey(name + "__shareable"))
                            cookie.Shareable = (m_SessionVariables[name + "__shareable"].ToString() == "1");
                        col.Add(cookie);
                    }
                    catch (Exception e)
                    { }
                }
            }
        }

        static public string ReplaceString(string str, string oldValue, string newValue, StringComparison comparison)
        {
            StringBuilder sb = new StringBuilder();

            int previousIndex = 0;
            int index = str.IndexOf(oldValue, comparison);
            while (index != -1)
            {
                sb.Append(str.Substring(previousIndex, index - previousIndex));
                sb.Append(newValue);
                index += oldValue.Length;

                previousIndex = index;
                index = str.IndexOf(oldValue, index, comparison);
            }
            sb.Append(str.Substring(previousIndex));

            return sb.ToString();
        }

        #region IXDocCache methods

        public string SessionID
        {
            get { return sSessionID; }
            set { sSessionID = value; }
        }

        public void ClearCache()
        {
            //m_TemplateOnce.Clear();
            m_ConfigVariables.Clear();
            m_ContextSessionVariables.Clear();
            m_ContextSessionVariablesString.Clear();
        }

        //public XProviderData DataProvider
        //{
        //    get { return m_dataProvider; }
        //    set { m_dataProvider = value; }
        //}

        public string GetCfg(string sVarName, string sDefault, XProviderData m_dataProvider  )
        {
            string sVal, sVar;
            if (m_ConfigVariables.Count == 0)
            {
                string sSQL = "SELECT value,ConfigClass,ConfigName FROM [S_ConfigVariables] ";
                DataTable dt = m_dataProvider.GetDataTable(sSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        sVal = Connectors.GetStringResult(row[0], "");
                        sVar = Connectors.GetStringResult(row["ConfigClass"], "") + "__" + row["ConfigName"];
                        m_ConfigVariables[sVar] = sVal;
                    }
                }
            }

            sVal = sDefault;
            if (!sVarName.Contains("__")) sVarName = "__" + sVarName;
            if (m_ConfigVariables.ContainsKey(sVarName))
            {
                sVal = m_ConfigVariables[sVarName].ToString();
            }
            //sVal = ReplaceSettings(sVal, "_");
            return sVal;
        }

//        int iInReplaceSettings = 0;
//        private string ReplaceSettings(string sLine, string sContextName)
//        {
//            if (iInReplaceSettings > 10) return sLine;
//            iInReplaceSettings++;
//            char cSep = '@';
//            string sResponse = "";
//            string[] aLine = sLine.Split(cSep);
//            for (int i = 0; i < aLine.GetLength(0); i++)
//            {
//                sResponse += aLine[i];
//                i++;
//                if (i < aLine.GetLength(0))
//                {
//                    string sVar = aLine[i];
//                    bool toEncode = false;
//                    if (sVar.ToLower().EndsWith("[encode]"))
//                    {
//                        sVar = sVar.Substring(0, sVar.Length - "[encode]".Length);
//                        toEncode = true;
//                    }
//                    //////string sVal = ((IXDataSource)this).GetSV(sVar, sContextName, "missingvalue");
//                    //////if (sVal == "missingvalue") sVal = ((IXDataSource)this).GetCfg(sVar, "missingvalue");
//                    string sVal = GetSV(sVar, sContextName, "missingvalue");
//                    //if (sVal == "missingvalue") sVal = GetCfg(sVar, "missingvalue");
//                    if (sVal == "missingvalue") sVal = GetSV(sVar, "_", "missingvalue");
//                    if (sVal != "missingvalue")
//                    {
//                        if (toEncode) sVal = HttpUtility.HtmlEncode(HttpUtility.UrlEncode(sVal));
//                        sResponse += sVal;
//                    }
//                    else
//                    {
//                        sResponse += "@";
//                        i--;
//                    }
//                }
//            }
//            iInReplaceSettings--;
//            //if (aLine.GetLength(0) > 1) sResponse = ReplaceSettings(sResponse, sContextName);
////            return sResponse.Trim();
//            return sResponse;
//        }

        public string GetSV(string sVarName, string sContextName, string sDefault)
        {
            LoadSVContext(sContextName);
            Hashtable m_SessionVariables =(Hashtable ) m_ContextSessionVariables[sContextName];
            string sVal = sDefault;

            if (sVarName == "*")
            {
                sVal = "";

                //Creates vector object
                SortedList  vecKeys = new SortedList ();

                foreach (string key in m_SessionVariables.Keys)
                {
                    vecKeys.Add(key, key);
                }

                foreach (string key in  vecKeys.Values )
                {
                    String value = m_SessionVariables[key].ToString ();
                    sVal += key.Replace("=", "\\=").Replace(";", "\\;") + "=" + value.Replace("=", "\\=").Replace(";", "\\;") + ";";
                }

                //foreach (DictionaryEntry entry in m_SessionVariables)
                //{
                //    sVal += entry.Key.ToString().Replace("=", "\\=").Replace(";", "\\;") + "=" + entry.Value.ToString().Replace("=", "\\=").Replace(";", "\\;") + ";";
                //}
            }
            else if (sVarName == "INCLUDEONCE")
            {
                sVal = GetIncludeOnce();
            }
            else if (sVarName == "ALLCONTEXTS")
            {
                sVal = "";
                //Creates vector object
                SortedList vecKeys1 = new SortedList();

                foreach (string key in m_ContextSessionVariables.Keys)
                {
                    vecKeys1.Add(key, key);
                }

                foreach (string key in vecKeys1.Values)
                {
                    m_SessionVariables = (Hashtable)m_ContextSessionVariables[key];
                    if (m_SessionVariables.Count > 0)
                        sVal += key + "|";
                }


                //foreach (DictionaryEntry entry in m_ContextSessionVariables)
                //{
                //    m_SessionVariables = (Hashtable)m_ContextSessionVariables[entry.Key];
                //    if (m_SessionVariables.Count > 0)
                //        sVal += entry.Key + "|";
                //}

            }
            else
            {
                sVal = sDefault;
                if (m_SessionVariables.ContainsKey(sVarName))
                    sVal = m_SessionVariables[sVarName].ToString();
                else
                    if (!bReplaceEmptySV)
                        if (sVal == "") sVal = sDefault;
                //sVal = ReplaceSettings(sVal, sContextName);
                // do not replace setting here but in the CParser
            }
            return sVal;
        }
        public void SetSV(string sVarName, string sContextName, string sVal)
        {
            LoadSVContext(sContextName);
            Hashtable m_SessionVariables = (Hashtable) m_ContextSessionVariables[sContextName];
            string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
            if (sVarName == "*")
            {
                DelSV("", sContextName, true);
                //split sVal
                //sVal = sVal.Replace("\\;", "<tilda>");
                //string[] aRows = sVal.Split(';');
                string[] aRows =Utils.MySplit(  sVal,';');

                for (int i = 0; i < aRows.Length; i++)
                {
                    //aRows[i] = aRows[i].Replace("<tilda>", ";");
                    //aRows[i] = aRows[i].Replace("\\=", "<tilda>");
                    //aRows[i] = aRows[i] + "=";
                    //string[] aCels = aRows[i].Split('=');
                    //string sVar1 = aCels[0].Replace("<tilda>", "=");
                    //string sVal1 = aCels[1].Replace("<tilda>", "=");
                    aRows[i] = aRows[i] + "=";
                    string[] aCels = Utils.MySplit(aRows[i],'=');
                    if (aCels[0] != "")
                        SetSV(aCels[0], sContextName, aCels[1]);
                }
            }
            else if (sVal == "-delete")
            {
                DelSV(sVarName, sContextName, false);
            }

            else
            {

                if (m_SessionVariables.Contains(sVarName))
                {
                    s_SessionVariables = ReplaceString(s_SessionVariables, "|" + sVarName + "|", "|", StringComparison.CurrentCultureIgnoreCase);
                    //s_SessionVariables = s_SessionVariables.Replace("|" + sVarName + "|", "|");
                }
                m_SessionVariables[sVarName] = sVal;
                s_SessionVariables += sVarName + "|";
                m_ContextSessionVariables[sContextName] = m_SessionVariables;
                m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
            }
        }
        public void DelSV(string sVarName, string sContextName, bool delfrom)
        {
            if (sContextName == "*")
            {
                //m_ContextSessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
                //m_ContextSessionVariablesString = CollectionsUtil.CreateCaseInsensitiveHashtable();
                foreach (DictionaryEntry entry in m_ContextSessionVariables)
                {
                    //m_ContextSessionVariables[entry.Key] = CollectionsUtil.CreateCaseInsensitiveHashtable();
                    m_ContextSessionVariablesString[entry.Key] = "|";
                }
                foreach (DictionaryEntry entry in m_ContextSessionVariablesString)
                {
                    m_ContextSessionVariables[entry.Key] = CollectionsUtil.CreateCaseInsensitiveHashtable();
                    //m_ContextSessionVariablesString[entry.Key] = "|";
                }
            }
            else
            {
                LoadSVContext(sContextName);
                if (sVarName == "")
                {
                    m_ContextSessionVariables[sContextName] = CollectionsUtil.CreateCaseInsensitiveHashtable();
                    m_ContextSessionVariablesString[sContextName] = "|";
                }
                else
                {
                    Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];
                    string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
                    if (m_SessionVariables.Contains(sVarName))
                    {
                        if (delfrom)
                        {
                            string[] a_SessionVariables = s_SessionVariables.Substring(s_SessionVariables.IndexOf("|" + sVarName + "|",StringComparison.CurrentCultureIgnoreCase  )).Split('|');
                            foreach (string isVarName in a_SessionVariables)
                            {
                                if (isVarName != "")
                                {
                                    m_SessionVariables.Remove(isVarName);
                                    s_SessionVariables = ReplaceString(s_SessionVariables, "|" + isVarName + "|", "|", StringComparison.CurrentCultureIgnoreCase);
                                    //s_SessionVariables = s_SessionVariables.Replace("|" + isVarName + "|", "|");
                                }
                            }
                        }
                        else
                        {
                            m_SessionVariables.Remove(sVarName);
                            s_SessionVariables = ReplaceString(s_SessionVariables, "|" + sVarName + "|", "|", StringComparison.CurrentCultureIgnoreCase);
                            //s_SessionVariables = s_SessionVariables.Replace("|" + sVarName + "|", "|");
                        }
                        m_ContextSessionVariables[sContextName] = m_SessionVariables;
                        m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
                    }
                }
            }
        }

        public void CommitSV()
        {
            //string sSQL = "";
            //foreach (DictionaryEntry de in m_ContextSessionVariables)
            //    sSQL+=SaveSVContext(de.Key .ToString());
            //if (sSQL !="") m_dataProvider.ExecuteNonQuery(sSQL);
            //m_ContextSessionVariables.Clear ();
            

        }
        private void LoadSVContext(string sContextName)
        {
            if (!m_ContextSessionVariables.Contains(sContextName))
            {
                Hashtable m_SessionVariables = CollectionsUtil.CreateCaseInsensitiveHashtable();
                string s_SessionVariables = "|";
                //string sSQL = "SELECT parameter,value FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "' order by ord";
                //DataTable dt = m_dataProvider.GetDataTable(sSQL);
                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    foreach (DataRow row in dt.Rows)
                //    {
                //        string sVar = Connectors.GetStringResult(row[0], "");
                //        string sVal = Connectors.GetStringResult(row[1], "");
                //        m_SessionVariables.Add(sVar, sVal);
                //        s_SessionVariables += sVar + "|";
                //    }
                //}
                m_ContextSessionVariables[sContextName] = m_SessionVariables;
                m_ContextSessionVariablesString[sContextName] = s_SessionVariables;
            }
        }
        private string  SaveSVContext(string sContextName)
        {
            string sSQL ="";
            //sSQL = "DELETE FROM [S_SessionVariables] WHERE SessionID='" + sSessionID.Replace("'", "''") + "' and InstanceID='" + sContextName.Replace("'", "''") + "';";
            //Hashtable m_SessionVariables = (Hashtable)m_ContextSessionVariables[sContextName];
            //string s_SessionVariables = (string)m_ContextSessionVariablesString[sContextName];
            //string[] a_SessionVariables = s_SessionVariables.Split('|');
 
            //int i = 0;
            //foreach (string sVarName  in a_SessionVariables) 
            //{
            //    if (sVarName != "")
            //    {
            //        string sVarVal = m_SessionVariables[sVarName].ToString ().Replace("'", "''");
            //        if(sVarVal !="")
            //            sSQL += "INSERT INTO [S_SessionVariables] ([SessionID],[InstanceID],[Parameter],[Value],[ord]) VALUES ('" + sSessionID.Replace("'", "''") + "','" + sContextName.Replace("'", "''") + "','" + sVarName + "','" + sVarVal + "'," + i + ");";
            //        i++;
            //    }
            //}
            return sSQL;

        }

        //public bool RequestIncludeOnce(string sTemplateName)
        //{
        //    //System.Diagnostics.Debug.WriteLine(sTemplateName);
        //    if (m_TemplateOnce.ContainsKey(sTemplateName)) return false;
        //    //System.Diagnostics.Debug.WriteLine(sTemplateName + " OK");
        //    m_TemplateOnce[sTemplateName] = 1;
        //    return true;
        //}
        //public void ClearIncludeOnce()
        //{
        //    m_TemplateOnce.Clear();
        //}

        //public Object RequestTemplateTree(int templateID)
        //{
        //    Object templateRoot = null;
        //    if (m_TemplateTree.ContainsKey(templateID))
        //        templateRoot = (Object)m_TemplateTree[templateID];
        //    return templateRoot;
        //}
        //public void AddTemplateTree(int templateID, Object templateRoot)
        //{
        //    m_TemplateTree[templateID] = templateRoot;
        //}
        //public string RequestTemplateText(int templateID)
        //{
        //    string sTemplateText = "notloaded";
        //    if (m_TemplateText.ContainsKey (templateID ))
        //        sTemplateText = (string ) m_TemplateText[templateID ];
        //    return sTemplateText ;
        //}
        //public void AddTemplateText(int templateID, string sTemplateText)
        //{
        //    m_TemplateText[templateID] = sTemplateText;
        //}
        //public int RequestTemplateIDForInclude(string templateName)
        //{
        //    int iTemplateID = -1;
        //    if (m_TemplateIDForInclude.ContainsKey(templateName))
        //        iTemplateID = (int) m_TemplateIDForInclude[templateName];
        //    return iTemplateID;

        //}
        //public void AddTemplateIDForInclude(string templateName, int iTemplateID)
        //{
        //    m_TemplateIDForInclude[templateName] = iTemplateID;
        //}
        //public void ClearTemplatesCache()
        //{
        //    m_TemplateOnce.Clear();
        //    m_TemplateText.Clear();
        //    m_TemplateTree.Clear();
        //    m_TemplateIDForInclude.Clear();
        //}


		#endregion

	}
    [Serializable]
    public class XDocCacheTemplates
    {
        #region Protected Members
        protected Hashtable m_TemplateTree = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_TemplateText = CollectionsUtil.CreateCaseInsensitiveHashtable();
        protected Hashtable m_TemplateIDForInclude = CollectionsUtil.CreateCaseInsensitiveHashtable();
        //protected Hashtable m_TemplateOnce = CollectionsUtil.CreateCaseInsensitiveHashtable();
        #endregion

        public XDocCacheTemplates()
        {
        }


        static public string ReplaceString(string str, string oldValue, string newValue, StringComparison comparison)
        {
            StringBuilder sb = new StringBuilder();

            int previousIndex = 0;
            int index = str.IndexOf(oldValue, comparison);
            while (index != -1)
            {
                sb.Append(str.Substring(previousIndex, index - previousIndex));
                sb.Append(newValue);
                index += oldValue.Length;

                previousIndex = index;
                index = str.IndexOf(oldValue, index, comparison);
            }
            sb.Append(str.Substring(previousIndex));

            return sb.ToString();
        }

        #region XDocCacheTemplates methods


        //public string GetIncludeOnce()
        //{
        //    string sVal = "";
        //    //Creates vector object
        //    SortedList vecKeys1 = new SortedList();

        //    foreach (string key in m_TemplateOnce.Keys)
        //    {
        //        vecKeys1.Add(key, key);
        //    }

        //    foreach (string key in vecKeys1.Values)
        //    {
        //            sVal += key + "|";
        //    }

        //    return sVal;
        //}
        //public bool RequestIncludeOnce(string sTemplateName)
        //{
        //    //System.Diagnostics.Debug.WriteLine(sTemplateName);
        //    if (m_TemplateOnce.ContainsKey(sTemplateName)) return false;
        //    //System.Diagnostics.Debug.WriteLine(sTemplateName + " OK");
        //    m_TemplateOnce[sTemplateName] = 1;
        //    return true;
        //}
        //public void ClearIncludeOnce()
        //{
        //    m_TemplateOnce.Clear();
        //}


        public Object RequestTemplateTree(int templateID, ref string sTemplateText)
        {
            Object templateRoot = null;
            sTemplateText = "notloaded";
            if (m_TemplateTree.ContainsKey(templateID))
                templateRoot = (Object)m_TemplateTree[templateID];
            if (m_TemplateText.ContainsKey(templateID))
                sTemplateText = (string)m_TemplateText[templateID];
            return templateRoot;
        }
        public void AddTemplateTree(int templateID, Object templateRoot, string sTemplateText)
        {
            m_TemplateTree[templateID] = templateRoot;
            m_TemplateText[templateID] = sTemplateText;
        }
        public string RequestTemplateText(int templateID)
        {
            string sTemplateText = "notloaded";
            if (m_TemplateText.ContainsKey(templateID))
                sTemplateText = (string)m_TemplateText[templateID];
            return sTemplateText;
        }
        public void AddTemplateText(int templateID, string sTemplateText)
        {
            m_TemplateText[templateID] = sTemplateText;
        }
        public int RequestTemplateIDForInclude(string templateName)
        {
            int iTemplateID = -1;
            if (m_TemplateIDForInclude.ContainsKey(templateName))
                iTemplateID = (int)m_TemplateIDForInclude[templateName];
            return iTemplateID;

        }
        public void AddTemplateIDForInclude(string templateName, int iTemplateID)
        {
            m_TemplateIDForInclude[templateName] = iTemplateID;
        }
        public void ClearTemplatesCache()
        {
            //m_TemplateOnce.Clear();
            m_TemplateText.Clear();
            m_TemplateTree.Clear();
            m_TemplateIDForInclude.Clear();
        }

        #endregion

    }
}

