using System;
using System.Diagnostics;

namespace XDataSourceModule
{
	public class FieldValues
	{
		#region Protected Members
		protected Array		m_Values = null;
		protected string	m_FieldName = "";
		protected int		m_Size = -1;
		#endregion

		public FieldValues()
		{
		}

		#region Public Methods
		public bool Initialize(string FieldName, int Size)
		{
			try
			{
				m_FieldName = FieldName;
				m_Size = Size;
				m_Values = Array.CreateInstance(typeof(object), Size);
				return true;
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex);
				throw;
			}
		}

		public bool AddValue(int Index, object Value)
		{
			if(Index >= m_Size)
				return false;

			try
			{
				m_Values.SetValue(Value, Index);

				return true;
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex);
				throw;
			}
		}

		public bool GetValue(int Index, out object Value)
		{
			Value = null;
			if(Index >= m_Size)
				return false;

			try
			{
				Value = m_Values.GetValue(Index);
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex);
				throw;
			}

			return true;

		}
		#endregion
	}
}
