using System;
using System.Collections;
using System.Diagnostics;
using System.Data.SqlClient;

namespace XDataSourceModule
{
	/// <summary>
	/// Summary description for XDataUpdate.
	/// </summary>
	public class XDataUpdate //: IXDataUpdate
	{
		#region Protected Members
		protected UpdateSourceCollection m_Updates = null;
		protected Hashtable m_returnParameters = null;
		protected int m_templateID = -1;
		#endregion

		public XDataUpdate()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#region IXDataUpdate methods
		public void Initialize(int templateID, Hashtable returnParameters)
		{
			m_templateID = templateID;
			m_Updates = new UpdateSourceCollection();
            m_returnParameters = returnParameters;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateName">The name of the template this update belongs to. An empty string represents the current template</param>
        /// <param name="updateName">The name of the update</param>
        /// <param name="arrConditions">Update effective parameters (#Pn#)</param>
        /// <param name="arrUpdParams">Update return parameters</param>
        /// <param name="fieldName">The updated column</param>
        /// <param name="fieldValue">The value stored in the INPUT element</param>
        /// <returns></returns>
		public bool AddUpdateField(string templateName, string updateName, string[] arrConditions, string[] arrUpdParams, string fieldName, string fieldValue)
		{
            string key = SourceUpdate.GetUpdateKey(updateName, arrConditions, templateName);
			SourceUpdate upd = m_Updates.GetSourceUpdateByKey(key);
			if(upd == null)
			{
				upd = new SourceUpdate();
				upd.Initialize(m_templateID, templateName, updateName, arrConditions);
				m_Updates.Add(upd);
			}
			return upd.AddFieldValue(fieldName, fieldValue, arrUpdParams);
		}

		public bool Execute(XProviderData dataProvider, out string error)
		{
            error = "";
			try
			{
                bool inTransaction = dataProvider.InTransaction;
                if (!inTransaction)
                    dataProvider.BeginTransaction(); 
				for(int index = 0; index < m_Updates.Count; index++)
				{
					SourceUpdate upd = m_Updates[index];
					if(upd == null)
						continue;

                    IDictionaryEnumerator enRet = m_returnParameters.GetEnumerator() ;
                    while (enRet.MoveNext())
                    {
                        upd.AddFieldValue(enRet.Key.ToString(), enRet.Value.ToString(), null);
                    }



                    if (upd.Execute(dataProvider, out error))
                    {
                        upd.GetNewParameters(ref m_returnParameters);
                    }
                    else
                    {
                        if (!inTransaction)
                            dataProvider.Rollback();
                        return false;
                    }
                }

                if (!inTransaction)
                    dataProvider.Commit();
                return true;
			}
			catch(Exception ex)
			{
                dataProvider.Rollback();
                //Trace.WriteLine(ex);
				throw;
			}
						
		}

		public bool GetNewParameters(out Hashtable newParamsValues)
		{
            newParamsValues = m_returnParameters;
            return true;

            /*newParamsValues = CollectionsUtil.CreateCaseInsensitiveHashtable();


            try
            {
                for(int index = 0; index < m_Updates.Count; index++)
                {
                    SourceUpdate upd = m_Updates[index];
                    if(!upd.GetNewParameters(ref newParamsValues))
                        return false;
                }

                return true;
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex);
                throw(ex);
            }*/

		}


		#endregion

	}
}
