using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Ajax;
using System.Collections;

namespace XDocUtilNamespace
{
    public class XDocUtil
    {
        #region IXDocUtil Members

        [Ajax.AjaxMethod(HttpSessionStateRequirement.ReadWrite)]
        public object InvokeMethod(string methodName, string  parameters)
        {
            object retVal = null;

            object[] invokeParams = new object[] { };
            if (parameters != null)
            {
                string [] vp = parameters.Split('|');
                invokeParams = new object[vp.Length];
                vp.CopyTo(invokeParams, 0);
            }

            try
            {
                MethodInfo mi = this.GetType().GetMethod(methodName);
                if (mi != null)
                {
                    retVal = mi.Invoke(this, invokeParams);
                }
            }
            catch { }

            return retVal;
        }

        #endregion

        public string WriteMessage(string message)
        {
            //to access session variables
            //HttpContext.Current.Session["EventManager"];
            string ret = "failure";
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter("c:\\temp\\test.txt", true);
                sw.WriteLine(message);
                ret = "OK";
            }
            catch
            {
            }
            finally
            {
                if (sw != null)
                    sw.Close();

            }

            return ret;
            
        }

        public string WriteMessage2(string message)
        {
            //to access session variables
            //HttpContext.Current.Session["EventManager"];
            string ret = "failure";
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter("c:\\temp\\test3.txt", true);
                sw.WriteLine(message);
                ret = "OK";
            }
            catch
            {
            }
            finally
            {
                if (sw != null)
                    sw.Close();

            }

            return ret;

        }

        public XDocUtil()
        {
        }
    }
}
